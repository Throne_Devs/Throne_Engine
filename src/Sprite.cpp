#include "stdafx.h"

#include "ThroneFramework.h"
#include "Sprite.h"
#include "ImageSampler.h"

namespace ThroneEngine
{
	Sprite::Sprite()
	{
	}


	Sprite::~Sprite()
	{
	}

	bool Sprite::initialize()
	{
		bool initSuccess = false;

		sampler = new ImageSampler();
		initSuccess = sampler->initialize(VkExtent3D{ 1, 1, 1 }, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT, VK_FORMAT_R8G8B8A8_UNORM, 1, VK_IMAGE_VIEW_TYPE_2D);

		borderImage.setImage(*sampler->image, 0);
		borderImage.updateVertices();
		return initSuccess;
	}

	bool Sprite::initialize(const std::string& filePath)
	{
		bool initSuccess = false;
		initSuccess = initialize();

		if (initSuccess)
		{
			initSuccess = loadTextureFromFile(filePath);
			borderImage.updateVertices();
		}

		return initSuccess;
	}

	bool Sprite::loadTextureFromFile(const std::string& filePath)
	{
		const bool initSuccess = sampler->initialize(filePath, commandQueue);
		borderImage.setImage(*sampler->image, 0);
		return initSuccess;
	}

}

