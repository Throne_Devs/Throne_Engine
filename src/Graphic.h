#pragma once
#include "stdafx.h"

#include "Mesh.h"
#include "ImageSampler.h"
#include "RectTransform.h"
#include "Color.h"
#include "Sprite.h"
#include "DescriptorSet.h"

namespace ThroneEngine
{
	class Graphic
	{
	public:
		Graphic();
		~Graphic();

		void initialize();
		void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex);
		void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, std::deque<Material*>& materials, 
			uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex);
		void updateRenderer() const;

		void setMaterialActive(int position, bool isActive);

		void setColor(const Color& newColor);
		Color getColor() const;

		RenderedObject* renderedObject = nullptr;
		Mesh* mesh = nullptr;
		Sprite mainTexture;
		
		RectTransform rectTransform;

	private:
		DescriptorSet viewOthoDescriptorSet;
		const RenderPass* renderPass = nullptr;
		Color color;

	};
}
