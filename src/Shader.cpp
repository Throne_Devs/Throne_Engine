#include "stdafx.h"

#include "Shader.h"
#include "UtilityFunctions.hpp"
#include "File.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	std::string Shader::dafaultEntryPointName = "main";

	Shader::Shader()
	{
	}


	Shader::~Shader()
	{
	}

	bool Shader::initialize(std::string shaderFilePath, VkShaderStageFlagBits shaderStage)
	{
		return initialize(shaderFilePath, shaderStage, dafaultEntryPointName);
	}

	bool Shader::initialize(std::string shaderFilePath, VkShaderStageFlagBits shaderStage, std::string entryPointName)
	{
		this->shaderStage = shaderStage;
		this->entryPointName = entryPointName;
		return createShaderModule(shaderFilePath, shaderModuleHandle);
	}

	void Shader::destroy()
	{
		if (shaderModuleHandle != VK_NULL_HANDLE)
		{
			vkDestroyShaderModule(logicalDevice, shaderModuleHandle, nullptr);
			shaderModuleHandle = VK_NULL_HANDLE;
		}
	}

	void Shader::getShaderStageCreateInfo(VkPipelineShaderStageCreateInfo& shaderStageCreateInfo)
	{
		shaderStageCreateInfo = {};
		shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStageCreateInfo.pNext = nullptr;
		shaderStageCreateInfo.flags = 0;
		shaderStageCreateInfo.stage = shaderStage;
		shaderStageCreateInfo.module = shaderModuleHandle;
		shaderStageCreateInfo.pName = entryPointName.data();
		shaderStageCreateInfo.pSpecializationInfo = nullptr;
	}

	inline bool Shader::createShaderModule(std::string& shaderFilePath, VkShaderModule& shaderModuleHandle)
	{
		std::vector<char> shaderCode = File::readBinary(shaderFilePath);
		VkShaderModuleCreateInfo shaderModuleCreateInfos = {};
		shaderModuleCreateInfos.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		shaderModuleCreateInfos.pNext = nullptr;
		shaderModuleCreateInfos.flags = 0;
		shaderModuleCreateInfos.codeSize = shaderCode.size();
		shaderModuleCreateInfos.pCode = reinterpret_cast<uint32 const *>(getVectorDataPointer(shaderCode));

		VkResult result = vkCreateShaderModule(logicalDevice, &shaderModuleCreateInfos, nullptr, &shaderModuleHandle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createShaderModule);
			#endif
			return false;
		}
		#endif

		return true;
	}

}
