#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class Fence
	{
	public:
		Fence();
		~Fence();
		VkFence handle = VK_NULL_HANDLE;
		bool fenceInUse = false;

		bool initialize();
		bool waitForFence( double timeout);
		bool reset();
		void destroy();

		static bool createFence(VkFence& fence);
		static bool waitForFences(uint32 numberOfFence, VkFence* fenceHanldes, VkBool32 waitForAll, double timeout);
		static bool resetFences(uint32 numberOfFence, VkFence* fenceHanldes);
	};
}



