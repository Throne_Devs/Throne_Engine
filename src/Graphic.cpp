#include "stdafx.h"

#include "Graphic.h"
#include "Material.h"
#include "MeshRenderer.h"
#include "RenderedObject.h"
#include "ThroneFramework.h"
#include "RenderPass.h"
#include "Sprite.h"

Graphic::Graphic() : 
color(255, 255, 255),
rectTransform(0, 0, 0, 0)
{
}


Graphic::~Graphic()
{
}

void ThroneEngine::Graphic::initialize()
{
	mesh = new Mesh();
	renderedObject = new RenderedObject();

	rectTransform.graphic = this;

	if (mainTexture.sampler == nullptr)
	{
		mainTexture.borderImage.mesh = mesh;
		mainTexture.initialize(path_uiDefaultTexture);
	}
}

void Graphic::initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	initialize(renderPass, graphicsPipeline, descriptorSet, std::deque<Material*>(), meshRendererBindingIndex, modelMatrixBindingIndex);
}

void ThroneEngine::Graphic::initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, std::deque<Material*>& materials,
	uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	initialize();

	this->renderPass = renderPass;

	MeshRenderer* meshRenderer = new MeshRenderer();
	meshRenderer->initialize(mesh, commandQueue, meshRendererBindingIndex, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	Material* baseMaterial = new Material();
	baseMaterial->initialize(mainTexture.sampler, 0, 0, 1);

	std::deque<RenderedObjectPart> renderedObjectParts;
	renderedObjectParts.emplace_back(mesh->parts[0], baseMaterial, graphicsPipeline);

	for (Material* material : materials)
	{
		renderedObjectParts.emplace_back(mesh->parts[0], material, graphicsPipeline);
	}
	
	std::deque<RenderedObjectRenderPassInfo> renderedObjectRenderPassInfo;
	renderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedObjectParts, renderPass, std::vector<DescriptorSet*>{&descriptorSet}));

	renderedObject->initialize(meshRenderer, renderedObjectRenderPassInfo, modelMatrixBindingIndex, rectTransform.getPosition(), rectTransform.getEulerAngles(), rectTransform.getScale());
	renderedObject->getRenderPassInfoByRenderPassHandle(renderPass->handle).batchableDescriptorSets.push_back(&descriptorSet);
}

void Graphic::updateRenderer() const
{
	mesh->set2DPoints((Transform&)rectTransform);
	renderedObject->meshRenderer->updateBufferData(commandQueue, std::vector<Fence>{});
	renderedObject->getRenderPassInfoByRenderPassHandle(renderPass->handle).renderedObjectParts[0].meshPart = renderedObject->meshRenderer->mesh->parts[0];
}

void ThroneEngine::Graphic::setMaterialActive(int position, bool isActive)
{
	renderedObject->renderPassesInfo[0].renderedObjectParts[position].isActive = isActive;
}

void Graphic::setColor(const Color& newColor)
{
	color = newColor;
	mesh->setColor(color);
	updateRenderer();
}

Color Graphic::getColor() const
{
	return color;
}
