#pragma once
#include "stdafx.h"

#include "UtilityStructures.h"
#include "Bindable.h"

namespace ThroneEngine
{
	class GraphicsPipeline : public Bindable
	{
	public:
		GraphicsPipeline();
		~GraphicsPipeline();

		VkPipelineLayout layoutHandle = VK_NULL_HANDLE;
		VkPipeline handle = VK_NULL_HANDLE;

		std::unordered_map<VkDynamicState, GraphicsPipelineDynamicState*> currentDynamicStates;
		std::unordered_map<VkDynamicState, std::vector<GraphicsPipelineDynamicState*>> dynamicStates;

		bool initialize(VkPipelineCreateFlags additionalOptions, GraphicsPipelineCreationParams& creationParams, RenderPass& renderPass, uint32 subpass, VkPipeline basePipelineHandle);
		void destroy();

		virtual void bind(BindableBindingInfo& bindingInfo) override;

	private:


		bool createPipelineLayout(std::vector<VkDescriptorSetLayout>& descriptorSetLayout, std::vector<VkPushConstantRange>& pushConstantRange);
		bool createPipelineCacheObject(std::vector<unsigned char>& cacheData, VkPipelineCache& pipelineCacheObject);
		bool getPipelineCacheObjectData(VkPipelineCache pipelineCache, std::vector<unsigned char>& pipelineCacheData);
		bool mergeMultiplePipelineCacheObjects(VkPipelineCache targetPipelineCache, std::vector<VkPipelineCache>& sourcePipelineCaches);

		static void createInputDescriptions(std::vector<VertexBufferSignature> vertexBufferInput, 
			std::vector<VkVertexInputAttributeDescription>& vertexInputAttributeDescriptions,
			std::vector<VkVertexInputBindingDescription>& vertexInputBindingDescriptions);

		bool createGraphicsPipelines(std::vector<VkGraphicsPipelineCreateInfo>& graphicsPipelineCreateInfos, VkPipelineCache pipelineCache, std::vector<VkPipeline>& graphicsPipelines);
		bool createGraphicsPipelines(VkGraphicsPipelineCreateInfo* graphicsPipelineCreateInfos, VkPipelineCache pipelineCache, VkPipeline* graphicsPipelines, uint32 amountOfGraphicsPipeline);
		static void specifyPipelineVertexInputState(GraphicsPipelineCreationParams& pipelineCreationParams, VkPipelineVertexInputStateCreateInfo& vertexInputStateCreateInfo, 
			std::vector<VkVertexInputAttributeDescription>& vertexInputAttributeDescriptions, std::vector<VkVertexInputBindingDescription>& vertexInputBindingDescriptions);
		static void specifyPipelineInputAssemblyState(GraphicsPipelineCreationParams& pipelineCreationParams, VkPipelineInputAssemblyStateCreateInfo& inputAssemblyStateCreateInfo);
		//static void specifyPipelineTesselationState(uint32 patchControlPointsCount, VkPipelineTessellationStateCreateInfo& tesselationStateCreateInfo);
		static void specifyPipelineViewportAndScissorTestState(ViewportInfo& viewportInfo, VkPipelineViewportStateCreateInfo& viewportStateCreateInfo);
		static void specifyPipelineRasterizationState(GraphicsPipelineRasterizerCreationParams& rasterizerCreationParams, VkPipelineRasterizationStateCreateInfo& rasterizationStateCreateInfo);
		static void specifyPipelineMultisampleState(GraphicsPipelineMultiSampleCreationParams& multisampleCreationParams, VkPipelineMultisampleStateCreateInfo& multisampleStateCreateInfo);
		static void specifyPipelineDepthAndStencilState(GraphicsPipelineDepthStencilCreationParams& depthStencilCreationParams, VkPipelineDepthStencilStateCreateInfo& depthAndStencilStateCreateInfo);
		static void specifyPipelineBlendState(GraphicsPipelineBlendlCreationParams& depthStencilCreationParams, VkPipelineColorBlendStateCreateInfo& blendStateCreateInfo);
		static void specifyPipelineDynamicState(std::vector<VkDynamicState>& dynamicState, VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo);
	};

	inline void GraphicsPipeline::bind(BindableBindingInfo& bindingInfo)
	{
		bindingInfo.commandBuffer->cmdBindPipeline(VK_PIPELINE_BIND_POINT_GRAPHICS, handle);
		for (auto dynamicState : currentDynamicStates)
		{
			dynamicState.second->recordCommands(bindingInfo.commandBuffer);
		}
	}
}



