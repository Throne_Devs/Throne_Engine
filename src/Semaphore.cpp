#include "stdafx.h"

#include "Semaphore.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	Semaphore::Semaphore()
	{
	}


	Semaphore::~Semaphore()
	{
	}

	bool Semaphore::initialize()
	{
		return createSemaphore(handle);
	}

	void Semaphore::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroySemaphore(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}

	bool Semaphore::createSemaphore(VkSemaphore& semaphore)
	{
		VkSemaphoreCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;

		VkResult result = vkCreateSemaphore(logicalDevice, &createInfo, nullptr, &semaphore);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createSemaphore);
			#endif
			return false;
		}
		#endif
		return true;
	}
}