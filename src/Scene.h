#pragma once
#include "stdafx.h"

#include "Window.h"
#include "CommandQueue.h"

namespace ThroneEngine
{
	class Scene
	{
	public:
		Scene();
		~Scene();

		Window* window;
		CommandQueue commandQueue;
		
		
		virtual bool initializeScene(Window* window, CommandQueue& commandQueue) = 0;
		virtual bool windowResizeEvent() = 0;


		virtual void update(float deltaTime) = 0;
		virtual void draw() = 0;

	};
}



