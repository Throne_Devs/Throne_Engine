#pragma once
#include "Selectable.h"

class Slider :
	public Selectable
{
public:
	Slider();
	~Slider();

	void initializeObject() override;
	void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex) override;

	Event<void> onClick;

	float value = 0; //Value between 0 and 1;

private: 
	FunctionHandle<void()> onClickHandle;
	void click();

	Sprite knobTexture;
	Sprite fillTexture;

};