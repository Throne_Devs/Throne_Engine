#pragma once
#include "Log.h"

#include <iostream>
#include <string>
#include <functional>

namespace glm{}

using namespace ThroneEngine;
using namespace glm;

#pragma region Defines

#define PLATFORM_WINDOWS
#define VK_NO_PROTOTYPES
#define TINYOBJLOADER_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define THRONE_API_ENTRY extern

//#define GLM_FORCE_LEFT_HANDED
//#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#pragma region Platform specific

#if defined PLATFORM_WINDOWS
#define NOMINMAX
#include <Windows.h>
#define LIBRARY_TYPE HMODULE
#define VK_USE_PLATFORM_WIN32_KHR

#elif defined PLATFORM_LINUX
#define LIBRARY_TYPE void*
#endif

#pragma endregion Platform specific

#pragma region Validations

#define ERROR_VALIDATION_LEVEL_ALL 0
#define ERROR_VALIDATION_LEVEL_NOTHING 1
#define ERROR_VALIDATION_LEVEL ERROR_VALIDATION_LEVEL_ALL

#if ERROR_VALIDATION_LEVEL == ERROR_VALIDATION_LEVEL_ALL

#define ERROR_MESSAGE_LEVEL_NO_MESSAGES 0
#define ERROR_MESSAGE_LEVEL_CONSOLE 1
#define ERROR_MESSAGE_LEVEL_EXCEPTION 2
#define ERROR_MESSAGE_LEVEL ERROR_MESSAGE_LEVEL_CONSOLE
#define ERROR_VALIDATION_NULLPTR_CHECK
#define ERROR_VALIDATION_VKSUCCESS_CHECK
#define ERROR_VALIDATION_INVALID_DATA
#define VK_NULL_HANDLE_DESTROY_CHECK
#define ERROR_MESSAGE_NEEDED ERROR_MESSAGE_LEVEL != ERROR_MESSAGE_LEVEL_NO_MESSAGES
#if ERROR_MESSAGE_LEVEL == ERROR_MESSAGE_LEVEL_CONSOLE
#define ERROR_MESSAGE(message)	std::string errorMessage = std::string(message) + std::string(" in file ") + std::string(__FILE__) + std::string(" at line ") + std::to_string(__LINE__); \
								Log::write(err_vulkanValidation, errorMessage); std::cerr << (errorMessage) << "\n"
#define ERROR_MESSAGE_NO_FILE_PATH(message) Log::write(err_vulkanValidation, message); std::cerr << (message) << "\n"

#elif ERROR_MESSAGE_LEVEL == ERROR_MESSAGE_LEVEL_EXCEPTION
#define ERROR_MESSAGE(message) Log::write(err_vulkanValidation, message); throw std::runtime_error(message);

#else
#define ERROR_MESSAGE(message)
#endif

#endif

#pragma endregion Validations

#pragma endregion Defines

#pragma region Global Constants

#if defined _DEBUG
const bool enableValidationLayers = true;
#else
const bool enableValidationLayers = false;
#endif

#pragma endregion Global Constants

#pragma region Type Definitions

//Redifining the fixed-width integer types found in the C11 standard to avoid having to type "_t" each time the types are used.

typedef unsigned int uint;
typedef BYTE byte;
typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef int_fast8_t intfast8;
typedef int_fast16_t intfast16;
typedef int_fast32_t intfast32;
typedef int_fast64_t intfast64;
typedef intmax_t intmax;
typedef intptr_t intptr;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef uint_fast8_t uintfast8;
typedef uint_fast16_t uintfast16;
typedef uint_fast32_t uintfast32;
typedef uint_fast64_t uintfast64;
typedef uint_least8_t uintleast8;
typedef uint_least16_t uintleast16;
typedef uint_least32_t uintleast32;
typedef uint_least64_t uintleast64;
typedef uintmax_t uintmax;
typedef uintptr_t uintptr;
#pragma endregion Type Definitions
