#pragma once
#include "Fence.h"
#include "CommandPool.h"
#include "Semaphore.h"
#include "SemaphorePool.h"

namespace ThroneEngine
{
	class CommandQueue
	{
	public:
		CommandQueue();
		~CommandQueue();

		VkQueue handle = VK_NULL_HANDLE;
		uint32 familyIndex;
		float priority;
		
		bool waitForQueue();

		bool submitCommandBuffer(WaitSemaphoreInfo& waitSemaphoresInfos, CommandBuffer commandBuffer, Semaphore& semaphoreToSignal, Fence& signalFence);
		bool submitCommandBuffer(WaitSemaphoreInfo& waitSemaphoresInfos, CommandBuffer commandBuffer, SemaphorePool& semaphoresToSignal, Fence& signalFence);
		bool submitCommandBuffers(WaitSemaphoreInfo& waitSemaphoresInfos, CommandPool& commandBuffers, Semaphore& semaphoreToSignal, Fence& signalFence);
		bool submitCommandBuffers(WaitSemaphoreInfo& waitSemaphoresInfos, CommandPool& commandBuffers, SemaphorePool& semaphoresToSignal, Fence& signalFence);
		bool submitCommandBuffers(
			WaitSemaphoreInfo& waitSemaphoresInfos, 
			CommandBuffer* commandBuffers, 
			uint32 numberOfCommandBuffer, 
			VkSemaphore* semaphoreToSignalHandles, 
			uint32 numberOfSemaphoreToSignal, 
			Fence& fenceToSignal);

	};
}



