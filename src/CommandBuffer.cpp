#include "stdafx.h"

#include "CommandBuffer.h"
#include "RenderPass.h"
#include "FrameBuffer.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	CommandBuffer::CommandBuffer()
	{
	}


	CommandBuffer::~CommandBuffer()
	{
	}

	bool CommandBuffer::beginRecordingOnPrimaryCommandBuffer(VkCommandBufferUsageFlags usage)
	{
		return beginRecording(usage, nullptr);
	}

	bool CommandBuffer::beginRecordingOnSecondaryCommandBuffer(
		VkCommandBufferUsageFlags usage,
		RenderPass& renderPass,
		uint32 subpass,
		FrameBuffer& frameBuffer,
		VkBool32 occlusionQuerryEnable,
		VkQueryControlFlags queryFlags,
		VkQueryPipelineStatisticFlags pipelineStatistics)
	{
		VkCommandBufferInheritanceInfo* secondaryBufferInfo = new VkCommandBufferInheritanceInfo();
		secondaryBufferInfo->sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		secondaryBufferInfo->pNext = nullptr;
		secondaryBufferInfo->renderPass = renderPass.handle;
		secondaryBufferInfo->subpass = subpass;
		secondaryBufferInfo->framebuffer = frameBuffer.handle;
		secondaryBufferInfo->occlusionQueryEnable = occlusionQuerryEnable;
		secondaryBufferInfo->queryFlags = queryFlags;
		secondaryBufferInfo->pipelineStatistics = pipelineStatistics;

		return beginRecordingOnSecondaryCommandBuffer(usage, secondaryBufferInfo);
	}

	bool CommandBuffer::beginRecordingOnSecondaryCommandBuffer(VkCommandBufferUsageFlags usage, VkCommandBufferInheritanceInfo* secondaryBufferInfo)
	{
		return beginRecording(usage, secondaryBufferInfo);
	}

	bool CommandBuffer::beginRecording(VkCommandBufferUsageFlags usage, VkCommandBufferInheritanceInfo* secondaryBufferInfo)
	{
		VkCommandBufferBeginInfo commandBufferBeginInfo = {};
		commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		commandBufferBeginInfo.pNext = nullptr;
		commandBufferBeginInfo.flags = usage;
		commandBufferBeginInfo.pInheritanceInfo = secondaryBufferInfo;

		VkResult result = vkBeginCommandBuffer(handle, &commandBufferBeginInfo);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_beginRecordingCommandBuffer);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool CommandBuffer::endRecording()
	{
		VkResult result = vkEndCommandBuffer(handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_endRecordingCommandBuffer);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool CommandBuffer::createCommandBuffers(VkCommandPool commandPoolHandle, VkCommandBufferLevel level, std::vector<CommandBuffer>& bufferHandles)
	{
		return createCommandBuffers(commandPoolHandle, level, bufferHandles, 0, bufferHandles.size());
	}

	bool CommandBuffer::createCommandBuffers( VkCommandPool commandPoolHandle, VkCommandBufferLevel level, std::vector<CommandBuffer>& bufferHandles, int beginIndex, int numberOfBuffer)
	{
		VkCommandBufferAllocateInfo allocateInfo = {};
		allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocateInfo.pNext = nullptr;
		allocateInfo.commandPool = commandPoolHandle;
		allocateInfo.level = level;
		allocateInfo.commandBufferCount = numberOfBuffer;

		VkResult result = vkAllocateCommandBuffers(logicalDevice, &allocateInfo, reinterpret_cast<VkCommandBuffer*>(&bufferHandles[beginIndex]));
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_commandBuffer);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool CommandBuffer::reset(VkCommandBufferResetFlags releaseResources)
	{
		VkResult result = vkResetCommandBuffer(handle, releaseResources);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_resetCommandBuffer);
			#endif
			return false;
		}
		#endif
		return true;
	}
}