#include "stdafx.h"

#include "OBJModel.h"
#include "R.h"

#include <TinyObjLoader/tiny_obj_loader.h>

namespace ThroneEngine
{
	bool OBJModel::loadModel(const char* filename, Mesh* mesh, const bool loadNormals, const bool loadTexCoords, const bool loadColor)
	{
		tinyobj::attrib_t attribs;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		std::string error;

		const bool result = LoadObj(&attribs, &shapes, &materials, &error, filename);
		#if defined ERROR_VALIDATION_INVALID_DATA
		if (!result)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(R::S::ErrorMessage::err_fileOpen + " " + error);
			#endif
			return false;
		}
		#endif

		uint32_t offset = 0;
		for (auto& shape : shapes)
		{
			const uint32_t partOffset = offset;
			for (auto index : shape.mesh.indices)
			{
				mesh->data.emplace_back(attribs.vertices[3 * index.vertex_index + 0]);
				mesh->data.emplace_back(attribs.vertices[3 * index.vertex_index + 1]);
				mesh->data.emplace_back(attribs.vertices[3 * index.vertex_index + 2]);
				++offset;

				if (loadTexCoords && attribs.texcoords.size() > 0)
				{
					mesh->data.emplace_back(attribs.texcoords[2 * index.texcoord_index + 0]);
					mesh->data.emplace_back(attribs.texcoords[2 * index.texcoord_index + 1]);
				}
				if (loadNormals && attribs.normals.size() > 0)
				{
					mesh->data.emplace_back(attribs.normals[3 * index.normal_index + 0]);
					mesh->data.emplace_back(attribs.normals[3 * index.normal_index + 1]);
					mesh->data.emplace_back(attribs.normals[3 * index.normal_index + 2]);
				}
				if(loadColor)
				{
					mesh->data.emplace_back(1);
					mesh->data.emplace_back(1);
					mesh->data.emplace_back(1);
				}
			}

			const uint32_t partVertexCount = offset - partOffset;
			if (0 < partVertexCount)
			{
				mesh->parts.push_back({ partOffset, partVertexCount });
			}
		}
		
		mesh->vertexSizeInBytes = sizeof(float) * 3;
		if (loadTexCoords == true)
		{
			mesh->vertexSizeInBytes += sizeof(float) * 2;
		}
		if (loadNormals == true)
		{
			mesh->vertexSizeInBytes += sizeof(float) * 3;
		}
		if(loadColor == true)
		{
			mesh->vertexSizeInBytes += sizeof(float) * 3;
		}
		mesh->setTotalAmountOfVertices();
		mesh->setTotalSize();

		return true;
	}

}

