#pragma once

#include "stdafx.h"
#include "VulkanFunctions.hpp"

using std::cout;

namespace ThroneEngine
{
	LIBRARY_TYPE vulkanLibrary;

	inline bool loadVulkanLibrary()
	{
		#if defined PLATFORM_WINDOWS
		vulkanLibrary = LoadLibrary(path_windowsVulkanLibrary);
		#elif defined PLATFORM_LINUX
		vulkanLibrary = dlopen(path_linuxVulkanLibrary, RTLD_NOW);
		#endif
		#if defined ERROR_VALIDATION_NULLPTR_CHECK
		if (vulkanLibrary == nullptr)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_vulkanLibraryConnect);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline void releaseVulkanLibrary()
	{
		#if defined PLATFORM_WINDOWS
		FreeLibrary(vulkanLibrary);
		#elif defined PLATFORM_LINUX
		dlclose(vulkanLibrary);
		#endif
		vulkanLibrary = nullptr;
	}

	inline bool isExtensionSupported(std::vector<VkExtensionProperties>& availableExtensions, const char* requiredExtension)
	{
		for (VkExtensionProperties& availableExtension : availableExtensions)
		{
			if (strcmp(requiredExtension, availableExtension.extensionName) == 0)
			{
				return true;
			}
		}
		return false;
	}

	inline bool isExtensionSupported(std::vector<const char*>& availableExtensions, const char* requiredExtension)
	{
		for (const char* & availableExtension : availableExtensions)
		{
			if (strcmp(requiredExtension, availableExtension) == 0)
			{
				return true;
			}
		}
		return false;
	}

	inline bool initializeVulkanFunctions()
	{
		if (loadVulkanLibrary() == false)
		{
			return false;
		}
		if (loadPFNGetInstanceProcAddr() == false)
		{
			return false;
		}

		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkEnumerateInstanceExtensionProperties, nullptr, "vkEnumerateInstanceExtensionProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkEnumerateInstanceLayerProperties, nullptr, "vkEnumerateInstanceLayerProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateInstance, nullptr, "vkCreateInstance");

		return true;
	}

	inline bool initializeInstanceLevelVulkanFunctions(const VkInstance& instance, std::vector<const char*>& enabledExtensions)
	{
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkEnumeratePhysicalDevices, instance, "vkEnumeratePhysicalDevices");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkEnumerateDeviceExtensionProperties, instance, "vkEnumerateDeviceExtensionProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceFeatures, instance, "vkGetPhysicalDeviceFeatures");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceProperties, instance, "vkGetPhysicalDeviceProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceQueueFamilyProperties, instance, "vkGetPhysicalDeviceQueueFamilyProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateDevice, instance, "vkCreateDevice");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetDeviceProcAddr, instance, "vkGetDeviceProcAddr");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkDestroyInstance, instance, "vkDestroyInstance");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateDebugReportCallbackEXT, instance, "vkCreateDebugReportCallbackEXT");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceMemoryProperties, instance, "vkGetPhysicalDeviceMemoryProperties");
		loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceFormatProperties, instance, "vkGetPhysicalDeviceFormatProperties");

		if (isExtensionSupported(enabledExtensions, VK_KHR_SURFACE_EXTENSION_NAME))
		{
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceSurfaceSupportKHR, instance, "vkGetPhysicalDeviceSurfaceSupportKHR");
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceSurfaceCapabilitiesKHR, instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceSurfaceFormatsKHR, instance, "vkGetPhysicalDeviceSurfaceFormatsKHR");
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkGetPhysicalDeviceSurfacePresentModesKHR, instance, "vkGetPhysicalDeviceSurfacePresentModesKHR");
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkDestroySurfaceKHR, instance, "vkDestroySurfaceKHR");
		}

		#if defined VK_USE_PLATFORM_WIN32_KHR
		if (isExtensionSupported(enabledExtensions, VK_KHR_WIN32_SURFACE_EXTENSION_NAME))
		{
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateWin32SurfaceKHR, instance, "vkCreateWin32SurfaceKHR");
		}
		#elif defined VK_USE_PLATFORM_XCB_KHR
		if (IsExtensionSupported(enabledExtensions, VK_KHR_XCB_SURFACE_EXTENSION_NAME))
		{
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateXcbSurfaceKHR, instance, "vkCreateXcbSurfaceKHR");
		}
		#elif defined VK_USE_PLATFORM_XLIB_KHR
		if (IsExtensionSupported(enabledExtensions, VK_KHR_XLIB_SURFACE_EXTENSION_NAME))
		{
			loadVulkanInstanceFunction(funcptr_vkGetInstanceProcAddr, funcptr_vkCreateXlibSurfaceKHR, instance, "vkCreateXlibSurfaceKHR");
		}
		#endif

		return true;
	}

	inline bool initializeDeviceLevelVulkanFunctions(const VkDevice& logicalDevice, std::vector<const char*>& enabledExtensions)
	{
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetDeviceQueue, logicalDevice, "vkGetDeviceQueue");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDeviceWaitIdle, logicalDevice, "vkDeviceWaitIdle");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyDevice, logicalDevice, "vkDestroyDevice");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateBuffer, logicalDevice, "vkCreateBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetBufferMemoryRequirements, logicalDevice, "vkGetBufferMemoryRequirements");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateCommandPool, logicalDevice, "vkCreateCommandPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkAllocateCommandBuffers, logicalDevice, "vkAllocateCommandBuffers");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateImageView, logicalDevice, "vkCreateImageView");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyImageView, logicalDevice, "vkDestroyImageView");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateShaderModule, logicalDevice, "vkCreateShaderModule");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyShaderModule, logicalDevice, "vkDestroyShaderModule");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreatePipelineLayout, logicalDevice, "vkCreatePipelineLayout");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyPipelineLayout, logicalDevice, "vkDestroyPipelineLayout");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateRenderPass, logicalDevice, "vkCreateRenderPass");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyRenderPass, logicalDevice, "vkDestroyRenderPass");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateGraphicsPipelines, logicalDevice, "vkCreateGraphicsPipelines");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyPipeline, logicalDevice, "vkDestroyPipeline");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateFramebuffer, logicalDevice, "vkCreateFramebuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyFramebuffer, logicalDevice, "vkDestroyFramebuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkBeginCommandBuffer, logicalDevice, "vkBeginCommandBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkEndCommandBuffer, logicalDevice, "vkEndCommandBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkResetCommandBuffer, logicalDevice, "vkResetCommandBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkResetCommandPool, logicalDevice, "vkResetCommandPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateSemaphore, logicalDevice, "vkCreateSemaphore");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateFence, logicalDevice, "vkCreateFence");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkWaitForFences, logicalDevice, "vkWaitForFences");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkResetFences, logicalDevice, "vkResetFences");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyFence, logicalDevice, "vkDestroyFence");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroySemaphore, logicalDevice, "vkDestroySemaphore");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkFreeCommandBuffers, logicalDevice, "vkFreeCommandBuffers");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyCommandPool, logicalDevice, "vkDestroyCommandPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkQueueWaitIdle, logicalDevice, "vkQueueWaitIdle");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkQueueSubmit, logicalDevice, "vkQueueSubmit");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateBuffer, logicalDevice, "vkCreateBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetBufferMemoryRequirements, logicalDevice, "vkGetBufferMemoryRequirements");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkBindBufferMemory, logicalDevice, "vkBindBufferMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyBuffer, logicalDevice, "vkDestroyBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateBufferView, logicalDevice, "vkCreateBufferView");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyBufferView, logicalDevice, "vkDestroyBufferView");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkMapMemory, logicalDevice, "vkMapMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkFlushMappedMemoryRanges, logicalDevice, "vkFlushMappedMemoryRanges");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkUnmapMemory, logicalDevice, "vkUnmapMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetImageMemoryRequirements, logicalDevice, "vkGetImageMemoryRequirements");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkBindImageMemory, logicalDevice, "vkBindImageMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkFreeMemory, logicalDevice, "vkFreeMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkFlushMappedMemoryRanges, logicalDevice, "vkFlushMappedMemoryRanges");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkAllocateMemory, logicalDevice, "vkAllocateMemory");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateImage, logicalDevice, "vkCreateImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyImage, logicalDevice, "vkDestroyImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateSampler, logicalDevice, "vkCreateSampler");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateDescriptorSetLayout, logicalDevice, "vkCreateDescriptorSetLayout");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateDescriptorPool, logicalDevice, "vkCreateDescriptorPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkAllocateDescriptorSets, logicalDevice, "vkAllocateDescriptorSets");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkUpdateDescriptorSets, logicalDevice, "vkUpdateDescriptorSets");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkFreeDescriptorSets, logicalDevice, "vkFreeDescriptorSets");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkResetDescriptorPool, logicalDevice, "vkResetDescriptorPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyDescriptorPool, logicalDevice, "vkDestroyDescriptorPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyDescriptorSetLayout, logicalDevice, "vkDestroyDescriptorSetLayout");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroySampler, logicalDevice, "vkDestroySampler");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreatePipelineCache, logicalDevice, "vkCreatePipelineCache");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetPipelineCacheData, logicalDevice, "vkGetPipelineCacheData");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkMergePipelineCaches, logicalDevice, "vkMergePipelineCaches");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyPipelineCache, logicalDevice, "vkDestroyPipelineCache");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateComputePipelines, logicalDevice, "vkCreateComputePipelines");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyEvent, logicalDevice, "vkDestroyEvent");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroyQueryPool, logicalDevice, "vkDestroyQueryPool");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdBeginRenderPass, logicalDevice, "vkCmdBeginRenderPass");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdBindPipeline, logicalDevice, "vkCmdBindPipeline");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdDraw, logicalDevice, "vkCmdDraw");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdEndRenderPass, logicalDevice, "vkCmdEndRenderPass");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdCopyBuffer, logicalDevice, "vkCmdCopyBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdCopyBufferToImage, logicalDevice, "vkCmdCopyBufferToImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdCopyImageToBuffer, logicalDevice, "vkCmdCopyImageToBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdBindVertexBuffers, logicalDevice, "vkCmdBindVertexBuffers");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdBindIndexBuffer, logicalDevice, "vkCmdBindIndexBuffer");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdPipelineBarrier, logicalDevice, "vkCmdPipelineBarrier");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdBindDescriptorSets, logicalDevice, "vkCmdBindDescriptorSets");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdNextSubpass, logicalDevice, "vkCmdNextSubpass");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdSetViewport, logicalDevice, "vkCmdSetViewport");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdSetScissor, logicalDevice, "vkCmdSetScissor");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdDrawIndexed, logicalDevice, "vkCmdDrawIndexed");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdDispatch, logicalDevice, "vkCmdDispatch");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdCopyImage, logicalDevice, "vkCmdCopyImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdPushConstants, logicalDevice, "vkCmdPushConstants");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdClearColorImage, logicalDevice, "vkCmdClearColorImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdClearDepthStencilImage, logicalDevice, "vkCmdClearDepthStencilImage");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdSetLineWidth, logicalDevice, "vkCmdSetLineWidth");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdSetDepthBias, logicalDevice, "vkCmdSetDepthBias");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdSetBlendConstants, logicalDevice, "vkCmdSetBlendConstants");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdExecuteCommands, logicalDevice, "vkCmdExecuteCommands");
		loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCmdClearAttachments, logicalDevice, "vkCmdClearAttachments");

		if (isExtensionSupported(enabledExtensions, VK_KHR_SWAPCHAIN_EXTENSION_NAME))
		{
			loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkCreateSwapchainKHR, logicalDevice, "vkCreateSwapchainKHR");
			loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkGetSwapchainImagesKHR, logicalDevice, "vkGetSwapchainImagesKHR");
			loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkAcquireNextImageKHR, logicalDevice, "vkAcquireNextImageKHR");
			loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkQueuePresentKHR, logicalDevice, "vkQueuePresentKHR");
			loadVulkanDeviceFunction(funcptr_vkGetDeviceProcAddr, funcptr_vkDestroySwapchainKHR, logicalDevice, "vkDestroySwapchainKHR");
		}

		return true;
	}

	inline VkResult vkEnumerateInstanceExtensionProperties(const char* pLayerName, uint32* pPropertyCount, VkExtensionProperties* pProperties)
	{
		return funcptr_vkEnumerateInstanceExtensionProperties(pLayerName, pPropertyCount, pProperties);
	}

	inline VkResult vkEnumerateInstanceLayerProperties(uint32* pPropertyCount, VkLayerProperties* pProperties)
	{
		return funcptr_vkEnumerateInstanceLayerProperties(pPropertyCount, pProperties);
	}

	inline VkResult vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkInstance* pInstance)
	{
		return funcptr_vkCreateInstance(pCreateInfo, pAllocator, pInstance);
	}

	inline VkResult vkEnumeratePhysicalDevices(VkInstance instance, uint32* pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices)
	{
		return funcptr_vkEnumeratePhysicalDevices(instance, pPhysicalDeviceCount, pPhysicalDevices);
	}

	inline void vkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties)
	{
		funcptr_vkGetPhysicalDeviceProperties(physicalDevice, pProperties);
	}

	inline void vkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures* pFeatures)
	{
		funcptr_vkGetPhysicalDeviceFeatures(physicalDevice, pFeatures);
	}

	inline VkResult vkCreateDevice(VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDevice* pDevice)
	{
		return funcptr_vkCreateDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);
	}

	inline PFN_vkVoidFunction vkGetDeviceProcAddr(VkDevice device, const char* pName)
	{
		return funcptr_vkGetDeviceProcAddr(device, pName);
	}

	inline VkResult vkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice, const char* pLayerName, uint32* pPropertyCount, VkExtensionProperties* pProperties)
	{
		return funcptr_vkEnumerateDeviceExtensionProperties(physicalDevice, pLayerName, pPropertyCount, pProperties);
	}

	inline void vkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, uint32* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties)
	{
		funcptr_vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, pQueueFamilyPropertyCount, pQueueFamilyProperties);
	}

	inline void vkGetPhysicalDeviceMemoryProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceMemoryProperties* pMemoryProperties)
	{
		funcptr_vkGetPhysicalDeviceMemoryProperties(physicalDevice, pMemoryProperties);
	}

	inline void vkGetPhysicalDeviceFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkFormatProperties* pFormatProperties)
	{
		funcptr_vkGetPhysicalDeviceFormatProperties(physicalDevice, format, pFormatProperties);
	}

	inline VkResult vkGetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice, uint32 queueFamilyIndex, VkSurfaceKHR surface, VkBool32* pSupported)
	{
		return funcptr_vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, pSupported);
	}

	inline VkResult vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities)
	{
		return funcptr_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, pSurfaceCapabilities);
	}

	inline VkResult vkGetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32* pSurfaceFormatCount, VkSurfaceFormatKHR* pSurfaceFormats)
	{
		return funcptr_vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pSurfaceFormatCount, pSurfaceFormats);
	}

	inline VkResult vkGetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32* pPresentModeCount, VkPresentModeKHR* pPresentModes)
	{
		return funcptr_vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, pPresentModes);
	}

	inline void vkDestroySurfaceKHR(VkInstance instance, VkSurfaceKHR surface, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroySurfaceKHR(instance, surface, pAllocator);
	}

	inline void vkDestroyInstance(VkInstance instance, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyInstance(instance, pAllocator);
	}

	inline VkResult vkCreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT * pCreateInfo, const VkAllocationCallbacks * pAllocator, VkDebugReportCallbackEXT * pCallback)
	{
		return funcptr_vkCreateDebugReportCallbackEXT(instance, pCreateInfo, pAllocator, pCallback);
	}

	inline PFN_vkVoidFunction vkGetInstanceProcAddr(VkInstance instance, const char* pName)
	{
		return funcptr_vkGetInstanceProcAddr(instance, pName);
	}

	inline bool loadPFNGetInstanceProcAddr()
	{
		return loadFunctionFromLibrary(funcptr_vkGetInstanceProcAddr, vulkanLibrary, "vkGetInstanceProcAddr");
	}

	#if defined VK_USE_PLATFORM_WIN32_KHR
	inline VkResult vkCreateWin32SurfaceKHR(VkInstance instance, const VkWin32SurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface)
	{
		return funcptr_vkCreateWin32SurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
	}

	#elif defined VK_USE_PLATFORM_XCB_KHR
	inline VkResult PFN_vkCreateXcbSurfaceKHR(VkInstance instance, const VkXcbSurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface)
	{
		return funcptr_vkCreateXcbSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
	}

	#elif defined VK_USE_PLATFORM_XLIB_KHR
	inline VkResult vkCreateXlibSurfaceKHR(VkInstance instance, const VkXlibSurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface)
	{
		return funcptr_vkCreateXlibSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
	}
	#endif

	inline void vkGetDeviceQueue(VkDevice device, uint32 queueFamilyIndex, uint32 queueIndex, VkQueue* pQueue)
	{
		funcptr_vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, pQueue);
	}

	inline VkResult vkDeviceWaitIdle(VkDevice device)
	{
		return funcptr_vkDeviceWaitIdle(device);
	}

	inline void vkDestroyDevice(VkDevice device, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyDevice(device, pAllocator);
	}

	inline VkResult vkCreateBuffer(VkDevice device, const VkBufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer)
	{
		return funcptr_vkCreateBuffer(device, pCreateInfo, pAllocator, pBuffer);
	}

	inline void vkGetBufferMemoryRequirements(VkDevice device, VkBuffer buffer, VkMemoryRequirements* pMemoryRequirements)
	{
		funcptr_vkGetBufferMemoryRequirements(device, buffer, pMemoryRequirements);
	}

	inline VkResult vkAllocateMemory(VkDevice device, const VkMemoryAllocateInfo* pAllocateInfo, const VkAllocationCallbacks* pAllocator, VkDeviceMemory* pMemory)
	{
		return funcptr_vkAllocateMemory(device, pAllocateInfo, pAllocator, pMemory);
	}

	inline VkResult vkCreateCommandPool(VkDevice device, const VkCommandPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkCommandPool* pCommandPool)
	{
		return funcptr_vkCreateCommandPool(device, pCreateInfo, pAllocator, pCommandPool);
	}

	inline VkResult vkAllocateCommandBuffers(VkDevice device, const VkCommandBufferAllocateInfo* pAllocateInfo, VkCommandBuffer* pCommandBuffers)
	{
		return funcptr_vkAllocateCommandBuffers(device, pAllocateInfo, pCommandBuffers);
	}

	inline VkResult vkCreateSwapchainKHR(VkDevice device, const VkSwapchainCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSwapchainKHR* pSwapchain)
	{
		return funcptr_vkCreateSwapchainKHR(device, pCreateInfo, pAllocator, pSwapchain);
	}

	inline VkResult vkGetSwapchainImagesKHR(VkDevice device, VkSwapchainKHR swapchain, uint32* pSwapchainImageCount, VkImage* pSwapchainImages)
	{
		return funcptr_vkGetSwapchainImagesKHR(device, swapchain, pSwapchainImageCount, pSwapchainImages);
	}

	inline VkResult vkAcquireNextImageKHR(VkDevice device, VkSwapchainKHR swapchain, uint64 timeout, VkSemaphore semaphore, VkFence fence, uint32* pImageIndex)
	{
		return funcptr_vkAcquireNextImageKHR(device, swapchain, timeout, semaphore, fence, pImageIndex);
	}

	inline VkResult vkQueuePresentKHR(VkQueue graphicsQueue, const VkPresentInfoKHR* pPresentInfo)
	{
		return funcptr_vkQueuePresentKHR(graphicsQueue, pPresentInfo);
	}

	inline void vkDestroySwapchainKHR(VkDevice device, VkSwapchainKHR swapchain, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroySwapchainKHR(device, swapchain, pAllocator);
	}

	inline VkResult vkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImageView* pView)
	{
		return funcptr_vkCreateImageView(device, pCreateInfo, pAllocator, pView);
	}

	inline void vkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyImageView(device, imageView, pAllocator);
	}

	inline void vkDestroyImage(VkDevice device, VkImage image, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyImage(device, image, pAllocator);
	}

	inline VkResult vkCreateShaderModule(VkDevice device, const VkShaderModuleCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkShaderModule* pShaderModule)
	{
		return funcptr_vkCreateShaderModule(device, pCreateInfo, pAllocator, pShaderModule);
	}

	inline void vkDestroyShaderModule(VkDevice device, VkShaderModule shaderModule, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyShaderModule(device, shaderModule, pAllocator);
	}

	inline VkResult vkCreatePipelineLayout(VkDevice device, const VkPipelineLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineLayout* pPipelineLayout)
	{
		return funcptr_vkCreatePipelineLayout(device, pCreateInfo, pAllocator, pPipelineLayout);
	}

	inline void vkDestroyPipelineLayout(VkDevice device, VkPipelineLayout pipelineLayout, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyPipelineLayout(device, pipelineLayout, pAllocator);
	}

	inline VkResult vkCreateRenderPass(VkDevice device, const VkRenderPassCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkRenderPass* pRenderPass)
	{
		return funcptr_vkCreateRenderPass(device, pCreateInfo, pAllocator, pRenderPass);
	}

	inline void vkDestroyRenderPass(VkDevice device, VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyRenderPass(device, renderPass, pAllocator);
	}

	inline VkResult vkCreateGraphicsPipelines(VkDevice device, VkPipelineCache pipelineCache, uint32 createInfoCount, const VkGraphicsPipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines)
	{
		return funcptr_vkCreateGraphicsPipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
	}

	inline VkResult vkCreateComputePipelines(VkDevice device, VkPipelineCache pipelineCache, uint32 createInfoCount, const VkComputePipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines)
	{
		return funcptr_vkCreateComputePipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
	}

	inline void vkDestroyPipeline(VkDevice device, VkPipeline pipeline, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyPipeline(device, pipeline, pAllocator);
	}

	inline void vkDestroyEvent(VkDevice device, VkEvent event, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyEvent(device, event, pAllocator);
	}

	inline void vkDestroyQueryPool(VkDevice device, VkQueryPool queryPool, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyQueryPool(device, queryPool, pAllocator);
	}

	inline VkResult vkCreateFramebuffer(VkDevice device, const VkFramebufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFramebuffer* pFramebuffer)
	{
		return funcptr_vkCreateFramebuffer(device, pCreateInfo, pAllocator, pFramebuffer);
	}

	inline void vkDestroyFramebuffer(VkDevice device, VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyFramebuffer(device, framebuffer, pAllocator);
	}

	inline VkResult vkBeginCommandBuffer(VkCommandBuffer commandBuffer, const VkCommandBufferBeginInfo* pBeginInfo)
	{
		return funcptr_vkBeginCommandBuffer(commandBuffer, pBeginInfo);
	}

	inline VkResult vkEndCommandBuffer(VkCommandBuffer commandBuffer)
	{
		return funcptr_vkEndCommandBuffer(commandBuffer);
	}

	inline VkResult vkResetCommandBuffer(VkCommandBuffer commandBuffer, VkCommandBufferResetFlags flags)
	{
		return funcptr_vkResetCommandBuffer(commandBuffer, flags);
	}

	inline VkResult vkResetCommandPool(VkDevice device, VkCommandPool commandPool, VkCommandPoolResetFlags flags)
	{
		return funcptr_vkResetCommandPool(device, commandPool, flags);
	}

	inline VkResult vkCreateSemaphore(VkDevice device, const VkSemaphoreCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSemaphore* pSemaphore)
	{
		return funcptr_vkCreateSemaphore(device, pCreateInfo, pAllocator, pSemaphore);
	}

	inline VkResult vkCreateFence(VkDevice device, const VkFenceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFence* pFence)
	{
		return funcptr_vkCreateFence(device, pCreateInfo, pAllocator, pFence);
	}

	inline VkResult vkWaitForFences(VkDevice device, uint32 fenceCount, const VkFence* pFences, VkBool32 waitAll, uint64 timeout)
	{
		return funcptr_vkWaitForFences(device, fenceCount, pFences, waitAll, timeout);
	}

	inline VkResult vkResetFences(VkDevice device, uint32 fenceCount, const VkFence* pFences)
	{
		return funcptr_vkResetFences(device, fenceCount, pFences);
	}

	inline void vkDestroyFence(VkDevice device, VkFence fence, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyFence(device, fence, pAllocator);
	}

	inline void vkDestroySemaphore(VkDevice device, VkSemaphore semaphore, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroySemaphore(device, semaphore, pAllocator);
	}

	inline void vkFreeCommandBuffers(VkDevice device, VkCommandPool commandPool, uint32 commandBufferCount, const VkCommandBuffer* pCommandBuffers)
	{
		funcptr_vkFreeCommandBuffers(device, commandPool, commandBufferCount, pCommandBuffers);
	}

	inline void vkDestroyCommandPool(VkDevice device, VkCommandPool commandPool, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyCommandPool(device, commandPool, pAllocator);
	}

	inline VkResult vkQueueWaitIdle(VkQueue graphicsQueue)
	{
		return funcptr_vkQueueWaitIdle(graphicsQueue);
	}

	inline VkResult vkCreateSampler(VkDevice device, const VkSamplerCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSampler* pSampler)
	{
		return funcptr_vkCreateSampler(device, pCreateInfo, pAllocator, pSampler);
	}

	inline VkResult vkCreateDescriptorSetLayout(VkDevice device, const VkDescriptorSetLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorSetLayout* pSetLayout)
	{
		return funcptr_vkCreateDescriptorSetLayout(device, pCreateInfo, pAllocator, pSetLayout);
	}

	inline VkResult vkCreateDescriptorPool(VkDevice device, const VkDescriptorPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorPool* pDescriptorPool)
	{
		return funcptr_vkCreateDescriptorPool(device, pCreateInfo, pAllocator, pDescriptorPool);
	}

	inline VkResult vkAllocateDescriptorSets(VkDevice device, const VkDescriptorSetAllocateInfo* pAllocateInfo, VkDescriptorSet* pDescriptorSets)
	{
		return funcptr_vkAllocateDescriptorSets(device, pAllocateInfo, pDescriptorSets);
	}

	inline void vkUpdateDescriptorSets(VkDevice device, uint32 descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites, uint32 descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies)
	{
		funcptr_vkUpdateDescriptorSets(device, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies);
	}

	inline VkResult vkFreeDescriptorSets(VkDevice device, VkDescriptorPool descriptorPool, uint32 descriptorSetCount, const VkDescriptorSet* pDescriptorSets)
	{
		return funcptr_vkFreeDescriptorSets(device, descriptorPool, descriptorSetCount, pDescriptorSets);
	}

	inline VkResult vkResetDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags)
	{
		return funcptr_vkResetDescriptorPool(device, descriptorPool, flags);
	}

	inline void vkDestroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyDescriptorPool(device, descriptorPool, pAllocator);
	}

	inline void vkDestroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyDescriptorSetLayout(device, descriptorSetLayout, pAllocator);
	}

	inline void vkDestroySampler(VkDevice device, VkSampler sampler, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroySampler(device, sampler, pAllocator);
	}

	inline VkResult vkQueueSubmit(VkQueue graphicsQueue, uint32 submitCount, const VkSubmitInfo* pSubmits, VkFence fence)
	{
		return funcptr_vkQueueSubmit(graphicsQueue, submitCount, pSubmits, fence);
	}

	inline VkResult vkCreatePipelineCache(VkDevice device, const VkPipelineCacheCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineCache* pPipelineCache)
	{
		return funcptr_vkCreatePipelineCache(device, pCreateInfo, pAllocator, pPipelineCache);
	}

	inline VkResult vkGetPipelineCacheData(VkDevice device, VkPipelineCache pipelineCache, size_t* pDataSize, void* pData)
	{
		return funcptr_vkGetPipelineCacheData(device, pipelineCache, pDataSize, pData);
	}

	inline VkResult vkMergePipelineCaches(VkDevice device, VkPipelineCache dstCache, uint32 srcCacheCount, const VkPipelineCache* pSrcCaches)
	{
		return funcptr_vkMergePipelineCaches(device, dstCache, srcCacheCount, pSrcCaches);
	}

	inline void vkDestroyPipelineCache(VkDevice device, VkPipelineCache pipelineCache, const VkAllocationCallbacks* pAllocator)
	{
		return funcptr_vkDestroyPipelineCache(device, pipelineCache, pAllocator);
	}

	inline VkResult vkBindBufferMemory(VkDevice device, VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset)
	{
		return funcptr_vkBindBufferMemory(device, buffer, memory, memoryOffset);
	}

	inline VkResult vkCreateImage(VkDevice device, const VkImageCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImage* pImage)
	{
		return funcptr_vkCreateImage(device, pCreateInfo, pAllocator, pImage);
	}

	inline VkResult vkCreateBufferView(VkDevice device, const VkBufferViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBufferView* pView)
	{
		return funcptr_vkCreateBufferView(device, pCreateInfo, pAllocator, pView);
	}

	inline void vkDestroyBufferView(VkDevice device, VkBufferView bufferView, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyBufferView(device, bufferView, pAllocator);
	}

	inline void vkDestroyBuffer(VkDevice device, VkBuffer buffer, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkDestroyBuffer(device, buffer, pAllocator);
	}

	inline VkResult vkMapMemory(VkDevice device, VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size, VkMemoryMapFlags flags, void** ppData)
	{
		return funcptr_vkMapMemory(device, memory, offset, size, flags, ppData);
	}

	inline VkResult vkFlushMappedMemoryRanges(VkDevice device, uint32 memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges)
	{
		return funcptr_vkFlushMappedMemoryRanges(device, memoryRangeCount, pMemoryRanges);
	}

	inline void vkUnmapMemory(VkDevice device, VkDeviceMemory memory)
	{
		funcptr_vkUnmapMemory(device, memory);
	}

	inline void vkGetImageMemoryRequirements(VkDevice device, VkImage image, VkMemoryRequirements* pMemoryRequirements)
	{
		funcptr_vkGetImageMemoryRequirements(device, image, pMemoryRequirements);
	}

	inline VkResult vkBindImageMemory(VkDevice device, VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset)
	{
		return funcptr_vkBindImageMemory(device, image, memory, memoryOffset);
	}

	inline void vkFreeMemory(VkDevice device, VkDeviceMemory memory, const VkAllocationCallbacks* pAllocator)
	{
		funcptr_vkFreeMemory(device, memory, pAllocator);
	}
}
