#pragma once
#include "stdafx.h"


namespace ThroneEngine
{
	class Directory
	{
	public:
		/// <summary>
		/// Initializes a new instance of the Directory class.
		/// </summary>
		Directory();

		/// <summary>
		/// Initializes a new instance of the Directory class and create the file if not exist.
		/// </summary>
		/// <param name="directoryPath">The directory pathto create if not exist.</param>
		Directory(const std::string& directoryPath);

		/// <summary>
		/// Creates all directories recursively.
		/// </summary>
		/// <param name="directoryPath">The directory path to create.</param>
		/// <returns>true if completed.</returns>
		static bool createDirectory(const std::string& directoryPath);

		/// <summary>
		/// Sets the directory for usage and create it if not exist.
		/// </summary>
		/// <param name="directoryPath">The directory path to use and create if not exist.</param>
		void setDirectory(const std::string& directoryPath);

		/// <summary>
		/// Verify if the current directory exist.
		/// </summary>
		/// <returns>true if it exist.</returns>
		bool exist() const;

		/// <summary>
		/// Verify if the directory path exist.
		/// </summary>
		/// <param name="directoryPath">The directory path to check.</param>
		/// <returns>true if it exist.</returns>
		static bool exist(const std::string& directoryPath);

		/// <summary>
		/// Deletes the current directory and everything in it.
		/// </summary>
		/// <returns>true if deletion completed.</returns>
		bool deleteDirectory() const;

		/// <summary>
		/// Deletes the directory and everything in it.
		/// </summary>
		/// <param name="directoryPath">The directory path.</param>
		/// <returns>true if deletion completed.</returns>
		static bool deleteDirectory(const std::string& directoryPath);

		/// <summary>
		/// Gets all directories in the current directory.
		/// </summary>
		/// <returns>all directories in the current one.</returns>
		std::vector<std::string> getDirectories() const;

		/// <summary>
		/// Gets all directories in the current directory.
		/// </summary>
		/// <param name="result">return all directories in the current one.</param>
		void getDirectories(std::vector<std::string>& result) const;

		/// <summary>
		/// Gets all directories in the directory path.
		/// </summary>
		/// <param name="directoryPath">The directory path to check.</param>
		/// <returns>all directories in the directory path.</returns>
		static std::vector<std::string> getDirectories(const std::string& directoryPath);

		/// <summary>
		/// Gets all directories in the directory path.
		/// </summary>
		/// <param name="directoryPath">The directory path to check.</param>
		/// <param name="result">return all directories in the directory path.</param>
		static void getDirectories(const std::string& directoryPath, std::vector<std::string>& result);

		/// <summary>
		/// Gets all files in the current directory.
		/// </summary>
		/// <returns>all files in the current directory.</returns>
		std::vector<std::string> getFiles() const;

		/// <summary>
		/// Gets all files in the current directory.
		/// </summary>
		/// <param name="result">return all files in the current directory.</param>
		void getFiles(std::vector<std::string>& result) const;

		/// <summary>
		/// Gets all files in the directory path.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files.</param>
		/// <returns>all the files in the directory path.</returns>
		static std::vector<std::string> getFiles(const std::string& directoryPath);

		/// <summary>
		/// Gets all files in the directory path.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files.</param>
		/// <param name="result">return all the files in the directory path.</param>
		static void getFiles(const std::string& directoryPath, std::vector<std::string>& result);

		/// <summary>
		/// Gets all files with specific ext in current directory.
		/// </summary>
		/// <param name="extension">The extension to get the files.</param>
		/// <returns>all files with the specific extension.</returns>
		std::vector<std::string> getFilesWithExt(const std::string& extension) const;

		/// <summary>
		/// Gets all files with specific ext in current directory.
		/// </summary>
		/// <param name="extension">The extension to get the files.</param>
		/// <param name="result">return all files with the specific extension.</param>
		void getFilesWithExt(const std::string& extension, std::vector<std::string>& result) const;

		/// <summary>
		/// Gets all files with specific ext in the specified directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files.</param>
		/// <param name="extension">The extension to get the files.</param>
		/// <returns>all files with the specific extension.</returns>
		static std::vector<std::string> getFilesWithExt(const std::string& directoryPath, const std::string& extension);

		/// <summary>
		/// Gets all files with specific ext in the specified directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files.</param>
		/// <param name="extension">The extension to get the files.</param>
		/// <param name="result">return all files with the specific extension.</param>
		static void getFilesWithExt(const std::string& directoryPath, const std::string& extension, std::vector<std::string>& result);

		/// <summary>
		/// Gets all files and directories from the current directory.
		/// </summary>
		/// <returns>all files and directories from the current one.</returns>
		std::vector<std::string> getFileSystemEntries() const;

		/// <summary>
		/// Gets all files and directories from the current directory.
		/// </summary>
		/// <param name="result">return all files and directories.</param>
		void getFileSystemEntries(std::vector<std::string>& result) const;

		/// <summary>
		/// Gets all files and directories from the specified directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files and directories.</param>
		/// <returns>all the files and directories.</returns>
		static std::vector<std::string> getFileSystemEntries(const std::string& directoryPath);

		/// <summary>
		/// Gets all files and directories from the specified directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to get the files and directories.</param>
		/// <param name="result">return all the files and directories.</param>
		static void getFileSystemEntries(const std::string& directoryPath, std::vector<std::string>& result);

		/// <summary>
		/// Gets the current working directory.
		/// </summary>
		/// <returns>current working directory</returns>
		static std::string getCurrentDirectory();

		/// <summary>
		/// Moves the current directory to the specified new directory.
		/// </summary>
		/// <param name="newDirectory">The new directory to move to.</param>
		void move(const std::string& newDirectory) const;

		/// <summary>
		/// Moves the directory path to the specified new directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to move.</param>
		/// <param name="newDirectory">The new directory to move to.</param>
		static void move(const std::string& directoryPath, const std::string& newDirectory);

		/// <summary>
		/// Moves the directory path to the specified new directory.
		/// </summary>
		/// <param name="directory">The directory to move.</param>
		/// <param name="newDirectory">The new directory to move to.</param>
		static void move(Directory& directory, const std::string& newDirectory);

		/// <summary>
		/// Copies the current directory in the specified new directory.
		/// </summary>
		/// <param name="newDirectory">The new directory to copy to.</param>
		void copy(const std::string& newDirectory) const;

		/// <summary>
		/// Copies the directory path in the specified new directory.
		/// </summary>
		/// <param name="directoryPath">The directory path to copy.</param>
		/// <param name="newDirectory">The new directory to copy to.</param>
		static void copy(const std::string& directoryPath, const std::string& newDirectory);

		/// <summary>
		/// Copies the directory path in the specified new directory.
		/// </summary>
		/// <param name="directory">The directory to copy.</param>
		/// <param name="newDirectory">The new directory to copy to.</param>
		static void copy(Directory& directory, const std::string& newDirectory);

	private:
		std::string directory;
	};
}
