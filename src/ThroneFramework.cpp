﻿#pragma once

#include "stdafx.h"
#include "ThroneFramework.hpp"
#include "CommandBuffer.h"
#include "CommandQueue.h"
#include "Window.h"

using namespace R::S::ErrorMessage;
using namespace R::S::Path;
using namespace R::S::StandardOutput;
using namespace R::C;

using std::cout;

namespace ThroneEngine
{
	VkDebugReportCallbackEXT debugCallBack;
	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	VkDevice logicalDevice;
	Window* window;
	CommandQueue commandQueue;
	

	bool initializeThroneFramework()
	{

		if (initializeVulkanFunctions() == false)
		{
			return false;
		}

		std::vector<VkExtensionProperties> availableExtensions;

		if (checkAvailableExtensions(false, availableExtensions) == false)
		{
			return false;
		}
		std::vector<const char*> requiredExtensions;
		std::vector<const char*> requiredLayers;
		std::vector<const char*> unsupportedExtensions;


		uint32 layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		addWin32SurfaceExtensions(requiredExtensions); /////// WIN32
		

		if (enableValidationLayers == true)
		{
			requiredExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
			requiredLayers.push_back(path_lunarGStandardValidation.c_str());
			requiredLayers.push_back("VK_LAYER_LUNARG_assistant_layer");
		}	
		if (verifyRequiredExtensions(availableExtensions, requiredExtensions, unsupportedExtensions))
		{
			if (createVulkanInstance(requiredExtensions, requiredLayers, instance))
			{
				initializeInstanceLevelVulkanFunctions(instance, requiredExtensions);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		setupDebugCallBack(instance, debugCallBack);

		window->initialize();

		std::vector<VkPhysicalDevice> physicalDevices;
		if (enumeratePhysicalDevices(physicalDevices) == false)
		{
			return false;
		}

		std::vector<char const*> requiredDeviceExtensions;
		addSwapchainExtension(requiredDeviceExtensions); /////////// Swapchains
		std::vector<QueueInfo> queueInfos;
		VkPhysicalDeviceFeatures requiredFeatures;
		setFeaturesToFalse(requiredFeatures);

		requiredFeatures.samplerAnisotropy = VK_TRUE;
		requiredFeatures.sampleRateShading = VK_TRUE;


		physicalDevice = findSuitablePhysicalDevice(physicalDevices, requiredDeviceExtensions, requiredFeatures, queueInfos, window->surface);
		if (physicalDevice == VK_NULL_HANDLE)
		{
			return false;
		}

		if (createLogicalDevice(logicalDevice, physicalDevice, requiredDeviceExtensions, requiredLayers, queueInfos, requiredFeatures) == false)
		{
			return false;
		}

		initializeDeviceLevelVulkanFunctions(logicalDevice, requiredDeviceExtensions);


		vkGetDeviceQueue(logicalDevice, 0, 0, &commandQueue.handle);

		window->initSurface();
		window->initSwapChain();
		window->initSwapChainImages();

		return true;
	}

	inline void destroyThroneFramework()
	{
		waitForDevice(logicalDevice);

		// TODO : destroy the things that needs to be destroyed

		window->deInitSwapChainImages();
		window->deInitSwapChain();
		window->deInitSurface();


		if (logicalDevice != VK_NULL_HANDLE)
		{
			vkDestroyDevice(logicalDevice, nullptr);
			logicalDevice = VK_NULL_HANDLE;
		}
		if (instance != VK_NULL_HANDLE)
		{
			vkDestroyInstance(instance, nullptr);
			instance = VK_NULL_HANDLE;
		}
	}

	inline bool createVulkanInstance(std::vector<const char*>& requiredExtensions, std::vector<const char*>& requiredLayers, VkInstance& instance)
	{
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pNext = nullptr;
		appInfo.pApplicationName = sdo_windowName.data();
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = sdo_windowName.data();
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.enabledLayerCount = requiredLayers.size();
		createInfo.ppEnabledLayerNames = requiredLayers.size() > 0 ? &requiredLayers[0] : nullptr;
		createInfo.enabledExtensionCount = requiredExtensions.size();
		createInfo.ppEnabledExtensionNames = requiredExtensions.size() > 0 ? &requiredExtensions[0] : nullptr;

		VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS || instance == VK_NULL_HANDLE)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_vulkanInstance);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline void setupDebugCallBack(VkInstance instance, VkDebugReportCallbackEXT& debugCallBack)
	{
		if (enableValidationLayers == false)
		{
			return;
		}
		VkDebugReportCallbackCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		createInfo.pfnCallback = debugCallback;
		VkResult result = vkCreateDebugReportCallbackEXT(instance, &createInfo, nullptr, &debugCallBack);
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_debugCallback);
			#endif
		}
	}

	inline VkShaderModule createShaderModule(const std::vector<char>& code)
	{
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.flags = 0;
		createInfo.pNext = nullptr;
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = code.size();
		createInfo.pCode = reinterpret_cast<const uint32*>(code.data());

		VkShaderModule shaderModule;
		VkResult result = vkCreateShaderModule(logicalDevice, &createInfo, nullptr, &shaderModule);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_shaderModuleCreation);
			#endif
		}
		#endif

		return shaderModule;
	}

	inline bool checkAvailableExtensions(const bool mustOutputExtensions, std::vector<VkExtensionProperties>& availableExtensions)
	{
		uint32 extensionsCount;
		VkResult result = vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, nullptr);

		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS || extensionsCount == 0)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_extensionNumber);
			#endif
			return false;
		}
		#endif

		availableExtensions.resize(extensionsCount);
		result = vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, &availableExtensions[0]);

		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS || availableExtensions.size() == 0 || availableExtensions.size() != extensionsCount)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_extensionEnumeration);
			#endif
			return false;
		}
		#endif

		if (mustOutputExtensions == true)
		{
			enumerateExtensions(availableExtensions);
		}

		return true;
	}

	inline bool verifyRequiredExtensions(std::vector<VkExtensionProperties>& availableExtensions, std::vector<const char*>& requiredExtensions, std::vector<const char*>& unsupportedExtensions)
	{
		for (const char* reqExtension : requiredExtensions)
		{
			if (isExtensionSupported(availableExtensions, reqExtension) == false)
			{
				unsupportedExtensions.push_back(reqExtension);
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_unSupportedExtension + std::string(reqExtension));
				#endif
			}
		}
		if (unsupportedExtensions.size() > 0)
		{
			return false;
		}
		return true;
	}

	void enumerateExtensions(std::vector<VkExtensionProperties>& availableExtensions)
	{
		cout << sdo_extensionNumber << availableExtensions.size() << "\n";
		for (int i = 0; i < availableExtensions.size(); i++)
		{
			cout << sdo_extension << availableExtensions[i].extensionName << "\n";
		}
	}

	inline bool enumeratePhysicalDevices(std::vector<VkPhysicalDevice>& physicalDevices)
	{
		uint32 devicesCount;
		VkResult result = vkEnumeratePhysicalDevices(instance, &devicesCount, nullptr);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_physicalDevicesNumber);
			#endif
			return false;
		}
		#endif
		physicalDevices.resize(devicesCount);
		result = vkEnumeratePhysicalDevices(instance, &devicesCount, &physicalDevices[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_physicalDevicesEnumerate);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline VkPhysicalDevice findSuitablePhysicalDevice(
		std::vector<VkPhysicalDevice>& physicalDevices,
		std::vector<const char*>& requiredDeviceExtensions,
		VkPhysicalDeviceFeatures& requiredFeatures,
		std::vector<QueueInfo>& queueInfos,
		VkSurfaceKHR surface)
	{
		for (VkPhysicalDevice& device : physicalDevices)
		{
			std::vector<VkExtensionProperties> availableExtensions;
			if (enumerateAvailableExtensionsOnDevice(device, availableExtensions) == true)
			{
				if (verifyRequiredExtensions(availableExtensions, requiredDeviceExtensions, std::vector<const char*>()))
				{
					VkPhysicalDeviceFeatures features;
					vkGetPhysicalDeviceFeatures(device, &features);

					if (requiredFeaturesAreSupported(requiredFeatures, features) == true)
					{
						VkPhysicalDeviceProperties properties;
						vkGetPhysicalDeviceProperties(device, &properties);

						std::vector<VkQueueFamilyProperties> queueFamilies;
						if (enumerateDeviceQueueFamilies(device, queueFamilies) == true)
						{
							queueInfos.push_back(QueueInfo());
							queueInfos[0].familyIndex = 0; ////////////// HARDCODE
							queueInfos[0].priorities.push_back(1.0f);
							int presentationQueueIndex = findSuitableQueueFamilyForSwapchainPresentation(device, queueFamilies, surface);
							if (presentationQueueIndex != -1)
							{
								return device;
							}
						}
					}
				}
			}
		}
		#if ERROR_MESSAGE_NEEDED
		ERROR_MESSAGE(err_physicalDevicesSuitable);
		#endif
		return nullptr;
	}

	inline bool enumerateAvailableExtensionsOnDevice(VkPhysicalDevice& device, std::vector<VkExtensionProperties>& availableExtensions)
	{
		uint32 extensionCount;
		VkResult result = vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS || extensionCount <= 0)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_deviceExtensionNumber);
			#endif
			return false;
		}
		#endif

		availableExtensions.resize(extensionCount);
		result = vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, &availableExtensions[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS || device == VK_NULL_HANDLE)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_physicalDevice);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline bool requiredFeaturesAreSupported(VkPhysicalDeviceFeatures& requiredFeatures, VkPhysicalDeviceFeatures& availableFeatures)
	{
		int s = sizeof(VkPhysicalDeviceFeatures) / sizeof(uint32);
		unsigned int* requiredAdress = (uint32*)&requiredFeatures;
		unsigned int* availableAdress = (uint32*)&availableFeatures;
		for (int i = 0; i < s; i++)
		{
			uint32* requiredValue = requiredAdress + i;
			if (*requiredValue == 1)
			{
				uint32* availableValue = availableAdress + i;
				if (*availableValue != *requiredValue)
				{
					return false;
				}
			}
		}
		return true;
	}

	inline bool enumerateDeviceQueueFamilies(VkPhysicalDevice& device, std::vector<VkQueueFamilyProperties>& queueFamilies)
	{
		uint32 queueFamiliesCount;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamiliesCount, nullptr);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (queueFamiliesCount <= 0)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_queueFamilyNumber);
			#endif
			return false;
		}
		#endif
		queueFamilies.resize(queueFamiliesCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamiliesCount, &queueFamilies[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (queueFamilies.size() != queueFamiliesCount)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_queueFamily);
			#endif
			return false;
		}
		#endif

		return true;
	}

	inline int findSuitableQueueFamily(std::vector<VkQueueFamilyProperties>& queueFamilies, VkQueueFlags desiredCapabilities)
	{
		int size = queueFamilies.size();
		for (int i = 0; i < size; i++)
		{
			if (queueFamilies[i].queueCount > 0 && queueFamilies[i].queueFlags & desiredCapabilities)
			{
				return i;
			}
		}
		return -1;
	}

	inline bool createLogicalDevice(
		VkDevice& logicalDevice,
		VkPhysicalDevice& physicalDevice,
		std::vector<char const*>& requiredDeviceExtensions,
		std::vector<const char*>& requiredLayers,
		std::vector<QueueInfo>& queueInfos,
		VkPhysicalDeviceFeatures& requiredFeatures)
	{
		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		for (int i = 0; i < queueInfos.size(); i++)
		{
			VkDeviceQueueCreateInfo graphicsQueue = {};
			graphicsQueue.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			graphicsQueue.pNext = nullptr;
			graphicsQueue.flags = 0;
			graphicsQueue.queueFamilyIndex = queueInfos[i].familyIndex;
			graphicsQueue.pQueuePriorities = &queueInfos[i].priorities[0];
			graphicsQueue.queueCount = queueInfos[i].priorities.size();
			queueCreateInfos.push_back(graphicsQueue);
		}

		VkDeviceCreateInfo deviceCreateInfo = {};
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pNext = nullptr;
		deviceCreateInfo.flags = 0;
		deviceCreateInfo.queueCreateInfoCount = queueCreateInfos.size();
		deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.size() > 0 ? &queueCreateInfos[0] : nullptr;
		deviceCreateInfo.enabledLayerCount = requiredLayers.size();
		deviceCreateInfo.ppEnabledLayerNames = requiredLayers.size() > 0 ? &requiredLayers[0] : nullptr;
		deviceCreateInfo.enabledExtensionCount = requiredDeviceExtensions.size();
		deviceCreateInfo.ppEnabledExtensionNames = requiredDeviceExtensions.size() > 0 ? &requiredDeviceExtensions[0] : nullptr;
		deviceCreateInfo.pEnabledFeatures = &requiredFeatures;

		VkResult result = vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &logicalDevice);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_logicalDeviceCreation);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline int findSuitableQueueFamilyForSwapchainPresentation(VkPhysicalDevice physicalDevice, std::vector<VkQueueFamilyProperties>& queueFamilies, VkSurfaceKHR surface)
	{
		for (int i = 0; i < queueFamilies.size(); i++)
		{
			VkBool32 presentationAvailable;
			VkResult result = vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentationAvailable);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if defined ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_physicalDeviceSupportForQueue);
				#endif
			}
			#endif
			if (presentationAvailable == VK_TRUE)
			{
				return i;
			}
		}
		#if defined ERROR_MESSAGE_NEEDED
		ERROR_MESSAGE(err_queueFamilySupportForPresentation);
		#endif
		return -1;
	}

	inline bool getAvailablePresentationModes(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, std::vector<VkPresentModeKHR>& availablePresentModes)
	{
		uint32 presentationModeCount;
		VkResult result = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModeCount, nullptr);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_deviceSurfaceNumber);
			#endif
			return false;
		}
		#endif

		availablePresentModes.resize(presentationModeCount);
		result = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModeCount, &availablePresentModes[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_availablePresentationModes);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline bool verifyDesiredPresentationModeIsAvailable(std::vector<VkPresentModeKHR>& availablePresentModes, VkPresentModeKHR desiredPresentMode)
	{
		for (int i = 0; i < availablePresentModes.size(); i++)
		{
			if (availablePresentModes[i] == desiredPresentMode)
			{
				return true;
			}
		}
		return false;
	}

	inline bool getSurfaceCapabilities(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR& surfaceCapabilities)
	{
		VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_physicalDeviceCapabality);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline void setFeaturesToFalse(VkPhysicalDeviceFeatures& features)
	{
		int s = sizeof(VkPhysicalDeviceFeatures) / sizeof(uint32);
		uint32* address = (uint32*)&features;
		for (int i = 0; i < s; i++)
		{
			uint32* value = address + i;
			*value = VK_FALSE;
		}
	}

	//inline bool synchronizeCommandBuffers(
	//	VkQueue firstQueue,
	//	VkQueue secondQueue,
	//	WaitSemaphoreInfo& waitSemaphoresInfoFirstQueue,
	//	WaitSemaphoreInfo& synchronizingSemaphoresFirstQueue,
	//	std::vector<VkCommandBuffer>& commandBuffersFirstQueue,
	//	std::vector<VkCommandBuffer>& commandBuffersSecondQueue,
	//	std::vector<VkSemaphore>& synchronizingSemaphoresSecondQueue)
	//{
	//	if (submitCommandBufferToQueue(firstQueue, waitSemaphoresInfoFirstQueue, commandBuffersFirstQueue, synchronizingSemaphoresFirstQueue.semaphore, VK_NULL_HANDLE) == false)
	//	{
	//		#if ERROR_MESSAGE_NEEDED
	//		ERROR_MESSAGE(err_syncCommandBuffersFirst);
	//		#endif
	//		return false;
	//	}
	//	if (submitCommandBufferToQueue(secondQueue, synchronizingSemaphoresFirstQueue, commandBuffersSecondQueue, synchronizingSemaphoresSecondQueue, VK_NULL_HANDLE) == false)
	//	{
	//		#if ERROR_MESSAGE_NEEDED
	//		ERROR_MESSAGE(err_syncCommandBuffersSecond);
	//		#endif
	//		return false;
	//	}
	//	return true;
	//}

	inline bool waitForDevice(VkDevice logicalDevice)
	{
		VkResult result = vkDeviceWaitIdle(logicalDevice);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_deviceWaitIdle);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline void freeCommandBuffers(VkDevice logicalDevice, VkCommandPool commandPool, std::vector<VkCommandBuffer>& cmdBuffersBeginRenderPass)
	{
		vkFreeCommandBuffers(logicalDevice, commandPool, cmdBuffersBeginRenderPass.size(), &cmdBuffersBeginRenderPass[0]);
	}

	inline void copyDataFromBufferToImage(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkImage dstImage, VkImageLayout imageLayout, std::vector<VkBufferImageCopy>& regions)
	{
		commandBuffer.cmdCopyBufferToImage(srcBuffer, dstImage, imageLayout, regions.size(), &regions[0]);
	}

	inline void copyDataFromImageToBuffer(CommandBuffer& commandBuffer, VkImage srcImage, VkBuffer dstBuffer, VkImageLayout imageLayout, std::vector<VkBufferImageCopy>& regions)
	{
		commandBuffer.cmdCopyImageToBuffer(srcImage, imageLayout, dstBuffer, regions.size(), &regions[0]);
	}

	inline bool freeDescriptorSets(VkDevice logicalDevice, VkDescriptorPool descriptorPool, std::vector<VkDescriptorSet>& descriptorSets)
	{
		VkResult result = vkFreeDescriptorSets(logicalDevice, descriptorPool, descriptorSets.size(), &descriptorSets[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_freeDescriptorSets);
			#endif
			return false;
		}
		#endif

		return true;
	}

	inline bool resetDescriptorPool(VkDevice logicalDevice, VkDescriptorPool descriptorPool)
	{
		VkResult result = vkResetDescriptorPool(logicalDevice, descriptorPool, 0);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_resetDescriptorPool);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline void destroyDescriptorPool(VkDevice logicalDevice, VkDescriptorPool& descriptorPool)
	{
		vkDestroyDescriptorPool(logicalDevice, descriptorPool, nullptr);
		descriptorPool = VK_NULL_HANDLE;
	}

	inline void destroyDescriptorSetLayout(VkDevice logicalDevice, VkDescriptorSetLayout& descriptorSetLayout)
	{
		vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, nullptr);
		descriptorSetLayout = VK_NULL_HANDLE;
	}


	inline void specifyPipelineTesselationState(uint32 patchControlPointsCount, VkPipelineTessellationStateCreateInfo& tesselationStateCreateInfo)
	{
		tesselationStateCreateInfo = {};
		tesselationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
		tesselationStateCreateInfo.pNext = nullptr;
		tesselationStateCreateInfo.flags = 0;
		tesselationStateCreateInfo.patchControlPoints = patchControlPointsCount;
	}


	inline bool createComputePipeline(VkDevice logicalDevice, VkPipelineCreateFlags additionalOptions, VkPipelineShaderStageCreateInfo & computeShaderStage, VkPipelineLayout pipelineLayout, VkPipeline basePipelineHandle, VkPipelineCache pipelineCache, VkPipeline & computePipeline)
	{
		VkComputePipelineCreateInfo computePipelineCreateInfo = {};
		computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
		computePipelineCreateInfo.pNext = nullptr;
		computePipelineCreateInfo.flags = additionalOptions;
		computePipelineCreateInfo.stage = computeShaderStage;
		computePipelineCreateInfo.layout = pipelineLayout;
		computePipelineCreateInfo.basePipelineHandle = basePipelineHandle;
		computePipelineCreateInfo.basePipelineIndex = -1;

		VkResult result = vkCreateComputePipelines(logicalDevice, pipelineCache, 1, &computePipelineCreateInfo, nullptr, &computePipeline);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createComputePipeline);
			#endif
			return false;
		}
		#endif



		return true;
	}

	inline void bindPipelineObject(CommandBuffer& commandBuffer, VkPipelineBindPoint pipelineType, VkPipeline pipeline)
	{
		commandBuffer.cmdBindPipeline(pipelineType, pipeline);
	}


	inline void destroyPipelineCacheObject(VkDevice logicalDevice, VkPipelineCache& pipelineCacheObject)
	{
		if (pipelineCacheObject != VK_NULL_HANDLE)
		{
			vkDestroyPipelineCache(logicalDevice, pipelineCacheObject, nullptr);
			pipelineCacheObject = VK_NULL_HANDLE;
		}
	}

	inline void provideDataThroughPushConstants(CommandBuffer& commandBuffer, VkPipelineLayout pipelineLayout, VkShaderStageFlags pipelineStages, uint32 offset, uint32 size, void * data)
	{
		commandBuffer.cmdPushConstants(pipelineLayout, pipelineStages, offset, size, data);
	}


	inline VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64 obj, size_t location, int32 code, const char* layerPrefix, const char* msg, void* userData)
	{
		ERROR_MESSAGE_NO_FILE_PATH(err_validationLayer + std::string(msg));
		return VK_FALSE;
	}


	#pragma region Temporary

	#if defined VK_USE_PLATFORM_WIN32_KHR
	inline void addWin32SurfaceExtensions(std::vector<const char*>& requiredExtensions)
	{
		requiredExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
		requiredExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
	}
	#endif
	inline void addSwapchainExtension(std::vector<const char*>& requiredExtensions)
	{
		requiredExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	}
	#pragma endregion
}