#pragma once
#include "stdafx.h"

#include "UtilityStructures.h"
#include "Vertex.h"

namespace ThroneEngine
{
	class Color;
	class Transform;

	struct Mesh
	{
		std::vector<float> data;
		std::vector<MeshPart> parts;
		std::vector<vec2> points;

		uint32 vertexSizeInBytes;
		uint32 totalSizeInBytes;
		uint32 totalAmountOfVertex;

		void reset();

		void setSizeVertices(const vec3& newSize, const vec3& lastSize);

		void set2DPoints(Transform& transform);
		void compute2DConvexHull(std::vector<vec2>& points);

		void setColor(const Color& color);

		void setTotalSize();

		void setTotalAmountOfVertices();

		MeshPart getFullMeshPart();
	};
}
