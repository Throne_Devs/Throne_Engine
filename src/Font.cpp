#include "stdafx.h"

#include "Font.h"
#include "Glyph.h"
#include "ThroneFramework.h"
#include "Text.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include FT_BITMAP_H
#include FT_STROKER_H

namespace
{
	template<typename T, typename U>
	inline T reinterpret(const U& input)
	{
		T output;
		std::memcpy(&output, &input, sizeof(U));
		return output;
	}

	uint64 combine(const float outlineThickness, const bool bold, const uint32 codePoint)
	{
		return static_cast<uint64>(reinterpret<uint32>(outlineThickness)) << 32 | static_cast<uint64>(bold) << 31 | codePoint;
	}
}

namespace ThroneEngine
{
	Font::Font() :
		library(nullptr),
		face(nullptr),
		streamRec(nullptr),
		stroker(nullptr),
		refCount(nullptr),
		info()
	{
	}

	Font::Font(const Font& copy) :
		library(copy.library),
		face(copy.face),
		streamRec(copy.streamRec),
		stroker(copy.stroker),
		refCount(copy.refCount),
		info(copy.info),
		pages(copy.pages),
		pixelBuffer(copy.pixelBuffer)
	{
		if (refCount)
		{
			(*refCount)++;
		}
	}

	Font::~Font()
	{
		cleanup();
	}


	bool Font::loadFromFile(const std::string& filePath)
	{
		cleanup();
		refCount = new int(1);

		FT_Library library;
		if (FT_Init_FreeType(&library) != 0)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_fontLoadFailed + filePath + err_fontInitFreetype);
			#endif
			return false;
		}
		this->library = library;

		FT_Face face;
		if (FT_New_Face(static_cast<FT_Library>(this->library), filePath.c_str(), 0, &face) != 0)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_fontLoadFailed + filePath + err_fontCreateFace);
			#endif
			return false;
		}

		FT_Stroker stroker;
		if (FT_Stroker_New(static_cast<FT_Library>(library), &stroker) != 0)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_fontLoadFromFile);
			#endif
			FT_Stroker_Done(stroker);
			FT_Done_Face(face);
			return false;
		}
		this->stroker = stroker;
		this->face = face;

		info.family = face->family_name ? face->family_name : std::string();

		return true;
	}

	const Font::Info& Font::getInfo() const
	{
		return info;
	}

	const Glyph& Font::getGlyph(const uint32 codePoint, const uint32 characterSize, bool bold, float outlineThickness) const
	{
		GlyphTable& glyphs = pages[characterSize].glyphs;
		uint64 key = combine(outlineThickness, bold, codePoint);

		const GlyphTable::const_iterator it = glyphs.find(key);
		if (it != glyphs.end())
		{
			return it->second;
		}

		Glyph glyph = loadGlyph(codePoint, characterSize, bold, outlineThickness);
		return glyphs.insert(std::make_pair(key, glyph)).first->second;
	}

	float Font::getKerning(const uint32 first, const uint32 second, const uint32 characterSize) const
	{
		if (first == 0 || second == 0)
			return 0.f;

		FT_Face face = static_cast<FT_Face>(this->face);

		if (face && FT_HAS_KERNING(face) && setCurrentSize(characterSize))
		{
			const FT_UInt indexFirst = FT_Get_Char_Index(face, first);
			const FT_UInt indexSecond = FT_Get_Char_Index(face, second);

			FT_Vector kerning;
			FT_Get_Kerning(face, indexFirst, indexSecond, FT_KERNING_DEFAULT, &kerning);

			if (!FT_IS_SCALABLE(face))
			{
				return static_cast<float>(kerning.x);
			}
			return static_cast<float>(kerning.x) / LEFT_SHIFT;
		}
		return 0.f;
	}

	float Font::getLineSpacing(const uint32 characterSize) const
	{
		const FT_Face face = static_cast<FT_Face>(this->face);
		if (face && setCurrentSize(characterSize))
		{
			return static_cast<float>(face->size->metrics.height) / LEFT_SHIFT;
		}
		return 0.f;
	}

	float Font::getUnderLinePosition(const uint32 characterSize) const
	{
		const FT_Face face = static_cast<FT_Face>(this->face);
		if (face && setCurrentSize(characterSize))
		{
			if (!FT_IS_SCALABLE(face))
			{
				return characterSize / UNDERLINE_PADDING_DIVIDE;
			}
			return -static_cast<float>(FT_MulFix(face->underline_position, face->size->metrics.y_scale)) / LEFT_SHIFT;
		}
		return 0.f;
	}

	float Font::getUnderLineThickness(const uint32 characterSize) const
	{
		const FT_Face face = static_cast<FT_Face>(this->face);
		if (face && setCurrentSize(characterSize))
		{
			if (!FT_IS_SCALABLE(face))
			{
				return characterSize / UNDERLINE_THICKNESS_DIVIDE;
			}
			return static_cast<float>(FT_MulFix(face->underline_thickness, face->size->metrics.y_scale)) / LEFT_SHIFT;
		}
		return 0.f;
	}

	ImageSampler* Font::getTexture(const uint32 characterSize) const
	{
		return pages[characterSize].imageSampler;
	}

	PixelMap & Font::getPixelMap(const uint32 characterSize) const
	{
		return pages[characterSize].pixelMap;
	}

	bool Font::updateTexture(std::vector<Fence>& imageFences) const
	{
		
		std::map<uint32, Page>::iterator iterator = pages.begin();
		while (iterator != pages.end())
		{
			if (iterator->second.textureNeedUpdate == true)
			{
				for (int i = 0; i < imageFences.size(); i++)
				{
					imageFences[i].waitForFence(TIMEOUT_FENCE);
				}
				VkExtent3D imageDimensions = {};
				imageDimensions.width = iterator->second.pixelMap.dimensionsInPixel.x;
				imageDimensions.height = iterator->second.pixelMap.dimensionsInPixel.y;
				imageDimensions.depth = iterator->second.pixelMap.dimensionsInPixel.z;
				if (iterator->second.pixelMap.pixelMapResized == true)
				{
					iterator->second.imageSampler->image->destroy();
					if (iterator->second.imageSampler->image->initializeAs2DSampledImage(imageDimensions,
						VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT, VK_FORMAT_R8G8B8A8_UNORM, 1, VK_IMAGE_VIEW_TYPE_2D) == false)
					{
						return false;
					}
					for (int i = 0; i < iterator->second.textureResizeEvent.size(); i++)
					{
						iterator->second.textureResizeEvent[i]->material->descriptorSet->updateDescriptorSets();
					}
					iterator->second.pixelMap.pixelMapResized = false;
				}
				iterator->second.imageSampler->image->updateImageMemory(iterator->second.pixelMap.pixels, commandQueue, 0, imageDimensions, VK_IMAGE_ASPECT_COLOR_BIT);
				iterator->second.textureNeedUpdate = false;
			}
			iterator++;
		}
		return true;
		
	}

	void Font::registerTextureResizeEvent(Text* text, uint32 characterSize) const
	{
		pages[characterSize].textureResizeEvent.push_back(text);
	}

	void Font::removeTextureResizeEvent(Text* text, uint32 characterSize) const
	{
		std::vector<Text*>::iterator index = std::find(pages[characterSize].textureResizeEvent.begin(), pages[characterSize].textureResizeEvent.end(), text);
		if (index != pages[characterSize].textureResizeEvent.end())
		{
			pages[characterSize].textureResizeEvent.erase(index);
		}		
	}

	Font& Font::operator=(const Font& right)
	{
		Font temp(right);

		std::swap(library, temp.library);
		std::swap(face, temp.face);
		std::swap(streamRec, temp.streamRec);
		std::swap(stroker, temp.stroker);
		std::swap(refCount, temp.refCount);
		std::swap(info, temp.info);
		std::swap(pages, temp.pages);
		std::swap(pixelBuffer, temp.pixelBuffer);

		return *this;
	}

	void Font::cleanup()
	{
		if (refCount)
		{
			(*refCount)--;

			if (*refCount == 0)
			{
				delete refCount;
				if (stroker)
				{
					FT_Stroker_Done(static_cast<FT_Stroker>(stroker));
				}
				if (face)
				{
					FT_Done_Face(static_cast<FT_Face>(face));
				}
				if (streamRec)
				{
					delete static_cast<FT_StreamRec*>(streamRec);
				}
				if (library)
				{
					FT_Done_FreeType(static_cast<FT_Library>(library));
				}
			}
		}

		library = nullptr;
		face = nullptr;
		stroker = nullptr;
		streamRec = nullptr;
		refCount = nullptr;

		std::map<uint32, Page>::iterator iterator = pages.begin();
		while (iterator != pages.end())
		{
			iterator->second.imageSampler->destroy();
			iterator++;
		}
		pages.clear();

		std::vector<uint8>().swap(pixelBuffer);
	}

	Glyph Font::loadGlyph(const uint32 codePoint, const uint32 characterSize, bool bold, float outlineThickness) const
	{
		Glyph glyph;
		FT_Glyph glyphdesc;
		const FT_Face face = static_cast<FT_Face>(this->face);

		//Flags corresponding to code point
		FT_Int32 flags = FT_LOAD_TARGET_NORMAL | FT_LOAD_FORCE_AUTOHINT;
		if (outlineThickness != 0)
		{
			flags |= FT_LOAD_NO_BITMAP;
		}

		//Transform void* to FT_Face, Set character size, Load glyph, Retreive glyph
		if (!face || !setCurrentSize(characterSize) || FT_Load_Char(face, codePoint, flags) != 0 || FT_Get_Glyph(face->glyph, &glyphdesc) != 0)
		{
			return glyph;
		}

		const FT_Pos weight = LEFT_SHIFT;
		const bool outline = glyphdesc->format == FT_GLYPH_FORMAT_OUTLINE;

		//Apply bold (high quality)
		if (outline)
		{
			if (bold)
			{
				FT_OutlineGlyph outlineGlyph = (FT_OutlineGlyph)glyphdesc;
				FT_Outline_Embolden(&outlineGlyph->outline, weight);
			}

			if (outlineThickness)
			{
				const FT_Stroker stroker = static_cast<FT_Stroker>(this->stroker);
				FT_Stroker_Set(stroker, static_cast<FT_Fixed>(outlineThickness * LEFT_SHIFT), FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);
				FT_Glyph_Stroke(&glyphdesc, stroker, true);
			}
		}

		//Convert Glyph to bitmap
		FT_Glyph_To_Bitmap(&glyphdesc, FT_RENDER_MODE_NORMAL, 0, 1);
		FT_Bitmap& bitmap = reinterpret_cast<FT_BitmapGlyph>(glyphdesc)->bitmap;

		//Apply bold (low quality)
		if (!outline)
		{
			if (bold)
			{
				FT_Bitmap_Embolden(static_cast<FT_Library>(library), &bitmap, weight, weight);
			}

			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (outlineThickness != 0)
			{
				#if defined ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_fontOutlineFailed);
				#endif
			}
			#endif
		}

		//Advance offset off glyph
		glyph.advance = static_cast<float>(face->glyph->metrics.horiAdvance) / LEFT_SHIFT;
		if (bold)
		{
			glyph.advance += static_cast<float>(weight) / LEFT_SHIFT;
		}

		int width = bitmap.width;
		int height = bitmap.rows;

		if (width > 0 && height > 0)
		{
			const uint32 padding = PADDING;
			width += 2 * padding;
			height += 2 * padding;

			//Get glyphs page of good size
			Page& page = pages[characterSize];

			//find position of glyph in pixelMap
			glyph.textureRect = findGlyphRect(page, width, height);

			//pixelMap data position in center
			glyph.textureRect.left += padding;
			glyph.textureRect.top += padding;
			glyph.textureRect.width -= 2 * padding;
			glyph.textureRect.height -= 2 * padding;

			//Glyph bounding box
			glyph.bounds.left = static_cast<float>(face->glyph->metrics.horiBearingX) / LEFT_SHIFT;
			glyph.bounds.top = -static_cast<float>(face->glyph->metrics.horiBearingY) / LEFT_SHIFT;
			glyph.bounds.width = static_cast<float>(face->glyph->metrics.width) / LEFT_SHIFT + outlineThickness * 2;
			glyph.bounds.height = static_cast<float>(face->glyph->metrics.height) / LEFT_SHIFT + outlineThickness * 2;

			//Fill buffer with white and transparent pixels
			pixelBuffer.resize(width * height * BUFFER_DATA_WIDTH);

			uint8* current = &pixelBuffer[0];
			uint8* end = current + width * height * BUFFER_DATA_WIDTH;

			while (current != end)
			{
				//Makes current pixels white and transparent
				*current++ = 255;
				*current++ = 255;
				*current++ = 255;
				*current++ = 0;
			}

			//Extract glyph's pixels from bitmap
			const uint8* pixels = bitmap.buffer;
			if (bitmap.pixel_mode == FT_PIXEL_MODE_MONO)
			{
				//Pixels are 1 bit monochrome
				for (uint32 y = padding; y < height - padding; y++)
				{
					for (uint32 x = padding; x < width - padding; x++)
					{
						//Color channel remain white, only fill alpha canal
						const std::size_t index = x + y * width;

						//Makes pixel transparent or not
						pixelBuffer[index * BUFFER_DATA_WIDTH + 3] = ((pixels[(x - padding) / 8]) & (1 << (7 - ((x - padding) % 8)))) ? 255 : 0;
					}
					pixels += bitmap.pitch;
				}
			}
			else
			{
				//Pixels are 8 bit gray levels
				for (uint32 y = padding; y < height - padding; y++)
				{
					for (uint32 x = padding; x < width - padding; x++)
					{
						//Color channel remain white, only fill alpha canal
						const std::size_t index = x + y * width;
						pixelBuffer[index * BUFFER_DATA_WIDTH + 3] = pixels[x - padding];
					}
					pixels += bitmap.pitch;
				}
			}

			//Write pixel to pixelMap
			const uint32 x = glyph.textureRect.left - padding;
			const uint32 y = glyph.textureRect.top - padding;
			const uint32 w = glyph.textureRect.width + 2 * padding;
			const uint32 h = glyph.textureRect.height + 2 * padding;
			page.pixelMap.update(pixelBuffer, w, h, x, y);
			pages[characterSize].textureNeedUpdate = true;
			glyph.metrics = face->glyph->metrics;
		}

		FT_Done_Glyph(glyphdesc);


		
		return glyph;
	}

	IntRect Font::findGlyphRect(Page & page, const uint32 width, const uint32 height) const
	{
		Row* row = nullptr;
		const FT_Face face = static_cast<FT_Face>(this->face);

		if (!page.rows.empty())
		{
			row = &*page.rows.begin() + page.index;

			if (row->width + width > page.pixelMap.dimensionsInPixel.x)
			{
				page.pixelMap.resize(page.pixelMap.dimensionsInPixel.x + width + PIXEL_MAP_WIDTH_PADDING, page.pixelMap.dimensionsInPixel.y);
			}

			if (row->top + height > page.pixelMap.dimensionsInPixel.y)
			{
				page.pixelMap.resize(page.pixelMap.dimensionsInPixel.x, page.pixelMap.dimensionsInPixel.y + height + PIXEL_MAP_WIDTH_PADDING);
			}
		}
		else
		{
			for (int i = 0; i < NUMBER_ROWS; i++)
			{
				page.rows.emplace_back(page.nextRow);
				page.nextRow += face->size->metrics.y_ppem;			
			}			

			row = &page.rows.front();

			page.pixelMap.initialize(width, height);
		}

		page.index++;
		if (page.rows.size() <= page.index)
			page.index = 0;

		const IntRect rect(row->width, row->top, width, height);
		row->width += width;
		return rect;
	}

	bool Font::setCurrentSize(const uint32 characterSize) const
	{
		const FT_Face face = static_cast<FT_Face>(this->face);
		const FT_UShort currentSize = face->size->metrics.x_ppem;

		if (currentSize != characterSize)
		{
			const FT_Error result = FT_Set_Pixel_Sizes(face, 0, characterSize);
			if (result == FT_Err_Invalid_Pixel_Size)
			{
				#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
				if (!FT_IS_SCALABLE(face))
				{
					#if defined ERROR_MESSAGE_NEEDED
					ERROR_MESSAGE(err_fontSizeSetBitmap.c_str() + characterSize);
					for (int i = 0; i < face->num_fixed_sizes; i++)
					{
						ERROR_MESSAGE(err_fontSizeAvailable.c_str() + face->available_sizes[i].height);
					}
					#endif			
				}
				#endif
			}
			return result == FT_Err_Ok;
		}
		return true;
	}

	Font::Page::Page() :
		nextRow(0)
	{
		pixelMap.initialize(1, 1, Color(255, 255, 255, 0));
		imageSampler = new ImageSampler();
		imageSampler->initialize(VkExtent3D{1, 1, 1}, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT, VK_FORMAT_R8G8B8A8_UNORM, 1, VK_IMAGE_VIEW_TYPE_2D);
	}
}
