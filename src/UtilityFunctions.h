#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	namespace Utility
	{
		THRONE_API_ENTRY float radianToDegree(const float radian);
		THRONE_API_ENTRY float degreeToRadian(const float degree);
		THRONE_API_ENTRY float get2DPerlinNoiseValue(float x, float y, float res);

		template<class T>
		THRONE_API_ENTRY inline T* getVectorDataPointer(std::vector<T>& vector);

		THRONE_API_ENTRY glm::vec4 normalize(glm::vec4&  vec);

		template<class T>
		THRONE_API_ENTRY inline T computeCrossXVec3(T v1y, T v1z, T v2y, T v2z);
		template<class T>
		THRONE_API_ENTRY inline T computeCrossYVec3(T v1x, T v1z, T v2x, T v2z);
		template<class T>
		THRONE_API_ENTRY inline T computeCrossZVec3(T v1x, T v1y, T v2x, T v2y);

		THRONE_API_ENTRY void crossProductVec3(glm::vec3& v1, glm::vec3& v2, glm::vec3& finalVector);
		THRONE_API_ENTRY void printVec3(std::string& message, glm::vec3& vec);
		THRONE_API_ENTRY void printVec3(char message[], glm::vec3& vec);
		THRONE_API_ENTRY void printVec4(std::string& message, glm::vec4& vec, bool printFourthComponent);

		THRONE_API_ENTRY bool pointIn2DConvexShape(const glm::vec2 pointToCheck, const std::vector<glm::vec2>& points);
		THRONE_API_ENTRY bool pointIn2DShape(const glm::vec2& pointToCheck, const std::vector<glm::vec2>& points);
		THRONE_API_ENTRY float cross(const glm::vec2& original, const glm::vec2& first, const glm::vec2& second);

		THRONE_API_ENTRY glm::vec2 coordsToPixels(const glm::vec2& coord);
		THRONE_API_ENTRY glm::vec2 pixelsToCoords(const glm::vec2& pixels);

		THRONE_API_ENTRY glm::vec2 pixelsToCoordsScale(const glm::vec2& pixels);
		THRONE_API_ENTRY glm::vec2 coordsToPixelsScale(const glm::vec2& coords);
	}
}

