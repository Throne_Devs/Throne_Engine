#pragma once
#include "stdafx.h"

#include "Transform.h"

namespace ThroneEngine
{
	class Graphic;

	class RectTransform : public Transform
	{
	public:
		RectTransform();
		RectTransform(const RectTransform& rectTransform);
		RectTransform(const vec2& position, const vec2& size);
		RectTransform(const float x, const float y, const float w, const float h);
		~RectTransform();

		void setPosition(const float x, const float y);
		void setPosition(const vec2& newPosition);

		void setOrigin(const float x, const float y);
		void setOrigin(const vec2& newOrigin);

		void setScale(const float x, const float y);
		void setScale(const vec2& newScale);

		void setAnchor(const float x, const float y);
		void setAnchor(const vec2& newAnchor);

		void setSize(const float w, const float h);
		void setSize(const vec2& newSize);

		void setRect(const float x, const float y, const float w, const float h);
		void setRect(const FloatRect& newRect);

		void setRotation(const float x, const float y, const float z);
		void setRotation(const vec3& newRotation);

		bool contains(const vec2& point);
		bool intersects(const FloatRect& rectTransform);
		bool intersects(const RectTransform& rectTransform);

		FloatRect getLocalBound() const;
		FloatRect getGlobalBound();

		Graphic* graphic = nullptr;

	private:		
		FloatRect rect;
		vec2 origin; //Normalized 0 -> 1 within the scale of the object
		vec2 anchor; //Normalized 0 -> 1 within the scale of the window
		vec2 scale;
		vec3 rotation;
		vec3 lastRotation;

		void setTransform();
		void setTransformPosition();
		void setTransformScale();
		void setTransformRotation();
		void updateTransform() const;

		vec2 positionInPixels() const;
		vec2 sizeInPixels() const;
		vec2 originInPixels() const;
		vec2 anchorInPixels() const;
	};
}
