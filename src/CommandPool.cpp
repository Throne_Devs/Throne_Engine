#include "stdafx.h"

#include "CommandPool.h"
#include "CommandBuffer.h"
#include "CommandQueue.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	CommandPool::CommandPool()
	{
	}


	CommandPool::~CommandPool()
	{
	}

	bool CommandPool::initialize(CommandQueue& queue, VkCommandPoolCreateFlags parameters)
	{
		return createCommandPool(queue, parameters);
	}

	bool CommandPool::initialize(CommandQueue& queue, VkCommandPoolCreateFlags parameters, int numberOfCommandBuffersToCreate, VkCommandBufferLevel commandBufferLevel)
	{
		bool result = createCommandPool(queue, parameters);
		if (result == false)
		{
			return false;
		}
		bool result2 = true;
		if (numberOfCommandBuffersToCreate > 0)
		{
			result2 = addCommandBuffers(numberOfCommandBuffersToCreate, commandBufferLevel);
		}
		return result && result2;
	}

	void CommandPool::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyCommandPool(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}

	bool CommandPool::reset(VkCommandPoolResetFlags releaseResources)
	{
		VkResult result = vkResetCommandPool(logicalDevice, handle, releaseResources);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_resetCommandBuffer);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool CommandPool::addCommandBuffers(int numberOfCommandBuffers, VkCommandBufferLevel commandBufferLevel)
	{
		int beginIndex = commandBuffers.size() > 0 ? commandBuffers.size() - 1 : 0;
		commandBuffers.resize(commandBuffers.size() + numberOfCommandBuffers);
		return CommandBuffer::createCommandBuffers(handle, commandBufferLevel, commandBuffers, beginIndex, numberOfCommandBuffers);
	}

	bool CommandPool::createCommandPool(CommandQueue& queue, VkCommandPoolCreateFlags parameters)
	{
		VkCommandPoolCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = parameters;
		createInfo.queueFamilyIndex = queue.familyIndex;

		VkResult result = vkCreateCommandPool(logicalDevice, &createInfo, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if defined ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_commandPoolCreation);
			#endif
			return false;
		}
		#endif
		return true;
	}
}