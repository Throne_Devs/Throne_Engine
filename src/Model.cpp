#include "stdafx.h"

#include "Model.h"
#include "OBJModel.h"

namespace ThroneEngine
{
	bool Model::loadModel(const std::string& filename, Mesh* mesh, const bool loadNormals, const bool loadTexCoords, const bool loadColor)
	{
		const std::size_t dot = filename.find_last_of('.');
		const std::string extension = filename.substr(dot + 1, filename.size() - dot);

		if (extension == "obj")
		{
			return OBJModel::loadModel(filename.c_str(), mesh, loadNormals, loadTexCoords, loadColor);
		}

		return false;
	}
}
