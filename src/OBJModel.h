#pragma once
#include "stdafx.h"

#include "Model.h"

namespace ThroneEngine
{
	class OBJModel
	{
	public:
		static bool loadModel(const char* filename, Mesh* mesh, const bool loadNormals, const bool loadTexCoords, const bool loadColor);
	};
}
