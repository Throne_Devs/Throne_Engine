#pragma once
#include "stdafx.h"

#include "Mesh.h"
#include "VertexBuffer.h"
#include "CommandQueue.h"
#include "Bindable.h"

namespace ThroneEngine
{
	class MeshRenderer : public Bindable
	{
	public:
		MeshRenderer();
		~MeshRenderer();

		Mesh* mesh;
		VertexBuffer vertexBuffer;


		bool initialize(Mesh* mesh, CommandQueue& commandQueue, uint32 bindingIndex, VkMemoryPropertyFlags memoryProperty);
		bool initialize(Mesh* mesh, CommandQueue& commandQueue, CommandBuffer& stagingCommandBuffer, uint32 bindingIndex, VkMemoryPropertyFlags memoryProperty);
		void updateMeshData(float* data, uint32 totalSizeInBytes);
		bool updateBufferData(CommandQueue& commandQueue, std::vector<Fence>& waitFences);
		bool updateBufferDataWithDeviceLocal(CommandQueue& commandQueue, CommandBuffer& stagingCommandBuffers);
		bool updateBufferDataWithHostVisible(CommandQueue& commandQueue);

		virtual void bind(BindableBindingInfo& bindingInfo) override;

	private:
		bool initializeBase(Mesh* mesh, VkMemoryPropertyFlags memoryProperty, uint32 bindingIndex);
	};

	inline void MeshRenderer::bind(BindableBindingInfo& bindingInfo)
	{
		vertexBuffer.bind(bindingInfo);
	}
}