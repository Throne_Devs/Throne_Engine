#include "stdafx.h"

#include "Renderer.h"
#include "Mesh.h"
#include "RenderPass.h"

namespace ThroneEngine
{
	Renderer::Renderer()
	{
	}


	Renderer::~Renderer()
	{
	}

	void Renderer::initializeTree(RenderPass& renderPass)
	{
		bindingTree.initializeTree(renderedObjects, renderPass);
	}

	void Renderer::recordCommands(CommandQueue& commandQueue, RenderPass& renderPass, FrameBuffer& frameBuffer, bool recreateTreeMap, CommandBuffer& commandBuffer)
	{
		if (recreateTreeMap == true)
		{
			initializeTree(renderPass);
		}

		commandBuffer.beginRecordingOnSecondaryCommandBuffer(
			VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, renderPass, 0, frameBuffer, false, 0, 0);

		bindingTree.recordCommands(commandBuffer);

		commandBuffer.endRecording();
	}
}

