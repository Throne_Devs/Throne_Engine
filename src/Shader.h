#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class Shader
	{
	public:
		Shader();
		~Shader();

		static std::string dafaultEntryPointName;

		VkShaderStageFlagBits shaderStage;
		VkShaderModule shaderModuleHandle = VK_NULL_HANDLE;
		std::string entryPointName;

		bool initialize(std::string shaderFilePath, VkShaderStageFlagBits shaderStage);
		bool initialize(std::string shaderFilePath, VkShaderStageFlagBits shaderStage, std::string entryPointName);
		void destroy();

		void getShaderStageCreateInfo(VkPipelineShaderStageCreateInfo& shaderStageCreateInfo);

		static bool createShaderModule(std::string& shaderFilePath, VkShaderModule& shaderModuleHandle);

	};
}



