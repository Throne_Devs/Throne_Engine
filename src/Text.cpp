#include "stdafx.h"

#include "Text.h"
#include "Vertex.h"
#include "Window.h"
#include "RenderPass.h"

namespace ThroneEngine
{
	Text::Text()
	{
	}


	Text::~Text()
	{
	}

	void Text::initializeMaterial(Font* font, uint32 characterSize, const std::wstring& textString, bool staticTextString)
	{
		this->font = font;
		this->characterSize = characterSize;
		this->textString = textString;
		this->staticTextString = staticTextString;

		material = new Material();
		material->initialize(font->getTexture(characterSize), 0, 0, 0);

		font->registerTextureResizeEvent(this, characterSize);
	}

	void Text::initializeMesh(RenderPass& renderPass, GraphicsPipeline* graphicsPipeline, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex, tvec3<byte>& textColor, vec3& position, vec3& rotation, vec3& scale)
	{
		this->textColor = textColor;

		Mesh* mesh = new Mesh();
		buildString(mesh);

		MeshRenderer* meshRenderer = new MeshRenderer();
		meshRenderer->initialize(mesh, commandQueue, meshRendererBindingIndex,
			staticTextString ? VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT : VkMemoryPropertyFlagBits(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT));

		textObject.initialize(meshRenderer, material, graphicsPipeline, &renderPass, modelMatrixBindingIndex, position, rotation, scale);
	}

	bool Text::setTextString(std::wstring& textString, CommandQueue& commandQueue, RenderPass& renderPass, std::vector<Fence>& waitFences)
	{
		if (staticTextString == false)
		{
			this->textString = textString;
			buildString(textObject.meshRenderer->mesh);
			if (textObject.meshRenderer->mesh->totalSizeInBytes >= 0)
			{
				textObject.meshRenderer->updateBufferData(commandQueue, waitFences);
				try
				{
					textObject.getRenderPassInfoByRenderPassHandle(renderPass.handle).renderedObjectParts[0].meshPart = textObject.meshRenderer->mesh->parts[0];
				}
				catch (const std::exception& e)
				{
					#if ERROR_MESSAGE_NEEDED
					ERROR_MESSAGE(e.what());
					#endif
					return false;
				}				
			}
			return true;
		}
		else
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_triedToUpdateStaticText);
			#endif
			return false;
		}
	}

	std::wstring& Text::getTextString()
	{
		return textString;
	}

	void Text::buildString(Mesh* mesh)
	{
		std::vector<float> data;
		int numberOfLetters = 0;
		float xVertexIndex = 0;
		float yVertexIndex = 0;
		float padding = 0.0f;
		float constantPadding = 2.0f;
		float spacing = font->getGlyph(L' ', characterSize, false).advance;
		float lineSpacing = font->getLineSpacing(characterSize);
		for (int i = 0; i < textString.size(); i++)
		{

			switch (textString[i])
			{
			case L' ':
			{
				padding = i - 1 > 0 ? font->getKerning(textString[i - 1], textString[i], characterSize) : 0;
				padding += constantPadding;
				xVertexIndex += spacing + padding;
				break;
			}
			case L'\n':
			{
				yVertexIndex -= lineSpacing;
				xVertexIndex = 0;
				break;
			}
			case L'\t':
				padding += constantPadding;
				padding *= 4;
				xVertexIndex += spacing + padding;
				break;
			default:
			{
				padding = i - 1 > 0 ? font->getKerning(textString[i - 1], textString[i], characterSize) : 0;
				padding += constantPadding;
				addLetter(const_cast<Glyph&>(font->getGlyph(textString[i], characterSize, false)), data, &xVertexIndex, yVertexIndex, padding);
				numberOfLetters++;
				break;
			}
			}
		}

		MeshPart part = {};
		part.vertexCount = numberOfLetters * 6;
		part.vertexOffset = 0;
		mesh->parts.clear();
		mesh->parts.push_back(part);
		mesh->vertexSizeInBytes = sizeof(VertexWithUVAndColor);
		mesh->setTotalAmountOfVertices();
		mesh->setTotalSize();
		mesh->data = data;
	}

	void Text::addLetter(Glyph& glyph, std::vector<float>& data, float* xVertexCoordinate, float yVertexCoordinate, float padding)
	{
		uint32 index = data.size();
		data.resize(data.size() + 48);

		float normalizedXCoord = *xVertexCoordinate / (window->swapchainExtent.width / 2.0f);
		float normalizedYCoord = yVertexCoordinate / (window->swapchainExtent.height / 2.0f);

		float normalizedGlyphWidth = glyph.bounds.width / (window->swapchainExtent.width / 2.0f);
		float normalizedGlyphHeight = glyph.bounds.height / (window->swapchainExtent.height / 2.0f);

		float normalizedGlyphBearingY = (glyph.metrics.horiBearingY / static_cast<float>(1 << 6)) / (window->swapchainExtent.height / 2.0f);
		float normalizedGlyphHeighMinusBearingY = ((glyph.metrics.height - glyph.metrics.horiBearingY) / static_cast<float>(1 << 6)) / (window->swapchainExtent.height / 2.0f);

		//V1
		data[index + 0] = normalizedXCoord;
		data[index + 1] = normalizedYCoord - normalizedGlyphHeighMinusBearingY;
		data[index + 2] = 0;
		data[index + 3] = glyph.textureRect.left;
		data[index + 4] = glyph.textureRect.top + glyph.textureRect.height;
		data[index + 5] = textColor.r;
		data[index + 6] = textColor.g;
		data[index + 7] = textColor.b;

		// V2
		data[index + 8] = normalizedXCoord + normalizedGlyphWidth;
		data[index + 9] = normalizedYCoord - normalizedGlyphHeighMinusBearingY;
		data[index + 10] = 0;
		data[index + 11] = glyph.textureRect.left + glyph.textureRect.width;
		data[index + 12] = glyph.textureRect.top + glyph.textureRect.height;
		data[index + 13] = textColor.r;
		data[index + 14] = textColor.g;
		data[index + 15] = textColor.b;

		// V3
		data[index + 16] = normalizedXCoord;
		data[index + 17] = normalizedYCoord + normalizedGlyphBearingY;
		data[index + 18] = 0;
		data[index + 19] = glyph.textureRect.left;
		data[index + 20] = glyph.textureRect.top;
		data[index + 21] = textColor.r;
		data[index + 22] = textColor.g;
		data[index + 23] = textColor.b;

		// V2
		data[index + 24] = normalizedXCoord + normalizedGlyphWidth;
		data[index + 25] = normalizedYCoord - normalizedGlyphHeighMinusBearingY;
		data[index + 26] = 0;
		data[index + 27] = glyph.textureRect.left + glyph.textureRect.width;
		data[index + 28] = glyph.textureRect.top + glyph.textureRect.height;
		data[index + 29] = textColor.r;
		data[index + 30] = textColor.g;
		data[index + 31] = textColor.b;

		// V4
		data[index + 32] = normalizedXCoord + normalizedGlyphWidth;
		data[index + 33] = normalizedYCoord + normalizedGlyphBearingY;
		data[index + 34] = 0;
		data[index + 35] = glyph.textureRect.left + glyph.textureRect.width;
		data[index + 36] = glyph.textureRect.top;
		data[index + 37] = textColor.r;
		data[index + 38] = textColor.g;
		data[index + 39] = textColor.b;

		// V3
		data[index + 40] = normalizedXCoord;
		data[index + 41] = normalizedYCoord + normalizedGlyphBearingY;
		data[index + 42] = 0;
		data[index + 43] = glyph.textureRect.left;
		data[index + 44] = glyph.textureRect.top;
		data[index + 45] = textColor.r;
		data[index + 46] = textColor.g;
		data[index + 47] = textColor.b;

		*xVertexCoordinate += glyph.bounds.width + padding;
	}
}