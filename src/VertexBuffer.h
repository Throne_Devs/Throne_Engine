#pragma once
#include "stdafx.h"

#include "GpuBuffer.h"
#include "Bindable.h"


namespace ThroneEngine
{

	class VertexBuffer : public GpuBuffer, public Bindable
	{
	public:
		VertexBuffer();
		~VertexBuffer();

		bool initialize(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage, uint32 bindingIndex);

		uint32 bindingIndex;
		
		virtual void bind(BindableBindingInfo& bindingInfo) override;
	};

	inline void VertexBuffer::bind(BindableBindingInfo& bindingInfo)
	{
		VkDeviceSize offsets[] = { 0 };
		bindingInfo.commandBuffer->cmdBindVertexBuffers(bindingIndex, 1, &handle, offsets);
	}

}