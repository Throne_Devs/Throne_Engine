#include "stdafx.h"

#include "File.h"

using namespace ThroneEngine;

File::File() = default;

File::File(const std::string& filePath, const bool create, const std::string& text)
{
	setFilePath(filePath, create, text);
}

File::~File()
{
	close();
}

bool File::createFile(const std::string& filePath, const std::string& text)
{
	writeAllLines(*const_cast<std::string*>(&text));
	return exist(filePath);
}


bool File::setFilePath(const std::string& filePath, const bool create, const std::string& text)
{
	this->filePath = filePath;
	if (!exist() && create)
	{
		return createFile(filePath, text);
	}
	return true;
}

std::string File::getFilePath() const
{
	return filePath;
}

void File::openRead(const std::string& filePath)
{
	open(filePath, std::ios::in);
}

void File::openReadBinary(const std::string& filePath)
{
	open(filePath, std::ios::in | std::ios::binary);
}

void File::openWriteBinary(const std::string& filePath)
{
	open(filePath, std::ios::out | std::ios::binary);
}

void File::openWrite(const std::string& filePath)
{
	open(filePath, std::ios::out);
}

bool File::exist()
{
	openRead(filePath);
	const bool exist = fstream.is_open();
	close();
	return exist;
}

bool File::exist(const std::string& filePath)
{
	File file(filePath, false);
	return file.exist();
}

bool File::deleteFile() const
{
	return remove(filePath.c_str());
}

bool File::deleteFile(const std::string& filePath)
{
	return remove(filePath.c_str());;
}

void File::appendAllText(const std::string& text)
{
	open(filePath, std::ios::app);
	fstream << "\n" + text;
	close();
}

void File::appendAllText(const std::string& filePath, const std::string& text)
{
	File f(filePath);
	f.appendAllText(text);
}

void File::appendAllLines(const std::vector<std::string>& allLines)
{
	std::string allText;
	for (int i = 0; i < allLines.size(); i++)
	{
		allText += allLines[i] + "\n";
	}
	appendAllText(allText);
}

void File::appendAllLines(const std::string& filePath, const std::vector<std::string>& allLines)
{
	File f(filePath);
	f.appendAllLines(allLines);
}

void File::appendAllBytes(const std::vector<char>& bytes)
{
	open(filePath, std::ios::app | std::ios::binary);
	fstream << bytes.data();
	close();
}

void File::appendAllBytes(const std::string& filePath, const std::vector<char>& bytes)
{
	File f(filePath);
	return f.appendAllBytes(bytes);
}

void File::writeAllLines(std::string& text)
{
	openWrite(filePath);
	fstream << text;
	close();
}

void File::writeAllLines(std::vector<std::string>& text)
{
	openWrite(filePath);
	for (int i = 0; i < text.size(); i++)
	{
		fstream << text[i] + "\n";
	}
	close();
}

void File::writeAllLines(const std::string& filePath, const std::string& text)
{
	File f(filePath);
	f.writeAllLines(*const_cast<std::string*>(&text));
}

void File::writeAllLines(const std::string& filePath, const std::vector<std::string>& text)
{
	File f(filePath);
	f.writeAllLines(*const_cast<std::vector<std::string>*>(&text));
}

std::vector<std::string> File::readLines()
{
	std::vector<std::string> lines;
	lines.reserve(size());
	openRead(filePath);
	readLines(lines);
	close();
	return lines;
}

void File::readLines(std::vector<std::string>& lines)
{
	openRead(filePath);
	std::string line;
	while (getline(fstream, line))
	{
		lines.push_back(line);
	}
	close();
}

std::vector<std::string> File::readLines(const std::string& filePath)
{
	File f(filePath);
	return f.readLines();
}

void File::readLines(const std::string& filePath, std::vector<std::string>& lines)
{
	File f(filePath);
	f.readLines(lines);
}

std::vector<char> File::readBinary()
{
	const size_t sizea = size();
	openReadBinary(filePath);

	std::vector<char> bytes;
	bytes.resize(sizea);
	fstream.read(bytes.data(), sizea);
	fstream.close();
	
	return bytes;
}

void File::readBinary(std::vector<char>& lines)
{
	lines = readBinary();
}

std::vector<char> File::readBinary(const std::string& filePath)
{
	File f(filePath);
	return f.readBinary();
}

void File::readBinary(const std::string& filePath, std::vector<char>& lines)
{
	File f(filePath);
	lines = f.readBinary();
}

void File::readAllText(std::string& text)
{
	openRead(filePath);
	std::string line;
	while (getline(fstream, line))
	{
		text += line + "\n";
	}
	close();
}

std::string File::readAllText(const std::string& filePath)
{
	File f(filePath);
	return f.readAllText();
}

void File::readAllText(const std::string& filePath, std::string& text)
{
	File f(filePath);
	f.readAllText(text);
}

std::string File::readAllText()
{
	openRead(filePath);
	std::string allLines;
	readAllText(allLines);
	close();
	return allLines;
}

void File::replace(const std::string& substitute)
{
	File substitutefile(substitute);
	writeAllLines(substitutefile.readAllText());
}

void File::replace(File& substitute)
{
	writeAllLines(substitute.readAllText());
}

void File::replace(const std::string& origin, const std::string& substitute)
{
	File originfile(origin);
	File substitutefile(substitute);
	replace(originfile, substitutefile);
}

void File::replace(File& origin, File& substitute)
{
	origin.replace(substitute);
}

void File::move(const std::string& newDirectory) const
{
	const int last = filePath.find_last_of('/');
	const std::string newFile = newDirectory + filePath.substr(last, filePath.size() - last);
	replace(newFile, filePath);
	deleteFile();
}

void File::move(const std::string& origin, const std::string& newDirectory)
{
	File originFile(origin);
	originFile.move(newDirectory);
}

void File::move(File& origin, const std::string& newDirectory)
{
	origin.move(newDirectory);
}

void File::copy(const std::string& newDirectory) const
{
	const int last = filePath.find_last_of('/');
	const std::string newFile = newDirectory + filePath.substr(last, filePath.size() - last);
	replace(newFile, filePath);
}

void File::copy(const std::string& origin, const std::string& newDirectory)
{
	File originfile(origin);
	originfile.copy(newDirectory);
}

void File::copy(File& origin, const std::string& newDirectory)
{
	origin.copy(newDirectory);
}

size_t File::size()
{
	openRead(filePath);
	fstream.seekg(0, std::ios::end);
	const size_t fileSize = static_cast<size_t>(fstream.tellg());
	close();
	return fileSize;
}

size_t File::size(const std::string& filePath)
{
	File f(filePath);
	return f.size();
}

void File::open(const std::string& filePath, const std::ios_base::openmode mode)
{
	fstream.open(filePath, mode);
}

void File::close()
{
	fstream.close();
}
