#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"
#include "GpuBuffer.h"
#include "CommandQueue.h"
#include "SemaphorePool.h"

namespace ThroneEngine
{
	class Image
	{
	public:
		Image();
		~Image();

		VkExtent3D dimensions;
		VkImage handle = VK_NULL_HANDLE;
		std::vector<VkImageView> imageViewHandles;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		bool initialize(
			VkImageType type,
			VkFormat format,
			VkExtent3D dimensions,
			uint32 numMipmaps,
			uint32 numLayers,
			VkSampleCountFlagBits samples,
			VkImageUsageFlags usageScenarios,
			bool isCubemap);
		bool initialize(
			VkImageType type,
			VkFormat format,
			VkExtent3D dimensions,
			uint32 numMipmaps,
			uint32 numLayers,
			VkSampleCountFlagBits samples,
			VkImageUsageFlags usageScenarios,
			bool isCubemap,
			VkImageLayout imageLayout);
		bool initializeAs2DImageWithView(
			VkFormat format,
			VkExtent3D dimensions,
			uint32 numMipmaps,
			uint32 numLayers,
			VkSampleCountFlagBits samples,
			VkImageUsageFlags usage,
			VkImageAspectFlags aspect);
		bool initializeAsInputAttachment(
			VkImageType type, 
			VkFormat format,
			VkExtent3D dimensions,
			VkImageUsageFlags usage,
			VkImageViewType viewType,
			VkImageAspectFlagBits aspect);
		bool initializeAs2DSampledImage(
			VkExtent3D& dimensions, 
			VkImageUsageFlags usage,
			VkImageAspectFlags aspect,
			VkFormat imageFormat,
			uint32 numberOfLayer,
			VkImageViewType viewType);
		bool initializeAs2DSampledImage(
			std::string& imagePath,
			VkImageUsageFlags usage,
			VkImageAspectFlags aspect,
			VkFormat imageFormat,
			CommandQueue& commandQueue);
		bool initializeAsStorageImage(
			VkFormat format, 
			VkExtent3D& dimensions,
			VkImage& storageImage,
			VkImageView& storageImageView);
		bool initializeAs2DCubeMapSampledImage(
			VkFormat format, 
			VkExtent3D& dimensions, 
			uint32 numMipmaps, 
			uint32 numLayers, 
			VkSampleCountFlagBits samples,
			VkImageUsageFlags usage, 
			VkImageAspectFlags aspect);
		bool initializeAs2DCubeMapSampledImage(
			std::vector<std::string>& imagePaths,
			VkFormat format,
			uint32 numMipmaps,
			uint32 numLayers,
			VkSampleCountFlagBits samples,
			VkImageUsageFlags usage,
			VkImageAspectFlags aspect,
			CommandQueue& commandQueue);

		void destroy();
		void destroyImageView();

		bool allocateMemory(VkMemoryPropertyFlags properties);
		bool initializeImageView(VkImageAspectFlags aspect, VkImageViewType viewType, VkFormat format, uint32 layer, uint32 layerCount);
		void copyDataFromBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkImageLayout imageLayout, std::vector<VkBufferImageCopy>& regions);
		void copyDataFromBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkImageLayout imageLayout, VkBufferImageCopy* regions, uint32 numberOfRegion);


		template <typename T>
		void updateImageMemory(std::vector<T>& data, CommandQueue& commandQueue, uint32 arrayLayer, VkExtent3D& imageDimensions, VkImageAspectFlags aspect);

		bool updateWithDeviceLocalMemory(
			CommandBuffer& commandBuffer,
			void* data,
			VkDeviceSize dataSize,
			VkImageSubresourceLayers
			dstImageSubresource,
			VkOffset3D dstImageOffset,
			VkExtent3D dstImageSize,
			VkImageLayout dstImageOldLayout,
			VkImageLayout dstImageNewLayout,
			VkAccessFlags dstImageCurrentAccess,
			VkAccessFlags dstImageNewAccess,
			VkPipelineStageFlags dstImageCurrentStages,
			VkPipelineStageFlags dstImageNewStages,
			CommandQueue& queue,
			SemaphorePool& semaphoresToSignal);

		void setMemoryBarrier(
			CommandBuffer& commandBuffer,
			std::vector<VkImageMemoryBarrier>& imageMemoryBarrier,
			VkImageAspectFlags aspect,
			VkPipelineStageFlags currentPipelineStage,
			VkPipelineStageFlags newPipelineStage);

		void setMemoryBarrier(
			CommandBuffer& commandBuffer,
			VkImageMemoryBarrier* imageMemoryBarrier,
			uint32 numberOfBarrier,
			VkImageAspectFlags aspect,
			VkPipelineStageFlags currentPipelineStage,
			VkPipelineStageFlags newPipelineStage);


		private:
			static bool loadDataFromFile(const char* filename, std::vector<unsigned char>* data, int& width, int& height, const int numRequestedComponents);
	};


	template<typename T>
	inline void Image::updateImageMemory(std::vector<T>& data, CommandQueue& commandQueue, uint32 arrayLayer, VkExtent3D& imageDimensions, VkImageAspectFlags aspect)
	{
		VkImageSubresourceLayers imageSubresourcesLayer = {};
		imageSubresourcesLayer.aspectMask = aspect;
		imageSubresourcesLayer.mipLevel = 0;
		imageSubresourcesLayer.baseArrayLayer = arrayLayer;
		imageSubresourcesLayer.layerCount = 1;

		CommandPool commandPool;
		commandPool.initialize(commandQueue, 0, 1, VK_COMMAND_BUFFER_LEVEL_PRIMARY);

		updateWithDeviceLocalMemory(commandPool.commandBuffers[0], getVectorDataPointer(data), data.size(), imageSubresourcesLayer,
			{ 0, 0, 0 }, imageDimensions, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, VK_ACCESS_SHADER_READ_BIT,
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, commandQueue, SemaphorePool());

		commandPool.destroy();
	}
}