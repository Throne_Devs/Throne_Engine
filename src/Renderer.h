#pragma once
#include "stdafx.h"

#include "RenderedObject.h"
#include "GraphicsPipeline.h"
#include "Material.h"
#include "MeshRenderer.h"
#include "FrameBuffer.h"
#include "BindingTree.h"

namespace ThroneEngine
{
	class Renderer
	{
	public:
		Renderer();
		~Renderer();

		std::vector<RenderedObject*> renderedObjects;
		BindingTree bindingTree;


		void initializeTree(RenderPass& renderPass);
		void recordCommands(CommandQueue& commandQueue, RenderPass& renderPass, FrameBuffer& frameBuffer, bool recreateTreeMap, CommandBuffer& commandBuffer);
	};

}


