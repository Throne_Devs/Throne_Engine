#pragma once
#include "stdafx.h"

#include "Mesh.h"
#include "UtilityFunctions.h"
#include "Transform.h"
#include "Font.h"

namespace ThroneEngine
{
	void Mesh::compute2DConvexHull(std::vector<vec2>& result)
	{
		std::vector<vec2> points = result;
		
		size_t nbEdges = points.size();
		size_t offset = 0;

		if (nbEdges <= 3)
		{
			return;
		}		

		result.resize(2 * nbEdges);
		std::sort(points.begin(), points.end(), 
			[] (const vec2& first, const vec2& second) 
		{
			return first.x < second.x || (first.x == second.x && first.y < second.y);
		});

		for (size_t i = 0; i < nbEdges; i++)
		{
			while (offset >= 2 && Utility::cross(result[offset - 2], result[offset - 1], points[i]) <= 0)
			{
				offset--;
			}
			result[offset++] = points[i];
		}

		for (size_t i = nbEdges - 1, t = offset + 1; i > 0; i--)
		{
			while (offset >= t && Utility::cross(result[offset - 2], result[offset - 1], points[i - 1]) <= 0)
			{
				offset--;
			}
			result[offset++] = points[i - 1];
		}

		result.resize(offset - 1);
	}

	void Mesh::setColor(const Color& color)
	{
		const int vertexSize = vertexSizeInBytes / sizeof(float);
		for (int i = 0; i < data.size(); i += vertexSize)
		{
			data[i + vertexSize - 3] = color.r / 255;
			data[i + vertexSize - 2] = color.g / 255;
			data[i + vertexSize - 1] = color.b / 255;
		}
	}

	void Mesh::reset()
	{
		data.erase(data.begin(), data.end());
		parts.erase(parts.begin(), parts.end());
		points.erase(points.begin(), points.end());

		vertexSizeInBytes = 0;
		totalSizeInBytes = 0;
		totalAmountOfVertex = 0;
	}

	void Mesh::setSizeVertices(const vec3& newSize, const vec3& lastSize)
	{
		const vec3 newSizeCoords = vec3(abs(Utility::pixelsToCoordsScale(newSize)), 1);
		const vec3 lasSizeCoords = vec3(abs(Utility::pixelsToCoordsScale(lastSize)), 1);

		for (int i = 0; i < data.size(); i += vertexSizeInBytes / sizeof(float))
		{
			data[i + 0] = data[i + 0] / lasSizeCoords.x * newSizeCoords.x;
			data[i + 1] = data[i + 1] / lasSizeCoords.y * newSizeCoords.y;
			data[i + 2] = data[i + 2] / lasSizeCoords.z * newSizeCoords.z;
		}
	}

	void Mesh::set2DPoints(Transform& transform)
	{
		for (int i = 0; i < data.size(); i += vertexSizeInBytes / sizeof(float))
		{
			if (std::find(points.begin(), points.end(), vec2(data[i], data[i + 1])) == points.end())
			{
				points.emplace_back(data[i], data[i + 1]);
			}
		}

		const vec2 scaling = transform.getScale();
		const vec2 position = transform.getPosition();
		for (int i = 0; i < points.size(); i++)
		{
			points[i] *= scaling;
			points[i] += position;
		}

		compute2DConvexHull(points);
	}

	void Mesh::setTotalSize()
	{
		totalSizeInBytes = 0;
		for (int i = 0; i < parts.size(); i++)
		{
			totalSizeInBytes += parts[i].vertexCount * vertexSizeInBytes;
		}
	}

	void Mesh::setTotalAmountOfVertices()
	{
		totalAmountOfVertex = 0;
		for (int i = 0; i < parts.size(); i++)
		{
			totalAmountOfVertex += parts[i].vertexCount;
		}
	}

	MeshPart Mesh::getFullMeshPart()
	{
		return MeshPart{ 0, totalAmountOfVertex };
	}
}
