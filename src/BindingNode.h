#pragma once
#include "stdafx.h"

#include "Bindable.h"
#include "RenderedObject.h"
#include "UtilityStructures.hpp"
#include "BindingTreePredicats.h"

namespace ThroneEngine
{
	class BindingNode
	{
	public:
		BindingNode();
		BindingNode(Bindable* currentBind);
		~BindingNode();

		Bindable* currentBind = nullptr;

		std::unordered_map<Bindable*, BindingNode> nextNodes;
		std::vector<RenderedObjectCommandRecorder> renderedObjectsParts;

		void addRenderedObjectPart(std::vector<std::pair<Bindable*, int>>& objectBindables, uint32 bindableToAddIndex,
			RenderedObjectCommandRecorder& partToAdd);
		void bind(BindableBindingInfo& bindingInfo);

	};

	inline void BindingNode::addRenderedObjectPart(std::vector<std::pair<Bindable*, int>>& objectBindables, uint32 bindableToAddIndex,
		RenderedObjectCommandRecorder& partToAdd)
	{
		if (bindableToAddIndex >= objectBindables.size())
		{
			renderedObjectsParts.push_back(partToAdd);
			return;
		}

		Bindable* bindable = objectBindables[bindableToAddIndex].first;
		BindingNode& nextNode = nextNodes[bindable];

		nextNode.addRenderedObjectPart(objectBindables, ++bindableToAddIndex, partToAdd);
		if (nextNode.currentBind == nullptr)
		{
			nextNode.currentBind = bindable;
		}
	}


	inline void BindingNode::bind(BindableBindingInfo& bindingInfo)
	{
		currentBind->bind(bindingInfo);

		for (int i = 0; i < renderedObjectsParts.size(); i++)
		{
			renderedObjectsParts[i].recordCommands(*bindingInfo.commandBuffer);
		}

		for (auto i = nextNodes.begin(); i != nextNodes.end(); i++)
		{
			i->second.bind(bindingInfo);
		}
	}
}



