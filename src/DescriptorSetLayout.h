#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class DescriptorSetLayout
	{
	public:
		DescriptorSetLayout();
		~DescriptorSetLayout();

		VkDescriptorSetLayout handle = VK_NULL_HANDLE;

		bool initialize(std::vector<VkDescriptorSetLayoutBinding>& bindings);

	private:

	};
}