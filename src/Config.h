#pragma once
#include "stdafx.h"

#include "Keyboard.h"
#include "Mouse.h"

namespace ThroneEngine
{
	class Config
	{
	public:
		static void initialize();		

		static Keyboard::Key getKey(const std::string& type);
		static std::vector<Keyboard::Key> getKeys();

		static Mouse::Button getButton(const std::string& type);
		static std::vector<Mouse::Button> getButtons();

	private:
		static void chooseFile();
		
		static void createDefaultFile();
		static void createKeysLines(std::ostringstream& configText);
		static void createButtonsLines(std::ostringstream& configText);
		static void setDefaultKeysValue();
		static void setDefaultButtonsValue();
		
		static void readConfigFile();
		static void extractFromQuote(std::string line, std::string& first, std::string& second);
		static void readKeys(const std::string& line);
		static void readButtons(const std::string& line);

		static std::string file;
		static std::map<std::string, Keyboard::Key> keys;
		static std::map<std::string, Mouse::Button> buttons;
	};
}
