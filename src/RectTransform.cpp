#include "stdafx.h"

#include "RectTransform.h"
#include "RenderedObject.h"
#include "Graphic.h"
#include "UtilityFunctions.h"

using namespace Utility;

RectTransform::RectTransform() :
RectTransform(0, 0, 0, 0)
{
}

RectTransform::RectTransform(const RectTransform& rectTransform) :
RectTransform(rectTransform.rect.left, rectTransform.rect.top, rectTransform.rect.width, rectTransform.rect.height)
{
}

RectTransform::RectTransform(const vec2& position, const vec2& size) :
RectTransform(position.x, position.y, size.x, size.y)
{
}

RectTransform::RectTransform(const float x, const float y, const float w, const float h)
{
	setAnchor(0, 0);
	setOrigin(0, 0);
	setScale(1, 1);	
	setRotation(0, 0, 0);
	setRect(x, y, w, h);
}

RectTransform::~RectTransform()
{
}

void RectTransform::setPosition(const float x, const float y)
{
	rect.left = x;
	rect.top = y;

	setTransform();
}

void RectTransform::setPosition(const vec2& newPosition)
{
	setPosition(newPosition.x, newPosition.y);
}

void RectTransform::setOrigin(const float x, const float y)
{
	float newX = x;
	newX = x < 0 ? 0 : x;
	newX = x > 1 ? 1 : x;

	float newY = y;
	newY = y < 0 ? 0 : y;
	newY = y > 1 ? 1 : y;

	origin.x = newX;
	origin.y = newY;

	setTransform();
}

void RectTransform::setOrigin(const vec2& newOrigin)
{
	setOrigin(newOrigin.x, newOrigin.y);
}

void RectTransform::setScale(const float x, const float y)
{
	scale.x = x < 0 ? 0 : x;
	scale.y = y < 0 ? 0 : y;

	setTransform();
}

void RectTransform::setScale(const vec2& newScale)
{
	setScale(newScale.x, newScale.y);
}

void RectTransform::setAnchor(const float x, const float y)
{
	float newX = x;
	newX = x < 0 ? 0 : x;
	newX = x > 1 ? 1 : x;

	float newY = y;
	newY = y < 0 ? 0 : y;
	newY = y > 1 ? 1 : y;

	anchor.x = newX;
	anchor.y = newY;

	setTransform();
}

void RectTransform::setAnchor(const vec2& newAnchor)
{
	setAnchor(newAnchor.x, newAnchor.y);
}

void RectTransform::setSize(const float w, const float h)
{
	rect.width = w < 0 ? 0 : w;
	rect.height = h < 0 ? 0 : h;

	if (graphic)
	{
		graphic->mainTexture.borderImage.setSize(rect.width, rect.height);
		graphic->updateRenderer();
	}
}

void RectTransform::setSize(const vec2& newSize)
{
	setSize(newSize.x, newSize.y);
}

void RectTransform::setRect(const float x, const float y, const float w, const float h)
{
	setPosition(x, y);
	setSize(w, h);
}

void RectTransform::setRect(const FloatRect& newRect)
{
	setRect(newRect.left, newRect.top, newRect.width, newRect.height);
}

void RectTransform::setRotation(const float x, const float y, const float z)
{
	rotation.x = x;
	rotation.y = y;
	rotation.z = z;

	setTransform();
}

void RectTransform::setRotation(const vec3& newRotation)
{
	setRotation(newRotation.x, newRotation.y, newRotation.z);
}

bool RectTransform::contains(const vec2& point)
{
	return getGlobalBound().contains(point);
}

bool RectTransform::intersects(const FloatRect& rectTransform)
{
	return getGlobalBound().intersects(rectTransform);
}

bool RectTransform::intersects(const RectTransform& rectTransform)
{
	return getGlobalBound().intersects(rectTransform.rect);
}

FloatRect RectTransform::getLocalBound() const
{
	return { 0, 0, sizeInPixels().x, sizeInPixels().y };
}

FloatRect RectTransform::getGlobalBound()
{
	return transformRect(getLocalBound());
}

void RectTransform::setTransform()
{
	reset();
	setTransformPosition();
	setTransformRotation();
	setTransformScale();	
	updateTransform();
}

void RectTransform::setTransformPosition()
{
	translate(vec3(positionInPixels(), 0.0f));
}

void RectTransform::setTransformScale()
{	
	Transform::scale(vec3(scale, 1));
}

void RectTransform::setTransformRotation()
{
	translate(vec3(originInPixels(), 0));
	rotate(degreeToRadian(rotation.x), degreeToRadian(rotation.y), degreeToRadian(rotation.z));
	translate(vec3(-originInPixels(), 0));
}

void RectTransform::updateTransform() const
{
	if (graphic && graphic->renderedObject)
	{
		graphic->renderedObject->modelMatrix.matrix = matrix;
		graphic->renderedObject->modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(mat4), value_ptr(graphic->renderedObject->modelMatrix.matrix));
	}
}

vec2 RectTransform::positionInPixels() const
{
	vec2 positionInPixels = rect.position();
	positionInPixels -= originInPixels();
	positionInPixels += anchorInPixels();
	return positionInPixels;
}

vec2 RectTransform::sizeInPixels() const
{
	return rect.size() * scale;
}

vec2 RectTransform::originInPixels() const
{
	return sizeInPixels() * origin;
}

vec2 RectTransform::anchorInPixels() const
{
	return { anchor.x * WINDOW_WIDTH, anchor.y * WINDOW_HEIGTH };
}
