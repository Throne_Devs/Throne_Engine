#pragma once
#include "stdafx.h"

struct Edge
{
	const glm::vec2 first, second;

	Edge(const glm::vec2& first, const glm::vec2& second):
	first(first),
	second(second)
	{
		
	}

	bool operator()(const glm::vec2& point) const
	{
		if(first.y > second.y)
		{
			return Edge{ second, first }(point);
		}

		if(point.y == first.y || point.y == second.y)
		{
			return operator()({ point.x, point.y + std::numeric_limits<float>().epsilon() });
		}

		if(point.y > second.y || point.y < first.y || point.x > std::max(first.x, second.x))
		{
			return false;
		}

		if(point.x < std::min(first.x, second.x))
		{
			return true;
		}

		const auto absfirst = abs(first.x - point.x) > std::numeric_limits<double>().min() ? (point.y - first.y) / (point.x - first.x) : std::numeric_limits<double>().max();
		const auto absSecond = abs(first.x - second.x) > std::numeric_limits<double>().min() ? (second.y - first.y) / (second.x - first.x) : std::numeric_limits<double>().max();
		return absfirst >= absSecond;
	}
};
