#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"

namespace ThroneEngine
{
	class CommandQueue;
	class CommandBuffer;
	class Window;

	THRONE_API_ENTRY bool initializeThroneFramework();	
	THRONE_API_ENTRY void destroyThroneFramework();

	THRONE_API_ENTRY VkInstance instance;
	THRONE_API_ENTRY VkPhysicalDevice physicalDevice;
	THRONE_API_ENTRY VkDevice logicalDevice;
	THRONE_API_ENTRY Window* window;
	THRONE_API_ENTRY CommandQueue commandQueue;

	///////// THE COMMENTED METHODS NEED TO BE MOVED TO THEIR PROPER CLASS 


	THRONE_API_ENTRY bool createVulkanInstance(std::vector<const char*>& requiredExtensions, std::vector<const char*>& requiredLayers, VkInstance& instance);
	THRONE_API_ENTRY void setupDebugCallBack(VkInstance instance, VkDebugReportCallbackEXT& debugCallBack);
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64 obj, size_t location, int32 code, const char* layerPrefix, const char* msg, void* userData);
	THRONE_API_ENTRY VkShaderModule createShaderModule(const std::vector<char>& code);
	THRONE_API_ENTRY bool checkAvailableExtensions(const bool mustOutputExtensions, std::vector<VkExtensionProperties>& availableExtensions);
	THRONE_API_ENTRY bool verifyRequiredExtensions(std::vector<VkExtensionProperties>& availableExtensions, std::vector<const char*>& requiredExtensions, std::vector<const char*>& unsupportedExtensions);
	THRONE_API_ENTRY void enumerateExtensions(std::vector<VkExtensionProperties>& availableExtensions);
	THRONE_API_ENTRY bool enumeratePhysicalDevices(std::vector<VkPhysicalDevice>& physicalDevices);
	THRONE_API_ENTRY VkPhysicalDevice findSuitablePhysicalDevice(std::vector<VkPhysicalDevice>& physicalDevices, std::vector<const char*>& requiredDeviceExtensions, VkPhysicalDeviceFeatures& requiredFeatures, std::vector<QueueInfo>& queueInfos, VkSurfaceKHR surface);
	THRONE_API_ENTRY bool enumerateAvailableExtensionsOnDevice(VkPhysicalDevice& device, std::vector<VkExtensionProperties>& availableExtensions);
	THRONE_API_ENTRY bool requiredFeaturesAreSupported(VkPhysicalDeviceFeatures& requiredFeatures, VkPhysicalDeviceFeatures& availableFeatures);
	THRONE_API_ENTRY bool enumerateDeviceQueueFamilies(VkPhysicalDevice& device, std::vector<VkQueueFamilyProperties>& queueFamilies);
	THRONE_API_ENTRY int findSuitableQueueFamily(std::vector<VkQueueFamilyProperties>& queueFamilies, VkQueueFlags desiredCapabilities);
	THRONE_API_ENTRY bool createLogicalDevice(VkDevice& logicalDevice, VkPhysicalDevice& physicalDevice, std::vector<char const*>& requiredDeviceExtensions, std::vector<const char*>& requiredLayers, std::vector<QueueInfo>& queueInfos, VkPhysicalDeviceFeatures& requiredFeatures);
	THRONE_API_ENTRY int findSuitableQueueFamilyForSwapchainPresentation(VkPhysicalDevice physicalDevice, std::vector<VkQueueFamilyProperties>& queueFamilies, VkSurfaceKHR surface);
	THRONE_API_ENTRY bool getAvailablePresentationModes(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, std::vector<VkPresentModeKHR>& availablePresentModes);
	THRONE_API_ENTRY bool verifyDesiredPresentationModeIsAvailable(std::vector<VkPresentModeKHR>& availablePresentModes, VkPresentModeKHR desiredPresentMode);
	THRONE_API_ENTRY bool getSurfaceCapabilities(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR& surfaceCapabilities);
	THRONE_API_ENTRY void setFeaturesToFalse(VkPhysicalDeviceFeatures& features);

	//THRONE_API_ENTRY bool synchronizeCommandBuffers(VkQueue firstQueue, VkQueue secondQueue, WaitSemaphoreInfo& waitSemaphoresInfoFirstQueue, WaitSemaphoreInfo& synchronizingSemaphoresFirstQueue, std::vector<VkCommandBuffer>& commandBuffersFirstQueue, std::vector<VkCommandBuffer>& commandBuffersSecondQueue, std::vector<VkSemaphore>& synchronizingSemaphoresSecondQueue);
	THRONE_API_ENTRY bool waitForDevice(VkDevice logicalDevice);
	THRONE_API_ENTRY void freeCommandBuffers(VkDevice logicalDevice, VkCommandPool commandPool, std::vector<VkCommandBuffer>& cmdBuffersBeginRenderPass);

	THRONE_API_ENTRY void copyDataFromImageToBuffer(CommandBuffer& commandBuffer, VkImage srcImage, VkBuffer dstBuffer, VkImageLayout imageLayout, std::vector<VkBufferImageCopy>& regions);

	THRONE_API_ENTRY bool freeDescriptorSets(VkDevice logicalDevice, VkDescriptorPool descriptorPool, std::vector<VkDescriptorSet>& descriptorSets);
	THRONE_API_ENTRY bool resetDescriptorPool(VkDevice logicalDevice, VkDescriptorPool descriptorPool);
	THRONE_API_ENTRY void destroyDescriptorPool(VkDevice logicalDevice, VkDescriptorPool& descriptorPool);
	THRONE_API_ENTRY void destroyDescriptorSetLayout(VkDevice logicalDevice, VkDescriptorSetLayout& descriptorSetLayout);


	THRONE_API_ENTRY void specifyPipelineTesselationState(uint32 patchControlPointsCount, VkPipelineTessellationStateCreateInfo& tesselationStateCreateInfo);
	THRONE_API_ENTRY bool createComputePipeline(VkDevice logicalDevice, VkPipelineCreateFlags additionalOptions, VkPipelineShaderStageCreateInfo& computeShaderStage, VkPipelineLayout pipelineLayout, VkPipeline basePipelineHandle, VkPipelineCache pipelineCache, VkPipeline& computePipeline);
	THRONE_API_ENTRY void bindPipelineObject(CommandBuffer& commandBuffer, VkPipelineBindPoint pipelineType, VkPipeline pipeline);

	THRONE_API_ENTRY void destroyPipelineCacheObject(VkDevice logicalDevice, VkPipelineCache& pipelineCacheObject);

	THRONE_API_ENTRY void provideDataThroughPushConstants(CommandBuffer& commandBuffer, VkPipelineLayout pipelineLayout, VkShaderStageFlags pipelineStages, uint32 offset, uint32 size, void * data);


	#pragma region Temporary

	#if defined VK_USE_PLATFORM_WIN32_KHR
	THRONE_API_ENTRY void addWin32SurfaceExtensions(std::vector<const char*>& requiredExtensions);
	#endif

	THRONE_API_ENTRY void addSwapchainExtension(std::vector<const char*>& requiredExtensions);

	#pragma endregion


}