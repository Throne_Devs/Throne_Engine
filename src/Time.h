#pragma once
#include "stdafx.h"

#include "R.h"

namespace ThroneEngine
{
	class Time
	{
	public:
		Time();

		float asSeconds() const;
		int32 asMilliseconds() const;
		int64 asMicroseconds() const;

		static const Time Zero;

	private:

		friend Time seconds(float);
		friend Time milliseconds(int32);
		friend Time microseconds(int64);

		explicit Time(int64 microseconds);

		int64_t microseconds;
	};

	Time seconds(float amount);
	Time milliseconds(int32 amount);
	Time microseconds(int64 amount);
	bool operator ==(Time left, Time right);
	bool operator !=(Time left, Time right);
	bool operator <(Time left, Time right);
	bool operator >(Time left, Time right);
	bool operator <=(Time left, Time right);
	bool operator >=(Time left, Time right);
	Time operator -(Time right);
	Time operator +(Time left, Time right);
	Time& operator +=(Time& left, Time right);
	Time operator -(Time left, Time right);
	Time& operator -=(Time& left, Time right);
	Time operator *(Time left, float right);
	Time operator *(Time left, int64 right);
	Time operator *(float left, Time right);
	Time operator *(int64 left, Time right);
	Time& operator *=(Time& left, float right);
	Time& operator *=(Time& left, int64 right);
	Time operator /(Time left, float right);
	Time operator /(Time left, int64 right);
	Time& operator /=(Time& left, float right);
	Time& operator /=(Time& left, int64 right);
	float operator /(Time left, Time right);
	Time operator %(Time left, Time right);
	Time& operator %=(Time& left, Time right);
}
