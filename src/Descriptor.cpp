#include "stdafx.h"

#include "Descriptor.h"

namespace ThroneEngine
{

	Descriptor::Descriptor()
	{
		writeDescriptorSet = {};
		writeDescriptorSet.pBufferInfo = nullptr;
		writeDescriptorSet.pImageInfo = nullptr;
		writeDescriptorSet.pTexelBufferView = nullptr;
	}


	Descriptor::~Descriptor()
	{

	}

	void Descriptor::initializeGenericWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding)
	{
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.pNext = nullptr;
		writeDescriptorSet.dstSet = descriptorSetHandle;
		writeDescriptorSet.dstBinding = binding;
		writeDescriptorSet.dstArrayElement = 0;
	}
}