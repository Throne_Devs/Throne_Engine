#pragma once
#include "Clock.h"
#include "glm\vec4.hpp"
#include "Keyboard.h"
#include "Mouse.h"
#include "SceneTest1.h"
#include "Font.h"

namespace ThroneEngine
{
	class Throne
	{
	public:
		Throne();
		~Throne();

		bool run();
        bool initialize();

		void onWindowResizeEvent();

	private:
		void input();
		void update();

		void fpsCounter();
		void moveCamera(vec4 direction) const;

		FunctionHandle<void()> onMoveFowardHandle;
		FunctionHandle<void()> onMoveBackwardHandle;
		FunctionHandle<void()> onMoveLeftHandle;
		FunctionHandle<void()> onMoveRightHandle;
		FunctionHandle<void()> onMoveUpHandle;
		FunctionHandle<void()> onMoveDownHandle;

		void onMoveForwardEvent() const;
		void onMoveBackwardEvent() const;
		void onMoveLeftEvent() const;
		void onMoveRightEvent() const;
		void onMoveUpEvent() const;
		void onMoveDownEvent() const;

		const int DEFAULT_SPEED = 1;
		const float CHANGE_SPEED = 0.135;

		int fpsCount = 0;
		float timeFps = 0;
		float cameraSpeed = DEFAULT_SPEED;

		Clock clock;
		float deltaTime;

		bool fullscreen = false;
		bool cameraIsActivated = true;
		bool cursorVisible = false;

		SceneTest1 scene;
	};
}
