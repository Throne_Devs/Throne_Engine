#pragma once
#include "stdafx.h"

#include "Window.h"
#include "ThroneFramework.h"

#if defined PLATFORM_WINDOWS

namespace ThroneEngine
{
	uint16* decode(uint16* begin, uint16* end, uint32& output)
	{
		const uint16 first = *begin++;
		// If it's a surrogate pair, first convert to a single UTF-32 character
		if ((first >= 0xD800) && (first <= 0xDBFF))
		{
			if (begin < end)
			{
				const uint32 second = *begin++;
				if ((second >= 0xDC00) && (second <= 0xDFFF))
				{
					// The second element is valid: convert the two elements to a UTF-32 character
					output = static_cast<uint32>(((first - 0xD800) << 10) + (second - 0xDC00) + 0x0010000);
				}
				else
				{
					// Invalid character
					output = 0;
				}
			}
			else
			{
				// Invalid character
				begin = end;
				output = 0;
			}
		}
		else
		{
			// Direct copy
			output = first;
		}
		return begin;
	}

	uint32* toUtf32(uint16* begin, uint16* end, uint32* output)
	{
		while (begin < end)
		{
			uint32 codepoint;
			begin = decode(begin, end, codepoint);
			*output++ = codepoint;
		}
		return output;
	}

	LRESULT CALLBACK Window::windowsEventHandler(const HWND hWnd, const UINT uMsg, const WPARAM wParam, const LPARAM lParam)
	{
		Window* window = reinterpret_cast<Window*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (window && window->hasFocus())
		{
			window->processEvent(uMsg, wParam, lParam);
			if (window->callback)
				return CallWindowProcW(reinterpret_cast<WNDPROC>(window->callback), window->win32Window, uMsg, wParam, lParam);
		}
		if (uMsg == WM_CLOSE)
			return 0;
		if ((uMsg == WM_SYSCOMMAND) && (wParam == SC_KEYMENU))
			return 0;
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	void Window::processEvent(const UINT uMsg, const WPARAM wParam, const LPARAM lParam)
	{
		if (win32Window == nullptr)
			return;
		switch (uMsg)
		{
			case WM_CLOSE:
			{
				EventWindow event;
				event.type = EventWindow::Closed;
				pushEvent(event);
				break;
			}
			case WM_SIZE:
			{
				if (wParam != SIZE_MINIMIZED && !resizing && lastSize != getSize())
				{
					lastSize = getSize();

					swapchainExtent.width = lastSize.x;
					swapchainExtent.height = lastSize.y;
					recreateSwapChain();

					EventWindow event;
					event.type = EventWindow::Resized;
					event.size.width = lastSize.x;
					event.size.height = lastSize.y;
					pushEvent(event);
					grabCursor(cursorGrabbed);
				}
				break;
			}
			case WM_ENTERSIZEMOVE:
			{
				resizing = true;
				grabCursor(false);
				break;
			}
			case WM_EXITSIZEMOVE:
			{
				resizing = false;
				if (lastSize != getSize())
				{
					lastSize = getSize();

					swapchainExtent.width = lastSize.x;
					swapchainExtent.height = lastSize.y;
					recreateSwapChain();

					EventWindow event;
					event.type = EventWindow::Resized;
					event.size.width = lastSize.x;
					event.size.height = lastSize.y;
					pushEvent(event);
				}
				grabCursor(cursorGrabbed);
				break;
			}
			case WM_GETMINMAXINFO:
			{
				MINMAXINFO* info = reinterpret_cast<MINMAXINFO*>(lParam);
				info->ptMaxTrackSize.x = WINDOW_WIDTH_MAXMININFO;
				info->ptMaxTrackSize.y = WINDOW_HEIGHT_MAXMININFO;
				break;
			}
			case WM_SETFOCUS:
			{
				grabCursor(cursorGrabbed);
				EventWindow event;
				event.type = EventWindow::GainedFocus;
				pushEvent(event);
				break;
			}
			case WM_KILLFOCUS:
			{
				grabCursor(false);
				EventWindow event;
				event.type = EventWindow::LostFocus;
				pushEvent(event);
				break;
			}
			case WM_CHAR:
			{
				if (keyRepeatEnable || (lParam & (1 << 30)) == 0)
				{
					uint32 character = static_cast<unsigned int>(wParam);
					if ((character >= 0xD800) && (character <= 0xDBFF))
					{
						surrogate = static_cast<uint16>(character);
					}
					else
					{
						if ((character >= 0xDC00) && (character <= 0xDFFF))
						{
							uint16 utf16[] = {surrogate, static_cast<uint16>(character)};
							toUtf32(utf16, utf16 + 2, &character);
							surrogate = 0;
						}
						EventWindow event;
						event.type = EventWindow::TextEntered;
						event.text.unicode = character;
						pushEvent(event);
					}
				}
				break;
			}
			case WM_KEYDOWN:
			case WM_SYSKEYDOWN:
			{
				if (keyRepeatEnable || ((HIWORD(lParam) & KF_REPEAT) == 0))
				{
					EventWindow event;
					event.type = EventWindow::KeyPressed;
					event.key.alt = HIWORD(GetAsyncKeyState(VK_MENU)) != 0;
					event.key.control = HIWORD(GetAsyncKeyState(VK_CONTROL)) != 0;
					event.key.shift = HIWORD(GetAsyncKeyState(VK_SHIFT)) != 0;
					event.key.system = HIWORD(GetAsyncKeyState(VK_LWIN)) || HIWORD(GetAsyncKeyState(VK_RWIN));
					event.key.code = virtualKeyCode(wParam, lParam);
					pushEvent(event);
				}
				break;
			}
			case WM_KEYUP:
			case WM_SYSKEYUP:
			{
				EventWindow event;
				event.type = EventWindow::KeyReleased;
				event.key.alt = HIWORD(GetAsyncKeyState(VK_MENU)) != 0;
				event.key.control = HIWORD(GetAsyncKeyState(VK_CONTROL)) != 0;
				event.key.shift = HIWORD(GetAsyncKeyState(VK_SHIFT)) != 0;
				event.key.system = HIWORD(GetAsyncKeyState(VK_LWIN)) || HIWORD(GetAsyncKeyState(VK_RWIN));
				event.key.code = virtualKeyCode(wParam, lParam);
				pushEvent(event);
				break;
			}
			case WM_MOUSEWHEEL:
			{
				POINT position;
				position.x = static_cast<int16>(LOWORD(lParam));
				position.y = static_cast<int16>(HIWORD(lParam));
				ScreenToClient(win32Window, &position);
				const int16 delta = static_cast<int16>(HIWORD(wParam));
				EventWindow event;
				event.type = EventWindow::MouseWheelMoved;
				event.mouseWheel.delta = delta / 120;
				event.mouseWheel.x = position.x;
				event.mouseWheel.y = position.y;
				pushEvent(event);
				event.type = EventWindow::MouseWheelScrolled;
				event.mouseWheelScroll.wheel = Mouse::VerticalWheel;
				event.mouseWheelScroll.delta = static_cast<float>(delta) / 120.f;
				event.mouseWheelScroll.x = position.x;
				event.mouseWheelScroll.y = position.y;
				pushEvent(event);
				break;
			}
			case WM_MOUSEHWHEEL:
			{
				POINT position;
				position.x = static_cast<int16>(LOWORD(lParam));
				position.y = static_cast<int16>(HIWORD(lParam));
				ScreenToClient(win32Window, &position);
				const int16 delta = static_cast<int16>(HIWORD(wParam));
				EventWindow event;
				event.type = EventWindow::MouseWheelScrolled;
				event.mouseWheelScroll.wheel = Mouse::HorizontalWheel;
				event.mouseWheelScroll.delta = -static_cast<float>(delta) / 120.f;
				event.mouseWheelScroll.x = position.x;
				event.mouseWheelScroll.y = position.y;
				pushEvent(event);
				break;
			}
			case WM_LBUTTONDOWN:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonPressed;
				event.mouseButton.button = Mouse::Left;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_LBUTTONUP:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonReleased;
				event.mouseButton.button = Mouse::Left;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_RBUTTONDOWN:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonPressed;
				event.mouseButton.button = Mouse::Right;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_RBUTTONUP:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonReleased;
				event.mouseButton.button = Mouse::Right;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_MBUTTONDOWN:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonPressed;
				event.mouseButton.button = Mouse::Middle;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_MBUTTONUP:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonReleased;
				event.mouseButton.button = Mouse::Middle;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_XBUTTONDOWN:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonPressed;
				event.mouseButton.button = HIWORD(wParam) == XBUTTON1 ? Mouse::XButton1 : Mouse::XButton2;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_XBUTTONUP:
			{
				EventWindow event;
				event.type = EventWindow::MouseButtonReleased;
				event.mouseButton.button = HIWORD(wParam) == XBUTTON1 ? Mouse::XButton1 : Mouse::XButton2;
				event.mouseButton.x = static_cast<int16>(LOWORD(lParam));
				event.mouseButton.y = static_cast<int16>(HIWORD(lParam));
				pushEvent(event);
				break;
			}
			case WM_MOUSELEAVE:
			{
				if (mouseInside)
				{
					mouseInside = false;
					EventWindow event;
					event.type = EventWindow::MouseLeft;
					pushEvent(event);
				}
				break;
			}
			case WM_MOUSEMOVE:
			{
				const int x = static_cast<int16>(LOWORD(lParam));
				const int y = static_cast<int16>(HIWORD(lParam));
				RECT area;
				GetClientRect(win32Window, &area);
				if ((wParam & (MK_LBUTTON | MK_MBUTTON | MK_RBUTTON | MK_XBUTTON1 | MK_XBUTTON2)) == 0)
				{
					if (GetCapture() == win32Window)
						ReleaseCapture();
				}
				else if (GetCapture() != win32Window)
				{
					SetCapture(win32Window);
				}
				if ((x < area.left) || (x > area.right) || (y < area.top) || (y > area.bottom))
				{
					if (mouseInside)
					{
						mouseInside = false;
						setTracking(false);
						EventWindow event;
						event.type = EventWindow::MouseLeft;
						pushEvent(event);
					}
				}
				else
				{
					if (!mouseInside)
					{
						mouseInside = true;
						setTracking(true);
						EventWindow event;
						event.type = EventWindow::MouseEntered;
						pushEvent(event);
					}
				}
				EventWindow event;
				event.type = EventWindow::MouseMoved;
				event.mouseMove.x = x;
				event.mouseMove.y = y;
				event.mouseMove.xRel = x - window->getSize().x / 2;
				event.mouseMove.yRel = y - window->getSize().y / 2;
				pushEvent(event);
				break;
			}
			default: ;
		}
	}

	Keyboard::Key Window::virtualKeyCode(const WPARAM key, LPARAM flags)
	{
		switch (key)
		{
			case VK_SHIFT:
			{
				static UINT lShift = MapVirtualKeyW(VK_LSHIFT, MAPVK_VK_TO_VSC);
				const UINT scancode = static_cast<UINT>((flags & (0xFF << 16)) >> 16);
				return scancode == lShift ? Keyboard::LShift : Keyboard::RShift;
			}
			case VK_MENU: return (HIWORD(flags) & KF_EXTENDED) ? Keyboard::RAlt : Keyboard::LAlt;
			case VK_CONTROL: return (HIWORD(flags) & KF_EXTENDED) ? Keyboard::RControl : Keyboard::LControl;
			case VK_LWIN: return Keyboard::LSystem;
			case VK_RWIN: return Keyboard::RSystem;
			case VK_APPS: return Keyboard::Menu;
			case VK_OEM_1: return Keyboard::SemiColon;
			case VK_OEM_2: return Keyboard::Slash;
			case VK_OEM_PLUS: return Keyboard::Equal;
			case VK_OEM_MINUS: return Keyboard::Dash;
			case VK_OEM_4: return Keyboard::LBracket;
			case VK_OEM_6: return Keyboard::RBracket;
			case VK_OEM_COMMA: return Keyboard::Comma;
			case VK_OEM_PERIOD: return Keyboard::Period;
			case VK_OEM_7: return Keyboard::Quote;
			case VK_OEM_5: return Keyboard::BackSlash;
			case VK_OEM_3: return Keyboard::Tilde;
			case VK_ESCAPE: return Keyboard::Escape;
			case VK_SPACE: return Keyboard::Space;
			case VK_RETURN: return Keyboard::Return;
			case VK_BACK: return Keyboard::BackSpace;
			case VK_TAB: return Keyboard::Tab;
			case VK_PRIOR: return Keyboard::PageUp;
			case VK_NEXT: return Keyboard::PageDown;
			case VK_END: return Keyboard::End;
			case VK_HOME: return Keyboard::Home;
			case VK_INSERT: return Keyboard::Insert;
			case VK_DELETE: return Keyboard::Delete;
			case VK_ADD: return Keyboard::Add;
			case VK_SUBTRACT: return Keyboard::Subtract;
			case VK_MULTIPLY: return Keyboard::Multiply;
			case VK_DIVIDE: return Keyboard::Divide;
			case VK_PAUSE: return Keyboard::Pause;
			case VK_F1: return Keyboard::F1;
			case VK_F2: return Keyboard::F2;
			case VK_F3: return Keyboard::F3;
			case VK_F4: return Keyboard::F4;
			case VK_F5: return Keyboard::F5;
			case VK_F6: return Keyboard::F6;
			case VK_F7: return Keyboard::F7;
			case VK_F8: return Keyboard::F8;
			case VK_F9: return Keyboard::F9;
			case VK_F10: return Keyboard::F10;
			case VK_F11: return Keyboard::F11;
			case VK_F12: return Keyboard::F12;
			case VK_F13: return Keyboard::F13;
			case VK_F14: return Keyboard::F14;
			case VK_F15: return Keyboard::F15;
			case VK_LEFT: return Keyboard::Left;
			case VK_RIGHT: return Keyboard::Right;
			case VK_UP: return Keyboard::Up;
			case VK_DOWN: return Keyboard::Down;
			case VK_NUMPAD0: return Keyboard::Numpad0;
			case VK_NUMPAD1: return Keyboard::Numpad1;
			case VK_NUMPAD2: return Keyboard::Numpad2;
			case VK_NUMPAD3: return Keyboard::Numpad3;
			case VK_NUMPAD4: return Keyboard::Numpad4;
			case VK_NUMPAD5: return Keyboard::Numpad5;
			case VK_NUMPAD6: return Keyboard::Numpad6;
			case VK_NUMPAD7: return Keyboard::Numpad7;
			case VK_NUMPAD8: return Keyboard::Numpad8;
			case VK_NUMPAD9: return Keyboard::Numpad9;
			case 'A': return Keyboard::A;
			case 'Z': return Keyboard::Z;
			case 'E': return Keyboard::E;
			case 'R': return Keyboard::R;
			case 'T': return Keyboard::T;
			case 'Y': return Keyboard::Y;
			case 'U': return Keyboard::U;
			case 'I': return Keyboard::I;
			case 'O': return Keyboard::O;
			case 'P': return Keyboard::P;
			case 'Q': return Keyboard::Q;
			case 'S': return Keyboard::S;
			case 'D': return Keyboard::D;
			case 'F': return Keyboard::F;
			case 'G': return Keyboard::G;
			case 'H': return Keyboard::H;
			case 'J': return Keyboard::J;
			case 'K': return Keyboard::K;
			case 'L': return Keyboard::L;
			case 'M': return Keyboard::M;
			case 'W': return Keyboard::W;
			case 'X': return Keyboard::X;
			case 'C': return Keyboard::C;
			case 'V': return Keyboard::V;
			case 'B': return Keyboard::B;
			case 'N': return Keyboard::N;
			case '0': return Keyboard::Num0;
			case '1': return Keyboard::Num1;
			case '2': return Keyboard::Num2;
			case '3': return Keyboard::Num3;
			case '4': return Keyboard::Num4;
			case '5': return Keyboard::Num5;
			case '6': return Keyboard::Num6;
			case '7': return Keyboard::Num7;
			case '8': return Keyboard::Num8;
			case '9': return Keyboard::Num9;
			default: ;
		}
		return Keyboard::Unknown;
	}

	uint64 Window::win32ClassIdCounter = 0;

	bool Window::initOsWindow()
	{
		WNDCLASSEX winClass;
		assert(swapchainExtent.width > 0);
		assert(swapchainExtent.height > 0);
		win32Instance = GetModuleHandle(nullptr);
		win32ClassName = windowName + "_" + std::to_string(win32ClassIdCounter);
		win32ClassIdCounter++;
		std::wstring win32ClassNameWideStr = std::wstring(win32ClassName.begin(), win32ClassName.end());
		const LPCWSTR win32ClassNameWideStr_2 = win32ClassNameWideStr.c_str();
		// Initialize the window class structure:
		winClass.cbSize = sizeof(WNDCLASSEX);
		winClass.style = CS_HREDRAW | CS_VREDRAW;
		winClass.lpfnWndProc = windowsEventHandler;
		winClass.cbClsExtra = 0;
		winClass.cbWndExtra = 0;
		winClass.hInstance = win32Instance; // hInstance
		winClass.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
		winClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
		winClass.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
		winClass.lpszMenuName = nullptr;
		winClass.lpszClassName = win32ClassNameWideStr_2;
		winClass.hIconSm = LoadIcon(nullptr, IDI_WINLOGO);
		// Register window class:
		if (!RegisterClassEx(&winClass))
		{
			return false;
		}
		const DWORD ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		const DWORD style = WS_OVERLAPPEDWINDOW | WS_CAPTION | WS_SYSMENU | WS_VISIBLE;//WS_MINIMIZEBOX;
		std::wstring windowNameWideStr = std::wstring(windowName.begin(), windowName.end());
		const LPCWSTR windowNameWideStr_2 = windowNameWideStr.c_str();
		// Create window with the registered class:
		RECT wr = {0, 0, LONG(swapchainExtent.width), LONG(swapchainExtent.height)};
		AdjustWindowRectEx(&wr, style, FALSE, ex_style);
		win32Window = CreateWindowEx(0,
		                             win32ClassNameWideStr_2, // class name
		                             windowNameWideStr_2, // app name
		                             style, // window style
		                             CW_USEDEFAULT, CW_USEDEFAULT, // x/y coords
		                             wr.right - wr.left, // width
		                             wr.bottom - wr.top, // height
		                             nullptr, // handle to parent
		                             nullptr, // handle to menu
		                             win32Instance, // hInstance
		                             nullptr); // no extra parameters
		if (!win32Window)
		{
			return false;
		}
		SetWindowLongPtr(win32Window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
		ShowWindow(win32Window, SW_SHOW);
		SetForegroundWindow(win32Window);
		SetFocus(win32Window);
		return true;
	}

	void Window::deInitOsWindow()
	{
		std::wstring win32ClassNameWideStr = std::wstring(win32ClassName.begin(), win32ClassName.end());
		const LPCWSTR win32ClassNameWideStr_2 = win32ClassNameWideStr.c_str();
		DestroyWindow(win32Window);
		UnregisterClass(win32ClassNameWideStr_2, win32Instance);
	}

	void Window::updateOsWindow() const
	{
		MSG msg;
		if (PeekMessage(&msg, win32Window, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	void Window::initOsSurface()
	{
		VkWin32SurfaceCreateInfoKHR createInfo;
		createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;
		createInfo.hinstance = win32Instance;
		createInfo.hwnd = win32Window;
		vkCreateWin32SurfaceKHR(instance, &createInfo, nullptr, &surface);
	}

	vec2 Window::getSize() const
	{
		RECT rect;
		GetClientRect(win32Window, &rect);
		return vec2(rect.right - rect.left, rect.bottom - rect.top);
	}

	void Window::setSize(const vec2& size) const
	{
		RECT rectangle = {0, 0, static_cast<long>(size.x), static_cast<long>(size.y)};
		AdjustWindowRect(&rectangle, GetWindowLong(win32Window, GWL_STYLE), false);
		const int width = rectangle.right - rectangle.left;
		const int height = rectangle.bottom - rectangle.top;
		SetWindowPos(win32Window, nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);
	}

	void Window::setTitle(const std::string title) const
	{
		std::wstring titleWide = std::wstring(title.begin(), title.end());
		const LPCWSTR titleWide2 = titleWide.c_str();
		SetWindowTextW(win32Window, titleWide2);
	}

	vec2 Window::getPosition() const
	{
		RECT rect;
		GetWindowRect(win32Window, &rect);
		return vec2(rect.left, rect.top);
	}

	void Window::setPosition(const vec2& position) const
	{
		SetWindowPos(win32Window, nullptr, position.x, position.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if (cursorGrabbed)
			grabCursor(true);
	}

	void Window::setMouseCursorVisible(const bool visible)
	{
		if (visible != mouseCursorVisible)
		{
			mouseCursorVisible = visible;
			ShowCursor(visible);
		}
	}

	void Window::setMouseCursorGrabbed(const bool grabbed)
	{
		cursorGrabbed = grabbed;
		grabCursor(cursorGrabbed);
	}

	void Window::requestFocus() const
	{
		const DWORD thisPid = GetWindowThreadProcessId(win32Window, nullptr);
		const DWORD foregroundPid = GetWindowThreadProcessId(GetForegroundWindow(), nullptr);
		if (thisPid == foregroundPid)
		{
			SetForegroundWindow(win32Window);
		}
		else
		{
			FLASHWINFO info;
			info.cbSize = sizeof(info);
			info.hwnd = win32Window;
			info.dwFlags = FLASHW_TRAY;
			info.dwTimeout = 0;
			info.uCount = 3;
			FlashWindowEx(&info);
		}
	}

	bool Window::hasFocus() const
	{
		return win32Window == GetForegroundWindow();
	}

	void Window::switchToFullscreen(const vec2& mode)
	{
		DEVMODE devMode;
		devMode.dmSize = sizeof(devMode);
		devMode.dmPelsWidth = mode.x;
		devMode.dmPelsHeight = mode.y;
		devMode.dmBitsPerPel = 32;
		devMode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;
		if (ChangeDisplaySettingsW(&devMode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			return;
		}
		SetWindowLongW(win32Window, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
		SetWindowLongW(win32Window, GWL_EXSTYLE, WS_EX_APPWINDOW);
		SetWindowPos(win32Window, HWND_TOP, 0, 0, mode.x, mode.y, SWP_FRAMECHANGED);
		ShowWindow(win32Window, SW_SHOW);
		fullscreenWindow = this;
	}

	void Window::setVisible(const bool visible) const
	{
		ShowWindow(win32Window, visible ? SW_SHOW : SW_HIDE);
	}

	void Window::grabCursor(const bool grabbed) const
	{
		if (grabbed)
		{
			RECT rect;
			GetClientRect(win32Window, &rect);
			MapWindowPoints(win32Window, nullptr, reinterpret_cast<LPPOINT>(&rect), 2);
			ClipCursor(&rect);
		}
		else
		{
			ClipCursor(nullptr);
		}
	}

	HWND Window::getSystemHandle() const
	{
		return win32Window;
	}

	void Window::setTracking(const bool track) const
	{
		TRACKMOUSEEVENT mouseEvent;
		mouseEvent.cbSize = sizeof(TRACKMOUSEEVENT);
		mouseEvent.dwFlags = track ? TME_LEAVE : TME_CANCEL;
		mouseEvent.hwndTrack = win32Window;
		mouseEvent.dwHoverTime = HOVER_DEFAULT;
		TrackMouseEvent(&mouseEvent);
	}
}
#endif //VK_USE_PLATFORM_WIN32_KHR
