#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class File
	{
	public:
		/// <summary>
		/// Initializes a new instance of the File class.
		/// </summary>
		File();

		/// <summary>
		/// Initializes a new instance of the File class.
		/// </summary>
		/// <param name="filePath">The file to set and create if not exist.</param>
		/// <param name="text">The text to write in the file if not exist.</param>
		File(const std::string& filePath, const bool create = true, const std::string& text = "");

		/// <summary>
		/// Finalizes an instance of the File class.
		/// </summary>
		~File();

		/// <summary>
		/// Creates or overwrites the specified file with a text.
		/// </summary>
		/// <param name="filePath">The file to create or overwrites.</param>
		/// <param name="text">The text to write in the file.</param>
		/// <returns>True if worked otherwise it's false.</returns>
		bool createFile(const std::string& filePath, const std::string& text = "");

		/// <summary>
		/// Change the filePath of the class and creates the file if it doesn't exist.
		/// </summary>
		/// <param name="filePath">The file to set and create if not exist.</param>
		/// <param name="text">The text to write in the file if not exist.</param>
		/// <param name="create">Shoudl create or not the file.</param>
		bool setFilePath(const std::string& filePath, const bool create = true, const std::string& text = "");

		/// <summary>
		/// Return the file path of the current file.
		/// </summary>
		/// <returns>file path of the current file.</returns>
		std::string getFilePath() const;

		/// <summary>
		/// Opens the file for reading only.
		/// </summary>
		/// <param name="filePath">The file path to open.</param>
		void openRead(const std::string& filePath);

		/// <summary>
		/// Opens the file for reading in binary.
		/// </summary>
		/// <param name="filePath">The file path to open.</param>
		void openReadBinary(const std::string& filePath);

		/// <summary>
		/// Opens the file for writing in binary.
		/// </summary>
		/// <param name="filePath">The file path to open.</param>
		void openWriteBinary(const std::string& filePath);

		/// <summary>
		/// Opens the file for writing only.
		/// </summary>
		/// <param name="filePath">The file path to open.</param>
		void openWrite(const std::string& filePath);

		/// <summary>
		/// Opens the specified file path with certain mode.
		/// </summary>
		/// <param name="filePath">The file path to open.</param>
		/// <param name="mode">The mode to open the file.</param>
		void open(const std::string& filePath, std::ios_base::openmode mode = std::ios_base::in | std::ios_base::out);

		/// <summary>
		/// Closes the file.
		/// </summary>
		void close();

		/// <summary>
		/// Verify if this file exist.
		/// </summary>
		/// <returns>True if it exist otherwise false.</returns>
		bool exist();

		/// <summary>
		/// Verify if the file exist.
		/// </summary>
		/// <param name="filePath">The file path to check if it exist.</param>
		/// <returns>True if it exist otherwise false.</returns>
		static bool exist(const std::string& filePath);

		/// <summary>
		/// Deletes this file.
		/// </summary>
		/// <returns>True if it's deleted, otherwise false.</returns>
		bool deleteFile() const;

		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="filePath">The file path to delete.</param>
		/// <returns>True if it's deleted, otherwise false.</returns>
		static bool deleteFile(const std::string& filePath);

		/// <summary>
		/// Appends all text to the end of the file.
		/// </summary>
		/// <param name="text">The text to add.</param>
		void appendAllText(const std::string& text);

		/// <summary>
		/// Appends all lines to the end of the file.
		/// </summary>
		/// <param name="allLines">All the lines to add.</param>
		void appendAllLines(const std::vector<std::string>& allLines);

		/// <summary>
		/// Appends all text to the end of the file.
		/// </summary>
		/// <param name="filePath">The file path to add the text.</param>
		/// <param name="text">The text to add.</param>
		static void appendAllText(const std::string& filePath, const std::string& text);

		/// <summary>
		/// Appends all lines to the end of the file.
		/// </summary>
		/// <param name="filePath">The file path to add the lines.</param>
		/// <param name="allLines">All the lines to add.</param>
		static void appendAllLines(const std::string& filePath, const std::vector<std::string>& allLines);

		/// <summary>
		/// Appends all bytes to the ned of the binary file.
		/// </summary>
		/// <param name="bytes">The bytes to append.</param>
		void appendAllBytes(const std::vector<char>& bytes);

		/// <summary>
		/// Appends all bytes to the ned of the binary file.
		/// </summary>
		/// <param name="filePath">The file path to add the bytes.</param>
		/// <param name="bytes">The bytes to append.</param>
		static void appendAllBytes(const std::string& filePath, const std::vector<char>& bytes);

		/// <summary>
		/// Writes all lines to the file and overwrites his content.
		/// </summary>
		/// <param name="text">The text to write.</param>
		void writeAllLines(std::string& text);

		/// <summary>
		/// Writes all lines to the file and overwrites his content.
		/// </summary>
		/// <param name="text">The text to write.</param>
		void writeAllLines(std::vector<std::string>& text);

		/// <summary>
		/// Writes all lines to the file and overwrites his content.
		/// </summary>
		/// <param name="filePath">The file path to write on.</param>
		/// <param name="text">The text to write.</param>
		static void writeAllLines(const std::string& filePath, const std::string& text);

		/// <summary>
		/// Writes all lines to the file and overwrites his content.
		/// </summary>
		/// <param name="filePath">The file path to write on.</param>
		/// <param name="text">The text to write.</param>
		static void writeAllLines(const std::string& filePath, const std::vector<std::string>& text);

		/// <summary>
		/// Reads all the lines of this file.
		/// </summary>
		/// <returns>contains all the lines of this file.</returns>
		std::vector<std::string> readLines();

		/// <summary>
		/// Reads all the lines of this file.
		/// </summary>
		/// <param name="lines">contains all the lines of this file.</param>
		void readLines(std::vector<std::string>& lines);

		/// <summary>
		/// Reads all the lines of this file.
		/// </summary>
		/// <param name="filePath">The file to read.</param>
		/// <returns>contains all the lines of this file.</returns>
		static std::vector<std::string> readLines(const std::string& filePath);

		/// <summary>
		/// Reads all the lines of this file.
		/// </summary>
		/// <param name="filePath">The file to read.</param>
		/// <param name="lines">contains all the lines of this file.</param>
		static void readLines(const std::string& filePath, std::vector<std::string>& lines);

		/// <summary>
		/// Reads all the current binary files.
		/// </summary>
		/// <returns>all bytes from the file.</returns>
		std::vector<char> readBinary();

		/// <summary>
		/// Reads all the current binary files.
		/// </summary>
		/// <param name="lines">return all bytes from the file.</param>
		void readBinary(std::vector<char>& lines);

		/// <summary>
		/// Reads all the specified binary files.
		/// </summary>
		/// <param name="filePath">The file pathto read.</param>
		/// <returns>all bytes from the specified file.</returns>
		static std::vector<char> readBinary(const std::string& filePath);

		/// <summary>
		/// Reads all the specified binary files.
		/// </summary>
		/// <param name="filePath">The file pathto read.</param>
		/// <param name="lines">return all bytes from the specified file.</param>
		static void readBinary(const std::string& filePath, std::vector<char>& lines);

		/// <summary>
		/// Reads all the text of this file.
		/// </summary>
		/// <returns>contain all the text.</returns>
		std::string readAllText();

		/// <summary>
		/// Reads all the text of this file.
		/// </summary>
		/// <param name="text">contains all the text.</param>
		void readAllText(std::string& text);

		/// <summary>
		/// Reads all the text of this file.
		/// </summary>
		/// <param name="filePath">The file to read.</param>
		/// <returns>contain all the text.</returns>
		static std::string readAllText(const std::string& filePath);

		/// <summary>
		/// Reads all the text of this file.
		/// </summary>
		/// <param name="filePath">The file to read.</param>
		/// <param name="text">contains all the text.</param>
		static void readAllText(const std::string& filePath, std::string& text);

		/// <summary>
		/// Replaces this file from his substitute.
		/// </summary>
		/// <param name="substitute">The substitute to replace this file.</param>
		void replace(const std::string& substitute);

		/// <summary>
		/// Replaces this file from his substitute.
		/// </summary>
		/// <param name="substitute">The substitute to replace this file.</param>
		void replace(File& substitute);

		/// <summary>
		/// Replaces the original file from his substitute.
		/// </summary>
		/// <param name="origin">The original file to replace.</param>
		/// <param name="substitute">The substitute to replace the original.</param>
		static void replace(const std::string& origin, const std::string& substitute);

		/// <summary>
		/// Replaces the original file from his substitute.
		/// </summary>
		/// <param name="origin">The original file to replace.</param>
		/// <param name="substitute">The substitute to replace the original.</param>
		static void replace(File& origin, File& substitute);

		/// <summary>
		/// Moves this file to a new directory.
		/// </summary>
		/// <param name="newDirectory">The new directory to move the file.</param>
		void move(const std::string& newDirectory) const;

		/// <summary>
		/// Moves the original file to a new directory.
		/// </summary>
		/// <param name="origin">The original file to move.</param>
		/// <param name="newDirectory">The new directory to move the file.</param>
		static void move(const std::string& origin, const std::string& newDirectory);

		/// <summary>
		/// Moves the original file to a new directory.
		/// </summary>
		/// <param name="origin">The original file to move.</param>
		/// <param name="newDirectory">The new directory to move the file.</param>
		static void move(File& origin, const std::string& newDirectory);

		/// <summary>
		/// Copies this file to an other directory.
		/// </summary>
		/// <param name="newDirectory">The directory to copy the file.</param>
		void copy(const std::string& newDirectory) const;

		/// <summary>
		/// Copies the original file to an other directory.
		/// </summary>
		/// <param name="origin">The original file to copy.</param>
		/// <param name="newDirectory">The directory to copy the file.</param>
		static void copy(const std::string& origin, const std::string& newDirectory);

		/// <summary>
		/// Copies the original file to an other directory.
		/// </summary>
		/// <param name="origin">The original file to copy.</param>
		/// <param name="newDirectory">The directory to copy the file.</param>
		static void copy(File& origin, const std::string& newDirectory);

		/// <summary>
		/// Size of current file.
		/// </summary>
		/// <returns>size_t of the current file.</returns>
		size_t size();

		/// <summary>
		/// Size of specified file.
		/// </summary>
		/// <param name="filePath">The file pathyo get the size.</param>
		/// <returns>size_t of the specified file.</returns>
		static size_t size(const std::string& filePath);

	private:
		std::fstream fstream;
		std::string filePath = "";
	};
}
