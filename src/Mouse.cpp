#include "stdafx.h"

#include "Mouse.h"
#include "Event.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	std::map<Mouse::Button, Event<void>> Mouse::onButtonMaintained;
	std::map<Mouse::Button, Event<void>> Mouse::onButtonPressed;
	std::map<Mouse::Button, Event<void>> Mouse::onButtonReleased;
	Event<void, const vec2&> Mouse::onMoved;

	void Mouse::notifyButtonMaintain()
	{
		for (const std::pair<Button, Event<void>> pair : onButtonMaintained)
		{
			checkIsButtonPressed(pair.first);
		}
	}

	void Mouse::notifyMove()
	{
		onMoved(getPosition(*window));
	}

	void Mouse::notifyButtonPressed()
	{
		for(std::pair<Button, Event<void>> pair : onButtonPressed)
		{
			pair.second();
		}
	}

	void Mouse::notifyButtonReleased()
	{
		for(std::pair<Button, Event<void>> pair : onButtonReleased)
		{
			pair.second();
		}
	}

	void Mouse::checkIsButtonPressed(const Button button)
	{
		if (isButtonPressed(button))
		{
			onButtonMaintained[button]();
		}
	}
}
