#include "stdafx.h"

#include "UtilityFunctions.h"
#include "Directory.h"
#include "File.h"

namespace ThroneEngine
{
	std::vector<std::string> Log::logs;

	void Log::write(std::string type, std::string message)
	{
		time_t t = std::time(nullptr);
		tm tm = *std::localtime(&t);
		std::transform(type.begin(), type.end(), type.begin(), toupper);
		std::ostringstream oss;
		oss << std::put_time(&tm, "[%H:%M:%S] ") << "[" << type << "]: " << message;
		logs.push_back(oss.str());
	}

	void Log::appendLog()
	{
		if (!logs.empty())
		{
			std::string allLogs;
			while (!logs.empty())
			{
				allLogs += logs.front() + "\n";
				logs.erase(logs.begin());
			}

			time_t t = std::time(nullptr);
			tm tm = *std::localtime(&t);
			std::ostringstream oss;
			oss << path_logsFiles << "/" << std::put_time(&tm, "%Y-%m-%d %H-%M-%S") << LOG_FILES_EXT;

			if (Directory::createDirectory(path_logsFiles))
			{
				File::writeAllLines(oss.str(), allLogs);
			}
		}
	}
}
