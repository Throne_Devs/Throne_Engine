#include "stdafx.h"

#include "Transform.h"
#include "UtilityFunctions.h"

namespace ThroneEngine
{
	/// <summary>
	/// Initializes a new instance of the Transform class with position at 0, angle at 0 and scaling of 1.
	/// </summary>
	Transform::Transform() : Transform(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1))
	{
	}

	/// <summary>
	/// Initializes a new instance of the Transform class with a position, an angle and a scaling.
	/// </summary>
	/// <param name="position">The position of the object</param>
	/// <param name="angle">The angle of the object</param>
	/// <param name="scaling">The scaling of the object</param>
	Transform::Transform(const glm::vec3& position, const glm::vec3& angle, glm::vec3& scaling) : matrix(1.0f)
	{
		translate(position);
		rotate(angle);
		scale(scaling);
	}

	/// <summary>
	/// Finalizes an instance of the Transform class.
	/// </summary>
	Transform::~Transform()
	{
	}

	void Transform::reset()
	{
		matrix = glm::mat4(1);
	}

	/// <summary>
	/// Translates the object with the specified translation.
	/// </summary>
	/// <param name="translation">The translation this object going to do.</param>
	void Transform::translate(const glm::vec3& translation)
	{
		matrix = glm::translate(matrix, translation);
	}

	/// <summary>
	/// Rotates to the specified angle around the z axis, around the x axis and around the y axis (in the same order)
	/// </summary>
	/// <param name="angle">The angles in radian.</param>
	void Transform::rotate(const glm::vec3& angle)
	{
		rotate(glm::vec3(0, 0, 1), angle.z);
		rotate(glm::vec3(1, 0, 0), angle.x);
		rotate(glm::vec3(0, 1, 0), angle.y);
	}

	/// <summary>
	/// Rotates to the specified angle around the z axis, around the x axis and around the y axis (in the same order)
	/// </summary>
	/// <param name="xAngle">The x angle in radian.</param>
	/// <param name="yAngle">The y angle in radian.</param>
	/// <param name="zAngle">The z angle in radian.</param>
	void Transform::rotate(const float xAngle, const float yAngle, const float zAngle)
	{
		rotate(glm::vec3(xAngle, yAngle, zAngle));
	}

	/// <summary>
	/// Rotates the object on the specified axis by an angle given.
	/// </summary>
	/// <param name="axis">The axis to rotate.</param>
	/// <param name="angle">The angle in radian.</param>
	void Transform::rotate(const glm::vec3& axis, const float angle)
	{
		matrix = glm::rotate(matrix, angle, axis);
	}

	/// <summary>
	/// Rotates the object around the specified position.
	/// </summary>
	/// <param name="angle">The angle in radian.</param>
	/// <param name="position">The position to rotate around.</param>
	void Transform::rotateAround(glm::vec3& angle, glm::vec3& position)
	{
		translate(-position);
		rotate(angle);
		translate(position);
	}

	/// <summary>
	/// Scales the specified object by a given scale.
	/// </summary>
	/// <param name="scaling">The scaling other than 0.</param>
	void Transform::scale(glm::vec3& scaling)
	{
		if (scaling.x == 0 || scaling.y == 0 || scaling.z == 0)
			scaling = glm::vec3(0.000001, 0.000001, 0.000001);
		matrix = glm::scale(matrix, scaling);
	}

	/// <summary>
	/// Rotate the object to view a specified target.
	/// </summary>
	/// <param name="target">The target the object need to look at.</param>
	void Transform::lookAt(Transform& target)
	{
		lookAt(static_cast<glm::vec3>(*target.position()));
	}

	/// <summary>
	/// Rotate the object to view a specified position.
	/// </summary>
	/// <param name="position">The position the object need to look at.</param>
	void Transform::lookAt(glm::vec3& position)
	{
		matrix = inverse(glm::lookAt(glm::vec3(*this->position()), position, glm::vec3(*this->up())));
	}

	void Transform::printPosition()
	{
		Utility::printVec4(std::string("Pos : "), *position(), false);
	}

	void Transform::getModelMatrixAttributeDesctription(std::vector<VkVertexInputAttributeDescription>& attributes, uint32 beginLocation)
	{		
		attributes.push_back(VkVertexInputAttributeDescription());
		attributes[attributes.size() - 1].binding = 1;
		attributes[attributes.size() - 1].location = beginLocation;
		attributes[attributes.size() - 1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributes[attributes.size() - 1].offset = 0;

		attributes.push_back(VkVertexInputAttributeDescription());
		attributes[attributes.size() - 1].binding = 1;
		attributes[attributes.size() - 1].location = beginLocation + 1;
		attributes[attributes.size() - 1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributes[attributes.size() - 1].offset = sizeof(float) * 4;

		attributes.push_back(VkVertexInputAttributeDescription());
		attributes[attributes.size() - 1].binding = 1;
		attributes[attributes.size() - 1].location = beginLocation + 2;
		attributes[attributes.size() - 1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributes[attributes.size() - 1].offset = sizeof(float) * 8;

		attributes.push_back(VkVertexInputAttributeDescription());
		attributes[attributes.size() - 1].binding = 1;
		attributes[attributes.size() - 1].location = beginLocation + 3;
		attributes[attributes.size() - 1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributes[attributes.size() - 1].offset = sizeof(float) * 12;
	}

	void Transform::getModelMatrixBindingDescription(std::vector<VkVertexInputBindingDescription>& bindings)
	{
		bindings.push_back(VkVertexInputBindingDescription());
		bindings[bindings.size() - 1].binding = 1;
		bindings[bindings.size() - 1].stride = sizeof(glm::mat4);
		bindings[bindings.size() - 1].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
	}

	glm::vec2 Transform::transformPoint(const float x, const float y)
	{
		return glm::vec2(matrix[0].x * x + matrix[1].x * y + matrix[3].x,
					matrix[0].y * x + matrix[1].y * y + matrix[3].y);
	}

	glm::vec2 Transform::transformPoint(const glm::vec2& point)
	{
		return transformPoint(point.x, point.y);
	}

	void Transform::setRotation(const glm::vec3& rotation)
	{
		rotate(glm::vec3(0, 0, 0));
		rotate(rotation);
	}

	void Transform::setScale(const glm::vec3& scaling)
	{
		matrix[0].x = scaling.x;
		matrix[1].y = scaling.y;
		matrix[2].z = scaling.z;
	}

	void Transform::setPosition(const glm::vec3& position)
	{
		matrix[3].x = position.x;
		matrix[3].y = position.y;
		matrix[3].z = position.z;
	}

	/// <summary>
	/// Gets the view matrix.
	/// </summary>
	/// <returns>The view matrix.</returns>
	glm::mat4 Transform::getViewMatrix()
	{
		return glm::lookAt(glm::vec3(*this->position()), glm::vec3(*this->forward()), glm::vec3(*this->up()));
	}

	glm::vec3 Transform::getScale()
	{
		return glm::vec3(length(matrix[0]), length(matrix[1]), length(matrix[2]));
	}

	glm::vec3 Transform::getEulerAngles() const
	{
		return eulerAngles(quat_cast(matrix));
	}

	glm::vec3 Transform::getPosition() const
	{
		return glm::vec3(matrix[3].x, matrix[3].y, matrix[3].z);
	}

	FloatRect Transform::transformRect(const FloatRect& rectangle)
	{
		const vec2 points[]
		{
			transformPoint(rectangle.left, rectangle.top),
			transformPoint(rectangle.left, rectangle.top + rectangle.height),
			transformPoint(rectangle.left + rectangle.width, rectangle.top),
			transformPoint(rectangle.left + rectangle.width, rectangle.top + rectangle.height)
		};
		
		float left = points[0].x;
		float top = points[0].y;
		float right = points[0].x;
		float bottom = points[0].y;

		for(int i = 1; i < 4; i++)
		{
			if (points[i].x < left) left = points[i].x;
			else if (points[i].x > right) right = points[i].x;
			if (points[i].y < top) top = points[i].y;
			else if (points[i].y > bottom) bottom = points[i].y;
		}

		return FloatRect(left, top, right - left, bottom - top);
	}
}
