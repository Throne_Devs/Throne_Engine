#include "stdafx.h"

#include "TerrainGenerator.h"
#include "UtilityFunctions.hpp"
#include "Vertex.h"


namespace ThroneEngine
{
	using Utility::get2DPerlinNoiseValue;

	TerrainGenerator::TerrainGenerator()
	{
	}


	TerrainGenerator::~TerrainGenerator()
	{
	}

	void TerrainGenerator::generateTerrain(Mesh * mesh, uint32 sizeX, uint32 sizeZ)
	{
		float tileSize = 0.5f;

		int amountOfTileX = sizeX / tileSize;
		int amountOfTileZ = sizeZ / tileSize;

		MeshPart part = {};
		part.vertexOffset = 0;
		part.vertexCount = amountOfTileX * amountOfTileZ * 6;
		mesh->parts.push_back(part);

		mesh->data.resize(amountOfTileX * amountOfTileZ * 48);


		int multiplier = 200 * 0.5f;
		int res = 50;

		float** perlinValues = new float*[amountOfTileZ + 1];
		float zValue = 0;
		float xValue = 0;
		for (int z = 0; z < amountOfTileZ + 1; z++, zValue += tileSize)
		{
			perlinValues[z] = new float[amountOfTileX + 1];
			xValue = 0;
			for (int x = 0; x < amountOfTileX + 1; x++, xValue += tileSize)
			{
				perlinValues[z][x] = (get2DPerlinNoiseValue(xValue, zValue, res) * multiplier) - multiplier;
			}
		}

		int index = 0;
		zValue = 0;
		xValue = 0;
		vec3 normal;
		for (int z = 0; z < amountOfTileZ; z++, zValue += tileSize)
		{
			const float zPlusTileSize = zValue + tileSize;
			xValue = 0;
			for (int x = 0; x < amountOfTileX; x++, xValue += tileSize)
			{
				const float xPlusTileSize = xValue + tileSize;
				const vec3 v1 = vec3(xValue, perlinValues[(int)(x)][(int)(z)], zValue);
				//const glm::vec3 normal = glm::cross(glm::vec3(xPlusTileSize, perlinValues[(int)(x + 1)][(int)(z)], zValue) - v1,
				//									glm::vec3(xValue, perlinValues[(int)(x)][(int)(z + 1)], zPlusTileSize) - v1);
				Utility::crossProductVec3(vec3(xPlusTileSize, perlinValues[(int)(x + 1)][(int)(z)], zValue) - v1, 
					vec3(xValue, perlinValues[(int)(x)][(int)(z + 1)], zPlusTileSize) - v1, normal);
				normal *= -1.0f;


				//V1
				mesh->data[index + 0] = xValue;
				mesh->data[index + 1] = perlinValues[(int)(x)][(int)(z)];
				mesh->data[index + 2] = zValue;
				mesh->data[index + 3] = 0;
				mesh->data[index + 4] = 0;
				mesh->data[index + 5] = normal.x;
				mesh->data[index + 6] = normal.y;
				mesh->data[index + 7] = normal.z;

				// V2
				mesh->data[index + 8] = xPlusTileSize;
				mesh->data[index + 9] = perlinValues[(int)(x + 1)][(int)(z)];
				mesh->data[index + 10] = zValue;
				mesh->data[index + 11] = 1;
				mesh->data[index + 12] = 0;
				mesh->data[index + 13] = normal.x;
				mesh->data[index + 14] = normal.y;
				mesh->data[index + 15] = normal.z;

				// V3
				mesh->data[index + 16] = xValue;
				mesh->data[index + 17] = perlinValues[(int)(x)][(int)(z + 1)];
				mesh->data[index + 18] = zPlusTileSize;
				mesh->data[index + 19] = 0;
				mesh->data[index + 20] = 1;
				mesh->data[index + 21] = normal.x;
				mesh->data[index + 22] = normal.y;
				mesh->data[index + 23] = normal.z;

				// V2
				mesh->data[index + 24] = xPlusTileSize;
				mesh->data[index + 25] = perlinValues[(int)(x + 1)][(int)(z)];
				mesh->data[index + 26] = zValue;
				mesh->data[index + 27] = 1;
				mesh->data[index + 28] = 0;
				mesh->data[index + 29] = normal.x;
				mesh->data[index + 30] = normal.y;
				mesh->data[index + 31] = normal.z;

				// V4
				mesh->data[index + 32] = xPlusTileSize;
				mesh->data[index + 33] = perlinValues[(int)(x + 1)][(int)(z + 1)];
				mesh->data[index + 34] = zPlusTileSize;
				mesh->data[index + 35] = 1;
				mesh->data[index + 36] = 1;
				mesh->data[index + 37] = normal.x;
				mesh->data[index + 38] = normal.y;
				mesh->data[index + 39] = normal.z;

				// V3
				mesh->data[index + 40] = xValue;
				mesh->data[index + 41] = perlinValues[(int)(x)][(int)(z + 1)];
				mesh->data[index + 42] = zPlusTileSize;
				mesh->data[index + 43] = 0;
				mesh->data[index + 44] = 1;
				mesh->data[index + 45] = normal.x;
				mesh->data[index + 46] = normal.y;
				mesh->data[index + 47] = normal.z;

				index += 48;
			}
		}


		mesh->vertexSizeInBytes = sizeof(float) * 8; // pos + uv + normal
		mesh->setTotalAmountOfVertices();
		mesh->setTotalSize();

		for (int x = 0; x < amountOfTileX; x++)
		{
			delete perlinValues[x];
		}
		delete perlinValues;
	}

	void TerrainGenerator::generateFlatTerrain(Mesh * mesh, uint32 sizeX, uint32 sizeZ, int height)
	{
		float tileSize = 0.5f;

		int amountOfTileX = sizeX / tileSize;
		int amountOfTileZ = sizeZ / tileSize;

		MeshPart part = {};
		part.vertexOffset = 0;
		part.vertexCount = amountOfTileX * amountOfTileZ * 6;
		mesh->parts.push_back(part);

		mesh->data.resize(amountOfTileX * amountOfTileZ * 48);


		int index = 0;
		float zValue = 0;
		float xValue = 0;
		vec3 normal(0, 1, 0);
		for (int z = 0; z < amountOfTileZ; z++, zValue += tileSize)
		{
			const float zPlusTileSize = zValue + tileSize;
			xValue = 0;
			for (int x = 0; x < amountOfTileX; x++, xValue += tileSize)
			{
				const float xPlusTileSize = xValue + tileSize;


				//V1
				mesh->data[index + 0] = xValue;
				mesh->data[index + 1] = height;
				mesh->data[index + 2] = zValue;
				mesh->data[index + 3] = 0;
				mesh->data[index + 4] = 0;
				mesh->data[index + 5] = normal.x;
				mesh->data[index + 6] = normal.y;
				mesh->data[index + 7] = normal.z;

				// V3
				mesh->data[index + 8] = xValue;
				mesh->data[index + 9] = height;
				mesh->data[index + 10] = zPlusTileSize;
				mesh->data[index + 11] = 0;
				mesh->data[index + 12] = 1;
				mesh->data[index + 13] = normal.x;
				mesh->data[index + 14] = normal.y;
				mesh->data[index + 15] = normal.z;

				// V2
				mesh->data[index + 16] = xPlusTileSize;
				mesh->data[index + 17] = height;
				mesh->data[index + 18] = zValue;
				mesh->data[index + 19] = 1;
				mesh->data[index + 20] = 0;
				mesh->data[index + 21] = normal.x;
				mesh->data[index + 22] = normal.y;
				mesh->data[index + 23] = normal.z;

				// V2
				mesh->data[index + 24] = xPlusTileSize;
				mesh->data[index + 25] = height;
				mesh->data[index + 26] = zValue;
				mesh->data[index + 27] = 1;
				mesh->data[index + 28] = 0;
				mesh->data[index + 29] = normal.x;
				mesh->data[index + 30] = normal.y;
				mesh->data[index + 31] = normal.z;

				// V3
				mesh->data[index + 32] = xValue;
				mesh->data[index + 33] = height;
				mesh->data[index + 34] = zPlusTileSize;
				mesh->data[index + 35] = 0;
				mesh->data[index + 36] = 1;
				mesh->data[index + 37] = normal.x;
				mesh->data[index + 38] = normal.y;
				mesh->data[index + 39] = normal.z;

				// V4
				mesh->data[index + 40] = xPlusTileSize;
				mesh->data[index + 41] = height;
				mesh->data[index + 42] = zPlusTileSize;
				mesh->data[index + 43] = 1;
				mesh->data[index + 44] = 1;
				mesh->data[index + 45] = normal.x;
				mesh->data[index + 46] = normal.y;
				mesh->data[index + 47] = normal.z;

				index += 48;
			}
		}


		mesh->vertexSizeInBytes = sizeof(float) * 8; // pos + uv + normal
		mesh->setTotalAmountOfVertices();
		mesh->setTotalSize();
	}

}