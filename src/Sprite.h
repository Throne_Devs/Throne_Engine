#pragma once
#include "stdafx.h"

#include "BorderImage.h"
#include "ImageSampler.h"

namespace ThroneEngine
{
	class Texture;

	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		bool initialize();
		bool initialize(const std::string& filePath);
		bool loadTextureFromFile(const std::string& filePath);

		ImageSampler* sampler = nullptr;
		BorderImage borderImage;

	};
}
