#pragma once
#include "VulkanFunctions.h"
#include "SemaphorePool.h"
#include <Glm/matrix.hpp>

#include <unordered_map>
#include <vector>
#include <array>
#include <deque>

namespace ThroneEngine
{
	class CommandBuffer;

	struct QueueInfo
	{
		uint32 familyIndex;
		std::vector<float> priorities;
	};

	struct WaitSemaphoreInfo
	{
		SemaphorePool semaphores;
		std::vector<VkPipelineStageFlags> waitingStage;
	};

	//struct BufferTransition
	//{
	//	VkBuffer buffer;
	//	VkAccessFlags currentAccess;
	//	VkAccessFlags newAccess;
	//	uint32 currentQueueFamily;
	//	uint32 newQueueFamily;
	//};
	//
	//struct ImageTransition
	//{
	//	VkImage image;
	//	VkAccessFlags currentAccess;
	//	VkAccessFlags newAccess;
	//	VkImageLayout currentLayout;
	//	VkImageLayout newLayout;
	//	uint32 currentQueueFamily;
	//	uint32 newQueueFamily;
	//	VkImageAspectFlags aspect;
	//};

	struct DescriptorInfos
	{
		VkDescriptorSet descriptorSet;
		uint32 descriptorBinding;
		uint32 arrayElement;
	};

	struct ImageDescriptorInfo
	{
		DescriptorInfos targetInfos;
		VkDescriptorType targetDescriptorType;
		std::vector<VkDescriptorImageInfo> imageInfos;
	};

	struct BufferDescriptorInfo
	{
		DescriptorInfos targetInfos;
		VkDescriptorType targetDescriptorType;
		std::vector<VkDescriptorBufferInfo> bufferInfos;
	};

	struct TexelBufferDescriptorInfo
	{
		DescriptorInfos targetInfos;
		VkDescriptorType targetDescriptorType;
		std::vector<VkBufferView> texelBufferViews;
	};

	struct CopyDescriptorInfo
	{
		DescriptorInfos targetInfos;
		DescriptorInfos sourceInfos;
		uint32 descriptorCount;
	};

	struct SubpassParameters
	{
		VkPipelineBindPoint pipelineType;
		std::vector<VkAttachmentReference> inputAttachments;
		std::vector<VkAttachmentReference> colorAttachments;
		std::vector<VkAttachmentReference> resolveAttachments;
		VkAttachmentReference depthStencilAttachment;
		std::vector<uint32> preserveAttachments;
	};

	struct ShaderStageParameters
	{
		VkShaderStageFlagBits shaderStage;
		VkShaderModule shaderModule;
		char const * entryPointName;
		VkSpecializationInfo const * specializationInfo;
	};

	struct ViewportInfo
	{
		std::vector<VkViewport> viewports;
		std::vector<VkRect2D> scissors;
	};

	struct GraphicsPipelineDynamicState
	{
		virtual void recordCommands(CommandBuffer* commandBuffer) = 0;
	};

	struct GraphicsPipelineViewportDynamicState : public GraphicsPipelineDynamicState
	{
		VkViewport viewport;
		void recordCommands(CommandBuffer* commandBuffer) override;
	};

	struct GraphicsPipelineScissorDynamicState : public GraphicsPipelineDynamicState
	{
		VkRect2D scissor;
		void recordCommands(CommandBuffer* commandBuffer) override;
	};

	struct VertexBufferBlock
	{
		VertexBufferBlock();
		VertexBufferBlock(uint32 typeSize, uint32 count, uint32 shaderLocation, VkFormat format);
		uint32 typeSize;
		uint32 count;
		uint32 shaderLocation;
		VkFormat format;
	};

	struct VertexBufferSignature
	{
		VertexBufferSignature();
		VertexBufferSignature(uint32 bindingIndex, VkVertexInputRate inputRate, std::vector<VertexBufferBlock> blocks);
		uint32 bindingIndex;
		VkVertexInputRate inputRate;
		std::vector<VertexBufferBlock> blocks;
	};

	struct GraphicsPipelineRasterizerCreationParams
	{
		bool depthClampEnable;
		bool rasterizerDiscardEnable;
		bool depthBiasEnable;
		float depthBiasConstantFactor;
		float depthBiasClamp;
		float depthBiasSlopeFactor;
		float lineWidth;
		VkPolygonMode polygonMode;
		VkCullModeFlags cullingMode;
		VkFrontFace frontFace;
	};

	struct GraphicsPipelineMultiSampleCreationParams
	{
		VkSampleCountFlagBits sampleCount;
		bool perSampleShadingEnable;
		float minSampleShading;
		VkSampleMask* sampleMasks;
		bool alphaToCoverageEnable;
		bool alphaToOneEnable;
	};

	struct GraphicsPipelineDepthStencilCreationParams
	{
		bool depthTestEnable;
		bool depthWriteEnable;
		VkCompareOp depthCompareOp;
		bool depthBoundTestEnable;
		float minDepthBounds;
		float maxDepthBounds;
		bool stencilTestEnable;
		VkStencilOpState frontStencilTestParameters;
		VkStencilOpState backStencilTestParameters;
	};

	struct GraphicsPipelineBlendlCreationParams
	{
		bool blendLogicOpEnable;
		VkLogicOp blendLogicOp;
		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates;
		std::array<float, 4> blendConstants;
	};

	class Shader;
	class DescriptorSetLayout;

	struct GraphicsPipelineCreationParams
	{
		std::vector<DescriptorSetLayout> descriptorSetLayouts;
		std::vector<VkPushConstantRange> pushConstantRanges;

		std::vector<Shader> shaders;

		std::vector<VertexBufferSignature> vertexInputSignatures;
		//std::vector<VkVertexInputBindingDescription> vertexInputBindingDescriptions;
		//std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescriptions;

		VkPrimitiveTopology primitiveType;
		bool primitiveRestartEnable;

		VkViewport viewport;
		VkRect2D scissor;

		std::vector<VkDynamicState> dynamicStates;
		std::unordered_map<VkDynamicState, std::vector<GraphicsPipelineDynamicState*>> dynamicStatesData;

		GraphicsPipelineRasterizerCreationParams rasterizerCreationParams;
		GraphicsPipelineMultiSampleCreationParams multisampleCreationParams;
		GraphicsPipelineDepthStencilCreationParams depthStencilParams;
		GraphicsPipelineBlendlCreationParams blendCreationParams;
	};

	struct DirectionalLight
	{
		glm::vec4 position;
		glm::vec4 direction;
	};

	struct MeshPart
	{
		uint32 vertexOffset;
		uint32 vertexCount;
	};

	class GraphicsPipeline;
	class Material;
	class RenderPass;
	class DescriptorSet;
	class RenderedObject;
	class CommandBuffer;

	struct RenderedObjectPart
	{
		RenderedObjectPart();
		RenderedObjectPart(MeshPart meshPart, Material* material, GraphicsPipeline* graphicsPipeline, bool isActive = true);
		MeshPart meshPart;
		Material* material = nullptr;
		GraphicsPipeline* graphicsPipeline;
		bool isActive = true;
	};

	struct RenderedObjectCommandRecorder
	{
		RenderedObjectCommandRecorder();
		RenderedObjectCommandRecorder(RenderedObjectPart* renderedObjectPart, RenderedObject* renderedObject);
		RenderedObjectPart* renderedObjectPart;
		RenderedObject* renderedObject;

		void recordCommands(CommandBuffer& commandBuffer);
	};

	struct RenderedObjectRenderPassInfo
	{
		RenderedObjectRenderPassInfo();
		RenderedObjectRenderPassInfo(std::deque<RenderedObjectPart>& renderedObjectParts, RenderPass* renderPass,
			std::vector<DescriptorSet*>& batchableDescriptorSets = std::vector<DescriptorSet*>{});
		std::deque<RenderedObjectPart> renderedObjectParts;
		std::vector<DescriptorSet*> batchableDescriptorSets;
		RenderPass* renderPass;
	};




}

