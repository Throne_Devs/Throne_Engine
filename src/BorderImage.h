#pragma once
#include "stdafx.h"

#include "SliceScaling.h"

namespace ThroneEngine 
{
	class Image;
	class Mesh;

	class BorderImage
	{
	public:
		BorderImage();
		~BorderImage();

		uvec2 getSize() const;
		void setSize(const uvec2& newSize);
		void setSize(const uint width, const uint height);

		void setImage(const Image& image, const uint32 border);

		void setBorders(const uvec2& size, const uint leftBorder = 0, const uint rightBorder = 0, const uint topBorder = 0, const uint bottomBorder = 0);
		void setBorders(const uint leftBorder = 0, const uint rightBorder = 0, const uint topBorder = 0, const uint bottomBorder = 0);
		void setLeftBorder(const uint leftBorder);
		void setRightBorder(const uint rightBorder);
		void setTopBorder(const uint topBorder);
		void setBottomBorder(const uint bottomBoder);

		uint leftBorder() const;
		uint rightBorder() const;
		uint topBorder() const;
		uint bottomBorder() const;
		
		void updateVertices(SliceScaling sliceScaling = Slice9) const;
		
		Mesh* mesh = nullptr;

	private:
		const Image* image = nullptr;		

		uint left;
		uint right;
		uint top;
		uint bottom;
		vec2 size;
	};
}
