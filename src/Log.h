#pragma once

#include <string>
#include <vector>

namespace ThroneEngine
{
	class Log
	{
	public:
		static void write(std::string type, std::string message);
		static void appendLog();

	private:
		static std::vector<std::string> logs;
	};
}