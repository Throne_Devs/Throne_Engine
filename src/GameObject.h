#pragma once
#include "stdafx.h"

#include "Transform.h"
#include "ImageSampler.h"

namespace ThroneEngine {
	struct Mesh;
}

class GameObject
{
public:
	GameObject();
	GameObject(const vec3& position, const vec3& rotation, vec3& scale);
	virtual ~GameObject();

	Transform* transform;
	RenderedObject* renderedObject;
};
