#pragma once
#include "stdafx.h"

#include "ImageSampler.h"
#include "CommandPool.h"
#include "ThroneFramework.h"

using ThroneEngine::Utility::getVectorDataPointer;

namespace ThroneEngine
{

	ImageSampler::ImageSampler()
	{
	}


	ImageSampler::~ImageSampler()
	{
	}

	bool ImageSampler::initialize(std::string imagePath, CommandQueue& queue)
	{
		dedicatedImage = true;
		image = new Image();

		if (initializeSampler() == false)
		{
			return false;
		}

		if (image->initializeAs2DSampledImage(imagePath, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT, VK_FORMAT_R8G8B8A8_UNORM, queue) == false)
		{
			return false;
		}

		setDescriptorType();
		descriptorShaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
		descriptorCount = image->imageViewHandles.size();

		return true;
	}

	bool ImageSampler::initialize(Image* image, bool dedicatedImage)
	{
		this->dedicatedImage = dedicatedImage;

		image = image;
		if (initializeSampler() == false)
		{
			return false;
		}

		setDescriptorType();
		descriptorShaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
		descriptorCount = image->imageViewHandles.size();

		return true;
	}


	bool ImageSampler::initialize(VkExtent3D& imageDimensions, VkImageUsageFlags usage, VkImageAspectFlags imageAspect, VkFormat imageFormat, uint32 numberOfLayer, VkImageViewType viewType)
	{
		dedicatedImage = true;
		image = new Image();

		initializeSampler();

		if (image->initializeAs2DSampledImage(imageDimensions, usage, imageAspect, imageFormat, numberOfLayer, viewType) == false)
		{
			return false;
		}

		setDescriptorType();
		descriptorShaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
		descriptorCount = image->imageViewHandles.size();

		return true;
	}

	bool ImageSampler::initialize(PixelMap& imageData, CommandQueue& queue)
	{
		dedicatedImage = true;
		image = new Image();

		VkExtent3D imageDimensions = {};
		imageDimensions.width = imageData.dimensionsInPixel.x;
		imageDimensions.height = imageData.dimensionsInPixel.y;
		imageDimensions.depth = imageData.dimensionsInPixel.z;

		initializeSampler();

		if (image->initializeAs2DSampledImage(imageDimensions, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT,
			VK_FORMAT_R8G8B8A8_UNORM, 1, VK_IMAGE_VIEW_TYPE_2D) == false)
		{
			return false;
		}

		image->updateImageMemory(imageData.pixels, queue, 0, imageDimensions, VK_IMAGE_ASPECT_COLOR_BIT);

		setDescriptorType();
		descriptorShaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
		descriptorCount = image->imageViewHandles.size();

		return true;
	}

	void ImageSampler::destroy()
	{
		if (samplerHandle != VK_NULL_HANDLE)
		{
			vkDestroySampler(logicalDevice, samplerHandle, nullptr);
			samplerHandle = VK_NULL_HANDLE;
		}
		if (dedicatedImage == true)
		{
			image->destroy();
		}
	}

	bool ImageSampler::initializeAsCubeMap(std::vector<std::string> imagePaths, CommandQueue& queue)
	{
		dedicatedImage = true;
		image = new Image();

		if (initializeSampler() == false)
		{
			return false;
		}

		if (image->initializeAs2DCubeMapSampledImage(imagePaths, VK_FORMAT_R8G8B8A8_UNORM, 1, 6,
			VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_ASPECT_COLOR_BIT, queue) == false)
		{
			return false;
		}

		setDescriptorType();
		descriptorShaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
		descriptorCount = image->imageViewHandles.size();

		return true;
	}

	void ImageSampler::initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding)
	{
		initializeGenericWriteDescriptorSet(descriptorSetHandle, binding);

		VkDescriptorImageInfo* imageInfos = new VkDescriptorImageInfo[image->imageViewHandles.size()];

		for (int i = 0; i < image->imageViewHandles.size(); i++)
		{
			imageInfos[i].sampler = samplerHandle;
			imageInfos[i].imageView = image->imageViewHandles[i];
			imageInfos[i].imageLayout = image->imageLayout;
		}

		if (writeDescriptorSet.pImageInfo != nullptr)
		{
			delete[] writeDescriptorSet.pImageInfo;
		}

		writeDescriptorSet.pImageInfo = imageInfos;
		writeDescriptorSet.pBufferInfo = nullptr;
		writeDescriptorSet.pTexelBufferView = nullptr;

		writeDescriptorSet.descriptorType = descriptorType;
		writeDescriptorSet.descriptorCount = descriptorCount;
	}

	VkExtent3D ImageSampler::getSize() const
	{
		return image->dimensions;
	}

	VkDescriptorSetLayoutBinding ImageSampler::getLayoutBinding(VkShaderStageFlags shaderStage, uint32 descriptorCount)
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		binding.descriptorCount = descriptorCount; // for arrays of descriptor (example arrays of imageSampler)
		binding.stageFlags = shaderStage;
		binding.pImmutableSamplers = nullptr;
		return binding;
	}

	inline bool ImageSampler::initializeSampler()
	{
		return initializeSampler(VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR, VK_SAMPLER_ADDRESS_MODE_REPEAT, 0.0f, false, 16.0f, false, VK_COMPARE_OP_ALWAYS, 0.0f, 1.0f, VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK, false);
	}

	inline bool ImageSampler::initializeSampler(VkFilter minFilter, VkFilter magFilter, VkSamplerMipmapMode mipmapMode, VkSamplerAddressMode adressMode, float mipLoadBias, bool anisotropyEnabled, float maxAnisotropy, bool compareEnable, VkCompareOp compareFunction, float minLod, float maxLod, VkBorderColor borderColor, bool unnormalizedCoordinates)
	{
		VkSamplerCreateInfo samplerCreateInfos = {};
		samplerCreateInfos.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerCreateInfos.pNext = nullptr;
		samplerCreateInfos.flags = 0;
		samplerCreateInfos.magFilter = magFilter;
		samplerCreateInfos.minFilter = minFilter;
		samplerCreateInfos.mipmapMode = mipmapMode;
		samplerCreateInfos.addressModeU = adressMode;
		samplerCreateInfos.addressModeV = adressMode;
		samplerCreateInfos.addressModeW = adressMode;
		samplerCreateInfos.mipLodBias = mipLoadBias;
		samplerCreateInfos.anisotropyEnable = anisotropyEnabled;
		samplerCreateInfos.maxAnisotropy = maxAnisotropy;
		samplerCreateInfos.compareEnable = compareEnable;
		samplerCreateInfos.compareOp = compareFunction;
		samplerCreateInfos.minLod = minLod;
		samplerCreateInfos.maxLod = maxLod;
		samplerCreateInfos.borderColor = borderColor;
		samplerCreateInfos.unnormalizedCoordinates = unnormalizedCoordinates;

		VkResult result = vkCreateSampler(logicalDevice, &samplerCreateInfos, nullptr, &samplerHandle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createSampler);
			#endif
			return false;
		}
		#endif
		return true;
	}

	void ImageSampler::setDescriptorType()
	{
		descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	}

}