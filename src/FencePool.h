#pragma once
#include "stdafx.h"

namespace ThroneEngine
{

	class FencePool
	{
	public:
		FencePool();
		~FencePool();

		std::vector<VkFence> fenceHandles;


		bool initialize(int numberOfFence);
		bool addFences(int numberOfFence);
		bool waitForFences(VkBool32 waitForAllFences, double timeout);
		bool resetFences();
	};

}