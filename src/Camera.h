#pragma once
#include "stdafx.h"
#include "Transform.h"

namespace ThroneEngine
{
	class Camera
	{
	public:
		Camera();
		Camera(vec3& position, vec3& viewPoint, vec3& up);
		Camera(glm::vec3& position, glm::vec3& viewPoint, glm::vec3& up, float fov, float nearPlane, float farPlane);
		~Camera();

		void move(vec4 direction, float deltaTime) const;
		void lookAt(mat4& modelView) const;
		void moveOrientation(int xrel, int yRel);
		void setPosition(vec3& position, vec3& viewPoint, vec3& up);

		Transform* transform;
		vec4 orientation;

		float fov = 0;
		float nearPlane = 0;
		float farPlane = 0;

	private:
		const int MAX_DEGREE = 89;
		const float ROTATE_SENSIBILITY = 0.05;
		const float MOVE_SENSIBILITY = 20;

		float phi = 0;
		float theta = 0;		

		void setViewPoint(vec3 viewPoint);
	};
}
