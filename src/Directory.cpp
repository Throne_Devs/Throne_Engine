#include "stdafx.h"

#include "Directory.h"

using namespace ThroneEngine;
using namespace std::experimental;

Directory::Directory() = default;

Directory::Directory(const std::string& directoryPath)
{
	setDirectory(directoryPath);
}

bool Directory::createDirectory(const std::string& directoryPath)
{
	return filesystem::create_directories(directoryPath);
}

void Directory::setDirectory(const std::string& directoryPath)
{
	directory = directoryPath;
	if (!exist())
	{
		createDirectory(directory);
	}
}

bool Directory::exist() const
{
	return filesystem::exists(directory);
}

bool Directory::exist(const std::string& directoryPath)
{
	Directory d(directoryPath);
	return d.exist();
}

bool Directory::deleteDirectory() const
{
	return filesystem::remove_all(directory);
}

bool Directory::deleteDirectory(const std::string& directoryPath)
{
	Directory d(directoryPath);
	return d.deleteDirectory();
}

std::vector<std::string> Directory::getDirectories() const
{
	return getDirectories(directory);
}

void Directory::getDirectories(std::vector<std::string>& result) const
{
	getDirectories(directory, result);
}

std::vector<std::string> Directory::getDirectories(const std::string& directoryPath)
{
	std::vector<std::string> directories;
	getDirectories(directoryPath, directories);
	return directories;
}

void Directory::getDirectories(const std::string& directoryPath, std::vector<std::string>& result)
{
	const filesystem::path p(directoryPath);
	const filesystem::directory_iterator start(p);
	const filesystem::directory_iterator end;

	std::transform(start, end, std::back_inserter(result),
		[](const filesystem::directory_entry& entry) -> std::string
	{
		if (!entry.path().has_extension())
		{
			return entry.path().filename().string();
		}
		return "";
	});

	result.erase(std::remove_if(result.begin(), result.end(),
		[](const std::string& entry) -> bool
	{
		return entry == "";
	}),
		result.end());
}

std::vector<std::string> Directory::getFiles() const
{
	return getFiles(directory);
}

void Directory::getFiles(std::vector<std::string>& result) const
{
	getFiles(directory, result);
}

std::vector<std::string> Directory::getFiles(const std::string& directoryPath)
{
	std::vector<std::string> files;
	getFiles(directoryPath, files);
	return files;
}

void Directory::getFiles(const std::string& directoryPath, std::vector<std::string>& result)
{
	const filesystem::path p(directoryPath);
	const filesystem::directory_iterator start(p);
	const filesystem::directory_iterator end;

	std::transform(start, end, std::back_inserter(result),
		[](const filesystem::directory_entry& entry) -> std::string
	{
		if (entry.path().has_extension())
		{
			return entry.path().filename().string();
		}
		return "";
	});

	result.erase(std::remove_if(result.begin(), result.end(),
		[](const std::string& entry) -> bool
	{
		return entry == "";
	}),
		result.end());
}

std::vector<std::string> Directory::getFilesWithExt(const std::string& extension) const
{
	return getFilesWithExt(directory, extension);
}

void Directory::getFilesWithExt(const std::string& extension, std::vector<std::string>& result) const
{
	getFilesWithExt(directory, extension, result);
}

std::vector<std::string> Directory::getFilesWithExt(const std::string& directoryPath, const std::string& extension)
{
	std::vector<std::string> files;
	getFilesWithExt(directoryPath, extension, files);
	return files;
}

void Directory::getFilesWithExt(const std::string& directoryPath, const std::string& extension, std::vector<std::string>& result)
{
	getFiles(directoryPath, result);

	for (int i = 0; i < result.size();)
	{
		if (result[i].find(extension) == std::string::npos)
		{
			result.erase(result.begin() + i);
		}
		else
		{
			i++;
		}
	}
}

std::string Directory::getCurrentDirectory()
{
	return filesystem::current_path().string();
}

void Directory::move(const std::string& newDirectory) const
{
	move(directory, newDirectory);
}

void Directory::move(const std::string& directoryPath, const std::string& newDirectory)
{
	copy(directoryPath, newDirectory);
	deleteDirectory(directoryPath);
}

void Directory::move(Directory& directory, const std::string& newDirectory)
{
	move(directory.directory, newDirectory);
}

void Directory::copy(const std::string& newDirectory) const
{
	copy(directory, newDirectory);
}

void Directory::copy(const std::string& directoryPath, const std::string& newDirectory)
{
	const std::string dir = newDirectory + "/" + directoryPath;
	createDirectory(dir);
	filesystem::copy(directoryPath, dir, filesystem::copy_options::recursive);
}

void Directory::copy(Directory& directory, const std::string& newDirectory)
{
	copy(directory.directory, newDirectory);
}

std::vector<std::string> Directory::getFileSystemEntries() const
{
	return getFileSystemEntries(directory);
}

void Directory::getFileSystemEntries(std::vector<std::string>& result) const
{
	getFileSystemEntries(directory, result);
}

std::vector<std::string> Directory::getFileSystemEntries(const std::string& directoryPath)
{
	std::vector<std::string> result;
	getFileSystemEntries(directoryPath, result);
	return result;
}

void Directory::getFileSystemEntries(const std::string& directoryPath, std::vector<std::string>& result)
{
	const filesystem::path p(directoryPath);
	const filesystem::directory_iterator start(p);
	const filesystem::directory_iterator end;

	std::transform(start, end, std::back_inserter(result),
		[](const filesystem::directory_entry& entry) -> std::string
	{
		return entry.path().filename().string();
	});
}
