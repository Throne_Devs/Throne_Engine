#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"
#include "CommandQueue.h"

namespace ThroneEngine
{
	class GpuBuffer
	{
	public:
		GpuBuffer();
		~GpuBuffer();

		VkBuffer handle = VK_NULL_HANDLE;
		VkBufferView viewHandle = VK_NULL_HANDLE;
		VkDeviceMemory memoryObject = VK_NULL_HANDLE;
		VkMemoryPropertyFlags memoryProperties = 0;
		uint32 currentBufferSizeInBytes = 0;

		// Only used for non device local memory
		void* mappedMemory = nullptr;
		VkMappedMemoryRange memoryRange;
		//

		bool initialize(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage);		
		void destroy();
		void destroyBufferView();

		bool allocateMemory(VkMemoryPropertyFlags properties);
		bool allocateMemory(VkMemoryPropertyFlags properties, VkDeviceMemory& memoryObject);
		bool createView(VkFormat format, VkDeviceSize memoryOffset, VkDeviceSize memorySize);
		bool mapHostVisibleMemory(VkDeviceSize offset, VkDeviceSize dataSize, void* data, bool unMapWhenFinished);
		bool mapHostVisibleMemory(VkDeviceMemory& memoryObject, VkDeviceSize offset, VkDeviceSize dataSize, void* data, bool unMapWhenFinished);
		bool updateHostVisibleMemory(VkDeviceSize offset, VkDeviceSize dataSize, void* data);
		void copyDataFromBufferToBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, std::vector<VkBufferCopy>& regions);
		void copyDataFromBufferToBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkBufferCopy* regions, uint32 numberOfRegion);

		void setMemoryBarrier(
			CommandBuffer& commandBuffer,
			std::vector<VkBufferMemoryBarrier>& bufferBarriers, 
			VkPipelineStageFlags currentPipelineStage, 
			VkPipelineStageFlags newPipelineStage);

		void setMemoryBarrier(
			CommandBuffer& commandBuffer,
			VkBufferMemoryBarrier* bufferBarrier,
			uint32 numberOfBarrier,
			VkPipelineStageFlags currentPipelineStage,
			VkPipelineStageFlags newPipelineStage);

		bool updateWithDeviceLocalMemory(
			CommandBuffer& commandBuffer,
			void * data, 
			VkDeviceSize dataSize, 
			VkDeviceSize dstOffset, 
			VkAccessFlags dstCurrentAccess,
			VkAccessFlags dstNewAccess, 
			VkPipelineStageFlags dstCurrentStage,
			VkPipelineStageFlags dstNewStage,
			CommandQueue& queue, 
			SemaphorePool& semaphoresToSignal);
		
	};
}


