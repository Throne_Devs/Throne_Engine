#pragma once
#include "stdafx.h"

#include "Descriptor.h"
#include "ImageSampler.h"

namespace ThroneEngine
{

	class ImageSamplerArray : public Descriptor
	{
	public:
		ImageSamplerArray();
		~ImageSamplerArray();

		std::vector<ImageSampler*> samplers;
		
		void initialize(uint32 descriptorCount, ImageSampler* samplers);

		void destroy();

		virtual void initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding) override;
		virtual void setDescriptorType() override;
	};

}