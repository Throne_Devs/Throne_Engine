#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"

namespace ThroneEngine
{

	class Descriptor
	{
	public:
		Descriptor();
		~Descriptor();

		VkDescriptorType descriptorType = VK_DESCRIPTOR_TYPE_MAX_ENUM;
		VkShaderStageFlags descriptorShaderStage = VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
		uint32 descriptorCount = 1;
		VkWriteDescriptorSet writeDescriptorSet;

		virtual void initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding) = 0;

	protected:
		void initializeGenericWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding);
		virtual void setDescriptorType() = 0;
	};

}