#pragma once
#include "stdafx.h"

#include "Event.h"

namespace ThroneEngine
{
	class Window;

	class Mouse
	{
	public:
		enum Button
		{
			Unknow = -1,
			Left = 0,
			Right,
			Middle,
			XButton1,
			XButton2,

			ButtonCount
		};
		enum Wheel
		{
			VerticalWheel,
			HorizontalWheel
		};

		static std::vector<std::string> buttonToString;
		static std::vector<std::string> wheelToString;
		
		static std::map<Button, Event<void>> onButtonPressed;
		static std::map<Button, Event<void>> onButtonMaintained;
		static std::map<Button, Event<void>> onButtonReleased;
		static Event<void, const vec2&> onMoved;
		
		static vec2 getPosition();
		static vec2 getPosition(const Window& relativeTo);
		static void setPosition(const vec2& position);
		static void setPosition(const vec2& position, const Window& relativeTo);
		
		static void notifyButtonPressed();
		static void notifyButtonMaintain();
		static void notifyButtonReleased();
		static void notifyMove();

	private:
		static bool isButtonPressed(Button button);
		static void checkIsButtonPressed(Button button);
	};
}
