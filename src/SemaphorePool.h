#pragma once
#include "stdafx.h"

#include "Semaphore.h"

namespace ThroneEngine
{

	class SemaphorePool
	{
	public:
		SemaphorePool();
		~SemaphorePool();

		std::vector<VkSemaphore> semaphoreHandles;

		bool initialize(int numberOfSemaphore);
		bool addSemaphores(int numberOfSemaphore);
	};

}