#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"
#include "Descriptor.h"
#include "DescriptorSetLayout.h"
#include "Bindable.h"

namespace ThroneEngine
{
	class DescriptorSet : public Bindable
	{
	public:
		DescriptorSet();
		~DescriptorSet();

		std::vector<Descriptor*> descriptors;
		DescriptorSetLayout setLayout;
		VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
		bool dedicatedPool;
		VkDescriptorSet handle = VK_NULL_HANDLE;
		uint32 descriptorSetLocation = 0;

		bool initialize(std::vector<Descriptor*>& descriptors, DescriptorSetLayout& setLayout, uint32 descriptorSetLocation);
		bool initialize(Descriptor** descriptors, uint32 amountOfResource, DescriptorSetLayout& setLayout, uint32 descriptorSetLocation);

		void addDescriptors(Descriptor** descriptors, uint32 amountOfResource);

		bool createDescriptorPool(std::vector<VkDescriptorPoolSize>& descriptorTypes, int maxSets, bool descriptorSetsCanBeFreedIndividualy, VkDescriptorPool& descriptorPool);
		bool allocateDescriptorSets(VkDescriptorPool descriptorPool, std::vector<VkDescriptorSetLayout>& descriptorSetLayouts);
		bool allocateDescriptorSets(VkDescriptorPool descriptorPool, VkDescriptorSetLayout* descriptorSetLayouts, uint32 amountOfSetLayouts);
		void updateDescriptorSets(std::vector<VkWriteDescriptorSet>* writeDescriptorSets, std::vector<VkCopyDescriptorSet>* copyDescriptorSets);
		void updateDescriptorSets();


		virtual void bind(BindableBindingInfo& bindingInfo) override;

	};

	inline void DescriptorSet::bind(BindableBindingInfo& bindingInfo)
	{
		bindingInfo.commandBuffer->cmdBindDescriptorSets(VK_PIPELINE_BIND_POINT_GRAPHICS, bindingInfo.pipelineLayout, descriptorSetLocation, 1, &handle, 0, nullptr);
	}
}



