#pragma once
#include "UtilityFunctions.h"

using Utility::getVectorDataPointer;

namespace ThroneEngine
{
	namespace Utility
	{
		template<class T>
		inline T * getVectorDataPointer(std::vector<T>& vector)
		{
			return vector.size() > 0 ? &vector[0] : nullptr;
		}

		template<class T>
		T computeCrossXVec3(T v1y, T v1z, T v2y, T v2z)
		{
			return (v1y * v2z) - (v1z * v2y);
		}
		template<class T>
		T computeCrossYVec3(T v1x, T v1z, T v2x, T v2z)
		{
			return (v1x * v2z) - (v1z * v2x);
		}
		template<class T>
		T computeCrossZVec3(T v1x, T v1y, T v2x, T v2y)
		{
			return (v1x * v2y) - (v1y * v2x);
		}
	}
}