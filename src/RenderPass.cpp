#pragma once
#include "stdafx.h"

#include "RenderPass.h"
#include "UtilityFunctions.hpp"
#include "FrameBuffer.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	RenderPass::RenderPass()
	{
	}

	RenderPass::~RenderPass()
	{
	}

	bool RenderPass::initialize(std::vector<VkAttachmentDescription>& attachmentsDescription, std::vector<SubpassParameters>& subpassParameters, std::vector<VkSubpassDependency>& subpassDependencies)
	{
		std::vector<VkSubpassDescription> subpassDescriptions;
		specifySubpassDescriptions(subpassParameters, subpassDescriptions);

		VkRenderPassCreateInfo renderPassCreateInfos = {};
		renderPassCreateInfos.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassCreateInfos.pNext = nullptr;
		renderPassCreateInfos.flags = 0;
		renderPassCreateInfos.attachmentCount = attachmentsDescription.size();
		renderPassCreateInfos.pAttachments = getVectorDataPointer(attachmentsDescription);
		renderPassCreateInfos.subpassCount = subpassDescriptions.size();
		renderPassCreateInfos.pSubpasses = getVectorDataPointer(subpassDescriptions);
		renderPassCreateInfos.dependencyCount = subpassDependencies.size();
		renderPassCreateInfos.pDependencies = getVectorDataPointer(subpassDependencies);

		VkResult result = vkCreateRenderPass(logicalDevice, &renderPassCreateInfos, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createRenderPass);
			#endif
			return false;
		}
		#endif

		return true;
	}

	void RenderPass::begin(CommandBuffer& commandBuffer, FrameBuffer& frameBuffer, VkRect2D& renderArea, std::vector<VkClearValue>& clearValues, bool usingSecondaryCommandBuffer)
	{
		VkSubpassContents subpassContent = usingSecondaryCommandBuffer ? VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS : VK_SUBPASS_CONTENTS_INLINE;

		VkRenderPassBeginInfo beginInfos = {};
		beginInfos.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		beginInfos.pNext = nullptr;
		beginInfos.renderPass = handle;
		beginInfos.framebuffer = frameBuffer.handle;
		beginInfos.renderArea = renderArea;
		beginInfos.clearValueCount = clearValues.size();
		beginInfos.pClearValues = getVectorDataPointer(clearValues);

		commandBuffer.cmdBeginRenderPass(&beginInfos, subpassContent);
	}

	void RenderPass::progressToNextSubpass(CommandBuffer & commandBuffer, bool usingSecondaryCommandBuffer)
	{
		VkSubpassContents subpassContent = usingSecondaryCommandBuffer ? VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS : VK_SUBPASS_CONTENTS_INLINE;
		commandBuffer.cmdNextSubpass(subpassContent);
	}

	void RenderPass::end(CommandBuffer & commandBuffer)
	{
		commandBuffer.cmdEndRenderPass();
	}

	void RenderPass::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyRenderPass(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}

	void RenderPass::specifySubpassDescriptions(std::vector<SubpassParameters>& subpassParameters, std::vector<VkSubpassDescription>& subpassDescriptions)
	{
		subpassDescriptions.resize(subpassParameters.size());

		int i = 0;
		for (SubpassParameters& param : subpassParameters)
		{
			VkSubpassDescription desc = {};
			desc.flags = 0;
			desc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
			desc.inputAttachmentCount = param.inputAttachments.size();
			desc.pInputAttachments = param.inputAttachments.size() > 0 ? &param.inputAttachments[0] : nullptr;
			desc.colorAttachmentCount = param.colorAttachments.size();
			desc.pColorAttachments = param.colorAttachments.size() > 0 ? &param.colorAttachments[0] : nullptr;
			desc.pResolveAttachments = param.resolveAttachments.size() > 0 ? &param.resolveAttachments[0] : nullptr;
			desc.pDepthStencilAttachment = &param.depthStencilAttachment;
			desc.preserveAttachmentCount = param.preserveAttachments.size();
			desc.pPreserveAttachments = param.preserveAttachments.size() > 0 ? &param.preserveAttachments[0] : nullptr;

			subpassDescriptions[i] = desc;

			i++;
		}
	}
}