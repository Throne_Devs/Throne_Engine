#pragma once
#include "stdafx.h"

#include "CommandBuffer.h"

namespace ThroneEngine
{
	class FrameBuffer;

	class RenderPass
	{
	public:
		RenderPass();
		~RenderPass();

		VkRenderPass handle = VK_NULL_HANDLE;

		bool initialize(std::vector<VkAttachmentDescription>& attachmentsDescription, std::vector<SubpassParameters>& subpassParameters, std::vector<VkSubpassDependency>& subpassDependencies);
		void begin(CommandBuffer& commandBuffer, FrameBuffer& frameBuffer, VkRect2D& renderArea, std::vector<VkClearValue>& clearValues, bool usingSecondaryCommandBuffer = false);
		void progressToNextSubpass(CommandBuffer& commandBuffer, bool usingSecondaryCommandBuffer = false);
		void end(CommandBuffer& commandBuffer);
		void destroy();


	private:
		void specifySubpassDescriptions(std::vector<SubpassParameters>& subpassParameters, std::vector<VkSubpassDescription>& subpassDescriptions);
	
	};
}