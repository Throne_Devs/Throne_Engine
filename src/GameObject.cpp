#include "stdafx.h"

#include "GameObject.h"
#include "ThroneFramework.h"
#include "Mesh.h"
#include "Material.h"
#include "RenderedObject.h"

GameObject::GameObject() :
	transform(new Transform())
{
}

GameObject::GameObject(const vec3& position, const vec3& rotation, vec3& scale) :
	transform(new Transform(position, rotation, scale))
{
}

GameObject::~GameObject()
{
	delete transform;
}
