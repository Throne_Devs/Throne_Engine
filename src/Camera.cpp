#include "stdafx.h"

#include "Camera.h"
#include "Keyboard.h"

namespace ThroneEngine
{
	Camera::Camera()
	{
	}

	Camera::Camera(vec3& position, vec3& viewPoint, vec3& up)
	{
		transform = new Transform(position);
		setPosition(position, viewPoint, up);
	}

	Camera::Camera(glm::vec3& position, glm::vec3& viewPoint, glm::vec3& up, float fov, float nearPlane, float farPlane) : fov(fov), nearPlane(nearPlane), farPlane(farPlane)
	{
		transform = new Transform(position);
		setPosition(position, viewPoint, up);
	}

	Camera::~Camera()
	{

	}

	void Camera::move(vec4 direction, float deltaTime) const
	{
		*transform->position() += direction * MOVE_SENSIBILITY * deltaTime;
		*transform->forward() = *transform->position() + orientation;
	}

	void Camera::lookAt(mat4& viewMatrix) const
	{
		viewMatrix = transform->getViewMatrix();
	}

	void Camera::moveOrientation(const int xrel, const int yRel)
	{
		phi += -yRel * ROTATE_SENSIBILITY;
		theta += -xrel * ROTATE_SENSIBILITY;

		if (phi > MAX_DEGREE)
			phi = MAX_DEGREE;
		else if (phi < -MAX_DEGREE)
			phi = -MAX_DEGREE;

		const float radPhi = radians(phi);
		const float radTheta = radians(theta);

		if (transform->up()->x == 1)
		{
			orientation.x = sin(radPhi);
			orientation.y = cos(radPhi) * cos(radTheta);
			orientation.z = cos(radPhi) * sin(radTheta);
		}
		else if (transform->up()->y == 1)
		{
			orientation.x = cos(radPhi) * sin(radTheta);
			orientation.y = sin(radPhi);
			orientation.z = cos(radPhi) * cos(radTheta);
		}
		else
		{
			orientation.x = cos(radPhi) * cos(radTheta);
			orientation.y = cos(radPhi) * sin(radTheta);
			orientation.z = sin(radPhi);
		}

		*transform->right() = dvec4(cross(vec3(*transform->up()), vec3(orientation)), transform->right()->w);
		*transform->right() = normalize(*transform->right());
		*transform->forward() = *transform->position() + orientation;
	}

	void Camera::setPosition(vec3& position, vec3& viewPoint, vec3& up)
	{
		*transform->position() = vec4(position, 1.0f);
		*transform->forward() = vec4(viewPoint, 0);
		*transform->up() = vec4(up, 0);

		setViewPoint(viewPoint);
	}

	void Camera::setViewPoint(vec3 viewPoint)
	{
		orientation = *transform->forward() - *transform->position();
		orientation = normalize(orientation);

		if (transform->up()->x == 1)
		{
			phi = asin(orientation.x);
			theta = acos(orientation.y / cos(phi));
			if (orientation.y < 0)
				theta *= -1;
		}
		else if (transform->up()->y == 1)
		{
			phi = asin(orientation.y);
			theta = acos(orientation.z / cos(phi));
			if (orientation.z < 0)
				theta *= -1;
		}
		else
		{
			phi = asin(orientation.x);
			theta = acos(orientation.z / cos(phi));
			if (orientation.z < 0)
				theta *= -1;
		}

		phi = degrees(phi);
		theta = degrees(theta);
	}
}
