#include "stdafx.h"

#include "FrameBuffer.h"
#include "UtilityFunctions.hpp"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	FrameBuffer::FrameBuffer()
	{
	}


	FrameBuffer::~FrameBuffer()
	{
	}

	bool FrameBuffer::initialize(RenderPass& renderPass, std::vector<VkImageView>& attachments, VkExtent2D& frameBufferSize, uint32 layers)
	{
		VkFramebufferCreateInfo frameBufferCreateInfos = {};
		frameBufferCreateInfos.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		frameBufferCreateInfos.pNext = nullptr;
		frameBufferCreateInfos.flags = 0;
		frameBufferCreateInfos.renderPass = renderPass.handle;
		frameBufferCreateInfos.attachmentCount = attachments.size();
		frameBufferCreateInfos.pAttachments = getVectorDataPointer(attachments);
		frameBufferCreateInfos.width = frameBufferSize.width;
		frameBufferCreateInfos.height = frameBufferSize.height;
		frameBufferCreateInfos.layers = layers;

		VkResult result = vkCreateFramebuffer(logicalDevice, &frameBufferCreateInfos, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createFrameBuffer);
			#endif
			return false;
		}
		#endif

		return true;
	}

	void FrameBuffer::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyFramebuffer(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}


}