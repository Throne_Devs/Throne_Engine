#include "stdafx.h"

#include "Config.h"
#include "UtilityFunctions.h"
#include "Directory.h"
#include "File.h"

namespace ThroneEngine
{
	std::string Config::file;
	std::map<std::string, Keyboard::Key> Config::keys;
	std::map<std::string, Mouse::Button> Config::buttons;

	void Config::initialize()
	{
		file = path_configurationFilesDefault;
		//createDefaultFile();

		if (Directory::createDirectory(path_configurationFiles))
		{
			chooseFile();
		}

		readConfigFile();
	}

	void Config::readConfigFile()
	{
		chooseFile();
		std::string line;
		std::ifstream stream(file);
		while(std::getline(stream, line) && !line.empty())
		{
			readKeys(line);
			readButtons(line);
		}
	}

	void Config::readKeys(const std::string& line)
	{
		if(line.substr(0, KEYS_INDEX.size()) == KEYS_INDEX)
		{
			std::string first, second;
			extractFromQuote(line, first, second);

			for(int i = 0; i < Keyboard::keyToString.size(); i++)
			{
				if (Keyboard::keyToString[i] == second)
				{
					keys[first] = static_cast<Keyboard::Key>(i - 1);
					break;
				}
			}				
		}
	}

	void Config::readButtons(const std::string& line)
	{
		if(line.substr(0, BUTTONS_INDEX.size()) == BUTTONS_INDEX)
		{
			std::string first, second;
			extractFromQuote(line, first, second);
			for(int i = 0; i < Mouse::buttonToString.size(); i++)
			{
				if (Mouse::buttonToString[i] == second)
				{
					buttons[first] = static_cast<Mouse::Button>(i - 1);
					break;
				}
			}
		}
	}

	Keyboard::Key Config::getKey(const std::string& type)
	{
		auto pos = keys.find(type);
		if (pos != keys.end())
		{
			return pos->second;
		}
		return Keyboard::Key::Unknown;
	}

	std::vector<Keyboard::Key> Config::getKeys()
	{
		std::vector<Keyboard::Key> allKeys;
		allKeys.reserve(keys.size());
		for(const std::pair<std::string, Keyboard::Key> pair : keys)
		{
			allKeys.push_back(pair.second);
		}
		return allKeys;
	}

	Mouse::Button Config::getButton(const std::string& type)
	{
		auto pos = buttons.find(type);
		if (pos != buttons.end())
		{
			return pos->second;
		}
		return Mouse::Button::Unknow;
	}

	std::vector<Mouse::Button> Config::getButtons()
	{
		std::vector<Mouse::Button> allButtons;
		allButtons.reserve(buttons.size());
		for(const std::pair<std::string, Mouse::Button> pair : buttons)
		{
			allButtons.push_back(pair.second);
		}
		return allButtons;
	}

	void Config::createDefaultFile()
	{
		setDefaultKeysValue();
		setDefaultButtonsValue();

		std::ostringstream configText;
		createKeysLines(configText);
		createButtonsLines(configText);

		File::writeAllLines(file, configText.str());
	}

	void Config::createKeysLines(std::ostringstream& configText)
	{
		std::map<std::string, Keyboard::Key>::iterator itKey = keys.begin();
		while (itKey != keys.end())
		{
			configText << KEYS_INDEX << " \"" << itKey->first << "\"" << " " << "\"" << Keyboard::keyToString[itKey->second + 1] << "\"\n";
			itKey++;
		}
	}

	void Config::createButtonsLines(std::ostringstream& configText)
	{
		std::map<std::string, Mouse::Button>::iterator itButton = buttons.begin();
		while (itButton != buttons.end())
		{
			configText << BUTTONS_INDEX << " \"" << itButton->first << "\"" << " " << "\"" << Mouse::buttonToString[itButton->second + 1] << "\"\n";
			itButton++;
		}
	}
	
	void Config::setDefaultKeysValue()
	{
		keys[FORWARD_INDEX] = Keyboard::W;
		keys[BACKWARD_INDEX] = Keyboard::S;
		keys[LEFT_INDEX] = Keyboard::A;
		keys[RIGHT_INDEX] = Keyboard::D;
		keys[UP_INDEX] = Keyboard::Space;
		keys[DOWN_INDEX] = Keyboard::LShift;
		keys[CLOSE_WINDOW_INDEX] = Keyboard::Escape;
		keys[MOUSE_VISIBILITY_INDEX] = Keyboard::LAlt;
		keys[MOUSE_GRABED_INDEX] = Keyboard::LControl;
		keys[TELEPORT_SUN_INDEX] = Keyboard::C;
		keys[PRINT_POSITION_INDEX] = Keyboard::P;
		keys[PRINT_VIEWPOINT_INDEX] = Keyboard::V;
		keys[CHANGE_PROJECTION_VALUES1] = Keyboard::Num1;
		keys[CHANGE_PROJECTION_VALUES2] = Keyboard::Num2;
		keys[CHANGE_PROJECTION_VALUES3] = Keyboard::Num3;
		keys[CHANGE_PROJECTION_VALUES4] = Keyboard::Num4;
		keys[CHANGE_PROJECTION_VALUES5] = Keyboard::Num5;
		keys[CHANGE_LIGHT_SETUP1] = Keyboard::Numpad1;
		keys[CHANGE_LIGHT_SETUP2] = Keyboard::Numpad2;
	}

	void Config::setDefaultButtonsValue()
	{
		buttons[RESET_CAMERA_SPEED_INDEX] = Mouse::Middle;
	}

	void Config::chooseFile()
	{   
		file = path_configurationFilesConfig;
		if (!File::exist(file))
		{
			file = path_configurationFilesDefault;
			if (!File::exist(file))
			{
				createDefaultFile();
			}
		}
	}

	void Config::extractFromQuote(std::string line, std::string& first, std::string& second)
	{
		line.erase(line.begin(), line.begin() + line.find('\"') + 1);
		first = line.substr(0, line.find('\"'));

		line.erase(line.begin(), line.begin() + line.find('\"') + 3);
		second = line.substr(0, line.size() - 1);
	}
}
