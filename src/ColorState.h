#pragma once
#include "stdafx.h"

#include "Color.h"

namespace ThroneEngine
{
	struct ColorState
	{
		Color normal;
		Color highlightedColor;
		Color pressedColor;
		Color disabledColor;
	};
}