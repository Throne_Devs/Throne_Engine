#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	template<typename T>
	class Rect
	{
	public:
		Rect();
		Rect(T rectLeft, T rectTop, T rectWidth, T rectHeight);
		Rect(const glm::vec2& position, const glm::vec2& size);

		template<typename U>
		explicit Rect(const Rect<U>& rect);

		bool contains(T x, T y) const;
		bool contains(const glm::vec2& point) const;

		bool intersects(const Rect<T>& rect) const;
		bool intersects(const Rect<T>& rect, Rect<T>& intersect) const;

		glm::tvec2<T, glm::highp> position() const;
		glm::tvec2<T, glm::highp> size() const;

		T left;
		T top;
		T width;
		T height;
	};

	template<typename T>
	bool operator ==(const Rect<T>& left, const Rect<T>& right);

	template<typename T>
	bool operator !=(const Rect<T>& left, const Rect<T>& right);

	typedef Rect<int> IntRect;
	typedef Rect<float> FloatRect;

	template <typename T>
	Rect<T>::Rect() :
		left(0),
		top(0),
		width(0),
		height(0)
	{
	}

	template <typename T>
	Rect<T>::Rect(T rectLeft, T rectTop, T rectWidth, T rectHeight) :
		left(rectLeft),
		top(rectTop),
		width(rectWidth),
		height(rectHeight)
	{
	}

	template <typename T>
	Rect<T>::Rect(const glm::vec2& position, const glm::vec2& size) :
		left(position.x),
		top(position.y),
		width(size.x),
		height(size.y)
	{
	}

	template <typename T>
	template <typename U>
	Rect<T>::Rect(const Rect<U>& rect) :
		left(static_cast<T>(rect.left)),
		top(static_cast<T>(rect.top)),
		width(static_cast<T>(rect.width)),
		height(static_cast<T>(rect.height))
	{
	}

	template <typename T>
	bool Rect<T>::contains(T x, T y) const
	{
		T minX = std::min(left, static_cast<T>(left + width));
		T maxX = std::max(left, static_cast<T>(left + width));
		T minY = std::min(top, static_cast<T>(top + height));
		T maxY = std::max(top, static_cast<T>(top + height));

		return (x >= minX) && (x < maxX) && (y >= minY) && (y < maxY);
	}

	template <typename T>
	bool Rect<T>::contains(const glm::vec2& point) const
	{
		return contains(point.x, point.y);
	}

	template <typename T>
	bool Rect<T>::intersects(const Rect<T>& rect) const
	{
		Rect<T> intersect;
		return intersects(rect, intersect);
	}

	template <typename T>
	bool Rect<T>::intersects(const Rect<T>& rect, Rect<T>& intersect) const
	{
		T r1MinX = std::min(left, static_cast<T>(left + width));
		T r1MaxX = std::max(left, static_cast<T>(left + width));
		T r1MinY = std::min(top, static_cast<T>(top + height));
		T r1MaxY = std::max(top, static_cast<T>(top + height));

		T r2MinX = std::min(rect.left, static_cast<T>(rect.left + rect.width));
		T r2MaxX = std::max(rect.left, static_cast<T>(rect.left + rect.width));
		T r2MinY = std::min(rect.top, static_cast<T>(rect.top + rect.height));
		T r2MaxY = std::max(rect.top, static_cast<T>(rect.top + rect.height));

		T interLeft = std::max(r1MinX, r2MinX);
		T interTop = std::max(r1MinY, r2MinY);
		T interRight = std::min(r1MaxX, r2MaxX);
		T interBottom = std::min(r1MaxY, r2MaxY);

		if ((interLeft < interRight) && (interTop < interBottom))
		{
			intersect = Rect<T>(interLeft, interTop, interRight - interLeft, interBottom - interTop);
			return true;
		}

		intersect = Rect<T>(0, 0, 0, 0);
		return false;
	}

	template<typename T>
	glm::tvec2<T, glm::highp> Rect<T>::position() const
	{
		return { left, top };
	}

	template<typename T>
	glm::tvec2<T, glm::highp> Rect<T>::size() const
	{
		return { width, height };
	}

	template <typename T>
	bool operator!=(const Rect<T>& left, const Rect<T>& right)
	{
		return (left.left == right.left) && (left.width == right.width) && (left.top == right.top) && (left.height == right.height);
	}

	template <typename T>
	bool operator==(const Rect<T>& left, const Rect<T>& right)
	{
		return !(left == right);
	}
}
