#pragma once
#include "stdafx.h"

#include "CommandBuffer.h"

namespace ThroneEngine
{
	struct BindableBindingInfo
	{
		CommandBuffer* commandBuffer;
		VkPipelineLayout pipelineLayout;
	};


	class Bindable
	{
	public:
		Bindable();
		~Bindable();

		virtual void bind(BindableBindingInfo& bindingInfo) = 0;
	};
}



