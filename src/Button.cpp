#pragma once
#include "stdafx.h"

#include "Button.h"
#include "Mouse.h"
#include "ThroneFramework.h"

using namespace Utility;

namespace ThroneEngine
{
	Button::Button()
	{
		onClickHandle.function = std::bind(&Button::click, this);
		onClickMaintainedHandle.function = std::bind(&Button::clickMaintained, this);
		onClickReleasedHandle.function = std::bind(&Button::clickReleased, this);

		Mouse::onButtonPressed[Mouse::Left] += onClickHandle;
		Mouse::onButtonMaintained[Mouse::Left] += onClickMaintainedHandle;
		Mouse::onButtonReleased[Mouse::Left] += onClickReleasedHandle;		
	}

	Button::~Button()
	{
	}

	void Button::initializeObject()
	{
		transition = ColorTint;
		colorState.normal = Color(255, 255, 255);
		colorState.highlightedColor = Color(245, 245, 245);
		colorState.pressedColor = Color(200, 200, 200);
		colorState.disabledColor = Color(162, 162, 162, 128);

		targetGraphic.mainTexture.borderImage.setBorders(10, 22, 10, 22);
		targetGraphic.rectTransform.setRect(0, 0, 160, 30);
	}

	void Button::click()
	{
		if(isHighlighted && isPressed == false)
		{			
			onClick();
			isPressed = true;
		}
	}

	void Button::clickMaintained()
	{
		if(isHighlighted && isPressed == true)
		{
			onClickMaintained();
		}
	}

	void Button::clickReleased()
	{
		if(isHighlighted)
		{
			onClickReleased();
			isPressed = false;
		}
	}
}
