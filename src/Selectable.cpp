#include "stdafx.h"

#include "Selectable.h"
#include "Mouse.h"

Selectable::Selectable()
{
	onMouseMoveHandle.function = std::bind(&Selectable::mouseMove, this, std::placeholders::_1);

	Mouse::onMoved += onMouseMoveHandle;
}

Selectable::~Selectable()
{
}

void ThroneEngine::Selectable::initializeObject()
{
	transition = ColorTint;
	colorState.normal = Color(255, 255, 255);
	colorState.highlightedColor = Color(245, 245, 245);
	colorState.pressedColor = Color(200, 200, 200);
	colorState.disabledColor = Color(162, 162, 162, 128);

	targetGraphic.mainTexture.borderImage.setBorders(10, 22, 10, 22);
	targetGraphic.rectTransform.setRect(0, 0, 32, 32);
}

void Selectable::initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	targetGraphic.initialize(renderPass, graphicsPipeline, descriptorSet, meshRendererBindingIndex, modelMatrixBindingIndex);
	initializeObject();
}

void ThroneEngine::Selectable::initialize(RenderPass * renderPass, GraphicsPipeline * graphicsPipeline, DescriptorSet & descriptorSet, std::deque<Material*> materials,
	uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	targetGraphic.initialize(renderPass, graphicsPipeline, descriptorSet, materials, meshRendererBindingIndex, modelMatrixBindingIndex);
	initializeObject();
}

void ThroneEngine::Selectable::mouseMove(const vec2 & mousePos)
{
	if (isInteractable && targetGraphic.rectTransform.contains(mousePos))
	{
		isHighlighted = true;
	}
	else
	{
		isHighlighted = false;
	}
}
