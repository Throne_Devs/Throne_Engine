#pragma once
#include "stdafx.h"

#include "DescriptorSetLayout.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	DescriptorSetLayout::DescriptorSetLayout()
	{
	}

	DescriptorSetLayout::~DescriptorSetLayout()
	{
	}
	bool DescriptorSetLayout::initialize(std::vector<VkDescriptorSetLayoutBinding>& bindings)
	{
		for (int i = 0; i < bindings.size(); i++)
		{
			bindings[i].binding = i;
		}


		VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
		descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		descriptorSetLayoutCreateInfo.pNext = nullptr;
		descriptorSetLayoutCreateInfo.flags = 0;
		descriptorSetLayoutCreateInfo.bindingCount = bindings.size();
		descriptorSetLayoutCreateInfo.pBindings = bindings.size() > 0 ? &bindings[0] : nullptr;

		VkResult result = vkCreateDescriptorSetLayout(logicalDevice, &descriptorSetLayoutCreateInfo, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createDescriptorSetLayout);
			#endif
			return false;
		}
		#endif
		return true;
	}
}