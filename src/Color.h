#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class Color
	{
	public:
		Color();
		Color(uint8 red, uint8 green, uint8 blue, uint8 alpha = 255);
		explicit Color(uint32 color);
		uint32 toInteger() const;

		static const Color Black;
		static const Color White;
		static const Color Red;
		static const Color Green;
		static const Color Blue;
		static const Color Yellow;
		static const Color Magenta;
		static const Color Cyan;
		static const Color Transparent;

		bool operator ==(const Color&  right) const;
		bool operator !=(const Color&  right) const;
		Color operator +(const Color&  right) const;
		Color operator -(const Color&  right) const;
		Color operator *(const Color&  right) const;
		Color& operator +=(const Color&  right);
		Color& operator -=(const Color&  right);
		Color& operator *=(const Color&  right);

		uint8 r;
		uint8 g;
		uint8 b;
		uint8 a;
	};
}
