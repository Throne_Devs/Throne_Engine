﻿#pragma once
#include "stdafx.h"

#include "SceneTest1.h"
#include "Vertex.h"
#include "OBJModel.h"
#include "TerrainGenerator.h"
#include "Clock.h"
#include "ThroneFramework.h"
#include "Shader.h"

namespace ThroneEngine
{
	SceneTest1::SceneTest1()
	{
	}


	SceneTest1::~SceneTest1()
	{
	}

	bool SceneTest1::initializeScene(Window* window, CommandQueue& commandQueue)
	{
		#pragma region Testing
		{
			glm::vec4 v1 = glm::vec4(10, 10, 10, 1);
			glm::vec4 v2 = glm::vec4(-10, 10, 10, 1);
			glm::vec4 v3 = glm::vec4(-10, -10, 10, 1);
			glm::vec4 v4 = glm::vec4(-10, -10, -10, 1);
			glm::vec4 v5 = glm::vec4(-10, -10, -10, 1);
			glm::vec4 v6 = glm::vec4(10, -10, -10, 1);
			glm::vec4 v7 = glm::vec4(10, 10, -10, 1);
			glm::vec4 v8 = glm::vec4(-10, 10, -10, 1);
			glm::vec4 v9 = glm::vec4(10, 10, -10, 1);
			glm::vec4 v10 = glm::vec4(10, -10, 10, 1);

			glm::mat4 lightM1 = glm::lookAt(glm::vec3(0.0f), glm::vec3(1.0f, 0, 0.0f), glm::vec3(0, 1.0f, 0));
			glm::mat4 lightInvM1 = glm::inverse(lightM1);

			v1 = lightM1 * v1;
			v2 = lightM1 * v2;
			v3 = lightM1 * v3;
			v4 = lightM1 * v4;
			v5 = lightM1 * v5;
			v6 = lightM1 * v6;
			v7 = lightM1 * v7;
			v8 = lightM1 * v8;
			v9 = lightM1 * v9;
			v10 = lightM1 * v10;
		}


		{
			glm::vec3 v1 = glm::vec3(1, 0, 0);
			glm::vec3 v2 = glm::vec3(0, 0, 1);
			glm::vec3 v3 = glm::vec3(0, 0, 1);


			glm::vec3 v4 = glm::vec3((v1.z * v2.y) - (v1.y * v2.z), (v1.x * v2.z) - (v1.z * v2.x), (v1.y * v2.x) - (v1.x * v2.y));
			//glm::vec3 v5 = glm::vec3((v1.y * v2.z) - (v1.z * v2.y), (v1.x * v2.z) - (v1.z * v2.x), (v1.y * v2.x) - (v1.x * v2.y));
			glm::vec3 v6 = glm::vec3((v1.z * v2.y) - (v1.y * v2.z), (v1.z * v2.x) - (v1.x * v2.z), (v1.y * v2.x) - (v1.x * v2.y)); // left hand
			//glm::vec3 v7 = glm::vec3((v1.z * v2.y) - (v1.y * v2.z), (v1.x * v2.z) - (v1.z * v2.x), (v1.x * v2.y) - (v1.y * v2.x));
			glm::vec3 v8 = glm::vec3((v1.y * v2.z) - (v1.z * v2.y), (v1.x * v2.z) - (v1.z * v2.x), (v1.x * v2.y) - (v1.y * v2.x)); // right hand
			glm::vec3 v9 = glm::vec3((v1.y * v2.z) - (v1.z * v2.y), (v1.x * v2.z) - (v1.z * v2.x), (v1.x * v2.y) - (v1.y * v2.x));


			float dot1 = glm::dot(v1, v4);
			float dot2 = glm::dot(v2, v4);

			//float dot3 = glm::dot(v1, v5);
			//float dot4 = glm::dot(v2, v5);

			float dot5 = glm::dot(v1, v6);
			float dot6 = glm::dot(v2, v6);

			//float dot7 = glm::dot(v1, v7);
			//float dot8 = glm::dot(v2, v7);

			float dot9 = glm::dot(v1, v8);
			float dot10 = glm::dot(v2, v8);

			float dot11 = glm::dot(v1, v9);
			float dot12 = glm::dot(v2, v9);


			int a = 1;
		}


		{
			glm::dvec3 set1[3] = { glm::dvec3(-1, 4, -3), glm::dvec3(6, -1, -5) , glm::dvec3(5, 4, -1) };
			glm::dvec3 set2[3] = { set1[0], glm::dvec3(0) , glm::dvec3(0) };

			for (int i = 1; i < 3; i++)
			{
				glm::dvec3 totalProj = glm::dvec3(0);
				for (int z = 0; z < i; z++)
				{
					glm::dvec3 projV = (glm::dot(set1[i], set2[z]) / glm::dot(set2[z], set2[z])) * set2[z];
					totalProj += projV;
				}
				set2[i] = set1[i] - totalProj;
			}

			double dot1 = glm::dot(set2[0], set2[1]);
			double dot2 = glm::dot(set2[0], set2[2]);
			double dot3 = glm::dot(set2[1], set2[2]);

			int a = 0;
		}


		#pragma endregion Testing


		this->window = window;
		this->commandQueue = commandQueue;

		Material::initializeStaticMaterialDescriptorSetLayout();

		globalsDescriptorSetLayout.initialize(std::vector<VkDescriptorSetLayoutBinding>{UniformBuffer::getLayoutBinding(VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
			ImageSampler::getLayoutBinding(VK_SHADER_STAGE_FRAGMENT_BIT, numberOfCascade), UniformBuffer::getLayoutBinding(VK_SHADER_STAGE_VERTEX_BIT), UniformBuffer::getLayoutBinding(VK_SHADER_STAGE_FRAGMENT_BIT)});
		viewProjectionDescriptorSetLayout.initialize(std::vector<VkDescriptorSetLayoutBinding>{UniformBuffer::getLayoutBinding(VK_SHADER_STAGE_VERTEX_BIT)});
		uiProjectionDescriptorSetLayout.initialize(std::vector<VkDescriptorSetLayoutBinding>{UniformBuffer::getLayoutBinding(VK_SHADER_STAGE_VERTEX_BIT)});

		initializeDepthImage();
		initializeShadowMap();

		//viewProjectionUniformBuffer.initializeAsUniformBuffer((sizeof(glm::mat4) * 3) + sizeof(glm::vec3), VK_SHADER_STAGE_VERTEX_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		viewProjectionUniformBuffer.initializeAsUniformBuffer((sizeof(glm::mat4) * 2) + sizeof(glm::vec4) + (sizeof(glm::mat4) * numberOfCascade), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		lightDataUniformBuffer.initializeAsUniformBuffer(sizeof(glm::vec4) * 2, VK_SHADER_STAGE_VERTEX_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		pcfDataUniformBuffer.initializeAsUniformBuffer(sizeof(glm::vec4) * numberOfCascade, VK_SHADER_STAGE_FRAGMENT_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		uiViewProjectionUniformBuffer.initializeAsUniformBuffer(sizeof(glm::mat4), VK_SHADER_STAGE_VERTEX_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

		globalsDescriptorSet.initialize(std::vector<Descriptor*>{&viewProjectionUniformBuffer, &shadowMap, &lightDataUniformBuffer, &pcfDataUniformBuffer}, globalsDescriptorSetLayout, 0);
		viewProjectionDescriptorSet.initialize(std::vector<Descriptor*>{&viewProjectionUniformBuffer}, viewProjectionDescriptorSetLayout, 0);
		uiProjectionDescriptorSet.initialize(std::vector<Descriptor*>{&uiViewProjectionUniformBuffer}, uiProjectionDescriptorSetLayout, 0);

		initializeVertexBuffersSignatures();

		initializeLight(0.0f);
		initializeViewProjectionMatrices();

		createMaterials();

		createRenderPassesAndFramebuffers();

		createGraphicsPipelines();

		createRenderedObjects();

		addObjectsToRenderers();

		cmdBuffersRenderPass.initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, window->swapchainImageCount, VK_COMMAND_BUFFER_LEVEL_PRIMARY);
		secondaryCmdBuffersRenderPass.resize(window->swapchainImageCount);
		secondaryCmdBuffersUiRenderPass.resize(window->swapchainImageCount);
		shadowSecondaryCommandBuffer.resize(window->swapchainImageCount);

		arialFont.updateTexture(commandBuffersFences);

		commandBuffersFences.resize(window->swapchainImageCount);
		for (int i = 0; i < commandBuffersFences.size(); i++)
		{
			commandBuffersFences[i].initialize();
		}


		renderFinishedSemaphore.initialize();


		beginRenderPassSemaphoreInfos = {};
		beginRenderPassSemaphoreInfos.semaphores.addSemaphores(1);
		beginRenderPassSemaphoreInfos.waitingStage.push_back(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);

		acquireNextImageFences.resize(window->swapchainImageCount);
		for (int i = 0; i < window->swapchainImageCount; i++)
		{
			acquireNextImageFences[i].initialize();
		}


		presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphore.handle;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &window->swapchain; // Can also be a pointer to an array
		presentInfo.pResults = nullptr;

		directions = new glm::vec4[amountOfBox];
		directions[0].x = 1;
		directions[0].y = 1;
		directions[0].z = 1;


		setupCommandBuffers(true, true);

		return true;
	}

	bool SceneTest1::windowResizeEvent()
	{
		vkDeviceWaitIdle(logicalDevice);

		for (int i = 0; i < sceneFrameBuffers.size(); i++)
		{
			sceneFrameBuffers[i].destroy();
		}
		for (int i = 0; i < shadowFrameBuffer.size(); i++)
		{
			shadowFrameBuffer[i].destroy();
		}
		for (int i = 0; i < uiFrameBuffers.size(); i++)
		{
			uiFrameBuffers[i].destroy();
		}

		sceneRenderPass.destroy();
		uiRenderPass.destroy();
		shadowRenderPass.destroy();

		sceneGraphicsPipeline.destroy();
		sceneTransparentGraphicsPipeline.destroy();
		skyboxGraphicsPipeline.destroy();
		shadowTransparentGraphicsPipeline.destroy();
		shadowGraphicsPipeline.destroy();
		uiTextGraphicsPipeline.destroy();
		uiButtonGraphicsPipeline.destroy();

		depthImage.destroy();

		initializeDepthImage();

		createRenderPassesAndFramebuffers();

		createGraphicsPipelines();

		createUiRenderPass();
		createUiFrameBuffers();

		globalsDescriptorSet.updateDescriptorSets();

		cmdBuffersRenderPass.reset(VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
		for (int i = 0; i < secondaryCmdBuffersRenderPass.size(); i++)
		{
			secondaryCmdBuffersRenderPass[i].reset(VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
		}

		setupCommandBuffers(false, false);

		return true;
	}

	void SceneTest1::update(float deltaTime)
	{
		frameTime += deltaTime;
		if (frameTime > 1)
		{
			frameTime = 0;
			//for (int i = 0; i < amountOfBox; i++)
			//{
			//	directions[i] = glm::vec4(((rand() % 5) - (rand() % 5)), ((rand() % 2) - (rand() % 2)), ((rand() % 5) - (rand() % 5)), 0);
			//}
			//directions[0] = directions[0] * -1.0f;
		}

		//boxes[0].modelMatrix.translate(directions[0] * deltaTime);
		//boxes[0].modelMatrix.rotate(directions[0], PI * deltaTime);
		//boxes[0].modelMatrix.setScale(directions[0]);
		//boxes[0].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[0].modelMatrix.matrix));

		//directions[0] *= 0.0f;

		//for (int i = 0; i < amountOfBox; i++)
		//{
		//	float rotation = PI / ((rand() % 5) - 5) * deltaTime;

		//	//boxes[i].modelMatrix.rotate(glm::vec3(0, 1, 0), rotation);
		//	//boxes[i].modelMatrix.translate(directions[i] * deltaTime);

		//	boxesRotation += rotation;
		//	glm::vec4 trans = *boxes[i].modelMatrix.position();

		//	boxes[i].modelMatrix.matrix = glm::mat4(1.0);
		//	
		//	//boxes[i].modelMatrix.rotate(glm::vec3(0, 1, 0), boxesRotation);	
		//	boxes[i].modelMatrix.scale(glm::vec3(0.5, 1.0, 1.0));
		//	//boxes[i].modelMatrix.translate((directions[i] * deltaTime) + trans);
		//	boxes[i].modelMatrix.setPosition((directions[0] * deltaTime) + trans);

		//	boxes[i].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[i].modelMatrix.matrix));
		//}

		/////////////////////

		/*float rotation = PI / ((rand() % 5) - 5) * deltaTime;

		*boxes[0].modelMatrix.position() += directions[0] * deltaTime;
		boxes[0].modelMatrix.rotate(glm::vec3(0, 1, 0), rotation);
		boxes[0].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[0].modelMatrix.matrix));

		boxes[1].modelMatrix.rotate(glm::vec3(0, 1, 0), rotation);
		*boxes[1].modelMatrix.position() += directions[0] * deltaTime;
		boxes[1].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[1].modelMatrix.matrix));

		boxesRotation += rotation;

		glm::vec4 pos1 = *boxes[2].modelMatrix.position();
		boxes[2].modelMatrix.matrix = glm::mat4(1.0);
		boxes[2].modelMatrix.translate((directions[0] * deltaTime) + pos1);
		boxes[2].modelMatrix.scale(glm::vec3(0.5, 1.0, 1.0));
		boxes[2].modelMatrix.rotate(glm::vec3(0, 1, 0), boxesRotation);
		boxes[2].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[2].modelMatrix.matrix));

		glm::vec4 pos2 = *boxes[3].modelMatrix.position();
		boxes[3].modelMatrix.matrix = glm::mat4(1.0);
		boxes[3].modelMatrix.rotate(glm::vec3(0, 1, 0), boxesRotation);
		boxes[3].modelMatrix.scale(glm::vec3(0.5, 1.0, 1.0));
		boxes[3].modelMatrix.setPosition((directions[0] * deltaTime) + pos2);
		boxes[3].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[3].modelMatrix.matrix));

		glm::vec4 pos3 = *boxes[4].modelMatrix.position();
		boxes[4].modelMatrix.matrix = glm::mat4(1.0);
		boxes[4].modelMatrix.setPosition((directions[0] * deltaTime) + pos3);
		boxes[4].modelMatrix.rotate(glm::vec3(0, 1, 0), boxesRotation);
		boxes[4].modelMatrix.scale(glm::vec3(0.5, 1.0, 1.0));
		boxes[4].modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(glm::mat4), glm::value_ptr(boxes[4].modelMatrix.matrix));
		*/




		skybox.modelMatrix.position()->x = mainCamera->transform->position()->x;
		skybox.modelMatrix.position()->y = mainCamera->transform->position()->y;
		skybox.modelMatrix.position()->z = mainCamera->transform->position()->z;
		skybox.modelMatrixBuffer.updateHostVisibleMemory(0, sizeof(mat4), value_ptr(skybox.modelMatrix.matrix));

		mainCamera->lookAt(viewMatrix);
		glm::mat4 viewProjection;

		switch (debugValueUseLightProjMatrix)
		{
		case -1:
		{
			viewProjection = projectionMatrix * viewMatrix;
			break;
		}
		case 0:
		{
			viewProjection = cascadeOrthoProjections[0];
			break;
		}
		case 1:
		{
			viewProjection = cascadeOrthoProjections[1];
			break;
		}
		case 2:
		{
			viewProjection = cascadeOrthoProjections[2];
			break;
		}
		case 3:
		{
			viewProjection = cascadeOrthoProjections[3];
			break;
		}
		default:
			break;
		}
		//glm::mat4 viewProjection = projectionMatrix * viewMatrix;


		//glm::mat4 shadowViewProjection = lightProjectionMatrix * lightViewMatrix;

		//initializeLight(deltaTime);
		initializeViewprojectionMatricesCascade();

		//setViewProjectionUniformBufferData(glm::value_ptr(viewProjection), glm::value_ptr(viewMatrix), glm::value_ptr(shadowViewProjection), &glm::vec3(*mainCamera->transform->position()));
		setViewProjectionUniformBufferData(glm::value_ptr(viewProjection), glm::value_ptr(viewMatrix), mainCamera->transform->position(),
			reinterpret_cast<float*>(cascadeOrthoProjections));

		uiViewProjectionUniformBuffer.mapHostVisibleMemory(0, sizeof(mat4), value_ptr(orthographicProjectionMatrix), true);


		arialFont.updateTexture(commandBuffersFences);

		setupCommandBuffers(false, false);
	}

	void SceneTest1::draw()
	{
		uint32 imageIndex;
		acquireNextImageFences[frameId % 2].waitForFence(TIMEOUT_FENCE);
		acquireNextImageFences[frameId % 2].reset();
		const VkResult result = vkAcquireNextImageKHR(logicalDevice, window->swapchain, std::numeric_limits<uint64>::max(), beginRenderPassSemaphoreInfos.semaphores.semaphoreHandles[0], acquireNextImageFences[frameId % 2].handle, &imageIndex);
		acquireNextImageFences[frameId % 2].fenceInUse = true;
		if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
			window->recreateSwapChain();

		presentInfo.pImageIndices = &imageIndex;



		commandBuffersFences[frameId % 2].waitForFence(TIMEOUT_FENCE);
		commandBuffersFences[frameId % 2].reset();
		commandQueue.submitCommandBuffer(beginRenderPassSemaphoreInfos, cmdBuffersRenderPass.commandBuffers[frameId % 2], renderFinishedSemaphore, commandBuffersFences[frameId % 2]);
		commandBuffersFences[frameId % 2].fenceInUse = true;

		commandQueue.waitForQueue();
		vkQueuePresentKHR(commandQueue.handle, &presentInfo);
		frameId++;
	}

	void SceneTest1::createMaterials()
	{
		createBoxMaterial();
		createTreeMaterials();
		createSkyboxMaterial();
		createGrassTerrainMaterial();
		createTextMaterial();
	}

	void SceneTest1::createGrassTerrainMaterial()
	{
		ImageSampler* grassTerrainTexture = new ImageSampler();
		grassTerrainTexture->initialize(path_grassTerrainTexture, commandQueue);
		terrainMaterial = new Material();
		terrainMaterial->initialize(grassTerrainTexture, 0, 0, 1);
	}

	void SceneTest1::createBoxMaterial()
	{
		ImageSampler* boxTexture = new ImageSampler();
		boxTexture->initialize(path_boxTexture, commandQueue);
		boxMaterial = new Material();
		boxMaterial->initialize(boxTexture, 0, 0, 1);
	}

	void SceneTest1::createSkyboxMaterial()
	{
		ImageSampler* skyboxTexture = new ImageSampler();
		skyboxTexture->initializeAsCubeMap(path_interstellarSkyboxTexture, commandQueue);
		skyboxMaterial = new Material();
		skyboxMaterial->initialize(skyboxTexture, 0, 0, 1);
	}

	void SceneTest1::createTreeMaterials()
	{
		std::string texturePaths[9] = { path_treeTextureEU55Rts2, path_treeTextureEU55Twg2, path_treeTextureEU55Brk2, path_treeTextureEU55Brn3, path_treeTextureEU55Brn7,
										path_treeTextureEU55Brn4, path_treeTextureEU55Twg1,  path_treeTextureEU55Bud1, path_treeTextureEU55Trk2 };
		transparentTreeObjectParts = new bool[9]{ false, false, false, true, true,
												true, false, true, false };
		treeMaterials = std::vector<Material*>(9);
		for (int i = 0; i < treeMaterials.size(); i++)
		{
			ImageSampler* imageSampler = new ImageSampler();
			imageSampler->initialize(texturePaths[i], commandQueue);
			treeMaterials[i] = new Material();
			treeMaterials[i]->initialize(imageSampler, 0, 0, 1);
		}
	}

	void SceneTest1::createTextMaterial()
	{
		arialFont.loadFromFile(path_arialFont);
		//arialFont.loadFromFile(path_itcedscrFont);
		//arialFont.loadFromFile(path_tahomaFont);
		//testText.initializeMaterial(&arialFont, 30, std::wstring(L"ThroneEngine, fait par Félix Lamontagne, Lucas Desjardins et Louis-Phillipe Hudon"), false);
		testText.initializeMaterial(&arialFont, 30, std::wstring(sdo_wStringFPSoutput + L"0"), false);
		//debugText.initializeMaterial(&arialFont, 30, std::wstring(L"0"), false);
		//testText.initializeMaterial(&arialFont, 30, std::wstring(L"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean venenatis ultricies lectus,\nvitae egestas ex cursus eu. Morbi eleifend pulvinar sapien, eu lacinia tellus bibendum non.\nVestibulum et consequat mauris. In feugiat turpis at nulla suscipit, vel mollis magna mattis.\nVivamus neque dolor, vestibulum non scelerisque sed, tincidunt quis massa. Pellentesque\nscelerisque erat vel ultrices tempus. Praesent quis quam euismod, feugiat augue quis,\nmalesuada mauris. Phasellus iaculis sollicitudin feugiat."), false);
		//testText.initializeMaterial(&arialFont, 30, std::wstring(L"Throne Engine"), &uiTextGraphicsPipeline);
		//testText.initializeMaterial(&arialFont, 100, std::wstring(L"Félix"), &uiTextGraphicsPipeline);
		//testText.initializeMaterial(&arialFont, 30, std::wstring(L"ThgjkLlpiFy"), &uiTextGraphicsPipeline);
	}

	void SceneTest1::initializeVertexBuffersSignatures()
	{
		defaultSceneRenderingVertexBufferSignature = VertexBufferSignature(0, VK_VERTEX_INPUT_RATE_VERTEX,
			{
				VertexBufferBlock(sizeof(vec3), 1, 0, VK_FORMAT_R32G32B32_SFLOAT),
				VertexBufferBlock(sizeof(vec2), 1, 1, VK_FORMAT_R32G32_SFLOAT),
				VertexBufferBlock(sizeof(vec3), 1, 2, VK_FORMAT_R32G32B32_SFLOAT)
			});

		defaultSceneRenderingModelMatrixBufferSignature = VertexBufferSignature(1, VK_VERTEX_INPUT_RATE_INSTANCE,
			{
			VertexBufferBlock(sizeof(vec4), 4, 3, VK_FORMAT_R32G32B32A32_SFLOAT)
			});
	}

	bool SceneTest1::setupCommandBuffers(bool initializeCmdBuffers, bool initializeTree)
	{
		if (initializeCmdBuffers == true)
		{
			initializeCommmandBuffers();
		}



		VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
		VkClearValue clearDepth = {};
		clearDepth.depthStencil.depth = 1.0f;
		VkClearValue clearDepth2 = {};
		clearDepth2.depthStencil.depth = 1.0f;



		if (initializeTree == true)
		{
			mainRenderer.initializeTree(sceneRenderPass);
			shadowRenderer.initializeTree(shadowRenderPass);
			uiRenderer.initializeTree(uiRenderPass);
		}

		int i = frameId % 2;

		commandBuffersFences[i].waitForFence(TIMEOUT_FENCE);

		for (int j = 0; j < numberOfCascade; j++)
		{
			shadowGraphicsPipeline.currentDynamicStates[VK_DYNAMIC_STATE_VIEWPORT] = shadowGraphicsPipeline.dynamicStates[VK_DYNAMIC_STATE_VIEWPORT][j];
			shadowGraphicsPipeline.currentDynamicStates[VK_DYNAMIC_STATE_SCISSOR] = shadowGraphicsPipeline.dynamicStates[VK_DYNAMIC_STATE_SCISSOR][j];
			shadowTransparentGraphicsPipeline.currentDynamicStates[VK_DYNAMIC_STATE_VIEWPORT] = shadowTransparentGraphicsPipeline.dynamicStates[VK_DYNAMIC_STATE_VIEWPORT][j];
			shadowTransparentGraphicsPipeline.currentDynamicStates[VK_DYNAMIC_STATE_SCISSOR] = shadowTransparentGraphicsPipeline.dynamicStates[VK_DYNAMIC_STATE_SCISSOR][j];
			shadowRenderer.recordCommands(commandQueue, shadowRenderPass, shadowFrameBuffer[j], false, shadowSecondaryCommandBuffer[i].commandBuffers[j]);
		}
		mainRenderer.recordCommands(commandQueue, sceneRenderPass, sceneFrameBuffers[i], false, secondaryCmdBuffersRenderPass[i].commandBuffers[0]);
		uiRenderer.recordCommands(commandQueue, uiRenderPass, uiFrameBuffers[i], false, secondaryCmdBuffersUiRenderPass[i].commandBuffers[0]);

		cmdBuffersRenderPass.commandBuffers[i].beginRecordingOnPrimaryCommandBuffer(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);

		for (int j = 0; j < numberOfCascade; j++)
		{
			cmdBuffersRenderPass.commandBuffers[i].cmdPushConstants(shadowGraphicsPipeline.layoutHandle, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &j);
			cmdBuffersRenderPass.commandBuffers[i].cmdPushConstants(shadowTransparentGraphicsPipeline.layoutHandle, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(int), &j);

			shadowRenderPass.begin(cmdBuffersRenderPass.commandBuffers[i], shadowFrameBuffer[j], cascadesScissors[j], std::vector<VkClearValue>{clearDepth2}, true);
			cmdBuffersRenderPass.commandBuffers[i].cmdExecuteCommands(1, reinterpret_cast<VkCommandBuffer*>(&shadowSecondaryCommandBuffer[i].commandBuffers[j]));
			cmdBuffersRenderPass.commandBuffers[i].cmdEndRenderPass();
		}

		sceneRenderPass.begin(cmdBuffersRenderPass.commandBuffers[i], sceneFrameBuffers[i], VkRect2D{ { 0, 0 },  window->swapchainExtent }, std::vector<VkClearValue>{clearColor, clearDepth}, true);
		cmdBuffersRenderPass.commandBuffers[i].cmdExecuteCommands(1, reinterpret_cast<VkCommandBuffer*>(&secondaryCmdBuffersRenderPass[i].commandBuffers[0]));
		cmdBuffersRenderPass.commandBuffers[i].cmdEndRenderPass();

		uiRenderPass.begin(cmdBuffersRenderPass.commandBuffers[i], uiFrameBuffers[i], VkRect2D{ { 0, 0 },  window->swapchainExtent }, std::vector<VkClearValue>{clearColor, clearDepth}, true);
		cmdBuffersRenderPass.commandBuffers[i].cmdExecuteCommands(1, reinterpret_cast<VkCommandBuffer*>(&secondaryCmdBuffersUiRenderPass[i].commandBuffers[0]));
		cmdBuffersRenderPass.commandBuffers[i].cmdEndRenderPass();

		cmdBuffersRenderPass.commandBuffers[i].endRecording();

		return true;
	}

	bool SceneTest1::initializeCommmandBuffers()
	{
		for (int i = 0; i < window->swapchainImageCount; i++)
		{
			shadowSecondaryCommandBuffer[i].initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, numberOfCascade, VK_COMMAND_BUFFER_LEVEL_SECONDARY);
			secondaryCmdBuffersRenderPass[i].initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, 1, VK_COMMAND_BUFFER_LEVEL_SECONDARY);
			secondaryCmdBuffersUiRenderPass[i].initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, 1, VK_COMMAND_BUFFER_LEVEL_SECONDARY);
		}
		return true;
	}


	bool SceneTest1::createGraphicsPipelines()
	{
		createSceneGraphicsPipeline(std::vector<DescriptorSetLayout>{globalsDescriptorSetLayout, Material::materialDescriptorSetLayout});
		createSceneTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>{globalsDescriptorSetLayout, Material::materialDescriptorSetLayout});
		createSkyboxGraphicsPipeline(std::vector<DescriptorSetLayout>{viewProjectionDescriptorSetLayout, Material::materialDescriptorSetLayout});

		createShadowPassGraphicsPipeline(std::vector<DescriptorSetLayout>{viewProjectionDescriptorSetLayout, Material::materialDescriptorSetLayout});
		createShadowsTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>{viewProjectionDescriptorSetLayout, Material::materialDescriptorSetLayout});

		createUiTextGraphicsPipeline(std::vector<DescriptorSetLayout>{Material::materialDescriptorSetLayout});
		createUiButtonGraphicsPipeline(std::vector<DescriptorSetLayout>{uiProjectionDescriptorSetLayout, Material::materialDescriptorSetLayout});
		return true;
	}

	bool SceneTest1::createSceneGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_shadowCascadeSceneShaderVert, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_shadowCascadeSceneShaderFrag, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescriptionsTEST;
		//std::vector<VkVertexInputBindingDescription> vertexInputBindingDescriptionsTEST;

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndNormal::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//vertexInputAttributeDescriptionsTEST.insert(vertexInputAttributeDescriptionsTEST.end(), attributeDescription.begin(), attributeDescription.end());

		//vertexInputBindingDescriptionsTEST.push_back(VertexWithUVAndNormal::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(vertexInputBindingDescriptionsTEST);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		pipelineCreation.scissor.extent = window->swapchainExtent;

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_BACK_BIT;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		//VkStencilOpState stencilTestParams = {}; // Used for when the stencil tests are needed
		//stencilTestParams.failOp = VK_STENCIL_OP_KEEP;
		//stencilTestParams.passOp = VK_STENCIL_OP_KEEP;
		//stencilTestParams.depthFailOp = VK_STENCIL_OP_KEEP;
		//stencilTestParams.compareOp = VK_COMPARE_OP_ALWAYS;
		//stencilTestParams.compareMask = 0;
		//stencilTestParams.writeMask = 0;
		//stencilTestParams.reference = 0;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				false,                          // VkBool32                 blendEnable
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |      // VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
				VK_COLOR_COMPONENT_B_BIT |
				VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_COPY;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};



		return sceneGraphicsPipeline.initialize(0, pipelineCreation, sceneRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createSkyboxGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_skyboxShaderVert, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_skyboxShaderFrag, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		pipelineCreation.vertexInputSignatures =
		{
			VertexBufferSignature(0, VK_VERTEX_INPUT_RATE_VERTEX, {
				VertexBufferBlock(sizeof(vec3), 1, 0, VK_FORMAT_R32G32B32_SFLOAT)
			}),
			VertexBufferSignature(1, VK_VERTEX_INPUT_RATE_INSTANCE, {
				VertexBufferBlock(sizeof(vec4), 4, 1, VK_FORMAT_R32G32B32A32_SFLOAT)
			})
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = (Vertex::getAttributeDescriptions());
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 1);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(Vertex::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);


		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		pipelineCreation.scissor.extent = window->swapchainExtent;

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_FRONT_BIT;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this needs to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				false,                          // VkBool32                 blendEnable
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |      // VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_COPY;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};

		return skyboxGraphicsPipeline.initialize(0, pipelineCreation, sceneRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createShadowPassGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		vertexShader.initialize(path_shadowCascadeShaderVert, VK_SHADER_STAGE_VERTEX_BIT);
		pipelineCreation.shaders.push_back(vertexShader);

		VkPushConstantRange pushConstantRange = {};
		pushConstantRange.offset = 0;
		pushConstantRange.size = sizeof(glm::vec2) * numberOfCascade;
		pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pipelineCreation.pushConstantRanges.push_back(pushConstantRange);

		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndNormal::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(VertexWithUVAndNormal::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		//pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		//pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.width = 12288;
		pipelineCreation.viewport.height = 12288;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		//pipelineCreation.scissor.extent = window->swapchainExtent;
		pipelineCreation.scissor.extent = VkExtent2D{ 12288, 12288 };

		pipelineCreation.dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
		pipelineCreation.dynamicStates.push_back(VK_DYNAMIC_STATE_SCISSOR);

		for (int i = 0; i < numberOfCascade; i++)
		{
			GraphicsPipelineViewportDynamicState* viewportDynamicState = new GraphicsPipelineViewportDynamicState();
			viewportDynamicState->viewport = cascadesViewports[i];
			GraphicsPipelineScissorDynamicState* scissorDynamicState = new GraphicsPipelineScissorDynamicState();
			scissorDynamicState->scissor = cascadesScissors[i];
			pipelineCreation.dynamicStatesData[VK_DYNAMIC_STATE_VIEWPORT].push_back(viewportDynamicState);
			pipelineCreation.dynamicStatesData[VK_DYNAMIC_STATE_SCISSOR].push_back(scissorDynamicState);
		}


		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = true;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.05f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 5.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_BACK_BIT;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				false,                          // VkBool32                 blendEnable
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |      // VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_COPY;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};



		return shadowGraphicsPipeline.initialize(0, pipelineCreation, shadowRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createSceneTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_shadowCascadeSceneShaderVert, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_shadowCascadeTransparentSceneShaderFrag, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndNormal::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(VertexWithUVAndNormal::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		pipelineCreation.scissor.extent = window->swapchainExtent;

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_NONE;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				false,                          // VkBool32                 blendEnable
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |      // VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_COPY;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};



		return sceneTransparentGraphicsPipeline.initialize(0, pipelineCreation, sceneRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createShadowsTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_shadowCascadeTransparentShaderVert, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_shadowCascadeTransparentShaderFrag, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		VkPushConstantRange pushConstantRange = {};
		pushConstantRange.offset = 0;
		pushConstantRange.size = sizeof(glm::vec2) * numberOfCascade;
		pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pipelineCreation.pushConstantRanges.push_back(pushConstantRange);


		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndNormal::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(VertexWithUVAndNormal::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		//pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		//pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.width = 12288;
		pipelineCreation.viewport.height = 12288;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		//pipelineCreation.scissor.extent = window->swapchainExtent;
		pipelineCreation.scissor.extent = VkExtent2D{ 12288, 12288 };

		pipelineCreation.dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
		pipelineCreation.dynamicStates.push_back(VK_DYNAMIC_STATE_SCISSOR);

		for (int i = 0; i < numberOfCascade; i++)
		{
			GraphicsPipelineViewportDynamicState* viewportDynamicState = new GraphicsPipelineViewportDynamicState();
			viewportDynamicState->viewport = cascadesViewports[i];
			GraphicsPipelineScissorDynamicState* scissorDynamicState = new GraphicsPipelineScissorDynamicState();
			scissorDynamicState->scissor = cascadesScissors[i];
			pipelineCreation.dynamicStatesData[VK_DYNAMIC_STATE_VIEWPORT].push_back(viewportDynamicState);
			pipelineCreation.dynamicStatesData[VK_DYNAMIC_STATE_SCISSOR].push_back(scissorDynamicState);
		}

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = true;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.05f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 5.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_NONE;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				false,                          // VkBool32                 blendEnable
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ONE,            // VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,                // VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |      // VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_COPY;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};



		return shadowTransparentGraphicsPipeline.initialize(0, pipelineCreation, shadowRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createUiTextGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_uiTextVertShader, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_uiTextFragShader, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndColor::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(VertexWithUVAndColor::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		pipelineCreation.scissor.extent = window->swapchainExtent;

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_NONE;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		// Alpha blending
		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				true,									// VkBool32                 blendEnable
				VK_BLEND_FACTOR_SRC_ALPHA,				// VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,    // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,						// VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,					// VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ZERO,					// VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,						// VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |				// VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
				VK_COLOR_COMPONENT_B_BIT |
				VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_CLEAR;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};



		return uiTextGraphicsPipeline.initialize(0, pipelineCreation, uiRenderPass, 0, VK_NULL_HANDLE);
	}

	bool SceneTest1::createUiButtonGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets)
	{
		GraphicsPipelineCreationParams pipelineCreation = {};

		pipelineCreation.descriptorSetLayouts.insert(pipelineCreation.descriptorSetLayouts.end(), descriptorSets.begin(), descriptorSets.end());

		Shader vertexShader;
		Shader fragmentShader;
		vertexShader.initialize(path_uiDefaultVertexShader, VK_SHADER_STAGE_VERTEX_BIT);
		fragmentShader.initialize(path_uiDefaultFragmentShader, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineCreation.shaders.push_back(vertexShader);
		pipelineCreation.shaders.push_back(fragmentShader);

		pipelineCreation.vertexInputSignatures =
		{
			defaultSceneRenderingVertexBufferSignature,
			defaultSceneRenderingModelMatrixBufferSignature
		};

		//std::vector<VkVertexInputAttributeDescription> attributeDescription = VertexWithUVAndColor::getAttributeDescriptions();
		//Transform::getModelMatrixAttributeDesctription(attributeDescription, 3);
		//pipelineCreation.vertexInputAttributeDescriptions.insert(pipelineCreation.vertexInputAttributeDescriptions.end(), attributeDescription.begin(), attributeDescription.end());
		//pipelineCreation.vertexInputBindingDescriptions.push_back(VertexWithUVAndColor::getBindingDescription());
		//Transform::getModelMatrixBindingDescription(pipelineCreation.vertexInputBindingDescriptions);

		pipelineCreation.primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		pipelineCreation.primitiveRestartEnable = false;

		pipelineCreation.viewport = {};
		pipelineCreation.viewport.x = 0.0f;
		pipelineCreation.viewport.y = 0.0f;
		pipelineCreation.viewport.width = (float)window->swapchainExtent.width;
		pipelineCreation.viewport.height = (float)window->swapchainExtent.height;
		pipelineCreation.viewport.minDepth = 0.0f;
		pipelineCreation.viewport.maxDepth = 1.0f;

		pipelineCreation.scissor = {};
		pipelineCreation.scissor.offset = { 0, 0 };
		pipelineCreation.scissor.extent = window->swapchainExtent;

		pipelineCreation.rasterizerCreationParams = {};
		pipelineCreation.rasterizerCreationParams.depthClampEnable = false;
		pipelineCreation.rasterizerCreationParams.rasterizerDiscardEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasEnable = false;
		pipelineCreation.rasterizerCreationParams.depthBiasConstantFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasClamp = 0.0f;
		pipelineCreation.rasterizerCreationParams.depthBiasSlopeFactor = 0.0f;
		pipelineCreation.rasterizerCreationParams.lineWidth = 1.0f;
		pipelineCreation.rasterizerCreationParams.polygonMode = VK_POLYGON_MODE_FILL;
		pipelineCreation.rasterizerCreationParams.cullingMode = VK_CULL_MODE_NONE;
		pipelineCreation.rasterizerCreationParams.frontFace = VK_FRONT_FACE_CLOCKWISE;

		pipelineCreation.multisampleCreationParams = {};
		pipelineCreation.multisampleCreationParams.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		pipelineCreation.multisampleCreationParams.perSampleShadingEnable = false;
		pipelineCreation.multisampleCreationParams.minSampleShading = 0.0f;
		pipelineCreation.multisampleCreationParams.sampleMasks = nullptr;
		pipelineCreation.multisampleCreationParams.alphaToCoverageEnable = false;
		pipelineCreation.multisampleCreationParams.alphaToOneEnable = false;

		pipelineCreation.depthStencilParams = {};
		pipelineCreation.depthStencilParams.depthTestEnable = true;
		pipelineCreation.depthStencilParams.depthWriteEnable = true;
		pipelineCreation.depthStencilParams.depthCompareOp = VK_COMPARE_OP_LESS;
		pipelineCreation.depthStencilParams.depthBoundTestEnable = false;
		pipelineCreation.depthStencilParams.minDepthBounds = 0.0f;
		pipelineCreation.depthStencilParams.maxDepthBounds = 1.0f;
		pipelineCreation.depthStencilParams.stencilTestEnable = false;
		pipelineCreation.depthStencilParams.frontStencilTestParameters = {}; // if the stencil tests are needed this need to be filled
		pipelineCreation.depthStencilParams.backStencilTestParameters = {};

		// Alpha blending
		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates = {
			{
				true,									// VkBool32                 blendEnable
				VK_BLEND_FACTOR_SRC_ALPHA,				// VkBlendFactor            srcColorBlendFactor
				VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,    // VkBlendFactor            dstColorBlendFactor
				VK_BLEND_OP_ADD,						// VkBlendOp                colorBlendOp
				VK_BLEND_FACTOR_ONE,					// VkBlendFactor            srcAlphaBlendFactor
				VK_BLEND_FACTOR_ZERO,					// VkBlendFactor            dstAlphaBlendFactor
				VK_BLEND_OP_ADD,						// VkBlendOp                alphaBlendOp
				VK_COLOR_COMPONENT_R_BIT |				// VkColorComponentFlags    colorWriteMask
				VK_COLOR_COMPONENT_G_BIT |
				VK_COLOR_COMPONENT_B_BIT |
				VK_COLOR_COMPONENT_A_BIT
			}
		};

		pipelineCreation.blendCreationParams = {};
		pipelineCreation.blendCreationParams.blendLogicOpEnable = false;
		pipelineCreation.blendCreationParams.blendLogicOp = VK_LOGIC_OP_CLEAR;
		pipelineCreation.blendCreationParams.attachmentBlendStates = attachmentBlendStates;
		pipelineCreation.blendCreationParams.blendConstants = std::array<float, 4U>{0.0f, 0.0f, 0.0f, 0.0f};

		return uiButtonGraphicsPipeline.initialize(0, pipelineCreation, uiRenderPass, 0, VK_NULL_HANDLE);
	}

	void SceneTest1::initializeDepthImage()
	{
		VkExtent3D depthImageDimensions = {};
		depthImageDimensions.width = window->swapchainExtent.width;
		depthImageDimensions.height = window->swapchainExtent.height;
		depthImageDimensions.depth = 1;

		depthImage.initialize(VK_IMAGE_TYPE_2D, VK_FORMAT_D32_SFLOAT, depthImageDimensions, 1, 1, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, false);
		depthImage.allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		depthImage.initializeImageView(VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_VIEW_TYPE_2D, VK_FORMAT_D32_SFLOAT, 0, 1);
	}

	void SceneTest1::initializeShadowMap()
	{
		for (int i = 0; i < numberOfCascade; i++)
		{
			VkExtent3D shadowMapImageDimensions = {};
			shadowMapImageDimensions.width = cascadesViewports[i].width;
			shadowMapImageDimensions.height = cascadesViewports[i].height;
			shadowMapImageDimensions.depth = 1;

			shadowMapImageSamplers[i].initialize(shadowMapImageDimensions, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_DEPTH_BIT,
				VK_FORMAT_D32_SFLOAT, 1, VK_IMAGE_VIEW_TYPE_2D);
		}

		shadowMap.initialize(numberOfCascade, shadowMapImageSamplers);
	}

	void SceneTest1::waitForDevice(VkDevice)
	{
		VkResult result = vkDeviceWaitIdle(logicalDevice);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_deviceWaitIdle);
			#endif
		}
		#endif
	}

	void SceneTest1::createRenderedObjects()
	{
		CommandPool stagingCommandBuffers;
		stagingCommandBuffers.initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, 4, VK_COMMAND_BUFFER_LEVEL_PRIMARY);


		createBoxObjects(stagingCommandBuffers, 0);
		createTreeObject(stagingCommandBuffers, 1);
		createSkyboxObject(stagingCommandBuffers, 2);
		createTerrainObject(stagingCommandBuffers, 3);
		createTextObject();
		createButtonObject();

		stagingCommandBuffers.destroy();
	}

	void SceneTest1::createTreeObject(CommandPool& commandBuffers, int bufferIndex)
	{
		OBJModel modelLoader;


		Mesh* treeMesh = new Mesh();
		modelLoader.loadModel(path_treeEU55Model.data(), treeMesh, true, true, false);
		MeshRenderer* treeMeshRenderer = new MeshRenderer();
		treeMeshRenderer->initialize(treeMesh, commandQueue, commandBuffers.commandBuffers[bufferIndex], defaultSceneRenderingVertexBufferSignature.bindingIndex, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		std::deque<RenderedObjectPart> renderedObjectParts;
		std::deque<RenderedObjectPart> renderedShadowObjectParts;
		for (int i = 0; i < 9; i++)
		{
			if (transparentTreeObjectParts[i] == false)
			{
				renderedObjectParts.emplace_back(treeMesh->parts[i], treeMaterials[i], &sceneGraphicsPipeline);
				renderedShadowObjectParts.emplace_back(treeMesh->parts[i], nullptr, &shadowGraphicsPipeline);
			}
			else
			{
				renderedObjectParts.emplace_back(treeMesh->parts[i], treeMaterials[i], &sceneTransparentGraphicsPipeline, renderTransparentPartsTree);
				renderedShadowObjectParts.emplace_back(treeMesh->parts[i], treeMaterials[i], &shadowTransparentGraphicsPipeline, renderTransparentPartsTree);
			}
		}

		std::deque<RenderedObjectRenderPassInfo> treeRenderedObjectRenderPassInfo;
		treeRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedObjectParts, &sceneRenderPass, std::vector<DescriptorSet*>{&globalsDescriptorSet}));
		treeRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedShadowObjectParts, &shadowRenderPass, std::vector<DescriptorSet*>{&viewProjectionDescriptorSet}));

		tree.initialize(treeMeshRenderer, treeRenderedObjectRenderPassInfo, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex, glm::vec3(45, -15, 45), glm::vec3(), glm::vec3(10, 10, 10));

	}

	void SceneTest1::createBoxObjects(CommandPool& commandBuffers, int bufferIndex)
	{
		OBJModel modelLoader;


		Mesh* boxMesh = new Mesh();
		modelLoader.loadModel(path_box3DModel.data(), boxMesh, true, true, false);
		MeshRenderer* boxMeshRenderer = new MeshRenderer();
		boxMeshRenderer->initialize(boxMesh, commandQueue, commandBuffers.commandBuffers[bufferIndex], defaultSceneRenderingVertexBufferSignature.bindingIndex, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		std::deque<RenderedObjectPart> renderedObjectParts;
		renderedObjectParts.emplace_back(boxMesh->parts[0], boxMaterial, &sceneGraphicsPipeline);
		std::deque<RenderedObjectPart> renderedShadowObjectParts;
		renderedShadowObjectParts.emplace_back(boxMesh->parts[0], nullptr, &shadowGraphicsPipeline);

		std::deque<RenderedObjectRenderPassInfo> boxRenderedObjectRenderPassInfo;
		boxRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedObjectParts, &sceneRenderPass, std::vector<DescriptorSet*>{&globalsDescriptorSet}));
		boxRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedShadowObjectParts, &shadowRenderPass, std::vector<DescriptorSet*>{&viewProjectionDescriptorSet}));

		boxes.resize(amountOfBox);
		for (int i = 0; i < amountOfBox; i++)
		{
			//boxes[i].initialize(boxMeshRenderer, boxRenderedObjectRenderPassInfo, glm::vec3(30, -15, (i * spaceBetweenBoxes) + 30));
			boxes[i].initialize(boxMeshRenderer, boxRenderedObjectRenderPassInfo, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex, glm::vec3(10, 0, 10));
		}
	}

	void SceneTest1::createTerrainObject(CommandPool& commandBuffers, int bufferIndex)
	{
		OBJModel modelLoader;

		Mesh* terrainMesh = new Mesh();
		//TerrainGenerator::generateTerrain(terrainMesh, 300, 300);
		TerrainGenerator::generateFlatTerrain(terrainMesh, 100, 100, -15);
		MeshRenderer* terrainMeshRenderer = new MeshRenderer();
		terrainMeshRenderer->initialize(terrainMesh, commandQueue, commandBuffers.commandBuffers[bufferIndex], defaultSceneRenderingVertexBufferSignature.bindingIndex, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		std::deque<RenderedObjectPart> renderedObjectParts;
		renderedObjectParts.emplace_back(terrainMesh->parts[0], terrainMaterial, &sceneGraphicsPipeline);
		std::deque<RenderedObjectPart> renderedShadowObjectParts;
		renderedShadowObjectParts.emplace_back(terrainMesh->parts[0], nullptr, &shadowGraphicsPipeline);

		std::deque<RenderedObjectRenderPassInfo> terrainRenderedObjectRenderPassInfo;
		terrainRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedObjectParts, &sceneRenderPass, std::vector<DescriptorSet*>{&globalsDescriptorSet}));
		terrainRenderedObjectRenderPassInfo.push_back(RenderedObjectRenderPassInfo(renderedShadowObjectParts, &shadowRenderPass, std::vector<DescriptorSet*>{&viewProjectionDescriptorSet}));

		terrain.initialize(terrainMeshRenderer, terrainRenderedObjectRenderPassInfo, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex);
	}

	void SceneTest1::createSkyboxObject(CommandPool& commandBuffers, int bufferIndex)
	{
		OBJModel modelLoader;

		Mesh* skyboxMesh = new Mesh();
		modelLoader.loadModel(path_skyboxCube.data(), skyboxMesh, false, false, false);
		MeshRenderer* skyboxMeshRenderer = new MeshRenderer();
		skyboxMeshRenderer->initialize(skyboxMesh, commandQueue, commandBuffers.commandBuffers[bufferIndex], defaultSceneRenderingVertexBufferSignature.bindingIndex, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		skybox.initialize(skyboxMeshRenderer, skyboxMaterial, &skyboxGraphicsPipeline, &sceneRenderPass, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex);
		skybox.getRenderPassInfoByRenderPassHandle(sceneRenderPass.handle).batchableDescriptorSets.push_back(&viewProjectionDescriptorSet);
	}

	void SceneTest1::createTextObject()
	{
		testText.initializeMesh(uiRenderPass, &uiTextGraphicsPipeline, defaultSceneRenderingVertexBufferSignature.bindingIndex, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex, 
			glm::tvec3<byte>(0, 0, 120), glm::vec3(-1, 0.93f, 0));
		//testText.initializeMesh(uiRenderPass, &uiTextGraphicsPipeline, glm::tvec3<byte>(0, 200, 0), glm::vec3(-1, 0.93f, 0));
		//debugText.initializeMesh(uiRenderPass, &uiTextGraphicsPipeline, defaultSceneRenderingVertexBufferSignature.bindingIndex, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex, 
		//	glm::tvec3<byte>(0, 0, 120), glm::vec3(0.0f, 0.0f, 0));
	}

	void SceneTest1::createButtonObject()
	{
		button.initialize(&uiRenderPass, &uiButtonGraphicsPipeline, uiProjectionDescriptorSet, 
			defaultSceneRenderingVertexBufferSignature.bindingIndex, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex);
		button.targetGraphic.rectTransform.setPosition(glm::vec2(window->getSize().x - button.targetGraphic.rectTransform.getLocalBound().width, 0));

		checkbox.initialize(&uiRenderPass, &uiButtonGraphicsPipeline, uiProjectionDescriptorSet, 
			defaultSceneRenderingVertexBufferSignature.bindingIndex, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex);
		checkbox.targetGraphic.rectTransform.setPosition(glm::vec2(window->getSize().x - checkbox.targetGraphic.rectTransform.getLocalBound().width, button.targetGraphic.rectTransform.getGlobalBound().height + 10));

		slider.initialize(&uiRenderPass, &uiButtonGraphicsPipeline, uiProjectionDescriptorSet,
			defaultSceneRenderingVertexBufferSignature.bindingIndex, defaultSceneRenderingModelMatrixBufferSignature.bindingIndex);
		slider.targetGraphic.rectTransform.setPosition(glm::vec2(window->getSize().x - slider.targetGraphic.rectTransform.getLocalBound().width, button.targetGraphic.rectTransform.getGlobalBound().height + checkbox.targetGraphic.rectTransform.getGlobalBound().height + 10));
	}

	void SceneTest1::addObjectsToRenderers()
	{
		addObjectsToMainRenderer();
		addObjectsToShadowRenderer();
		addObjectsToUiRenderer();
	}

	void SceneTest1::addObjectsToMainRenderer()
	{
		for (int i = 0; i < boxes.size(); i++)
		{
			mainRenderer.renderedObjects.push_back(&boxes[i]);
		}
		mainRenderer.renderedObjects.push_back(&skybox);
		mainRenderer.renderedObjects.push_back(&terrain);
		mainRenderer.renderedObjects.push_back(&tree);
	}

	void SceneTest1::addObjectsToShadowRenderer()
	{
		for (int i = 0; i < boxes.size(); i++)
		{
			shadowRenderer.renderedObjects.push_back(&boxes[i]);
		}
		shadowRenderer.renderedObjects.push_back(&terrain);
		shadowRenderer.renderedObjects.push_back(&tree);
	}

	void SceneTest1::addObjectsToUiRenderer()
	{
		uiRenderer.renderedObjects.push_back(&testText.textObject);
		uiRenderer.renderedObjects.push_back(button.targetGraphic.renderedObject);
		uiRenderer.renderedObjects.push_back(checkbox.targetGraphic.renderedObject);
		uiRenderer.renderedObjects.push_back(slider.targetGraphic.renderedObject);
		//uiRenderer.renderedObjects.push_back(&button.buttonObject);
		//uiRenderer.renderedObjects.push_back(&debugText.textObject);
	}

	void SceneTest1::initializeLight(float deltaTime)
	{
		////Pos : X : 8.98064, Y : 0.88286, Z : 4.81288
		////ViewPoint: X: 9.421, Y : 0.400524, Z : 5.57014
		////sunCamera = new Camera(glm::vec3(5.4, 12.7, -13.2), glm::vec3(5.8, 12, -12.5), glm::vec3(0, 1, 0));
		//sunCamera = new Camera(glm::vec3(8.98064, 0.88286, 4.81288), glm::vec3(9.421, 0.400524, 5.57014), glm::vec3(0, 1, 0));
		//sun.direction = glm::normalize(glm::vec3(5.8, 12, -12.5) - glm::vec3(5.4, 12.7, -13.2));
		//sun.position = *sunCamera->transform->position();
		//sunCamera->lookAt(lightViewMatrix);
		//lightDataUniformBuffer.mapHostVisibleMemory(0, sizeof(glm::vec3) * 2, &sun, false);

		glm::vec3 pos = glm::vec3(0.0, 0.0, 0.0);
		glm::vec3 viewPoint = glm::normalize(glm::vec3(cos(lightAngle), -0.40f, sin(lightAngle)));
		//glm::vec3 viewPoint = glm::normalize(glm::vec3(1.0f, -0.40f, 0.0f));
		//glm::vec3 viewPoint = glm::normalize(glm::vec3(0.0001f, -1.0f, 0.0f));
		//glm::vec3 viewPoint = glm::normalize(glm::vec3(1.0f, 0.0f, 0.0f));
		lightAngle += (PI / 20.0f) * deltaTime;
		//glm::vec3 viewSpacePoint = glm::vec3(0.679251, -0.30667, 0.666762);
		//glm::vec3 pos = glm::vec3(0.0, 0.0, 0.0);
		//glm::vec3 viewSpacePoint = glm::vec3(0.0f, 0.0f, 1.0f);

		initializeLightTESTING(pos, viewPoint);
	}

	void SceneTest1::initializeLightTESTING(glm::vec3 pos, glm::vec3 viewPoint)
	{
		glm::vec3 direction = viewPoint - pos;
		glm::vec3 right = glm::vec3(direction.z, 0, -direction.x);
		glm::vec3 up = glm::normalize(glm::cross(direction, right));
		sunCamera = new Camera(pos, viewPoint, up, 0, 0.1f, 1000);
		sun.direction = glm::vec4(direction, 1.0f);
		sun.position = *sunCamera->transform->position();

		sunCamera->lookAt(lightViewMatrix);
		lightDataUniformBuffer.mapHostVisibleMemory(0, sizeof(glm::vec4) * 2, &sun, true);
	}

	void SceneTest1::initializeViewProjectionMatrices()
	{
		glm::vec3 position = glm::vec3(0, 0, 10);
		glm::vec3 forward = glm::normalize(glm::vec3(1, 0, 10) - position);

		camera1 = new Camera(position, position + forward, glm::vec3(0, 1, 0), 70, 0.2, 1000.0f);
		camera2 = new Camera(glm::vec3(0, 0, 0), glm::vec3(1.0f, 0, 0), glm::vec3(0, 1, 0), 70, 1.0, 1000);

		mainCamera = camera1;

		initializeViewProjectionMatrices(camera1->nearPlane, camera1->farPlane, camera1->fov);
		initializeUiViewProjectionMatrices();
	}

	void SceneTest1::initializeViewProjectionMatricesTESTING(double near_, double far_, float fov, double nearLightFrustrum, double farLightFrustrum, double fovLightFrustrum)
	{
		const glm::mat4 correctionMatrix = glm::mat4(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);

		projectionMatrix = glm::perspective(static_cast<double>(glm::radians(fov)), static_cast<double>(window->getSize().x / window->getSize().y), near_, far_);
		projectionMatrix = correctionMatrix * projectionMatrix; // Correct the matrix to get the Y axis upwards and not downwards

		mainCamera->lookAt(viewMatrix);

		lightProjectionMatrix = glm::perspective(static_cast<double>(glm::radians(fovLightFrustrum)), static_cast<double>(window->getSize().x / window->getSize().y), nearLightFrustrum, farLightFrustrum);
		lightProjectionMatrix = correctionMatrix * lightProjectionMatrix;

		glm::mat4 viewProjection = projectionMatrix * viewMatrix;
		//glm::mat4 shadowViewProjection = lightProjectionMatrix * lightViewMatrix;

		//setViewProjectionUniformBufferData(glm::value_ptr(viewProjection), glm::value_ptr(viewMatrix), glm::value_ptr(shadowViewProjection), &glm::vec3(*mainCamera->transform->position()));
		setViewProjectionUniformBufferData(glm::value_ptr(viewProjection), glm::value_ptr(viewMatrix), mainCamera->transform->position(),
			reinterpret_cast<float*>(cascadeOrthoProjections));
	}

	void SceneTest1::initializeViewProjectionMatrices(double near_, double far_, float fov)
	{
		const glm::mat4 correctionMatrix = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);
		double ratio = window->getSize().x / window->getSize().y;

		projectionMatrix = glm::perspective(static_cast<double>(glm::radians(fov)), ratio, near_, far_);
		projectionMatrix = correctionMatrix * projectionMatrix; // Correct the matrix to get the Y axis upwards and not downwards

		mainCamera->lookAt(viewMatrix);

		glm::mat4 viewProjection = projectionMatrix * viewMatrix;

		initializeShaderPcfData();
		initializeViewprojectionMatricesCascade();

		setViewProjectionUniformBufferData(glm::value_ptr(viewProjection), glm::value_ptr(viewMatrix), mainCamera->transform->position(),
			reinterpret_cast<float*>(cascadeOrthoProjections));

	}

	void SceneTest1::initializeUiViewProjectionMatrices()
	{
		const glm::mat4 correctionMatrix = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);
		orthographicProjectionMatrix = glm::ortho(0.0f, window->getSize().x, window->getSize().y, 0.0f, 0.0f, 100.0f);
		orthographicProjectionMatrix = correctionMatrix * orthographicProjectionMatrix;

		glm::mat4 viewProjection = orthographicProjectionMatrix * viewMatrix;

		uiViewProjectionUniformBuffer.mapHostVisibleMemory(0, sizeof(mat4), value_ptr(viewProjection), true);

	}

	void SceneTest1::switchPlayerCamera()
	{
		const glm::mat4 correctionMatrix = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);
		double ratio = window->getSize().x / window->getSize().y;


		if (cameraIndexUsed == 0)
		{
			cameraIndexUsed = 1;
			mainCamera = camera2;
		}
		else if (cameraIndexUsed == 1)
		{
			cameraIndexUsed = 0;
			mainCamera = camera1;
		}

		projectionMatrix = glm::perspective(static_cast<double>(glm::radians(mainCamera->fov)), ratio, (double)mainCamera->nearPlane, (double)mainCamera->farPlane);
		projectionMatrix = correctionMatrix * projectionMatrix; // Correct the matrix to get the Y axis upwards and not downwards
		mainCamera->lookAt(viewMatrix);
		glm::mat4 viewProjection = projectionMatrix * viewMatrix;
	}

	void SceneTest1::initializeViewprojectionMatricesCascade()
	{
		glm::vec3 points[8];

		float cascadeSize = camera1->farPlane / (float)numberOfCascade;

		float nearPlane = camera1->nearPlane;
		float farPlane = camera1->farPlane;
		float clipRange = farPlane - nearPlane;
		float planeRatio = farPlane / nearPlane;

		//float planeRatio = farPlane / nearPlane;

		glm::vec3 direction = sun.direction;
		glm::vec3 right = glm::vec3(direction.z, 0, -direction.x);
		glm::vec3 up = glm::normalize(glm::cross(direction, right));

		// Transform the projection points depth component from [-1.0, 1.0] to [0.0, 1.0] and flip the y axis
		const glm::mat4 correctionMatrix = glm::mat4(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);

		glm::mat4 viewMatrixCam1;
		camera1->lookAt(viewMatrixCam1);
		glm::mat4 invProj = glm::inverse(projectionMatrix);
		glm::mat4 invView = glm::inverse(viewMatrixCam1);
		glm::mat4 invLight = glm::inverse(lightViewMatrix);

		for (int i = 0; i < numberOfCascade; i++)
		{
			// Create the points for the frustum in projection space [0.0, 1.0]
			// near
			points[0] = glm::vec3(1.0f, -1.0f, 0.0f);
			points[1] = glm::vec3(-1.0f, -1.0f, 0.0f);
			points[2] = glm::vec3(1.0f, 1.0f, 0.0f);
			points[3] = glm::vec3(-1.0f, 1.0f, 0.0f);


			// Calculate the farValue
			// Based on the practical method presented in https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch10.html
			float cascadeRatio = (i + 1) / (float)numberOfCascade;
			float logSplit = nearPlane * std::pow(planeRatio, cascadeRatio);
			float uniformSplit = nearPlane + (clipRange * cascadeRatio);
			float lambda = 0.95f;
			float split = (lambda * logSplit) + ((1 - lambda) * uniformSplit);


			glm::vec4 projectionFarPlaneValue = projectionMatrix * glm::vec4(0.0f, 0.0f, -split, 1.0f);
			projectionFarPlaneValue /= projectionFarPlaneValue.w;

			// far
			points[4] = glm::vec3(1.0f, -1.0f, projectionFarPlaneValue.z);
			points[5] = glm::vec3(-1.0f, -1.0f, projectionFarPlaneValue.z);
			points[6] = glm::vec3(1.0f, 1.0f, projectionFarPlaneValue.z);
			points[7] = glm::vec3(-1.0f, 1.0f, projectionFarPlaneValue.z);

			// Transform the frustum points from projectionSpace to viewSpace then viewSpace to worldSpace then worldSpace to lightViewSpace
			for (int z = 0; z < 8; z++)
			{
				glm::vec4 viewSpacePoint = invProj * glm::vec4(points[z], 1.0f);
				viewSpacePoint = viewSpacePoint / viewSpacePoint.w;

				glm::vec4 worldSpacePoint = invView * viewSpacePoint;

				glm::vec4 lightSpacePoint = lightViewMatrix * worldSpacePoint;

				points[z] = lightSpacePoint;
			}

			// Find the minimum and maximum of the frustum
			auto minXPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.x < left.x; };
			auto minYPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.y < left.y; };
			auto minZPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.z < left.z; };

			auto maxXPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.x > left.x; };
			auto maxYPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.y > left.y; };
			auto maxZPredicat = [](glm::vec3& right, glm::vec3& left) -> bool { return right.z > left.z; };

			std::vector<glm::vec3> pointsVector(8);
			std::copy(points, &points[8], pointsVector.data());

			glm::vec3 minPoint = glm::vec3(std::min_element(pointsVector.begin(), pointsVector.end(), minXPredicat)->x,
				std::min_element(pointsVector.begin(), pointsVector.end(), minYPredicat)->y,
				std::min_element(pointsVector.begin(), pointsVector.end(), minZPredicat)->z);

			glm::vec3 maxPoint = glm::vec3(std::min_element(pointsVector.begin(), pointsVector.end(), maxXPredicat)->x,
				std::min_element(pointsVector.begin(), pointsVector.end(), maxYPredicat)->y,
				std::min_element(pointsVector.begin(), pointsVector.end(), maxZPredicat)->z);


			glm::vec3 cuboidSize = glm::abs(maxPoint - minPoint);
			glm::vec3 halfSize = cuboidSize / 2.0f;

			// Create a cuboid that completly surround the frustum
			glm::vec3 cuboidPoints[8]
			{
				// near
				glm::vec3(maxPoint.x, minPoint.y, minPoint.z),
				glm::vec3(minPoint.x, minPoint.y, minPoint.z),
				glm::vec3(maxPoint.x, maxPoint.y, minPoint.z),
				glm::vec3(minPoint.x, maxPoint.y, minPoint.z),

				// far
				glm::vec3(maxPoint.x, minPoint.y, maxPoint.z),
				glm::vec3(minPoint.x, minPoint.y, maxPoint.z),
				glm::vec3(maxPoint.x, maxPoint.y, maxPoint.z),
				glm::vec3(minPoint.x, maxPoint.y, maxPoint.z)
			};

			// Find the cuboidCenter
			glm::vec3 cuboidCenter = glm::vec3(0.0f);
			for (int z = 0; z < 8; z++)
			{
				cuboidCenter += cuboidPoints[z];
			}
			cuboidCenter /= 8.0f;

			// Find the the position for the lightViewMatrix relative to the cuboidCenter
			glm::vec3 relativeLightPosition = glm::vec3(cuboidCenter.x, cuboidCenter.y, cuboidCenter.z + halfSize.z + 100);

			// Transform the relativePosition and the cuboidCenter from lightViewSpace to worldSpace
			relativeLightPosition = invLight * glm::vec4(relativeLightPosition, 1.0f);
			cuboidCenter = invLight * glm::vec4(cuboidCenter, 1.0f);
			glm::vec3 debugDirection = glm::normalize(cuboidCenter - relativeLightPosition);

			// Create the lightViewMatrix and the orthographic projection
			glm::mat4 lightView = glm::lookAt(relativeLightPosition, glm::vec3(cuboidCenter), up);
			cascadeOrthoProjections[i] = glm::ortho(-halfSize.x, halfSize.x, -halfSize.y, halfSize.y, 0.0f, cuboidSize.z + 100);

			cascadeOrthoProjections[i] = correctionMatrix * cascadeOrthoProjections[i] * lightView;
		}

	}

	void SceneTest1::initializeShaderPcfData()
	{
		if (pcfData == nullptr)
		{
			pcfData = new glm::vec4[numberOfCascade];
		}

		for (int i = 0; i < numberOfCascade; i++)
		{
			pcfData[i].x = 1.0f / cascadesScissors[i].extent.width; // dx
			pcfData[i].y = 1.0f / cascadesScissors[i].extent.height; // dy
			pcfData[i].z = basePcfRange - (4 * i); // range
			pcfData[i].w = 1.0f / pow((pcfData[i].z * 2) + 1, 2); // inverseCount
		}

		pcfDataUniformBuffer.mapHostVisibleMemory(0, sizeof(glm::vec4) * numberOfCascade, pcfData, true);
	}

	void SceneTest1::setViewProjectionUniformBufferData(float* viewProjection, float* viewMatrix, float* shadowViewProjection, glm::vec3* cameraPosition)
	{
		viewProjectionUniformBuffer.mapHostVisibleMemory(0, (sizeof(glm::mat4) * 3) + sizeof(glm::vec3), viewProjection, false);
		viewProjectionUniformBuffer.updateHostVisibleMemory(sizeof(glm::mat4) * 1, sizeof(glm::mat4), viewMatrix);
		viewProjectionUniformBuffer.updateHostVisibleMemory(sizeof(glm::mat4) * 2, sizeof(glm::mat4), shadowViewProjection);
		viewProjectionUniformBuffer.updateHostVisibleMemory(sizeof(glm::mat4) * 3, sizeof(glm::vec3), cameraPosition);
	}

	void SceneTest1::setViewProjectionUniformBufferData(float* viewProjection, float* viewMatrix, glm::vec4* cameraPosition, float* shadowViewProjectionMatrices)
	{
		//viewProjectionUniformBuffer.mapHostVisibleMemory(0, (sizeof(glm::mat4) * 2) + sizeof(glm::vec3) + (sizeof(glm::mat4) * numberOfCascade) + (sizeof(float)), viewProjection, false);
		//viewProjectionUniformBuffer.updateHostVisibleMemory(sizeof(glm::mat4) * 1, sizeof(glm::mat4), viewMatrix);
		//viewProjectionUniformBuffer.updateHostVisibleMemory(sizeof(glm::mat4) * 2, sizeof(glm::vec3), cameraPosition);
		//viewProjectionUniformBuffer.updateHostVisibleMemory((sizeof(glm::mat4) * 2) + sizeof(glm::vec3), (sizeof(glm::mat4) * numberOfCascade), shadowViewProjectionMatrices);
		//viewProjectionUniformBuffer.updateHostVisibleMemory((sizeof(glm::mat4) * 2) + sizeof(glm::vec3) + (sizeof(glm::mat4) * numberOfCascade), sizeof(float), &farPlane);

		uint32 sizeMat4 = sizeof(glm::mat4);
		uint32 sizeFloat = sizeof(float);
		uint32 sizeVec4 = sizeof(glm::vec4);

		float* data = new float[((sizeof(glm::mat4) * 2) + sizeof(glm::vec4) + (sizeof(glm::mat4) * numberOfCascade) + (sizeof(float))) / sizeof(float)];
		memcpy(data, viewProjection, sizeMat4);
		memcpy(data + (sizeMat4 / sizeFloat), viewMatrix, sizeMat4);
		memcpy(data + ((sizeMat4 * 2) / sizeFloat), shadowViewProjectionMatrices, sizeMat4 * numberOfCascade);
		memcpy(data + (((sizeMat4 * 2) + (sizeMat4 * numberOfCascade)) / sizeFloat), reinterpret_cast<float*>(cameraPosition), sizeVec4);
		viewProjectionUniformBuffer.mapHostVisibleMemory(0, (sizeMat4 * 2) + sizeVec4 + (sizeMat4 * numberOfCascade), data, true);
	}

	void SceneTest1::createRenderPassesAndFramebuffers()
	{
		createSceneRenderPass();
		createSceneFramebuffers();

		createShadowRenderPass();
		createShadowFramebuffer();

		createUiRenderPass();
		createUiFrameBuffers();
	}

	void SceneTest1::createSceneRenderPass()
	{
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.flags = 0;
		colorAttachment.format = window->surfaceFormat.format;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentDescription depthAttachment = {};
		depthAttachment.flags = 0;
		depthAttachment.format = VK_FORMAT_D32_SFLOAT;
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		SubpassParameters subPassParams = {};
		subPassParams.colorAttachments.push_back(colorAttachmentRef);
		subPassParams.pipelineType = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subPassParams.depthStencilAttachment = depthAttachmentRef;

		VkSubpassDependency subpassDepency = {};
		subpassDepency.srcSubpass = VK_SUBPASS_EXTERNAL;
		subpassDepency.dstSubpass = 0;
		subpassDepency.srcStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		subpassDepency.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		subpassDepency.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		subpassDepency.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		subpassDepency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		VkSubpassDependency subpassDepency2 = {};
		subpassDepency2.srcSubpass = 0;
		subpassDepency2.dstSubpass = VK_SUBPASS_EXTERNAL;
		subpassDepency2.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		subpassDepency2.dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		subpassDepency2.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		subpassDepency2.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		subpassDepency2.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		//VkSubpassDependency subpassDepency = {};
		//subpassDepency.srcSubpass = VK_SUBPASS_EXTERNAL;
		//subpassDepency.dstSubpass = 0;
		//subpassDepency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		//subpassDepency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		//subpassDepency.srcAccessMask = 0;
		//subpassDepency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		//subpassDepency.dependencyFlags = 0;


		sceneRenderPass.initialize(std::vector<VkAttachmentDescription>{ colorAttachment, depthAttachment }, std::vector<SubpassParameters>{subPassParams},
			std::vector<VkSubpassDependency>{ subpassDepency, subpassDepency2 });
	}

	void SceneTest1::createSceneFramebuffers()
	{
		sceneFrameBuffers.resize(window->swapchainImageViews.size());
		for (size_t i = 0; i < sceneFrameBuffers.size(); i++)
		{
			sceneFrameBuffers[i].initialize(sceneRenderPass, std::vector<VkImageView>{window->swapchainImageViews[i], depthImage.imageViewHandles[0]}, window->swapchainExtent, 1);
		}
	}

	void SceneTest1::createShadowRenderPass()
	{
		VkAttachmentDescription shadowMapAttachment = {};
		shadowMapAttachment.flags = 0;
		shadowMapAttachment.format = VK_FORMAT_D32_SFLOAT;
		shadowMapAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		shadowMapAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		shadowMapAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		shadowMapAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		shadowMapAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		shadowMapAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		shadowMapAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 0;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		SubpassParameters subPassParams = {};
		subPassParams.pipelineType = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subPassParams.depthStencilAttachment = depthAttachmentRef;


		VkSubpassDependency subpassDepency = {};
		subpassDepency.srcSubpass = VK_SUBPASS_EXTERNAL;
		subpassDepency.dstSubpass = 0;
		subpassDepency.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		subpassDepency.dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		subpassDepency.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
		subpassDepency.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		subpassDepency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		VkSubpassDependency subpassDepency2 = {};
		subpassDepency2.srcSubpass = 0;
		subpassDepency2.dstSubpass = VK_SUBPASS_EXTERNAL;
		subpassDepency2.srcStageMask = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		subpassDepency2.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		subpassDepency2.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		subpassDepency2.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		subpassDepency2.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		//VkSubpassDependency subpassDepency = {};
		//subpassDepency.srcSubpass = VK_SUBPASS_EXTERNAL;
		//subpassDepency.dstSubpass = 0;
		//subpassDepency.srcStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
		//subpassDepency.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
		//subpassDepency.srcAccessMask = 0;
		//subpassDepency.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		//subpassDepency.dependencyFlags = 0;

		shadowRenderPass.initialize(std::vector<VkAttachmentDescription>{ shadowMapAttachment }, std::vector<SubpassParameters>{subPassParams},
			std::vector<VkSubpassDependency>{ subpassDepency, subpassDepency2 });
	}

	void SceneTest1::createShadowFramebuffer()
	{
		//shadowFrameBuffer.initialize(shadowRenderPass, std::vector<VkImageView>{shadowMap.image->imageViewHandle}, window->swapchainExtent, 1);
		shadowFrameBuffer.resize(numberOfCascade);
		for (int i = 0; i < numberOfCascade; i++)
		{
			shadowFrameBuffer[i].initialize(shadowRenderPass, std::vector<VkImageView>{shadowMap.samplers[i]->image->imageViewHandles[0]}, cascadesScissors[i].extent, 1);
		}
	}

	void SceneTest1::createUiRenderPass()
	{
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.flags = 0;
		colorAttachment.format = window->surfaceFormat.format;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentDescription depthAttachment = {};
		depthAttachment.flags = 0;
		depthAttachment.format = VK_FORMAT_D32_SFLOAT;
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		SubpassParameters subPassParams = {};
		subPassParams.colorAttachments.push_back(colorAttachmentRef);
		subPassParams.pipelineType = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subPassParams.depthStencilAttachment = depthAttachmentRef;

		VkSubpassDependency subpassDepency = {};
		subpassDepency.srcSubpass = VK_SUBPASS_EXTERNAL;
		subpassDepency.dstSubpass = 0;
		subpassDepency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		subpassDepency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		subpassDepency.srcAccessMask = 0;
		subpassDepency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		subpassDepency.dependencyFlags = 0;


		uiRenderPass.initialize(std::vector<VkAttachmentDescription>{ colorAttachment, depthAttachment }, std::vector<SubpassParameters>{subPassParams},
			std::vector<VkSubpassDependency>{ subpassDepency});
	}
	void SceneTest1::createUiFrameBuffers()
	{
		uiFrameBuffers.resize(window->swapchainImageViews.size());
		for (size_t i = 0; i < uiFrameBuffers.size(); i++)
		{
			uiFrameBuffers[i].initialize(uiRenderPass, std::vector<VkImageView>{window->swapchainImageViews[i], depthImage.imageViewHandles[0]}, window->swapchainExtent, 1);
		}
	}
}