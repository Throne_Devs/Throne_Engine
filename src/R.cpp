#pragma once
#include "stdafx.h"

namespace R
{
	namespace S
	{
		namespace ErrorMessage
		{
			string err_consolePaused = "Press any key to continue...";
			string err_unManaged = "Unmanaged error detected in main!";
			string err_unSupportedOperation = "Unsuported operation at ";
			string err_throneInitialization = "Could not initialize the Throne framework!";
			string err_vulkanInstance = "Could not create the Vulkan instance!";
			string err_extensionNumber = "Could not get the number of instance extensions!";
			string err_extensionEnumeration = "Could not enumerate instance extensions!";
			string err_unSupportedExtension = "Unsupported extension is required : ";
			string err_physicalDevicesNumber = "Could not get the number of physical devices!";
			string err_physicalDevicesEnumerate = "Could not enumerate physical devices!";
			string err_physicalDevicesSuitable = "Could not find a suitable physical device!";
			string err_deviceExtensionNumber = "Could not get the number of device extension properties!";
			string err_physicalDevice = "Could not get the physical devices!";
			string err_queueFamilyNumber = "Could not get the number of queue families!";
			string err_queueFamily = "Could not get the queue families!";
			string err_logicalDeviceCreation = "Could not create the logical device!";
			string err_physicalDeviceSupportForQueue = "Could not get the physical device surface support of a specific queue!";
			string err_queueFamilySupportForPresentation = "Could not find a queue family that supports presentation!";
			string err_deviceSurfaceNumber = "Could not get the number of device surface presentation modes!";
			string err_availablePresentationModes = "Could not get the available presentation modes!";
			string err_physicalDeviceCapabality = "Could not get the physical device surface capabilities!";
			string err_commandPoolCreation = "Could not create the command pool!";
			string err_commandBuffer = "Could not allocate the command buffers!";
			string err_vulkanLibrary = "Could not connect with a Vulkan Runtime Library!";
			string err_vulkanLibraryConnect = "Could not connect with a Vulkan Runtime Library!";
			string err_physicalSurfacePresentMode = "Could not get physical surface present modes!";
			string err_swapChainImages = "Could not get swapchain images!";
			string err_swapChain = "Could not create swapchain!";
			string err_imageViewCreation = "Could not create image view!";
			string err_fileOpen = "Could not open file!";
			string err_functionLoad = "Could not load function : ";
			string err_vulkanInstanceFunctionLoad = "Could not load vulkan instance function : ";
			string err_vulkanDeviceFunctionLoad = "Could not load vulkan device function : ";
			string err_shaderModuleCreation = "Could not create shader module!";
			string err_pipelineLayoutCreation = "Could not create pipeline layout!";
			string err_renderPassCreation = "Could not create render pass!";
			string err_pipelineCreation = "Could not create graphics pipeline!";
			string err_framebufferCreation = "Could not create frame buffer!";
			string err_beginRecordingCommandBuffer = "Could not begin the recording on the command buffer!";
			string err_endRecordingCommandBuffer = "Could not end the recording on the command buffer!";
			string err_resetCommandBuffer = "Could not reset the command buffer!";
			string err_createSemaphore = "Could not create the semaphore!";
			string err_createFence = "Could not create the fence!";
			string err_waitForFences = "Waiting for fences failed!";
			string err_resetFences = "Could not reset the fences!";
			string err_queueSubmit = "Could not submit the command buffers to the queue!";
			string err_syncCommandBuffersFirst = "Could not synchronize the first command buffer!";
			string err_syncCommandBuffersSecond = "Could not synchronize the second command buffer!";
			string err_queueWaitIdle = "Could not wait for the queue!";
			string err_deviceWaitIdle = "Could not wait for the device!";
			string err_destroyNullFence = "Could not destroy a fence with a null handle!";
			string err_destroyNullSemaphore = "Could not destroy a semaphore with a null handle!";
			string err_destroyNullCommandPool = "Could not destroy a command pool with a null handle!";
			string err_createBuffer = "Could not create the buffer!";
			string err_allocateBufferMemory = "Could not allocate the memory to the buffer!";
			string err_nullBufferMemoryObject = "Could not allocate memory to a buffer with a null memory object!";
			string err_bindBufferMemory = "Could not bind the buffer memory!";
			string err_createBufferView = "Could not create the buffer view!";
			string err_createImage = "Could not create the image!";
			string err_allocateImageMemory = "Could not allocate the memory to the buffer!";
			string err_nullImageMemoryObject = "Could not allocate memory to a buffer with a null memory object!";
			string err_bindImageMemory = "Could not bind the buffer memory!";
			string err_validationLayer = "The validation layer detected an error : ";
			string err_createImageView = "Could not create the image view!";
			string err_mapMemory = "Could not map the memory!";
			string err_flushMappedMemory = "Could not flush the mapped memory!";
			string err_createSampler = "Could not create the sampler!";
			string err_unsupportedPhysicalDeviceFormatProperty = "Unsupported physical device format property required!";
			string err_unsupportedPhysicalDeviceBufferFeature = "Unsupported physical device buffer feature!";
			string err_createDescriptorSetLayout = "Could not create the descriptor set layout!";
			string err_createDescriptorPool = "Could not create the descriptor pool!";
			string err_allocateDescriptorSets = "Could not allocate the descriptor sets!";
			string err_freeDescriptorSets = "Could not free the descriptor sets!";
			string err_resetDescriptorPool = "Could not reset the descriptor pool!";
			string err_createRenderPass = "Could not create the render pass!";
			string err_createFrameBuffer = "Could not create the framebuffer!";
			string err_readImage = "Could not read image!";
			string err_createShaderModule = "Could not create the shader module!";
			string err_createPipelineLayout = "Could not create the pipelineLayout";
			string err_getPipelineCacheDataSize = "Could not get the pipeline cache data size!";
			string err_getPipelineCacheData = "Could not get the pipeline cache data!";
			string err_mergePipelineCacheObjects = "Could not merge the pipeline cache objects!";
			string err_createGraphicsPipelines = "Could not create the graphics pipelines!";
			string err_createComputePipeline = "Could not create the compute pipeline!";
			string err_debugCallback = "Could not setup debug callback!";
			string err_vulkanValidation = "VULKAN_VALIDATION";
			string err_globalCatch = "GLOBAL_CATCH";
			string err_fontSizeAvailable = "Size available: ";
			string err_fontSizeSetBitmap = "Failed to set bitmap font size to ";
			string err_fontOutlineFailed = "Failed to  outline glyph (no fallback available)";
			string err_fontLoadFromFile = "Failed to load font from memory(failed to set the unicode character set)";
			string err_fontLoadFailed = "Failed to load font ";
			string err_fontInitFreetype = " (failed to initialize freetype)";
			string err_fontCreateFace = " (failed to create the font face)";
			string err_invalidMeshRendererMemoryProperties = "The memory properties used for a MeshRenderer are not valid (It needs to be HOST_VISIBLE or DEVICE_LOCAL).";
			string err_triedToUpdateStaticText = "A static Text is being updated.";
			string err_getRenderPassInfoByRenderPassHandleNotFound = "Could not find the renderPass in RenderedObject::getRenderPassInfoByRenderPassHandle";
		}
		namespace StandardOutput
		{
			string sdo_windowName = "Throne Engine";
			string sdo_extensionNumber = "Number of extensions : ";
			string sdo_extension = " Extension : ";
			string sdo_shaderMethodToInvoke = "main";
			string sdo_stringFPSoutput = "FPS : ";
			wstring sdo_wStringFPSoutput = L"FPS : ";
		}
		namespace Path
		{
			LPCWSTR path_windowsVulkanLibrary = _T("vulkan-1.dll");
			string path_linuxVulkanLibrary = "libvulkan.so.1";

			string path_triangleShaderVert1 = "Ressources/Shaders/1/vert.spv";
			string path_triangleShaderFrag1 = "Ressources/Shaders/1/frag.spv";

			string path_triangleShaderVert2 = "Ressources/Shaders/2/vert.spv";
			string path_triangleShaderFrag2 = "Ressources/Shaders/2/frag.spv";

			string path_skyboxShaderVert = "Ressources/Shaders/skybox/vert.spv";
			string path_skyboxShaderFrag = "Ressources/Shaders/skybox/frag.spv";

			string path_shadowShaderVert = "Ressources/Shaders/Shadows/shadowsVert.spv";
			string path_shadowSceneShaderVert = "Ressources/Shaders/Shadows/sceneVert.spv";
			string path_shadowSceneShaderFrag = "Ressources/Shaders/Shadows/sceneFrag.spv";

			string path_shadowTransparentShaderVert = "Ressources/Shaders/Shadows/Transparent/shadowsVert.spv";
			string path_shadowTransparentShaderFrag = "Ressources/Shaders/Shadows/Transparent/shadowsFrag.spv";
			string path_shadowTransparentSceneShaderFrag = "Ressources/Shaders/Shadows/Transparent/sceneFrag.spv";


			string path_shadowCascadeShaderVert = "Ressources/Shaders/CascadedShadows/CascadedShadowsVert.spv";
			string path_shadowCascadeSceneShaderVert = "Ressources/Shaders/CascadedShadows/CascadedSceneVert.spv";
			string path_shadowCascadeSceneShaderFrag = "Ressources/Shaders/CascadedShadows/CascadedSceneFrag.spv";

			string path_shadowCascadeTransparentShaderVert = "Ressources/Shaders/CascadedShadows/Transparent/CascadedTransparentShadowsVert.spv";
			string path_shadowCascadeTransparentShaderFrag = "Ressources/Shaders/CascadedShadows/Transparent/CascadedTransparentShadowsFrag.spv";
			string path_shadowCascadeTransparentSceneShaderFrag = "Ressources/Shaders/CascadedShadows/Transparent/CascadedTransparentSceneFrag.spv";

			string path_lunarGStandardValidation = "VK_LAYER_LUNARG_standard_validation";
			string path_dragon3DModel = "Ressources/Models/Dragon3.obj";
			string path_box3DModel = "Ressources/Models/Box.obj";
			string path_boxTexture = "Ressources/Textures/boxTexture.jpg";
			string path_planeModel = "Ressources/Models/Plane.obj";
			std::vector<std::string> path_testSkyboxTexture = std::vector<string>{ "Ressources/Textures/Skybox/TestSkybox/right.png", "Ressources/Textures/Skybox/TestSkybox/left.png", "Ressources/Textures/Skybox/TestSkybox/top.png", "Ressources/Textures/Skybox/TestSkybox/bottom.png", "Ressources/Textures/Skybox/TestSkybox/front.png", "Ressources/Textures/Skybox/TestSkybox/back.png" };
			std::vector<std::string> path_interstellarSkyboxTexture = std::vector<string>{ "Ressources/Textures/Skybox/Interstellar/right.png", "Ressources/Textures/Skybox/Interstellar/left.png", "Ressources/Textures/Skybox/Interstellar/top.png", "Ressources/Textures/Skybox/Interstellar/bottom.png", "Ressources/Textures/Skybox/Interstellar/front.png", "Ressources/Textures/Skybox/Interstellar/back.png" };
			string path_skyboxCube = "Ressources/Models/Skybox.obj";
			string path_grassTerrainTexture = "Ressources/Textures/Grass.png";

			#pragma region TreeEU55
			string path_treeEU55Model = "Ressources/Models/TreeEU55.obj";

			string path_treeTextureEU55Brk1 = "Ressources/Textures/Tree/Bark/EU55brk1.png";
			string path_treeTextureEU55Brk2 = "Ressources/Textures/Tree/Bark/EU55brk2.png";
			string path_treeTextureEU55Brk3 = "Ressources/Textures/Tree/Bark/EU55brk3.png";

			string path_treeTextureEU55Brn1 = "Ressources/Textures/Tree/Branch/EU55brn1.png";
			string path_treeTextureEU55Brn2 = "Ressources/Textures/Tree/Branch/EU55brn2.png";
			string path_treeTextureEU55Brn3 = "Ressources/Textures/Tree/Branch/EU55brn3.png";
			string path_treeTextureEU55Brn4 = "Ressources/Textures/Tree/Branch/EU55brn4.png";
			string path_treeTextureEU55Brn5 = "Ressources/Textures/Tree/Branch/EU55brn5.png";
			string path_treeTextureEU55Brn6 = "Ressources/Textures/Tree/Branch/EU55brn6.png";
			string path_treeTextureEU55Brn7 = "Ressources/Textures/Tree/Branch/EU55brn7.png";

			string path_treeTextureEU55Bud1 = "Ressources/Textures/Tree/Bud/EU55bud1.png";
			string path_treeTextureEU55Bud2 = "Ressources/Textures/Tree/Bud/EU55bud2.png";

			string path_treeTextureEU55Flw1 = "Ressources/Textures/Tree/Flower/EU55flw1.png";

			string path_treeTextureEU55Rts1 = "Ressources/Textures/Tree/Roots/EU55rts1.png";
			string path_treeTextureEU55Rts2 = "Ressources/Textures/Tree/Roots/EU55rts2.png";
			string path_treeTextureEU55Rts3 = "Ressources/Textures/Tree/Roots/EU55rts2.png";

			string path_treeTextureEU55Stk1 = "Ressources/Textures/Tree/Stalk/EU55stk1.png";

			string path_treeTextureEU55Trk1 = "Ressources/Textures/Tree/Trunk/EU55trk1.png";
			string path_treeTextureEU55Trk2 = "Ressources/Textures/Tree/Trunk/EU55trk2.png";
			string path_treeTextureEU55Trk3 = "Ressources/Textures/Tree/Trunk/EU55trk3.png";

			string path_treeTextureEU55Twg1 = "Ressources/Textures/Tree/Twig/EU55twg1.png";
			string path_treeTextureEU55Twg2 = "Ressources/Textures/Tree/Twig/EU55twg2.png";
			#pragma endregion

			string path_configurationFiles = "cfg";
			string path_configurationFilesDefault = "cfg/default.cfg";
			string path_configurationFilesConfig = "cfg/config.cfg";
			string path_logsFiles = "Logs";

			string path_arialFont = "Ressources/Fonts/Arial.ttf";
			string path_itcedscrFont = "Ressources/Fonts/ITCEDSCR.ttf";
			string path_tahomaFont = "Ressources/Fonts/tahoma.ttf";

			string path_uiTextVertShader = "Ressources/Shaders/UI/uiTextVert.spv";
			string path_uiTextFragShader = "Ressources/Shaders/UI/uiTextFrag.spv";

			string path_uiDefaultTexture = "Ressources/Textures/UI/UI_Default.png";
			string path_uiKnobTexture = "Ressources/Textures/UI/Knob.png";
			string path_uiInputFieldBackgroundTexture = "Ressources/Textures/UI/InputFieldBackground.png";
			string path_uiDropDownArrowTexture = "Ressources/Textures/UI/DropDownArrow.png";
			string path_uiCheckmarkTexture = "Ressources/Textures/UI/Checkmark.png";
			string path_uiBackgroundTexture = "Ressources/Textures/UI/Background.png";

			string path_uiDefaultVertexShader = "Ressources/Shaders/UI/uiDefaultVert.spv";
			string path_uiDefaultFragmentShader = "Ressources/Shaders/UI/uiDefaultFrag.spv";
		}
		namespace C
		{
			const string CONFIG_FILES_EXT = ".cfg";
			const string LOG_FILES_EXT = ".log";
			const string KEYS_INDEX = "Key";
			const string FORWARD_INDEX = "Foward";
			const string BACKWARD_INDEX = "Backward";
			const string LEFT_INDEX = "Left";
			const string RIGHT_INDEX = "Right";
			const string UP_INDEX = "Up";
			const string DOWN_INDEX = "Down";
			const string CLOSE_WINDOW_INDEX = "CloseWindow";
			const string MOUSE_VISIBILITY_INDEX = "ToggleMouseVisibility";
			const string MOUSE_GRABED_INDEX = "ToggleMouseGrabed";
			const string TELEPORT_SUN_INDEX = "TeleportToSun";
			const string PRINT_POSITION_INDEX = "PrintCurrentPosition";
			const string PRINT_VIEWPOINT_INDEX = "PrintCurrentViewpoint";
			const string BUTTONS_INDEX = "Button";
			const string RESET_CAMERA_SPEED_INDEX = "ResetCameraSpeed";
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES1 = "ChangeProjectionValues1";
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES2 = "ChangeProjectionValues2";
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES3 = "ChangeProjectionValues3";
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES4 = "ChangeProjectionValues4";
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES5 = "ChangeProjectionValues5";
			THRONE_API_ENTRY const string CHANGE_LIGHT_SETUP1 = "ChangeLightSetup1";
			THRONE_API_ENTRY const string CHANGE_LIGHT_SETUP2 = "ChangeLightSetup2";
		}
	}

	namespace C
	{
		const int TIMEOUT_FENCE = 5000000000;

		const double PI = 3.16159265359;

		uint32 WINDOW_WIDTH = 1280;
		uint32 WINDOW_HEIGTH = 720;

		const uint32 WINDOW_WIDTH_MAXMININFO = 50000;
		const uint32 WINDOW_HEIGHT_MAXMININFO = 50000;

		const double PROJECTION_NEAR = 0.1;
		const double PROJECTION_FAR = 2000.0;
	}
}
