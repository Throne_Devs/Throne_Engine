#include "stdafx.h"

#include "UtilityStructures.hpp"

namespace ThroneEngine
{
	RenderedObjectRenderPassInfo::RenderedObjectRenderPassInfo()
	{
	}
	RenderedObjectRenderPassInfo::RenderedObjectRenderPassInfo(std::deque<RenderedObjectPart>& renderedObjectParts, RenderPass* renderPass,
		std::vector<DescriptorSet*>& batchableDescriptorSets) : renderedObjectParts(renderedObjectParts), renderPass(renderPass), batchableDescriptorSets(batchableDescriptorSets)
	{
	}

	RenderedObjectPart::RenderedObjectPart()
	{
	}
	RenderedObjectPart::RenderedObjectPart(MeshPart meshPart, Material* material, GraphicsPipeline* graphicsPipeline, bool isActive) :
		meshPart(meshPart), material(material), graphicsPipeline(graphicsPipeline), isActive(isActive)
	{
	}

	RenderedObjectCommandRecorder::RenderedObjectCommandRecorder()
	{
	}
	RenderedObjectCommandRecorder::RenderedObjectCommandRecorder(RenderedObjectPart* renderedObjectPart, RenderedObject* renderedObject) :
		renderedObjectPart(renderedObjectPart), renderedObject(renderedObject)
	{
	}

	VertexBufferSignature::VertexBufferSignature()
	{
	}
	VertexBufferSignature::VertexBufferSignature(uint32 bindingIndex, VkVertexInputRate inputRate, std::vector<VertexBufferBlock> blocks) :
		bindingIndex(bindingIndex), inputRate(inputRate), blocks(blocks)
	{
	}

	VertexBufferBlock::VertexBufferBlock()
	{
	}
	VertexBufferBlock::VertexBufferBlock(uint32 typeSize, uint32 count, uint32 shaderLocation, VkFormat format) :
		typeSize(typeSize), count(count), shaderLocation(shaderLocation), format(format)
	{
	}
}

