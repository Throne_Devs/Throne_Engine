#pragma once
#include "stdafx.h"

#include "Bindable.h"

namespace ThroneEngine
{
	class GreaterPredicat
	{
	public:
		inline constexpr bool operator()(const std::pair<Bindable*, int>& left, const std::pair<Bindable*, int>& right) const
		{
			return left.second > right.second;
		}
	};

	template <class Bindable, class BindingNode>
	class FindBindingNodePredicat
	{
	public:
		Bindable* searchingValue = nullptr;
		inline constexpr bool operator()(const BindingNode& currentValue) const
		{
			return currentValue.currentBind == searchingValue;
		}
	};
}
