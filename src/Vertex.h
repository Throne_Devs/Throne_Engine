#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"

namespace ThroneEngine
{
	enum VertexFormat
	{
		VERTEX_FORMAT_P,
		VERTEX_FORMAT_P_C,
		VERTEX_FORMAT_P_UV,
		VERTEX_FORMAT_P_UV_N,
		VERTEX_FORMAT_P_UV_C
	};


	struct VertexWithColor
	{
		vec3 position;
		vec3 color;

		static VkVertexInputBindingDescription getBindingDescription();
		static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
	};

	struct Vertex
	{
		vec3 position;

		static VkVertexInputBindingDescription getBindingDescription();
		static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
	};

	struct VertexWithTextureUV
	{
		vec3 position;
		vec2 textureUV;

		static VkVertexInputBindingDescription getBindingDescription();
		static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
	};

	struct VertexWithUVAndNormal
	{
		vec3 position;
		vec2 textureUV;
		vec3 normal;

		static VkVertexInputBindingDescription getBindingDescription();
		static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
	};

	struct VertexWithUVAndColor
	{
		vec3 position;
		vec2 textureUV;
		vec3 color;

		static VkVertexInputBindingDescription getBindingDescription();
		static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
	};
}

