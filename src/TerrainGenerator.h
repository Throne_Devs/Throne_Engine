#pragma once
#include "stdafx.h"

#include "Mesh.h"

namespace ThroneEngine
{
	class TerrainGenerator
	{
		TerrainGenerator();
		~TerrainGenerator();

	public:


		static void generateTerrain(Mesh* mesh, uint32 sizeX, uint32 sizeZ);
		static void generateFlatTerrain(Mesh* mesh, uint32 sizeX, uint32 sizeZ, int height);
	};
}



