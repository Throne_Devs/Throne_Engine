#include "stdafx.h"

#include "DescriptorSet.h"
#include "UtilityFunctions.hpp"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	DescriptorSet::DescriptorSet()
	{
	}


	DescriptorSet::~DescriptorSet()
	{
	}

	bool DescriptorSet::initialize(std::vector<Descriptor*>& descriptors, DescriptorSetLayout& setLayout, uint32 descriptorSetLocation)
	{
		return initialize(getVectorDataPointer(descriptors), descriptors.size(), setLayout, descriptorSetLocation);
	}


	bool DescriptorSet::initialize(Descriptor** descriptors, uint32 amountOfResource, DescriptorSetLayout& setLayout, uint32 descriptorSetLocation)
	{
		this->setLayout = setLayout;
		this->descriptorSetLocation = descriptorSetLocation;

		addDescriptors(descriptors, amountOfResource);

		std::vector<VkDescriptorPoolSize> descriptorPoolSize(amountOfResource);


		for (int i = 0; i < amountOfResource; i++)
		{
			descriptorPoolSize[i].descriptorCount = descriptors[i]->descriptorCount; // for arrays of descriptor (example arrays of imageSampler)
			descriptorPoolSize[i].type = descriptors[i]->descriptorType;
		}

		if (createDescriptorPool(descriptorPoolSize, 1, false, descriptorPool) == false)
		{
			return false;
		}
		if (allocateDescriptorSets(descriptorPool, &setLayout.handle, 1) == false)
		{
			return false;
		}

		std::vector<VkWriteDescriptorSet> writeDescriptorSets(amountOfResource);
		for (int i = 0; i < amountOfResource; i++)
		{
			this->descriptors[i]->initializeWriteDescriptorSet(handle, i);
			writeDescriptorSets[i] = this->descriptors[i]->writeDescriptorSet;
		}

		updateDescriptorSets(&writeDescriptorSets, nullptr);

		return true;
	}

	void DescriptorSet::addDescriptors(Descriptor** descriptors, uint32 amountOfResource)
	{
		this->descriptors.insert(this->descriptors.end(), descriptors, descriptors + amountOfResource);
	}

	bool DescriptorSet::createDescriptorPool(std::vector<VkDescriptorPoolSize>& descriptorTypes, int maxSets, bool descriptorSetsCanBeFreedIndividualy, VkDescriptorPool & descriptorPool)
	{
		VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
		descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		descriptorPoolCreateInfo.pNext = nullptr;
		descriptorPoolCreateInfo.flags = descriptorSetsCanBeFreedIndividualy == true ? VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT : 0;
		descriptorPoolCreateInfo.maxSets = maxSets;
		descriptorPoolCreateInfo.poolSizeCount = descriptorTypes.size();
		descriptorPoolCreateInfo.pPoolSizes = descriptorTypes.size() > 0 ? &descriptorTypes[0] : nullptr;

		VkResult result = vkCreateDescriptorPool(logicalDevice, &descriptorPoolCreateInfo, nullptr, &descriptorPool);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createDescriptorPool);
			#endif
			return false;
		}
		#endif

		return true;
	}

	bool DescriptorSet::allocateDescriptorSets(VkDescriptorPool descriptorPool, std::vector<VkDescriptorSetLayout>& descriptorSetLayouts)
	{
		return allocateDescriptorSets(descriptorPool, getVectorDataPointer(descriptorSetLayouts), descriptorSetLayouts.size());
	}

	bool DescriptorSet::allocateDescriptorSets(VkDescriptorPool descriptorPool, VkDescriptorSetLayout* descriptorSetLayouts, uint32 amountOfSetLayouts)
	{
		VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
		descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		descriptorSetAllocateInfo.pNext = nullptr;
		descriptorSetAllocateInfo.descriptorPool = descriptorPool;
		descriptorSetAllocateInfo.descriptorSetCount = amountOfSetLayouts;
		descriptorSetAllocateInfo.pSetLayouts = descriptorSetLayouts;

		VkResult result = vkAllocateDescriptorSets(logicalDevice, &descriptorSetAllocateInfo, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_allocateDescriptorSets);
			#endif
			return false;
		}
		#endif

		return true;
	}

	void DescriptorSet::updateDescriptorSets(std::vector<VkWriteDescriptorSet>* writeDescriptorSets, std::vector<VkCopyDescriptorSet>* copyDescriptorSets)
	{
		vkUpdateDescriptorSets(logicalDevice, 
			writeDescriptorSets == nullptr ? 0 : writeDescriptorSets->size(), writeDescriptorSets == nullptr ? nullptr : getVectorDataPointer(*writeDescriptorSets),
			copyDescriptorSets == nullptr ? 0 : copyDescriptorSets->size(), copyDescriptorSets == nullptr ? 0 : getVectorDataPointer(*copyDescriptorSets));
	}

	void DescriptorSet::updateDescriptorSets()
	{
		std::vector<VkWriteDescriptorSet> writeDescriptorSets(descriptors.size());
		for (int i = 0; i < descriptors.size(); i++)
		{
			this->descriptors[i]->initializeWriteDescriptorSet(handle, i);
			writeDescriptorSets[i] = this->descriptors[i]->writeDescriptorSet;
		}

		updateDescriptorSets(&writeDescriptorSets, nullptr);
	}
}