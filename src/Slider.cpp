#include "stdafx.h"
#include "Slider.h"
#include "ThroneFramework.h"
#include "Material.h"

Slider::Slider()
{
}

Slider::~Slider()
{
}

void Slider::initializeObject()
{
	transition = ColorTint;
	colorState.normal = Color(255, 255, 255);
	colorState.highlightedColor = Color(245, 245, 245);
	colorState.pressedColor = Color(200, 200, 200);
	colorState.disabledColor = Color(162, 162, 162, 128);

	targetGraphic.mainTexture.borderImage.setBorders(10, 22, 10, 22);
	targetGraphic.rectTransform.setRect(0, 0, 160, 20);
}

void Slider::initialize(RenderPass * renderPass, GraphicsPipeline * graphicsPipeline, DescriptorSet & descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	targetGraphic.mainTexture.initialize(path_uiBackgroundTexture);
	knobTexture.initialize(path_uiKnobTexture);
	fillTexture.initialize(path_uiDefaultTexture);

	Material* knobMaterial = new Material();
	knobMaterial->initialize(knobTexture.sampler, 0, 0, 1);

	Material* fillMaterial = new Material();
	fillMaterial->initialize(fillTexture.sampler, 0, 0, 1);

	std::deque<Material*> materials;
	materials.emplace_back(fillMaterial);
	materials.emplace_back(knobMaterial);

	Selectable::initialize(renderPass, graphicsPipeline, descriptorSet, materials, meshRendererBindingIndex, modelMatrixBindingIndex);
}

void Slider::click()
{
	if (isHighlighted)
	{
		onClick();
	}
}
