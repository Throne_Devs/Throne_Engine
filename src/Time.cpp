#include "stdafx.h"

#include "Time.h"

namespace ThroneEngine
{
	const Time Time::Zero;

	Time::Time() : microseconds(0)
	{
	}

	float Time::asSeconds() const
	{
		return microseconds / 1000000.f;
	}

	int32 Time::asMilliseconds() const
	{
		return static_cast<int32>(microseconds / 1000);
	}

	int64 Time::asMicroseconds() const
	{
		return microseconds;
	}

	Time::Time(const int64 microseconds) : microseconds(microseconds)
	{
	}

	Time seconds(const float amount)
	{
		return Time(static_cast<int64>(amount * 1000000));
	}

	Time milliseconds(const int32 amount)
	{
		return Time(static_cast<int32>(amount) * 1000);
	}

	Time microseconds(const int64 amount)
	{
		return Time(amount);
	}

	bool operator ==(Time left, Time right)
	{
		return left.asMicroseconds() == right.asMicroseconds();
	}

	bool operator!=(Time left, Time right)
	{
		return left.asMicroseconds() != right.asMicroseconds();
	}

	bool operator<(Time left, Time right)
	{
		return left.asMicroseconds() < right.asMicroseconds();
	}

	bool operator>(Time left, Time right)
	{
		return left.asMicroseconds() > right.asMicroseconds();
	}

	bool operator<=(Time left, Time right)
	{
		return left.asMicroseconds() <= right.asMicroseconds();
	}

	bool operator>=(Time left, Time right)
	{
		return left.asMicroseconds() >= right.asMicroseconds();
	}

	Time operator-(Time right)
	{
		return microseconds(-right.asMicroseconds());
	}

	Time operator+(Time left, Time right)
	{
		return microseconds(left.asMicroseconds() + right.asMicroseconds());
	}

	Time& operator+=(Time& left, Time right)
	{
		return left = left + right;
	}

	Time operator-(Time left, Time right)
	{
		return microseconds(left.asMicroseconds() - right.asMicroseconds());
	}

	Time& operator-=(Time& left, Time right)
	{
		return left = left - right;
	}

	Time operator*(Time left, float right)
	{
		return seconds(left.asSeconds() * right);
	}

	Time operator*(Time left, int64 right)
	{
		return microseconds(left.asMicroseconds() * right);
	}

	Time operator*(float left, Time right)
	{
		return right * left;
	}

	Time operator*(int64 left, Time right)
	{
		return right * left;
	}

	Time& operator*=(Time& left, float right)
	{
		return left = left * right;
	}

	Time& operator*=(Time& left, int64 right)
	{
		return left = left * right;
	}

	Time operator/(Time left, float right)
	{
		return seconds(left.asSeconds() / right);
	}

	Time operator/(Time left, int64 right)
	{
		return microseconds(left.asMicroseconds() / right);
	}

	Time& operator/=(Time& left, float right)
	{
		return left = left / right;
	}

	Time& operator/=(Time& left, int64 right)
	{
		return left = left / right;
	}

	float operator/(Time left, Time right)
	{
		return left.asSeconds() / right.asSeconds();
	}

	Time operator%(Time left, Time right)
	{
		return microseconds(left.asMicroseconds() % right.asMicroseconds());
	}

	Time& operator%=(Time& left, Time right)
	{
		return left = left % right;
	}
}
