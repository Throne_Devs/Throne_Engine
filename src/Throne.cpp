#pragma once

#include "stdafx.h"
#include "Throne.h"
#include "ThroneFramework.h"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "UtilityFunctions.h"
#include "Config.h"

#include <locale>

using namespace std::chrono_literals;
using namespace std::placeholders;

namespace ThroneEngine
{
	Throne::Throne()
	{

	}

	Throne::~Throne()
	{

	}

	bool Throne::run()
	{
		srand(time(NULL));

		scene.initializeScene(window, commandQueue);

		try
		{
			clock.restart();
			while (window->isOpen())
			{
				input();
				update();
				scene.draw();
			}
			vkDeviceWaitIdle(logicalDevice);
			Log::appendLog();
		}

		//Custom exceptions catches will go here, which will return false.
		//If no exceptions are catched then execution is considered successful.

		catch (const std::exception& e)
		{
			Log::write(err_globalCatch, e.what());
			Log::appendLog();
			return false;
		}

		return true;
	}

	bool Throne::initialize()
	{
		window = new Window(WINDOW_WIDTH, WINDOW_HEIGTH, sdo_windowName, this);

		Config::initialize();

		onMoveFowardHandle.function = std::bind(&Throne::onMoveForwardEvent, this);
		onMoveBackwardHandle.function = std::bind(&Throne::onMoveBackwardEvent, this);
		onMoveLeftHandle.function = std::bind(&Throne::onMoveLeftEvent, this);
		onMoveRightHandle.function = std::bind(&Throne::onMoveRightEvent, this);
		onMoveUpHandle.function = std::bind(&Throne::onMoveUpEvent, this);
		onMoveDownHandle.function = std::bind(&Throne::onMoveDownEvent, this);

		Keyboard::onKeyMaintained[Config::getKey(FORWARD_INDEX)] += onMoveFowardHandle;
		Keyboard::onKeyMaintained[Config::getKey(BACKWARD_INDEX)] += onMoveBackwardHandle;
		Keyboard::onKeyMaintained[Config::getKey(LEFT_INDEX)] += onMoveLeftHandle;
		Keyboard::onKeyMaintained[Config::getKey(RIGHT_INDEX)] += onMoveRightHandle;
		Keyboard::onKeyMaintained[Config::getKey(UP_INDEX)] += onMoveUpHandle;
		Keyboard::onKeyMaintained[Config::getKey(DOWN_INDEX)] += onMoveDownHandle;

		if (initializeThroneFramework() == false)
		{
			ERROR_MESSAGE(err_throneInitialization);
			return false;
		}

		return true;
	}

	void Throne::onWindowResizeEvent()
	{
		scene.windowResizeEvent();
	}

	void Throne::update()
	{
		deltaTime = clock.restart().asSeconds();
		fpsCounter();
		if (cameraIsActivated == true)
		{
			Mouse::setPosition(vec2(window->getSize().x / 2, window->getSize().y / 2), *window);
		}

		Keyboard::notifyKeyMaintained();
		Mouse::notifyButtonMaintain();

		scene.update(deltaTime);
	}

	void Throne::fpsCounter()
	{
		fpsCount++;
		timeFps += deltaTime;
		if (timeFps >= 1)
		{
			std::cout << sdo_stringFPSoutput << fpsCount << std::endl;
			scene.testText.setTextString(sdo_wStringFPSoutput + std::to_wstring(fpsCount), commandQueue, scene.uiRenderPass, scene.commandBuffersFences);
			fpsCount = 0;
			timeFps = 0;
		}
	}

	void Throne::moveCamera(vec4 direction) const
	{
		Utility::normalize(direction);
		if (window->hasFocus())
		{
			scene.mainCamera->move(direction, deltaTime * cameraSpeed);
		}
	}

	void Throne::onMoveForwardEvent() const
	{
		moveCamera(scene.mainCamera->orientation);
	}

	void Throne::onMoveBackwardEvent() const
	{
		moveCamera(-scene.mainCamera->orientation);
	}

	void Throne::onMoveLeftEvent() const
	{
		moveCamera(*scene.mainCamera->transform->right());
	}

	void Throne::onMoveRightEvent() const
	{
		moveCamera(-*scene.mainCamera->transform->right());
	}

	void Throne::onMoveUpEvent() const
	{
		moveCamera(*scene.mainCamera->transform->up());
	}

	void Throne::onMoveDownEvent() const
	{
		moveCamera(-*scene.mainCamera->transform->up());
	}

	void Throne::input()
	{
		EventWindow event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case EventWindow::Closed:
			{
				window->close();
				break;
			}
			case EventWindow::KeyPressed:
			{
				Keyboard::notifyKeyPressed();
				break;
			}
			case EventWindow::KeyReleased:
			{
				Keyboard::notifyKeyReleased();
				if (event.key.code == Keyboard::Key::Right)
				{
					scene.directions[0].z = 1.001;
					break;
				}
				if (event.key.code == Keyboard::Key::Left)
				{
					scene.directions[0].z = 0.999;
					break;
				}
				if (event.key.code == Keyboard::Key::Up)
				{
					scene.directions[0].y = 1.001;
					break;
				}
				if (event.key.code == Keyboard::Key::Down)
				{
					scene.directions[0].y = 0.999;
					break;
				}
				if (event.key.code == Config::getKey(CLOSE_WINDOW_INDEX))
				{
					window->close();
					break;
				}
				if (event.key.code == Config::getKey(MOUSE_GRABED_INDEX))
				{
					cameraIsActivated = !cameraIsActivated;
					break;
				}
				if (event.key.code == Config::getKey(MOUSE_VISIBILITY_INDEX))
				{
					cursorVisible = !cursorVisible;
					window->setMouseCursorVisible(!cursorVisible);
					break;
				}
				if (event.key.code == Config::getKey(TELEPORT_SUN_INDEX))
				{
					scene.mainCamera->setPosition(	glm::vec3(*scene.sunCamera->transform->position()), 
													glm::vec3(*scene.sunCamera->transform->forward()), 
													glm::vec3(0, 1, 0));
					break;
				}
				if (event.key.code == Config::getKey(PRINT_POSITION_INDEX))
				{
					scene.mainCamera->transform->printPosition();
					break;
				}
				if (event.key.code == Config::getKey(PRINT_VIEWPOINT_INDEX))
				{
					Utility::printVec4(std::string("ViewPoint: "), *scene.mainCamera->transform->forward(), false);
					break;
				}		
				if (event.key.code == Config::getKey("ChangeProjectionValues1"))
				{
					scene.debugValueUseLightProjMatrix = -1;
					break;
				}
				if (event.key.code == Config::getKey("ChangeProjectionValues2"))
				{
					scene.debugValueUseLightProjMatrix = 0;
					break;
				}
				if (event.key.code == Config::getKey("ChangeProjectionValues3"))
				{
					scene.debugValueUseLightProjMatrix = 1;
					break;
				}
				if (event.key.code == Config::getKey("ChangeProjectionValues4"))
				{
					scene.debugValueUseLightProjMatrix = 2;
					break;
				}
				if (event.key.code == Config::getKey("ChangeProjectionValues5"))
				{
					scene.debugValueUseLightProjMatrix = 3;
					break;
				}
				if (event.key.code == Config::getKey("ChangeLightSetup1"))
				{
					scene.switchPlayerCamera();
					break;
				}
				if (event.key.code == Config::getKey("ChangeLightSetup2"))
				{
					break;
				}
				if (event.key.code == Config::getKey("SwitchTransparentTree"))
				{
					for (RenderedObjectPart& part : scene.tree.getRenderPassInfoByRenderPassHandle(scene.sceneRenderPass).renderedObjectParts)
					{
						if (part.graphicsPipeline == &scene.sceneTransparentGraphicsPipeline)
						{
							part.isActive = !part.isActive;
						}		
					}
					for (RenderedObjectPart& part : scene.tree.getRenderPassInfoByRenderPassHandle(scene.shadowRenderPass).renderedObjectParts)
					{
						if (part.graphicsPipeline == &scene.shadowTransparentGraphicsPipeline)
						{
							part.isActive = !part.isActive;
						}				
					}
					break;
				}
				break;
			}
			case EventWindow::MouseMoved:
			{
				Mouse::notifyMove();
				if (cameraIsActivated == true)
				{
					scene.mainCamera->moveOrientation(event.mouseMove.xRel, event.mouseMove.yRel);
				}
				break;
			}
			case EventWindow::MouseWheelMoved:
			{
				if(event.mouseWheel.delta < 0)
				{
					if (cameraSpeed - CHANGE_SPEED >= 0)
					{
						cameraSpeed -= CHANGE_SPEED;
					}
				}
				else if (event.mouseWheel.delta > 0)
				{
					cameraSpeed += CHANGE_SPEED;
				}
				break;
			}
			case EventWindow::MouseButtonPressed:
			{
				Mouse::notifyButtonPressed();
				if(event.mouseButton.button == Config::getButton(RESET_CAMERA_SPEED_INDEX))
				{
					cameraSpeed = DEFAULT_SPEED;
				}
				break;
			}
			case EventWindow::MouseButtonReleased:
			{
				Mouse::notifyButtonReleased();
				break;
			}
			default:
				break;
			}
		}
	}
}
