#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	class Semaphore
	{
	public:
		Semaphore();
		~Semaphore();

		VkSemaphore handle = VK_NULL_HANDLE;

		bool initialize();
		void destroy();

		static bool createSemaphore(VkSemaphore& semaphore);
	};
}