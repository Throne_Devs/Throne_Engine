#pragma once
#include "stdafx.h"

#include "Transform.h"
#include "VertexBuffer.h"
#include "MeshRenderer.h"
#include "Material.h"
#include "GraphicsPipeline.h"
#include "UtilityStructures.h"
#include "RenderPass.h"

namespace ThroneEngine
{
	class RenderedObject
	{
	public:
		RenderedObject();
		~RenderedObject();

		Transform modelMatrix;
		VertexBuffer modelMatrixBuffer;
		MeshRenderer* meshRenderer;

		std::deque<RenderedObjectRenderPassInfo> renderPassesInfo;

		//std::unordered_map<VkRenderPass, RenderedObjectRenderPassInfo> renderPassInfo;

		bool initialize(MeshRenderer* meshRenderer, Material* material, GraphicsPipeline* graphicsPipeline, RenderPass* renderPass, uint32 modelMatrixBindingIndex,
			glm::vec3& position = glm::vec3{ 0, 0, 0 }, glm::vec3& rotation = glm::vec3{ 0, 0, 0 }, glm::vec3& scale = glm::vec3{ 1, 1, 1 });
		bool initialize(MeshRenderer* meshRenderer, std::deque<RenderedObjectRenderPassInfo>& renderPassInfo, uint32 modelMatrixBindingIndex,
			glm::vec3& position = glm::vec3{ 0, 0, 0 }, glm::vec3& rotation = glm::vec3{ 0, 0, 0 }, glm::vec3& scale = glm::vec3{ 1, 1, 1 });

		RenderedObjectRenderPassInfo& getRenderPassInfoByRenderPassHandle(VkRenderPass renderPassHandle);
		RenderedObjectRenderPassInfo& getRenderPassInfoByRenderPassHandle(RenderPass& renderPass);

	private:
		bool initializeModelMatrixBuffer(glm::vec3& position, glm::vec3& rotation, glm::vec3& scale, uint32 modelMatrixBindingIndex);

		class FindRenderPassPredicat
		{
		public:
			VkRenderPass searchingValue = VK_NULL_HANDLE;
			inline constexpr bool operator()(const RenderedObjectRenderPassInfo& currentValue) const
			{
				return currentValue.renderPass->handle == searchingValue;
			}
		};

		FindRenderPassPredicat findRenderPassPredicat;
	};

	inline RenderedObjectRenderPassInfo& RenderedObject::getRenderPassInfoByRenderPassHandle(VkRenderPass renderPassHandle)
	{
		findRenderPassPredicat.searchingValue = renderPassHandle;
		return *std::find_if(renderPassesInfo.begin(), renderPassesInfo.end(), findRenderPassPredicat);
	}

	inline RenderedObjectRenderPassInfo& RenderedObject::getRenderPassInfoByRenderPassHandle(RenderPass& renderPass)
	{
		findRenderPassPredicat.searchingValue = renderPass.handle;
		return *std::find_if(renderPassesInfo.begin(), renderPassesInfo.end(), findRenderPassPredicat);
	}
}



