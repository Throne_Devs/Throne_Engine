#pragma once
#include "stdafx.h"

#include "Image.h"
#include "CommandBuffer.h"
#include "UtilityFunctions.hpp"
#include "ThroneFramework.h"

#define STB_IMAGE_IMPLEMENTATION
#include <Stbi/stb_image.h>

namespace ThroneEngine
{
	Image::Image()
	{
	}

	Image::~Image()
	{
	}

	bool Image::initialize(
		VkImageType type,
		VkFormat format,
		VkExtent3D dimensions,
		uint32 numMipmaps,
		uint32 numLayers,
		VkSampleCountFlagBits samples,
		VkImageUsageFlags usageScenarios,
		bool isCubemap)
	{
		return initialize(type, format, dimensions, numMipmaps, numLayers, samples, usageScenarios, isCubemap, VK_IMAGE_LAYOUT_UNDEFINED);
	}

	bool Image::initialize(
		VkImageType type,
		VkFormat format,
		VkExtent3D dimensions,
		uint32 numMipmaps,
		uint32 numLayers,
		VkSampleCountFlagBits samples,
		VkImageUsageFlags usageScenarios,
		bool isCubemap,
		VkImageLayout imageLayout)
	{
		this->dimensions = dimensions;
		this->imageLayout = imageLayout;

		VkImageCreateInfo imageCreateInfos = {};
		imageCreateInfos.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfos.pNext = nullptr;
		imageCreateInfos.flags = isCubemap ? VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : 0;
		imageCreateInfos.imageType = type;
		imageCreateInfos.extent = dimensions;
		imageCreateInfos.mipLevels = numMipmaps;
		imageCreateInfos.arrayLayers = isCubemap ? 6 * numLayers : numLayers;
		imageCreateInfos.samples = samples;
		imageCreateInfos.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfos.usage = usageScenarios;
		imageCreateInfos.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageCreateInfos.queueFamilyIndexCount = 0;
		imageCreateInfos.pQueueFamilyIndices = nullptr;
		imageCreateInfos.initialLayout = imageLayout;
		imageCreateInfos.format = format;

		VkResult result = vkCreateImage(logicalDevice, &imageCreateInfos, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createImage);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool Image::initializeAs2DSampledImage(VkExtent3D& dimensions, VkImageUsageFlags usage, VkImageAspectFlags aspect, VkFormat imageFormat, uint32 numberOfLayer, VkImageViewType viewType)
	{
		VkFormatProperties formatProperties;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);

		if ((formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT) == false)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_unsupportedPhysicalDeviceFormatProperty);
			#endif
			return false;
		}

		if ((formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT) == false)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_unsupportedPhysicalDeviceFormatProperty);
			#endif
			return false;
		}

		if (initialize(VK_IMAGE_TYPE_2D, imageFormat, dimensions, 1, numberOfLayer, VK_SAMPLE_COUNT_1_BIT, usage, false) == false)
		{
			return false;
		}

		if (allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == false)
		{
			return false;
		}

		for (int i = 0; i < numberOfLayer; i++)
		{
			if (initializeImageView(aspect, viewType, imageFormat, i, 1) == false)
			{
				return false;
			}
		}



		return true;
	}

	bool Image::initializeAs2DSampledImage(std::string& imagePath, VkImageUsageFlags usage, VkImageAspectFlags aspect, VkFormat imageFormat, CommandQueue& commandQueue)
	{
		std::vector<unsigned char> textureData;

		int width = 0;
		int height = 0;

		if (loadDataFromFile(imagePath.data(), &textureData, width, height, 4) == false)
		{
			return false;
		}

		VkExtent3D imageDimensions = {};
		imageDimensions.width = width;
		imageDimensions.height = height;
		imageDimensions.depth = 1;

		if (initializeAs2DSampledImage(imageDimensions, usage, aspect, imageFormat, 1, VK_IMAGE_VIEW_TYPE_2D) == false)
		{
			return false;
		}



		VkImageSubresourceLayers imageSubresourcesLayer = {};
		imageSubresourcesLayer.aspectMask = aspect;
		imageSubresourcesLayer.mipLevel = 0;
		imageSubresourcesLayer.baseArrayLayer = 0;
		imageSubresourcesLayer.layerCount = 1;

		CommandPool commandPool;
		commandPool.initialize(commandQueue, 0, 1, VK_COMMAND_BUFFER_LEVEL_PRIMARY);

		if (updateWithDeviceLocalMemory(commandPool.commandBuffers[0], getVectorDataPointer(textureData), textureData.size(), imageSubresourcesLayer,
			{ 0, 0, 0 }, imageDimensions, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, VK_ACCESS_SHADER_READ_BIT,
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, commandQueue, SemaphorePool()) == false)
		{
			return false;
		}

		return true;
	}

	bool Image::initializeAsStorageImage(VkFormat format, VkExtent3D& dimensions, VkImage& storageImage, VkImageView& storageImageView)
	{
		VkFormatProperties formatProperties;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &formatProperties);

		if ((formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT) == false)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_unsupportedPhysicalDeviceFormatProperty);
			#endif
			return false;
		}

		if (initialize(VK_IMAGE_TYPE_2D, format, { dimensions.width, dimensions.height, 1 }, 1, 1, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_STORAGE_BIT, false) == false)
		{
			return false;
		}

		if (allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == false)
		{
			return false;
		}

		if (initializeImageView(VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D, format, 0, 1) == false)
		{
			return false;
		}

		return true;
	}

	bool Image::initializeAs2DCubeMapSampledImage(VkFormat format, VkExtent3D& dimensions, uint32 numMipmaps, uint32 numLayers, VkSampleCountFlagBits samples, VkImageUsageFlags usage, VkImageAspectFlags aspect)
	{
		if (initialize(VK_IMAGE_TYPE_2D, format, dimensions, numMipmaps, numLayers, samples, usage, true) == false)
		{
			return false;
		}
		if (allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == false)
		{
			return false;
		}
		if (initializeImageView(aspect, VK_IMAGE_VIEW_TYPE_CUBE, format, 0, VK_REMAINING_ARRAY_LAYERS) == false)
		{
			return false;
		}
		return true;
	}

	bool Image::initializeAs2DCubeMapSampledImage(std::vector<std::string>& imagePaths, VkFormat format, uint32 numMipmaps, uint32 numLayers, VkSampleCountFlagBits samples, VkImageUsageFlags usage, VkImageAspectFlags aspect, CommandQueue& commandQueue)
	{
		int width = 0;
		int height = 0;
		int numComponents = 0;

		stbi_info(imagePaths[0].data(), &width, &height, &numComponents);

		VkExtent3D imageDimensions = {};
		imageDimensions.width = width;
		imageDimensions.height = height;
		imageDimensions.depth = 1;

		if (initializeAs2DCubeMapSampledImage(format, imageDimensions, numMipmaps, numLayers, samples, usage, aspect) == false)
		{
			return false;
		}

		CommandPool commandPool;
		commandPool.initialize(commandQueue, 0, 6, VK_COMMAND_BUFFER_LEVEL_PRIMARY);

		for (int i = 0; i < imagePaths.size(); i++)
		{
			std::vector<unsigned char> textureData;

			loadDataFromFile(imagePaths[i].data(), &textureData, width, height, numComponents);

			VkImageSubresourceLayers imageSubresourcesLayer = {};
			imageSubresourcesLayer.aspectMask = aspect;
			imageSubresourcesLayer.mipLevel = 0;
			imageSubresourcesLayer.baseArrayLayer = i;
			imageSubresourcesLayer.layerCount = 1;

			VkDeviceSize imageDataSize = imageDimensions.width * imageDimensions.height * sizeof(float);
			if (updateWithDeviceLocalMemory(commandPool.commandBuffers[i], getVectorDataPointer(textureData), textureData.size(), imageSubresourcesLayer,
				{ 0, 0, 0 }, imageDimensions, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, VK_ACCESS_SHADER_READ_BIT,
				VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, commandQueue, SemaphorePool()) == false)
			{
				return false;
			}
		}
		return true;
	}

	bool Image::initializeAs2DImageWithView(
		VkFormat format,
		VkExtent3D dimensions,
		uint32 numMipmaps,
		uint32 numLayers,
		VkSampleCountFlagBits samples,
		VkImageUsageFlags usage,
		VkImageAspectFlags aspect)
	{
		if (initialize(VK_IMAGE_TYPE_2D, format, { dimensions.width, dimensions.height, 1 }, numMipmaps, numLayers, samples, usage, false) == false)
		{
			return false;
		}
		if (allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == false)
		{
			return false;
		}
		for (int i = 0; i < numLayers; i++)
		{
			if (initializeImageView(aspect, VK_IMAGE_VIEW_TYPE_2D, format, i, 1) == false)
			{
				return false;
			}
		}

		return true;
	}

	bool Image::initializeAsInputAttachment(
		VkImageType type,
		VkFormat format,
		VkExtent3D dimensions,
		VkImageUsageFlags usage,
		VkImageViewType viewType,
		VkImageAspectFlagBits aspect)
	{
		VkFormatProperties formatProperties;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &formatProperties);

		if (aspect & VK_IMAGE_ASPECT_COLOR_BIT)
		{
			if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT))
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_unsupportedPhysicalDeviceFormatProperty);
				#endif
				return false;
			}
		}

		if (aspect & (VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_DEPTH_BIT))	// Pourquoi deux fois la m�me chose, � v�rifier. -F�lix
		{
			if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT))
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_unsupportedPhysicalDeviceFormatProperty);
				#endif
				return false;
			}
		}

		if (initialize(type, format, dimensions, 1, 1, VK_SAMPLE_COUNT_1_BIT, usage | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, false) == false)
		{
			return false;
		}

		if (allocateMemory(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == false)
		{
			return false;
		}

		if (initializeImageView(aspect, viewType, format, 0, VK_REMAINING_ARRAY_LAYERS) == false)
		{
			return false;
		}
	}

	void Image::destroy()
	{
		destroyImageView();
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyImage(logicalDevice, handle, nullptr);
		}
		handle = VK_NULL_HANDLE;
	}

	void Image::destroyImageView()
	{
		for (int i = 0; i < imageViewHandles.size(); i++)
		{
			if (imageViewHandles[i] != VK_NULL_HANDLE)
			{
				vkDestroyImageView(logicalDevice, imageViewHandles[i], nullptr);
				imageViewHandles[i] = VK_NULL_HANDLE;
			}
		}

	}

	bool Image::allocateMemory(VkMemoryPropertyFlags properties)
	{
		VkPhysicalDeviceMemoryProperties memoryProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);
		VkMemoryRequirements memoryRequirements;
		vkGetImageMemoryRequirements(logicalDevice, handle, &memoryRequirements);
		VkDeviceMemory memoryObject = VK_NULL_HANDLE;

		for (int i = 0; i < memoryProperties.memoryTypeCount; i++)
		{
			if ((memoryRequirements.memoryTypeBits & (1 << i)) && ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties))
			{
				VkMemoryAllocateInfo bufferMemoryAllocateInfo = {};
				bufferMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				bufferMemoryAllocateInfo.pNext = nullptr;
				bufferMemoryAllocateInfo.allocationSize = memoryRequirements.size;
				bufferMemoryAllocateInfo.memoryTypeIndex = i;

				VkResult result = vkAllocateMemory(logicalDevice, &bufferMemoryAllocateInfo, nullptr, &memoryObject);
				#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
				if (result != VK_SUCCESS)
				{
					#if ERROR_MESSAGE_NEEDED
					ERROR_MESSAGE(err_allocateImageMemory);
					#endif
					return false;
				}
				#endif
				break;
			}
		}

		if (memoryObject != VK_NULL_HANDLE)
		{
			VkResult result = vkBindImageMemory(logicalDevice, handle, memoryObject, 0);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_bindImageMemory);
				#endif
				return false;
			}
			#endif
		}
		else
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_nullImageMemoryObject);
			#endif
			return false;
		}

		return true;
	}

	bool Image::initializeImageView(VkImageAspectFlags aspect, VkImageViewType viewType, VkFormat format, uint32 layer, uint32 layerCount)
	{
		imageViewHandles.push_back(VK_NULL_HANDLE);

		VkImageSubresourceRange subresourceRange = {};
		subresourceRange.aspectMask = aspect;
		subresourceRange.baseMipLevel = 0;
		subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
		subresourceRange.baseArrayLayer = layer;
		subresourceRange.layerCount = layerCount;

		VkComponentMapping componentMapping = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };

		VkImageViewCreateInfo imageViewCreateInfos = {};
		imageViewCreateInfos.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfos.pNext = nullptr;
		imageViewCreateInfos.flags = 0;
		imageViewCreateInfos.image = handle;
		imageViewCreateInfos.viewType = viewType;
		imageViewCreateInfos.format = format;
		imageViewCreateInfos.components = componentMapping;
		imageViewCreateInfos.subresourceRange = subresourceRange;

		VkResult result = vkCreateImageView(logicalDevice, &imageViewCreateInfos, nullptr, &imageViewHandles[layer]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createImageView);
			#endif
			return false;
		}
		#endif
		return true;
	}

	void Image::copyDataFromBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkImageLayout imageLayout, std::vector<VkBufferImageCopy>& regions)
	{
		copyDataFromBuffer(commandBuffer, srcBuffer, imageLayout, getVectorDataPointer(regions), regions.size());
	}

	void Image::copyDataFromBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkImageLayout imageLayout, VkBufferImageCopy* regions, uint32 numberOfRegion)
	{
		commandBuffer.cmdCopyBufferToImage(srcBuffer, handle, imageLayout, numberOfRegion, regions);
	}

	bool Image::updateWithDeviceLocalMemory(
		CommandBuffer& commandBuffer,
		void* data,
		VkDeviceSize dataSize,
		VkImageSubresourceLayers dstImageSubresource,
		VkOffset3D dstImageOffset,
		VkExtent3D dstImageSize,
		VkImageLayout dstImageOldLayout,
		VkImageLayout dstImageNewLayout,
		VkAccessFlags dstImageCurrentAccess,
		VkAccessFlags dstImageNewAccess,
		VkPipelineStageFlags dstImageCurrentStages,
		VkPipelineStageFlags dstImageNewStages,
		CommandQueue& queue,
		SemaphorePool& semaphoresToSignal)
	{
		GpuBuffer stagingBuffer;
		if (stagingBuffer.initialize(dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT) == false)
		{
			return false;
		}

		VkDeviceMemory memoryObject;
		if (stagingBuffer.allocateMemory(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memoryObject) == false)
		{
			return false;
		}
		if (stagingBuffer.mapHostVisibleMemory(memoryObject, 0, dataSize, data, true) == false)
		{
			return false;
		}

		if (commandBuffer.beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr) == false)
		{
			return false;
		}

		VkImageMemoryBarrier imageTransition = {};
		imageTransition.image = handle;
		imageTransition.srcAccessMask = dstImageCurrentAccess;
		imageTransition.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageTransition.oldLayout = dstImageOldLayout;
		imageTransition.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		imageTransition.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageTransition.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		setMemoryBarrier(commandBuffer, &imageTransition, 1, dstImageSubresource.aspectMask, dstImageCurrentStages, VK_PIPELINE_STAGE_TRANSFER_BIT);

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource = dstImageSubresource;
		region.imageOffset = dstImageOffset;
		region.imageExtent = dstImageSize;
		copyDataFromBuffer(commandBuffer, stagingBuffer.handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &region, 1);

		imageTransition.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		imageTransition.dstAccessMask = dstImageNewAccess;
		imageTransition.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		imageTransition.newLayout = dstImageNewLayout;
		setMemoryBarrier(commandBuffer, &imageTransition, 1, dstImageSubresource.aspectMask, VK_PIPELINE_STAGE_TRANSFER_BIT, dstImageNewStages);

		if (commandBuffer.endRecording() == false)
		{
			return false;
		}

		Fence fence;
		if (fence.initialize() == false)
		{
			return false;
		}
		fence.fenceInUse = true;

		WaitSemaphoreInfo waitSemaphoresInfos = {};
		if (queue.submitCommandBuffer(waitSemaphoresInfos, commandBuffer, semaphoresToSignal, fence) == false)
		{
			return false;
		}

		if (fence.waitForFence(TIMEOUT_FENCE) == false)
		{
			return false;
		}

		//destroyBuffer(); // TODO : rajouter les destroy quand ce sera fait
		//freeMemoryObject();

		return true;
	}

	void Image::setMemoryBarrier(
		CommandBuffer& commandBuffer,
		std::vector<VkImageMemoryBarrier>& imageMemoryBarrier,
		VkImageAspectFlags aspect,
		VkPipelineStageFlags currentPipelineStage,
		VkPipelineStageFlags newPipelineStage)
	{
		setMemoryBarrier(commandBuffer, getVectorDataPointer(imageMemoryBarrier), imageMemoryBarrier.size(), aspect, currentPipelineStage, newPipelineStage);
	}

	void Image::setMemoryBarrier(
		CommandBuffer& commandBuffer,
		VkImageMemoryBarrier* imageMemoryBarrier,
		uint32 numberOfBarrier,
		VkImageAspectFlags aspect,
		VkPipelineStageFlags currentPipelineStage,
		VkPipelineStageFlags newPipelineStage)
	{
		for (int i = 0; i < numberOfBarrier; i++)
		{
			imageMemoryBarrier[i].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			imageMemoryBarrier[i].pNext = nullptr;

			VkImageSubresourceRange subresourceRange = {};
			subresourceRange.aspectMask = aspect;
			subresourceRange.baseMipLevel = 0;
			subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
			subresourceRange.baseArrayLayer = 0;
			subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

			imageMemoryBarrier[i].subresourceRange = subresourceRange;
		}
		commandBuffer.cmdPipelineBarrier(currentPipelineStage, newPipelineStage, 0, 0, nullptr, 0, nullptr, numberOfBarrier, imageMemoryBarrier);
		imageLayout = imageMemoryBarrier->newLayout;
	}
	bool Image::loadDataFromFile(const char * filename, std::vector<unsigned char>* data, int & width, int & height, const int numRequestedComponents)
	{
		int numComponents = 0;

		std::unique_ptr<unsigned char, void(*)(void*)> stbiData(stbi_load(filename, &width, &height, &numComponents, numRequestedComponents), stbi_image_free);

		#if defined ERROR_VALIDATION_INVALID_DATA
		if (!stbiData || width <= 0 || height <= 0 || numComponents <= 0)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(R::S::ErrorMessage::err_readImage);
			#endif
			return false;
		}
		#endif

		const int dataSize = width * height * (0 < numRequestedComponents ? numRequestedComponents : numComponents);
		data->resize(dataSize);

		memcpy(data->data(), stbiData.get(), dataSize);
		return true;
	}
}