#include "stdafx.h"

#include "BorderImage.h"
#include "Image.h"
#include "Mesh.h"

BorderImage::BorderImage(): 
left(0),
right(0),
top(0),
bottom(0),
size(0, 0)
{
}

BorderImage::~BorderImage()
{
}

uvec2 BorderImage::getSize() const
{
	return size;
}

void BorderImage::setSize(const uvec2& newSize)
{
	setSize(newSize.x, newSize.y);
}

void BorderImage::setSize(const uint width, const uint height)
{
	size.x = width;
	size.y = height;

	updateVertices();
}

void BorderImage::setImage(const Image& image, const uint32 border)
{
	left = border;
	right = border;
	top = border;
	bottom = border;

	this->image = &image;

	updateVertices();
}

void BorderImage::setBorders(const uvec2& size, const uint leftBorder, const uint rightBorder, const uint topBorder, const uint bottomBorder)
{
	left = leftBorder;
	right = rightBorder;
	top = topBorder;
	bottom = bottomBorder;
	
	setSize(size);

	updateVertices();
}

void BorderImage::setBorders(const uint leftBorder, const uint rightBorder, const uint topBorder, const uint bottomBorder)
{
	setBorders(size, leftBorder, rightBorder, topBorder, bottomBorder);
}

void BorderImage::setLeftBorder(const uint leftBorder)
{
	left = leftBorder;
	updateVertices();
}

void BorderImage::setRightBorder(const uint rightBorder)
{
	right = rightBorder;
	updateVertices();
}

void BorderImage::setTopBorder(const uint topBorder)
{
	top = topBorder;
	updateVertices();
}

void BorderImage::setBottomBorder(const uint bottomBoder)
{
	bottom = bottomBoder;
	updateVertices();
}

uint BorderImage::leftBorder() const
{
	return left;
}

uint BorderImage::rightBorder() const
{
	return right;
}

uint BorderImage::topBorder() const
{
	return top;
}

uint BorderImage::bottomBorder() const
{
	return bottom;
}

void BorderImage::updateVertices(SliceScaling sliceScaling) const
{
	//			How 9-Slice Scaling is done
	//
	//				0-----1-----2-----3
	//				|  \  |	 \  |  \  |
	//				4-----5-----6-----7
	//				|  \  |  \  |  \  |
	//				8-----9-----10----11
	//				|  \  |  \  |  \  |
	//				12----13----14----15
	//
	//		Order of vertices for quads creation
	//
	//		   First Triangle  Second Triangle	
	//
	//			 0   5   4 		 0   5   1 
	//			 1   6   5 		 1   6   2 
	//			 2   7   6 		 2   7   3 
	//			 4   9   8 		 4   9   5 
	//			 5  10   9 		 5  10   6 
	//			 6  11  10 		 6  11   7 
	//			 8  13  12 		 8  13   9 
	//			 9  14  13 		 9  14  10 
	//			10  15  14 		10  15  11

	const uint verticesCount = 54;
	const uint verticesOrder[verticesCount] = { 0,  5,  4,	 0,  5,  1, 
											    1,  6,  5,	 1,  6,  2, 
												2,  7,  6,	 2,  7,  3, 
												4,  9,  8,	 4,  9,  5, 
												5, 10,  9,	 5, 10,  6, 
												6, 11, 10,	 6, 11,  7, 
												8, 13, 12,	 8, 13,  9, 
												9, 14, 13,	 9, 14, 10, 
											   10, 15, 14,	10, 15, 11 };

	if(image && mesh)
	{
		mesh->reset();

		const int nbCol = 4;
		const int nbRow = 4;

		const uint imageWidth = image->dimensions.width;
		const uint imageHeight = image->dimensions.height;
		
		float xPos[nbCol] = { 0, left, size.x - (imageWidth - right), size.x };
		float yPos[nbRow] = { 0, top, size.y - (imageHeight - bottom), size.y };

		float xTextCoords[nbCol] = { 0, static_cast<float>(left) / imageWidth, static_cast<float>(right) / imageWidth, 1 };
		float yTextCoords[nbRow] = { 0, static_cast<float>(top) / imageHeight, static_cast<float>(bottom) / imageHeight, 1 };

		if(size.x < 0 || size.y < 0)
		{
			xPos[1] = 0;
			xPos[2] = 0;

			xTextCoords[1] = 0;
			xTextCoords[2] = 0;
		}

		if (size.x < right)
		{
			xPos[1] = left  * (size.x / imageWidth);
			xPos[2] = right * (size.x / imageWidth);
		}

		if (size.y < bottom)
		{
			yPos[1] = top    * (size.y / imageHeight);
			yPos[2] = bottom * (size.y / imageHeight);
		}

		vec2 verticesPosition[nbCol * nbRow];
		vec2 verticesTexCoords[nbCol * nbRow];

		for(int i = 0; i < nbCol; i++)
		{
			for(int j = 0; j < nbRow; j++)
			{
				verticesPosition[i + nbRow * j] = vec2(xPos[i], yPos[j]);
				verticesTexCoords[i + nbRow * j] = vec2(xTextCoords[i], yTextCoords[j]);
			}
		}

		for(int i = 0; i < verticesCount; i++)
		{
			vec2 position = verticesPosition[verticesOrder[i]];
			vec2 textCoords = verticesTexCoords[verticesOrder[i]];
			
			mesh->data.emplace_back(position.x);
			mesh->data.emplace_back(position.y);
			mesh->data.emplace_back(0);

			mesh->data.emplace_back(textCoords.x);
			mesh->data.emplace_back(textCoords.y);
		
			mesh->data.emplace_back(1);
			mesh->data.emplace_back(1);
			mesh->data.emplace_back(1);
		}

		mesh->parts.push_back({ 0,  verticesCount });
		mesh->vertexSizeInBytes = sizeof(float) * 8;
		mesh->setTotalAmountOfVertices();
		mesh->setTotalSize();
	}
}
