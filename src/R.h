#pragma once

#include <vector>

namespace R
{
	namespace S
	{
		using std::string;
		using std::wstring;

        namespace ErrorMessage
        {
            THRONE_API_ENTRY string err_consolePaused;
            THRONE_API_ENTRY string err_unManaged;
            THRONE_API_ENTRY string err_unSupportedOperation;
            THRONE_API_ENTRY string err_throneInitialization;
            THRONE_API_ENTRY string err_vulkanInstance;
            THRONE_API_ENTRY string err_extensionNumber;
            THRONE_API_ENTRY string err_extensionEnumeration;
            THRONE_API_ENTRY string err_unSupportedExtension;
            THRONE_API_ENTRY string err_physicalDevicesNumber;
            THRONE_API_ENTRY string err_physicalDevicesEnumerate;
            THRONE_API_ENTRY string err_physicalDevicesSuitable;
            THRONE_API_ENTRY string err_deviceExtensionNumber;
            THRONE_API_ENTRY string err_physicalDevice;
            THRONE_API_ENTRY string err_queueFamilyNumber;
            THRONE_API_ENTRY string err_queueFamily;
            THRONE_API_ENTRY string err_logicalDeviceCreation;
            THRONE_API_ENTRY string err_physicalDeviceSupportForQueue;
            THRONE_API_ENTRY string err_queueFamilySupportForPresentation;
            THRONE_API_ENTRY string err_deviceSurfaceNumber;
            THRONE_API_ENTRY string err_availablePresentationModes;
            THRONE_API_ENTRY string err_physicalDeviceCapabality;
            THRONE_API_ENTRY string err_commandPoolCreation;
            THRONE_API_ENTRY string err_commandBuffer;
            THRONE_API_ENTRY string err_vulkanLibrary;
            THRONE_API_ENTRY string err_vulkanLibraryConnect;
            THRONE_API_ENTRY string err_physicalSurfacePresentMode;
            THRONE_API_ENTRY string err_swapChain;
            THRONE_API_ENTRY string err_swapChainImages;
            THRONE_API_ENTRY string err_imageViewCreation;
            THRONE_API_ENTRY string err_fileOpen;
            THRONE_API_ENTRY string err_functionLoad;
            THRONE_API_ENTRY string err_vulkanInstanceFunctionLoad;
            THRONE_API_ENTRY string err_vulkanDeviceFunctionLoad;
            THRONE_API_ENTRY string err_shaderModuleCreation;
            THRONE_API_ENTRY string err_pipelineLayoutCreation;
            THRONE_API_ENTRY string err_renderPassCreation;
            THRONE_API_ENTRY string err_pipelineCreation;
            THRONE_API_ENTRY string err_framebufferCreation;
            THRONE_API_ENTRY string err_beginRecordingCommandBuffer;
			THRONE_API_ENTRY string err_endRecordingCommandBuffer;
			THRONE_API_ENTRY string err_resetCommandBuffer;
			THRONE_API_ENTRY string err_createSemaphore;
			THRONE_API_ENTRY string err_createFence;
			THRONE_API_ENTRY string err_waitForFences;
			THRONE_API_ENTRY string err_resetFences;
			THRONE_API_ENTRY string err_queueSubmit;
			THRONE_API_ENTRY string err_syncCommandBuffersFirst;
			THRONE_API_ENTRY string err_syncCommandBuffersSecond;
			THRONE_API_ENTRY string err_queueWaitIdle;
			THRONE_API_ENTRY string err_deviceWaitIdle;
			THRONE_API_ENTRY string err_destroyNullFence;
			THRONE_API_ENTRY string err_destroyNullSemaphore;
			THRONE_API_ENTRY string err_destroyNullCommandPool;
			THRONE_API_ENTRY string err_createBuffer;
			THRONE_API_ENTRY string err_allocateBufferMemory;
			THRONE_API_ENTRY string err_nullBufferMemoryObject;
			THRONE_API_ENTRY string err_bindBufferMemory;
			THRONE_API_ENTRY string err_createBufferView;
			THRONE_API_ENTRY string err_createImage;
			THRONE_API_ENTRY string err_allocateImageMemory;
			THRONE_API_ENTRY string err_nullImageMemoryObject;
			THRONE_API_ENTRY string err_bindImageMemory;
			THRONE_API_ENTRY string err_validationLayer;
			THRONE_API_ENTRY string err_createImageView;
			THRONE_API_ENTRY string err_mapMemory;
			THRONE_API_ENTRY string err_flushMappedMemory;
			THRONE_API_ENTRY string err_createSampler;
			THRONE_API_ENTRY string err_unsupportedPhysicalDeviceFormatProperty;
			THRONE_API_ENTRY string err_unsupportedPhysicalDeviceBufferFeature;
			THRONE_API_ENTRY string err_createDescriptorSetLayout;
			THRONE_API_ENTRY string err_createDescriptorPool;
			THRONE_API_ENTRY string err_allocateDescriptorSets;
			THRONE_API_ENTRY string err_freeDescriptorSets;
			THRONE_API_ENTRY string err_resetDescriptorPool;
			THRONE_API_ENTRY string err_createRenderPass;
			THRONE_API_ENTRY string err_createFrameBuffer;
			THRONE_API_ENTRY string err_readImage;
			THRONE_API_ENTRY string err_createShaderModule;
			THRONE_API_ENTRY string err_createPipelineLayout;
			THRONE_API_ENTRY string err_getPipelineCacheDataSize;
			THRONE_API_ENTRY string	err_getPipelineCacheData;
			THRONE_API_ENTRY string err_mergePipelineCacheObjects;
			THRONE_API_ENTRY string err_createGraphicsPipelines;
			THRONE_API_ENTRY string err_createComputePipeline;
            THRONE_API_ENTRY string err_debugCallback;
            THRONE_API_ENTRY string err_vulkanValidation;
            THRONE_API_ENTRY string err_globalCatch;
			THRONE_API_ENTRY string err_fontSizeAvailable;
			THRONE_API_ENTRY string err_fontSizeSetBitmap;
			THRONE_API_ENTRY string err_fontOutlineFailed;
			THRONE_API_ENTRY string err_fontLoadFromFile;
			THRONE_API_ENTRY string err_fontLoadFailed;
			THRONE_API_ENTRY string err_fontInitFreetype;
			THRONE_API_ENTRY string err_fontCreateFace;
			THRONE_API_ENTRY string err_invalidMeshRendererMemoryProperties;
			THRONE_API_ENTRY string err_triedToUpdateStaticText;
			THRONE_API_ENTRY string err_getRenderPassInfoByRenderPassHandleNotFound;
        }

		namespace StandardOutput
		{
			THRONE_API_ENTRY string sdo_windowName;
			THRONE_API_ENTRY string sdo_extensionNumber;
			THRONE_API_ENTRY string sdo_extension;
			THRONE_API_ENTRY string sdo_shaderMethodToInvoke;
			THRONE_API_ENTRY string sdo_stringFPSoutput;
			THRONE_API_ENTRY wstring sdo_wStringFPSoutput;
		}

		namespace Path
		{
			THRONE_API_ENTRY LPCWSTR path_windowsVulkanLibrary;
			THRONE_API_ENTRY string path_linuxVulkanLibrary;
			THRONE_API_ENTRY string path_triangleShaderVert1;
			THRONE_API_ENTRY string path_triangleShaderFrag1;
			THRONE_API_ENTRY string path_triangleShaderVert2;
			THRONE_API_ENTRY string path_triangleShaderFrag2;
            THRONE_API_ENTRY string path_skyboxShaderVert;
            THRONE_API_ENTRY string path_skyboxShaderFrag;

			THRONE_API_ENTRY string path_shadowShaderVert;
			THRONE_API_ENTRY string path_shadowSceneShaderVert;
			THRONE_API_ENTRY string path_shadowSceneShaderFrag;

			THRONE_API_ENTRY string path_shadowTransparentShaderVert;
			THRONE_API_ENTRY string path_shadowTransparentShaderFrag;
			THRONE_API_ENTRY string path_shadowTransparentSceneShaderFrag;

			THRONE_API_ENTRY string path_shadowCascadeShaderVert;
			THRONE_API_ENTRY string path_shadowCascadeSceneShaderVert;
			THRONE_API_ENTRY string path_shadowCascadeSceneShaderFrag;

			THRONE_API_ENTRY string path_shadowCascadeTransparentShaderVert;
			THRONE_API_ENTRY string path_shadowCascadeTransparentShaderFrag;
			THRONE_API_ENTRY string path_shadowCascadeTransparentSceneShaderFrag;

            THRONE_API_ENTRY string path_lunarGStandardValidation;
			THRONE_API_ENTRY string path_dragon3DModel;
			THRONE_API_ENTRY string path_box3DModel;
			THRONE_API_ENTRY string path_boxTexture;
			THRONE_API_ENTRY string path_planeModel;
			THRONE_API_ENTRY std::vector<std::string> path_testSkyboxTexture;
            THRONE_API_ENTRY std::vector<std::string> path_interstellarSkyboxTexture;
            THRONE_API_ENTRY string path_skyboxCube;
			THRONE_API_ENTRY string path_grassTerrainTexture;

			#pragma region TreeEU55
			THRONE_API_ENTRY string path_treeEU55Model;

			THRONE_API_ENTRY string path_treeTextureEU55Brk1;
			THRONE_API_ENTRY string path_treeTextureEU55Brk2;
			THRONE_API_ENTRY string path_treeTextureEU55Brk3;

			THRONE_API_ENTRY string path_treeTextureEU55Brn1;
			THRONE_API_ENTRY string path_treeTextureEU55Brn2;
			THRONE_API_ENTRY string path_treeTextureEU55Brn3;
			THRONE_API_ENTRY string path_treeTextureEU55Brn4;
			THRONE_API_ENTRY string path_treeTextureEU55Brn5;
			THRONE_API_ENTRY string path_treeTextureEU55Brn6;
			THRONE_API_ENTRY string path_treeTextureEU55Brn7;

			THRONE_API_ENTRY string path_treeTextureEU55Bud1;
			THRONE_API_ENTRY string path_treeTextureEU55Bud2;

			THRONE_API_ENTRY string path_treeTextureEU55Flw1;

			THRONE_API_ENTRY string path_treeTextureEU55Rts1;
			THRONE_API_ENTRY string path_treeTextureEU55Rts2;
			THRONE_API_ENTRY string path_treeTextureEU55Rst3;

			THRONE_API_ENTRY string path_treeTextureEU55Stk1;

			THRONE_API_ENTRY string path_treeTextureEU55Trk1;
			THRONE_API_ENTRY string path_treeTextureEU55Trk2;
			THRONE_API_ENTRY string path_treeTextureEU55Trk3;

			THRONE_API_ENTRY string path_treeTextureEU55Twg1;
			THRONE_API_ENTRY string path_treeTextureEU55Twg2;
			#pragma endregion

		
			THRONE_API_ENTRY string path_configurationFiles;
			THRONE_API_ENTRY string path_configurationFilesDefault;
			THRONE_API_ENTRY string path_configurationFilesConfig;
			THRONE_API_ENTRY string path_logsFiles;		

			THRONE_API_ENTRY string path_arialFont;
			THRONE_API_ENTRY string path_itcedscrFont;
			THRONE_API_ENTRY string path_tahomaFont;

			THRONE_API_ENTRY string path_uiTextVertShader;
			THRONE_API_ENTRY string path_uiTextFragShader;

			THRONE_API_ENTRY string path_uiDefaultTexture;
			THRONE_API_ENTRY string path_uiKnobTexture;
			THRONE_API_ENTRY string path_uiInputFieldBackgroundTexture;
			THRONE_API_ENTRY string path_uiDropDownArrowTexture;
			THRONE_API_ENTRY string path_uiCheckmarkTexture;
			THRONE_API_ENTRY string path_uiBackgroundTexture;

			THRONE_API_ENTRY string path_uiDefaultVertexShader;
			THRONE_API_ENTRY string path_uiDefaultFragmentShader;
		}

		namespace C
        {
			THRONE_API_ENTRY const string CONFIG_FILES_EXT;
			THRONE_API_ENTRY const string LOG_FILES_EXT;
			THRONE_API_ENTRY const string KEYS_INDEX;
			THRONE_API_ENTRY const string FORWARD_INDEX;
			THRONE_API_ENTRY const string BACKWARD_INDEX;
			THRONE_API_ENTRY const string LEFT_INDEX;
			THRONE_API_ENTRY const string RIGHT_INDEX;
			THRONE_API_ENTRY const string UP_INDEX;
			THRONE_API_ENTRY const string DOWN_INDEX;
			THRONE_API_ENTRY const string CLOSE_WINDOW_INDEX;
			THRONE_API_ENTRY const string MOUSE_VISIBILITY_INDEX;
			THRONE_API_ENTRY const string MOUSE_GRABED_INDEX;
			THRONE_API_ENTRY const string TELEPORT_SUN_INDEX;
			THRONE_API_ENTRY const string PRINT_POSITION_INDEX;
			THRONE_API_ENTRY const string PRINT_VIEWPOINT_INDEX;
			THRONE_API_ENTRY const string BUTTONS_INDEX;
			THRONE_API_ENTRY const string RESET_CAMERA_SPEED_INDEX;
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES1;
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES2;
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES3;
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES4;
			THRONE_API_ENTRY const string CHANGE_PROJECTION_VALUES5;
			THRONE_API_ENTRY const string CHANGE_LIGHT_SETUP1;
			THRONE_API_ENTRY const string CHANGE_LIGHT_SETUP2;
        }
	}

	namespace C
	{
        THRONE_API_ENTRY const int TIMEOUT_FENCE;

		THRONE_API_ENTRY const double PI;

		THRONE_API_ENTRY uint32 WINDOW_WIDTH;
		THRONE_API_ENTRY uint32 WINDOW_HEIGTH;
		THRONE_API_ENTRY const uint32 WINDOW_WIDTH_MAXMININFO;
		THRONE_API_ENTRY const uint32 WINDOW_HEIGHT_MAXMININFO;

		THRONE_API_ENTRY const double PROJECTION_NEAR;
		THRONE_API_ENTRY const double PROJECTION_FAR;
	}
}
