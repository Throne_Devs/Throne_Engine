#pragma once
#include "stdafx.h"

#include "Selectable.h"

class Checkbox : public Selectable
{
public:
	Checkbox();
	~Checkbox();

	void initializeObject() override;
	void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex) override;

	Event<void> onClick;

	bool isOn = false;

private:
	FunctionHandle<void()> onClickHandle;
	Sprite checkmarkTexture;

	void click();
};
