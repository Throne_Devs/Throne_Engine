#pragma once

//Keep this include first to avoid compiler errors.
#include "Configuration.h"

//#include "..\Visual Leak Detector\include\vld.h"
#include "R.h"
#include "UtilityStructures.h"

using namespace R::S::ErrorMessage;
using namespace R::S::Path;
using namespace R::S::StandardOutput;
using namespace R::S::C;
using namespace R::C;

#include <Vulkan/vulkan.h>

#pragma region GLM
#include <Glm/detail/type_vec.hpp>
#include <Glm/detail/type_mat.hpp>
#include <Glm/matrix.hpp>
#include <Glm/gtc/type_ptr.hpp>

#include <Glm/gtx/euler_angles.hpp>
#include <Glm/gtx/transform.hpp>
#include <Glm/gtc/matrix_transform.hpp>
#include <Glm/detail/func_geometric.hpp>
#include <Glm/detail/func_trigonometric.hpp>

#pragma endregion GLM

#pragma region STD

#include <memory>
#include <algorithm>
#include <functional>
#include <math.h>
#include <exception>
#include <thread>
#include <limits>

#pragma region TIME
#include <iomanip>
#include <ctime>
#include <sstream>
#include <locale>
#pragma endregion TIME

#pragma region STREAM
#include <fstream>
#include <iostream>
#include <filesystem>
#include <sstream>
#pragma endregion STREAM

#pragma region STD_DATA_STRUCTURES
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <vector>
#include <deque>
#include <queue>
#include <list>
#include <string>
#include <cstring>
#pragma endregion STD_DATA_STRUCTURES


#pragma endregion STD

#pragma region Windows
#if defined PLATFORM_WINDOWS
#include <Windows.h>
#endif PLATFORM_WINDOWS
#pragma endregion Windows

#pragma region BOOST
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>
#pragma endregion BOOST

