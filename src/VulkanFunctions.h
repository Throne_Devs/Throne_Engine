#pragma once
#include "stdafx.h"

#include "Vulkan\vulkan.h"

namespace ThroneEngine
{
	template<class F, class L>
	THRONE_API_ENTRY inline bool loadFunctionFromLibrary(F& func, const L library, const std::string funcName);
	template<class F, class L>
	THRONE_API_ENTRY inline bool loadFunctionFromLibrary(F& func, const L library, const char* funcName);

	template<class F, class I>
	THRONE_API_ENTRY inline bool loadVulkanInstanceFunction(const PFN_vkGetInstanceProcAddr funcLoader, F& func, const I instance, const char* funcName);

	template<class F, class D>
	THRONE_API_ENTRY inline bool loadVulkanDeviceFunction(const PFN_vkGetDeviceProcAddr funcLoader, F& func, const D device, const char* funcName);

	THRONE_API_ENTRY LIBRARY_TYPE vulkanLibrary;

	THRONE_API_ENTRY bool isExtensionSupported(std::vector<VkExtensionProperties>& availableExtensions, const char* requiredExtension);
	THRONE_API_ENTRY bool isExtensionSupported(std::vector<const char*>& availableExtensions, const char* requiredExtension);

	THRONE_API_ENTRY bool loadVulkanLibrary();
	THRONE_API_ENTRY void releaseVulkanLibrary();
	THRONE_API_ENTRY bool loadPFNGetInstanceProcAddr();

	THRONE_API_ENTRY bool initializeVulkanFunctions();
	THRONE_API_ENTRY bool initializeInstanceLevelVulkanFunctions(const VkInstance& instance, std::vector<const char*>& enabledExtensions);
	THRONE_API_ENTRY bool initializeDeviceLevelVulkanFunctions(const VkDevice& logicalDevice, std::vector<const char*>& enabledExtensions);

	// Global level

	THRONE_API_ENTRY PFN_vkGetInstanceProcAddr funcptr_vkGetInstanceProcAddr;
	THRONE_API_ENTRY PFN_vkVoidFunction vkGetInstanceProcAddr(VkInstance instance, const char* pName);

	THRONE_API_ENTRY PFN_vkEnumerateInstanceExtensionProperties funcptr_vkEnumerateInstanceExtensionProperties;
	THRONE_API_ENTRY VkResult vkEnumerateInstanceExtensionProperties(const char* pLayerName, uint32* pPropertyCount, VkExtensionProperties* pProperties);

	THRONE_API_ENTRY PFN_vkEnumerateInstanceLayerProperties funcptr_vkEnumerateInstanceLayerProperties;
	THRONE_API_ENTRY VkResult vkEnumerateInstanceLayerProperties(uint32* pPropertyCount, VkLayerProperties* pProperties);

	THRONE_API_ENTRY PFN_vkCreateInstance funcptr_vkCreateInstance;
	THRONE_API_ENTRY VkResult vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkInstance* pInstance);

	#pragma region Instance level

	THRONE_API_ENTRY PFN_vkEnumeratePhysicalDevices funcptr_vkEnumeratePhysicalDevices;
	THRONE_API_ENTRY VkResult vkEnumeratePhysicalDevices(VkInstance instance, uint32* pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices);

	THRONE_API_ENTRY PFN_vkEnumerateDeviceExtensionProperties funcptr_vkEnumerateDeviceExtensionProperties;
	THRONE_API_ENTRY VkResult vkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice, const char* pLayerName, uint32* pPropertyCount, VkExtensionProperties* pProperties);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceFeatures funcptr_vkGetPhysicalDeviceFeatures;
	THRONE_API_ENTRY void vkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures* pFeatures);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceProperties funcptr_vkGetPhysicalDeviceProperties;
	THRONE_API_ENTRY void vkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceQueueFamilyProperties funcptr_vkGetPhysicalDeviceQueueFamilyProperties;
	THRONE_API_ENTRY void vkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, uint32* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceMemoryProperties funcptr_vkGetPhysicalDeviceMemoryProperties;
	THRONE_API_ENTRY void vkGetPhysicalDeviceMemoryProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceMemoryProperties* pMemoryProperties);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceFormatProperties funcptr_vkGetPhysicalDeviceFormatProperties; 
	THRONE_API_ENTRY void vkGetPhysicalDeviceFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkFormatProperties* pFormatProperties);

	THRONE_API_ENTRY PFN_vkCreateDevice funcptr_vkCreateDevice;
	THRONE_API_ENTRY VkResult vkCreateDevice(VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDevice* pDevice);

	THRONE_API_ENTRY PFN_vkGetDeviceProcAddr funcptr_vkGetDeviceProcAddr;
	THRONE_API_ENTRY PFN_vkVoidFunction vkGetDeviceProcAddr(VkDevice device, const char* pName);

	THRONE_API_ENTRY PFN_vkDestroyInstance funcptr_vkDestroyInstance;
	THRONE_API_ENTRY void vkDestroyInstance(VkInstance instance, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateDebugReportCallbackEXT funcptr_vkCreateDebugReportCallbackEXT;
	THRONE_API_ENTRY VkResult vkCreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback);

	#pragma region Instance level from extension

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceSurfaceSupportKHR funcptr_vkGetPhysicalDeviceSurfaceSupportKHR;
	THRONE_API_ENTRY VkResult vkGetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice, uint32 queueFamilyIndex, VkSurfaceKHR surface, VkBool32* pSupported);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR funcptr_vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
	THRONE_API_ENTRY VkResult vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceSurfaceFormatsKHR funcptr_vkGetPhysicalDeviceSurfaceFormatsKHR;
	THRONE_API_ENTRY VkResult vkGetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32* pSurfaceFormatCount, VkSurfaceFormatKHR* pSurfaceFormats);

	THRONE_API_ENTRY PFN_vkGetPhysicalDeviceSurfacePresentModesKHR funcptr_vkGetPhysicalDeviceSurfacePresentModesKHR;
	THRONE_API_ENTRY VkResult vkGetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32* pPresentModeCount, VkPresentModeKHR* pPresentModes);

	THRONE_API_ENTRY PFN_vkDestroySurfaceKHR funcptr_vkDestroySurfaceKHR;
	THRONE_API_ENTRY void vkDestroySurfaceKHR(VkInstance instance, VkSurfaceKHR surface, const VkAllocationCallbacks* pAllocator);

	#if defined VK_USE_PLATFORM_WIN32_KHR
	THRONE_API_ENTRY PFN_vkCreateWin32SurfaceKHR funcptr_vkCreateWin32SurfaceKHR;
	THRONE_API_ENTRY VkResult vkCreateWin32SurfaceKHR(VkInstance instance, const VkWin32SurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface);

	#elif defined VK_USE_PLATFORM_XCB_KHR
	THRONE_API_ENTRY PFN_vkCreateXcbSurfaceKHR funcptr_vkCreateXcbSurfaceKHR;
	THRONE_API_ENTRY VkResult PFN_vkCreateXcbSurfaceKHR(VkInstance instance, const VkXcbSurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface);

	#elif defined VK_USE_PLATFORM_XLIB_KHR
	THRONE_API_ENTRY PFN_vkCreateXlibSurfaceKHR funcptr_vkCreateXlibSurfaceKHR;
	THRONE_API_ENTRY VkResult vkCreateXlibSurfaceKHR(VkInstance instance, const VkXlibSurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface);

	#endif

	#pragma endregion Instance level from extension

	#pragma endregion Instance level

	// Device level

	THRONE_API_ENTRY PFN_vkGetDeviceQueue funcptr_vkGetDeviceQueue;
	THRONE_API_ENTRY void vkGetDeviceQueue(VkDevice device, uint32 queueFamilyIndex, uint32 queueIndex, VkQueue* pQueue);

	THRONE_API_ENTRY PFN_vkDeviceWaitIdle funcptr_vkDeviceWaitIdle;
	THRONE_API_ENTRY VkResult vkDeviceWaitIdle(VkDevice device);

	THRONE_API_ENTRY PFN_vkDestroyDevice funcptr_vkDestroyDevice;
	THRONE_API_ENTRY void vkDestroyDevice(VkDevice device, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateBuffer funcptr_vkCreateBuffer;
	THRONE_API_ENTRY VkResult vkCreateBuffer(VkDevice device, const VkBufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer);

	THRONE_API_ENTRY PFN_vkGetBufferMemoryRequirements funcptr_vkGetBufferMemoryRequirements;
	THRONE_API_ENTRY void vkGetBufferMemoryRequirements(VkDevice device, VkBuffer buffer, VkMemoryRequirements* pMemoryRequirements);

	THRONE_API_ENTRY PFN_vkAllocateMemory funcptr_vkAllocateMemory;
	THRONE_API_ENTRY VkResult vkAllocateMemory(VkDevice device, const VkMemoryAllocateInfo* pAllocateInfo, const VkAllocationCallbacks* pAllocator, VkDeviceMemory* pMemory);

	THRONE_API_ENTRY PFN_vkBindBufferMemory funcptr_vkBindBufferMemory;
	THRONE_API_ENTRY VkResult vkBindBufferMemory(VkDevice device, VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset);

	THRONE_API_ENTRY PFN_vkCreateImage funcptr_vkCreateImage;
	THRONE_API_ENTRY VkResult vkCreateImage(VkDevice device, const VkImageCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImage* pImage);

	THRONE_API_ENTRY PFN_vkGetImageMemoryRequirements funcptr_vkGetImageMemoryRequirements;
	THRONE_API_ENTRY void vkGetImageMemoryRequirements(VkDevice device, VkImage image, VkMemoryRequirements* pMemoryRequirements);

	THRONE_API_ENTRY PFN_vkBindImageMemory funcptr_vkBindImageMemory;
	THRONE_API_ENTRY VkResult vkBindImageMemory(VkDevice device, VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset);

	THRONE_API_ENTRY PFN_vkCreateImageView funcptr_vkCreateImageView;
	THRONE_API_ENTRY VkResult vkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImageView* pView);

	THRONE_API_ENTRY PFN_vkMapMemory funcptr_vkMapMemory;
	THRONE_API_ENTRY VkResult vkMapMemory(VkDevice device, VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size, VkMemoryMapFlags flags, void** ppData);

	THRONE_API_ENTRY PFN_vkFlushMappedMemoryRanges funcptr_vkFlushMappedMemoryRanges;
	THRONE_API_ENTRY VkResult vkFlushMappedMemoryRanges(VkDevice device, uint32 memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges);

	THRONE_API_ENTRY PFN_vkUnmapMemory funcptr_vkUnmapMemory;
	THRONE_API_ENTRY void vkUnmapMemory(VkDevice device, VkDeviceMemory memory);
	
	THRONE_API_ENTRY PFN_vkBeginCommandBuffer funcptr_vkBeginCommandBuffer;
	THRONE_API_ENTRY VkResult vkBeginCommandBuffer(VkCommandBuffer commandBuffer, const VkCommandBufferBeginInfo* pBeginInfo);

	THRONE_API_ENTRY PFN_vkEndCommandBuffer funcptr_vkEndCommandBuffer;
	THRONE_API_ENTRY VkResult vkEndCommandBuffer(VkCommandBuffer commandBuffer);

	THRONE_API_ENTRY PFN_vkQueueSubmit funcptr_vkQueueSubmit;
	THRONE_API_ENTRY VkResult vkQueueSubmit(VkQueue graphicsQueue, uint32 submitCount, const VkSubmitInfo* pSubmits, VkFence fence);

	THRONE_API_ENTRY PFN_vkDestroyImageView funcptr_vkDestroyImageView;
	THRONE_API_ENTRY void vkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyImage funcptr_vkDestroyImage;
	THRONE_API_ENTRY void vkDestroyImage(VkDevice device, VkImage image, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyBuffer funcptr_vkDestroyBuffer;
	THRONE_API_ENTRY void vkDestroyBuffer(VkDevice device, VkBuffer buffer, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkFreeMemory funcptr_vkFreeMemory;
	THRONE_API_ENTRY void vkFreeMemory(VkDevice device, VkDeviceMemory memory, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateCommandPool funcptr_vkCreateCommandPool;
	THRONE_API_ENTRY VkResult vkCreateCommandPool(VkDevice device, const VkCommandPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkCommandPool* pCommandPool);

	THRONE_API_ENTRY PFN_vkAllocateCommandBuffers funcptr_vkAllocateCommandBuffers;
	THRONE_API_ENTRY VkResult vkAllocateCommandBuffers(VkDevice device, const VkCommandBufferAllocateInfo* pAllocateInfo, VkCommandBuffer* pCommandBuffers);

	THRONE_API_ENTRY PFN_vkCreateSemaphore funcptr_vkCreateSemaphore;
	THRONE_API_ENTRY VkResult vkCreateSemaphore(VkDevice device, const VkSemaphoreCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSemaphore* pSemaphore);

	THRONE_API_ENTRY PFN_vkCreateFence funcptr_vkCreateFence;
	THRONE_API_ENTRY VkResult vkCreateFence(VkDevice device, const VkFenceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFence* pFence);

	THRONE_API_ENTRY PFN_vkWaitForFences funcptr_vkWaitForFences;
	THRONE_API_ENTRY VkResult vkWaitForFences(VkDevice device, uint32 fenceCount, const VkFence* pFences, VkBool32 waitAll, uint64 timeout);

	THRONE_API_ENTRY PFN_vkResetFences funcptr_vkResetFences;
	THRONE_API_ENTRY VkResult vkResetFences(VkDevice device, uint32 fenceCount, const VkFence* pFences);

	THRONE_API_ENTRY PFN_vkDestroyFence funcptr_vkDestroyFence;
	THRONE_API_ENTRY void vkDestroyFence(VkDevice device, VkFence fence, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroySemaphore funcptr_vkDestroySemaphore;
	THRONE_API_ENTRY void vkDestroySemaphore(VkDevice device, VkSemaphore semaphore, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkResetCommandBuffer funcptr_vkResetCommandBuffer;
	THRONE_API_ENTRY VkResult vkResetCommandBuffer(VkCommandBuffer commandBuffer, VkCommandBufferResetFlags flags);

	THRONE_API_ENTRY PFN_vkFreeCommandBuffers funcptr_vkFreeCommandBuffers;
	THRONE_API_ENTRY void vkFreeCommandBuffers(VkDevice device, VkCommandPool commandPool, uint32 commandBufferCount, const VkCommandBuffer* pCommandBuffers);

	THRONE_API_ENTRY PFN_vkResetCommandPool funcptr_vkResetCommandPool;
	THRONE_API_ENTRY VkResult vkResetCommandPool(VkDevice device, VkCommandPool commandPool, VkCommandPoolResetFlags flags);

	THRONE_API_ENTRY PFN_vkDestroyCommandPool funcptr_vkDestroyCommandPool;
	THRONE_API_ENTRY void vkDestroyCommandPool(VkDevice device, VkCommandPool commandPool, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateBufferView funcptr_vkCreateBufferView;
	THRONE_API_ENTRY VkResult vkCreateBufferView(VkDevice device, const VkBufferViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBufferView* pView);

	THRONE_API_ENTRY PFN_vkDestroyBufferView funcptr_vkDestroyBufferView;
	THRONE_API_ENTRY void vkDestroyBufferView(VkDevice device, VkBufferView bufferView, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkQueueWaitIdle funcptr_vkQueueWaitIdle;
	THRONE_API_ENTRY VkResult vkQueueWaitIdle(VkQueue graphicsQueue);

	THRONE_API_ENTRY PFN_vkCreateSampler funcptr_vkCreateSampler;
	THRONE_API_ENTRY VkResult vkCreateSampler(VkDevice device, const VkSamplerCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSampler* pSampler);

	THRONE_API_ENTRY PFN_vkCreateDescriptorSetLayout funcptr_vkCreateDescriptorSetLayout;
	THRONE_API_ENTRY VkResult vkCreateDescriptorSetLayout(VkDevice device, const VkDescriptorSetLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorSetLayout* pSetLayout);

	THRONE_API_ENTRY PFN_vkCreateDescriptorPool funcptr_vkCreateDescriptorPool;
	THRONE_API_ENTRY VkResult vkCreateDescriptorPool(VkDevice device, const VkDescriptorPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorPool* pDescriptorPool);

	THRONE_API_ENTRY PFN_vkAllocateDescriptorSets funcptr_vkAllocateDescriptorSets;
	THRONE_API_ENTRY VkResult vkAllocateDescriptorSets(VkDevice device, const VkDescriptorSetAllocateInfo* pAllocateInfo, VkDescriptorSet* pDescriptorSets);

	THRONE_API_ENTRY PFN_vkUpdateDescriptorSets funcptr_vkUpdateDescriptorSets;
	THRONE_API_ENTRY void vkUpdateDescriptorSets(VkDevice device, uint32 descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites, uint32 descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies);
	
	THRONE_API_ENTRY PFN_vkFreeDescriptorSets funcptr_vkFreeDescriptorSets;
	THRONE_API_ENTRY VkResult vkFreeDescriptorSets(VkDevice device, VkDescriptorPool descriptorPool, uint32 descriptorSetCount, const VkDescriptorSet* pDescriptorSets);

	THRONE_API_ENTRY PFN_vkResetDescriptorPool funcptr_vkResetDescriptorPool;
	THRONE_API_ENTRY VkResult vkResetDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags);

	THRONE_API_ENTRY PFN_vkDestroyDescriptorPool funcptr_vkDestroyDescriptorPool;
	THRONE_API_ENTRY void vkDestroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyDescriptorSetLayout funcptr_vkDestroyDescriptorSetLayout;
	THRONE_API_ENTRY void vkDestroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroySampler funcptr_vkDestroySampler;
	THRONE_API_ENTRY void vkDestroySampler(VkDevice device, VkSampler sampler, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateRenderPass funcptr_vkCreateRenderPass;
	THRONE_API_ENTRY VkResult vkCreateRenderPass(VkDevice device, const VkRenderPassCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkRenderPass* pRenderPass);

	THRONE_API_ENTRY PFN_vkCreateFramebuffer funcptr_vkCreateFramebuffer;
	THRONE_API_ENTRY VkResult vkCreateFramebuffer(VkDevice device, const VkFramebufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFramebuffer* pFramebuffer);

	THRONE_API_ENTRY PFN_vkDestroyFramebuffer funcptr_vkDestroyFramebuffer;
	THRONE_API_ENTRY void vkDestroyFramebuffer(VkDevice device, VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyRenderPass funcptr_vkDestroyRenderPass;
	THRONE_API_ENTRY void vkDestroyRenderPass(VkDevice device, VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator);
	
	THRONE_API_ENTRY PFN_vkCreatePipelineCache funcptr_vkCreatePipelineCache;
	THRONE_API_ENTRY VkResult vkCreatePipelineCache(VkDevice device, const VkPipelineCacheCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineCache* pPipelineCache);

	THRONE_API_ENTRY PFN_vkGetPipelineCacheData funcptr_vkGetPipelineCacheData;
	THRONE_API_ENTRY VkResult vkGetPipelineCacheData(VkDevice device, VkPipelineCache pipelineCache, size_t* pDataSize, void* pData);

	THRONE_API_ENTRY PFN_vkMergePipelineCaches funcptr_vkMergePipelineCaches;
	THRONE_API_ENTRY VkResult vkMergePipelineCaches(VkDevice device, VkPipelineCache dstCache, uint32 srcCacheCount, const VkPipelineCache* pSrcCaches);

	THRONE_API_ENTRY PFN_vkDestroyPipelineCache funcptr_vkDestroyPipelineCache;
	THRONE_API_ENTRY void vkDestroyPipelineCache(VkDevice device, VkPipelineCache pipelineCache, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateGraphicsPipelines funcptr_vkCreateGraphicsPipelines;
	THRONE_API_ENTRY VkResult vkCreateGraphicsPipelines(VkDevice device, VkPipelineCache pipelineCache, uint32 createInfoCount, const VkGraphicsPipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines);

	THRONE_API_ENTRY PFN_vkCreateComputePipelines funcptr_vkCreateComputePipelines;
	THRONE_API_ENTRY VkResult vkCreateComputePipelines(VkDevice device, VkPipelineCache pipelineCache, uint32 createInfoCount, const VkComputePipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines);

	THRONE_API_ENTRY PFN_vkDestroyPipeline funcptr_vkDestroyPipeline;
	THRONE_API_ENTRY void vkDestroyPipeline(VkDevice device, VkPipeline pipeline, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyEvent funcptr_vkDestroyEvent;
	THRONE_API_ENTRY void vkDestroyEvent(VkDevice device, VkEvent event, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkDestroyQueryPool funcptr_vkDestroyQueryPool;
	THRONE_API_ENTRY void vkDestroyQueryPool(VkDevice device, VkQueryPool queryPool, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreateShaderModule funcptr_vkCreateShaderModule;
	THRONE_API_ENTRY VkResult vkCreateShaderModule(VkDevice device, const VkShaderModuleCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkShaderModule* pShaderModule);

	THRONE_API_ENTRY PFN_vkDestroyShaderModule funcptr_vkDestroyShaderModule;
	THRONE_API_ENTRY void vkDestroyShaderModule(VkDevice device, VkShaderModule shaderModule, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCreatePipelineLayout funcptr_vkCreatePipelineLayout;
	THRONE_API_ENTRY VkResult vkCreatePipelineLayout(VkDevice device, const VkPipelineLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineLayout* pPipelineLayout);

	THRONE_API_ENTRY PFN_vkDestroyPipelineLayout funcptr_vkDestroyPipelineLayout;
	THRONE_API_ENTRY void vkDestroyPipelineLayout(VkDevice device, VkPipelineLayout pipelineLayout, const VkAllocationCallbacks* pAllocator);

	THRONE_API_ENTRY PFN_vkCmdPipelineBarrier funcptr_vkCmdPipelineBarrier;
	THRONE_API_ENTRY PFN_vkCmdCopyBuffer funcptr_vkCmdCopyBuffer;
	THRONE_API_ENTRY PFN_vkCmdCopyBufferToImage funcptr_vkCmdCopyBufferToImage;
	THRONE_API_ENTRY PFN_vkCmdCopyImageToBuffer funcptr_vkCmdCopyImageToBuffer;
	THRONE_API_ENTRY PFN_vkCmdBindDescriptorSets funcptr_vkCmdBindDescriptorSets;
	THRONE_API_ENTRY PFN_vkCmdBeginRenderPass funcptr_vkCmdBeginRenderPass;
	THRONE_API_ENTRY PFN_vkCmdNextSubpass funcptr_vkCmdNextSubpass;
	THRONE_API_ENTRY PFN_vkCmdEndRenderPass funcptr_vkCmdEndRenderPass;
	THRONE_API_ENTRY PFN_vkCmdBindPipeline funcptr_vkCmdBindPipeline;
	THRONE_API_ENTRY PFN_vkCmdSetViewport funcptr_vkCmdSetViewport;	
	THRONE_API_ENTRY PFN_vkCmdSetScissor funcptr_vkCmdSetScissor;
	THRONE_API_ENTRY PFN_vkCmdBindVertexBuffers funcptr_vkCmdBindVertexBuffers;	
	THRONE_API_ENTRY PFN_vkCmdDraw funcptr_vkCmdDraw;
	THRONE_API_ENTRY PFN_vkCmdDrawIndexed funcptr_vkCmdDrawIndexed;	
	THRONE_API_ENTRY PFN_vkCmdDispatch funcptr_vkCmdDispatch;	
	THRONE_API_ENTRY PFN_vkCmdCopyImage funcptr_vkCmdCopyImage;
	THRONE_API_ENTRY PFN_vkCmdPushConstants funcptr_vkCmdPushConstants;
	THRONE_API_ENTRY PFN_vkCmdClearColorImage funcptr_vkCmdClearColorImage;
	THRONE_API_ENTRY PFN_vkCmdClearDepthStencilImage funcptr_vkCmdClearDepthStencilImage;
	THRONE_API_ENTRY PFN_vkCmdBindIndexBuffer funcptr_vkCmdBindIndexBuffer;
	THRONE_API_ENTRY PFN_vkCmdSetLineWidth funcptr_vkCmdSetLineWidth;
	THRONE_API_ENTRY PFN_vkCmdSetDepthBias funcptr_vkCmdSetDepthBias;
	THRONE_API_ENTRY PFN_vkCmdSetBlendConstants funcptr_vkCmdSetBlendConstants;
	THRONE_API_ENTRY PFN_vkCmdExecuteCommands funcptr_vkCmdExecuteCommands;
	THRONE_API_ENTRY PFN_vkCmdClearAttachments funcptr_vkCmdClearAttachments;
	
	// Device level from extension

	THRONE_API_ENTRY PFN_vkCreateSwapchainKHR funcptr_vkCreateSwapchainKHR;
	THRONE_API_ENTRY VkResult vkCreateSwapchainKHR(VkDevice device, const VkSwapchainCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSwapchainKHR* pSwapchain);

	THRONE_API_ENTRY PFN_vkGetSwapchainImagesKHR funcptr_vkGetSwapchainImagesKHR;
	THRONE_API_ENTRY VkResult vkGetSwapchainImagesKHR(VkDevice device, VkSwapchainKHR swapchain, uint32* pSwapchainImageCount, VkImage* pSwapchainImages);

	THRONE_API_ENTRY PFN_vkAcquireNextImageKHR funcptr_vkAcquireNextImageKHR;
	THRONE_API_ENTRY VkResult vkAcquireNextImageKHR(VkDevice device, VkSwapchainKHR swapchain, uint64 timeout, VkSemaphore semaphore, VkFence fence, uint32* pImageIndex);

	THRONE_API_ENTRY PFN_vkQueuePresentKHR funcptr_vkQueuePresentKHR;
	THRONE_API_ENTRY VkResult vkQueuePresentKHR(VkQueue graphicsQueue, const VkPresentInfoKHR* pPresentInfo);

	THRONE_API_ENTRY PFN_vkDestroySwapchainKHR funcptr_vkDestroySwapchainKHR;
	THRONE_API_ENTRY void vkDestroySwapchainKHR(VkDevice device, VkSwapchainKHR swapchain, const VkAllocationCallbacks* pAllocator);
}