#pragma once

#include "stdafx.h"
#include "Throne.h"

#include <iostream>

static void pauseConsoleAndWait();

int main()
{
	Throne throne;
    bool success;

	try
	{
        if (!throne.initialize())
        {
            pauseConsoleAndWait();
            return EXIT_FAILURE;
        }

        success = throne.run();
	}
	catch (const std::exception& e)
	{
		#if defined ERROR_MESSAGE_NEEDED
		ERROR_MESSAGE(strcat(const_cast<char*>(R::S::ErrorMessage::err_unManaged.data()), e.what()));
		#endif
		return EXIT_FAILURE;
	}

	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}

static void pauseConsoleAndWait()
{
    ERROR_MESSAGE(R::S::ErrorMessage::err_consolePaused);
    std::cin.get();
}