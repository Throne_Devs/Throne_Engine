#pragma once
#include "stdafx.h"

#include "Event.h"
#include "Selectable.h"

namespace ThroneEngine
{
	class Button : public Selectable
	{
	public:
		Button();
		~Button();

		void initializeObject() override;

		Event<void> onClick;
		Event<void> onClickMaintained;
		Event<void> onClickReleased;
		Event<void> onHover;

	private:
		FunctionHandle<void()> onClickHandle;
		FunctionHandle<void()> onClickMaintainedHandle;
		FunctionHandle<void()> onClickReleasedHandle;		

		void click();
		void clickMaintained();
		void clickReleased();		
	};
}
