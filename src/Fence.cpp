#include "stdafx.h"

#include "Fence.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{

	Fence::Fence()
	{
	}


	Fence::~Fence()
	{
	}

	bool Fence::initialize()
	{
		return createFence(handle);
	}

	bool Fence::waitForFence(double timeout)
	{
		if (fenceInUse == true)
		{
			return waitForFences(1, &handle, VK_TRUE, timeout);
		}
		else
		{
			return true;
		}
	}

	bool Fence::reset()
	{
		fenceInUse = false;
		return resetFences(1, &handle);
	}

	void Fence::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyFence(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}

	inline bool Fence::createFence(VkFence& fence)
	{
		VkFenceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;

		VkResult result = vkCreateFence(logicalDevice, &createInfo, nullptr, &fence);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createFence);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline bool Fence::waitForFences(uint32 numberOfFence, VkFence* fenceHanldes, VkBool32 waitForAll, double timeout)
	{
		VkResult result = vkWaitForFences(logicalDevice, numberOfFence, fenceHanldes, waitForAll, timeout);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_waitForFences);
			#endif
			return false;
		}
		#endif
		return true;
	}

	inline bool Fence::resetFences(uint32 numberOfFence, VkFence* fenceHanldes)
	{
		VkResult result = vkResetFences(logicalDevice, numberOfFence, fenceHanldes);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_resetFences);
			#endif
			return false;
		}
		#endif
		return true;
	}
}