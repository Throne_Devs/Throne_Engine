#include "stdafx.h"

#include "ImageSamplerArray.h"

namespace ThroneEngine
{

	ImageSamplerArray::ImageSamplerArray()
	{
	}


	ImageSamplerArray::~ImageSamplerArray()
	{
	}

	void ImageSamplerArray::initialize(uint32 descriptorCount, ImageSampler* samplers)
	{
		this->descriptorCount = descriptorCount;
		for (int i = 0; i < descriptorCount; i++)
		{
			this->samplers.push_back(&samplers[i]);
		}
		setDescriptorType();
	}

	void ImageSamplerArray::destroy()
	{
		for (int i = 0; i < samplers.size(); i++)
		{
			samplers[i]->destroy();
		}
	}

	void ImageSamplerArray::initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding)
	{
		initializeGenericWriteDescriptorSet(descriptorSetHandle, binding);

		VkDescriptorImageInfo* imageInfos = new VkDescriptorImageInfo[samplers.size()];

		for (int i = 0; i < samplers.size(); i++)
		{
			imageInfos[i].sampler = samplers[i]->samplerHandle;
			imageInfos[i].imageView = samplers[i]->image->imageViewHandles[0];
			imageInfos[i].imageLayout = samplers[i]->image->imageLayout;
		}

		if (writeDescriptorSet.pImageInfo != nullptr)
		{
			delete[] writeDescriptorSet.pImageInfo;
		}

		writeDescriptorSet.pImageInfo = imageInfos;
		writeDescriptorSet.pBufferInfo = nullptr;
		writeDescriptorSet.pTexelBufferView = nullptr;

		writeDescriptorSet.descriptorType = descriptorType;
		writeDescriptorSet.descriptorCount = descriptorCount;
	}

	void ImageSamplerArray::setDescriptorType()
	{
		descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	}

}