#include "stdafx.h"

#include "GraphicsPipeline.h"
#include "UtilityFunctions.hpp"
#include "RenderPass.h"
#include "ThroneFramework.h"
#include "DescriptorSetLayout.h"
#include "Shader.h"

namespace ThroneEngine
{

	GraphicsPipeline::GraphicsPipeline()
	{
	}


	GraphicsPipeline::~GraphicsPipeline()
	{
	}

	bool GraphicsPipeline::initialize(VkPipelineCreateFlags additionalOptions, GraphicsPipelineCreationParams& creationParams, RenderPass& renderPass, uint32 subpass, VkPipeline basePipelineHandle)
	{
		std::vector<VkDescriptorSetLayout> layouts(creationParams.descriptorSetLayouts.size());
		for (int i = 0; i < layouts.size(); i++)
		{
			layouts[i] = creationParams.descriptorSetLayouts[i].handle;
		}
		createPipelineLayout(layouts, creationParams.pushConstantRanges);


		std::vector<VkPipelineShaderStageCreateInfo> shaderStageCreateInfos(creationParams.shaders.size());
		for (int i = 0; i < shaderStageCreateInfos.size(); i++)
		{
			creationParams.shaders[i].getShaderStageCreateInfo(shaderStageCreateInfos[i]);
		}

		std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescriptions;
		std::vector<VkVertexInputBindingDescription> vertexInputBindingDescriptions;
		createInputDescriptions(creationParams.vertexInputSignatures, vertexInputAttributeDescriptions, vertexInputBindingDescriptions);
		VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo;
		specifyPipelineVertexInputState(creationParams, vertexInputStateCreateInfo, vertexInputAttributeDescriptions, vertexInputBindingDescriptions);

		VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo;
 		specifyPipelineInputAssemblyState(creationParams, inputAssemblyStateCreateInfo);

		ViewportInfo viewportInfos = {};
		viewportInfos.viewports.push_back(creationParams.viewport);
		viewportInfos.scissors.push_back(creationParams.scissor);

		VkPipelineViewportStateCreateInfo viewportStateCreateInfo;
		specifyPipelineViewportAndScissorTestState(viewportInfos, viewportStateCreateInfo);

		VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo;
		specifyPipelineRasterizationState(creationParams.rasterizerCreationParams, rasterizationStateCreateInfo);

		VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo;
		specifyPipelineMultisampleState(creationParams.multisampleCreationParams, multisampleStateCreateInfo);

		VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo;
		specifyPipelineDepthAndStencilState(creationParams.depthStencilParams, depthStencilStateCreateInfo);

		VkPipelineColorBlendStateCreateInfo blendStateCreateInfo;
		specifyPipelineBlendState(creationParams.blendCreationParams, blendStateCreateInfo);

		VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo;
		if (creationParams.dynamicStates.size() > 0)
		{
			specifyPipelineDynamicState(creationParams.dynamicStates, dynamicStateCreateInfo);
			dynamicStates = creationParams.dynamicStatesData;
		}

		VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {};
		graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		graphicsPipelineCreateInfo.pNext = nullptr;
		graphicsPipelineCreateInfo.flags = additionalOptions;
		graphicsPipelineCreateInfo.stageCount = shaderStageCreateInfos.size();
		graphicsPipelineCreateInfo.pStages = getVectorDataPointer(shaderStageCreateInfos);
		graphicsPipelineCreateInfo.pVertexInputState = &vertexInputStateCreateInfo;
		graphicsPipelineCreateInfo.pInputAssemblyState = &inputAssemblyStateCreateInfo;
		graphicsPipelineCreateInfo.pTessellationState = nullptr;
		graphicsPipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
		graphicsPipelineCreateInfo.pRasterizationState = &rasterizationStateCreateInfo;
		graphicsPipelineCreateInfo.pMultisampleState = &multisampleStateCreateInfo;
		graphicsPipelineCreateInfo.pDepthStencilState = &depthStencilStateCreateInfo;
		graphicsPipelineCreateInfo.pColorBlendState = &blendStateCreateInfo;
		graphicsPipelineCreateInfo.pDynamicState = creationParams.dynamicStates.size() > 0 ? &dynamicStateCreateInfo : nullptr;
		graphicsPipelineCreateInfo.layout = layoutHandle;
		graphicsPipelineCreateInfo.renderPass = renderPass.handle;
		graphicsPipelineCreateInfo.subpass = subpass;
		graphicsPipelineCreateInfo.basePipelineHandle = basePipelineHandle;
		graphicsPipelineCreateInfo.basePipelineIndex = -1;

		std::vector<unsigned char> cacheData;
		VkPipelineCache pipelineCacheObject;
		createPipelineCacheObject(cacheData, pipelineCacheObject);

		if (createGraphicsPipelines(&graphicsPipelineCreateInfo, pipelineCacheObject, &handle, 1) == false)
		{
			return false;
		}
		return true;
	}

	void GraphicsPipeline::destroy()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyPipeline(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}

		if (layoutHandle != VK_NULL_HANDLE)
		{
			vkDestroyPipelineLayout(logicalDevice, layoutHandle, nullptr);
			layoutHandle = VK_NULL_HANDLE;
		}
	}

	inline void GraphicsPipeline::specifyPipelineVertexInputState(GraphicsPipelineCreationParams& pipelineCreationParams, VkPipelineVertexInputStateCreateInfo& vertexInputStateCreateInfo,
		std::vector<VkVertexInputAttributeDescription>& vertexInputAttributeDescriptions, std::vector<VkVertexInputBindingDescription>& vertexInputBindingDescriptions)
	{
		vertexInputStateCreateInfo = {};
		vertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputStateCreateInfo.pNext = nullptr;
		vertexInputStateCreateInfo.flags = 0;
		vertexInputStateCreateInfo.vertexBindingDescriptionCount = vertexInputBindingDescriptions.size();
		vertexInputStateCreateInfo.pVertexBindingDescriptions = getVectorDataPointer(vertexInputBindingDescriptions);
		vertexInputStateCreateInfo.vertexAttributeDescriptionCount = vertexInputAttributeDescriptions.size();
		vertexInputStateCreateInfo.pVertexAttributeDescriptions = getVectorDataPointer(vertexInputAttributeDescriptions);
	}

	inline void GraphicsPipeline::specifyPipelineInputAssemblyState(GraphicsPipelineCreationParams& pipelineCreationParams, VkPipelineInputAssemblyStateCreateInfo& inputAssemblyStateCreateInfo)
	{
		inputAssemblyStateCreateInfo = {};
		inputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssemblyStateCreateInfo.pNext = nullptr;
		inputAssemblyStateCreateInfo.flags = 0;
		inputAssemblyStateCreateInfo.topology = pipelineCreationParams.primitiveType;
		inputAssemblyStateCreateInfo.primitiveRestartEnable = pipelineCreationParams.primitiveRestartEnable;
	}

	inline void GraphicsPipeline::specifyPipelineViewportAndScissorTestState(ViewportInfo& viewportInfo, VkPipelineViewportStateCreateInfo & viewportStateCreateInfo)
	{
		viewportStateCreateInfo = {};
		viewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportStateCreateInfo.pNext = nullptr;
		viewportStateCreateInfo.flags = 0;
		viewportStateCreateInfo.viewportCount = viewportInfo.viewports.size();
		viewportStateCreateInfo.pViewports = getVectorDataPointer(viewportInfo.viewports);
		viewportStateCreateInfo.scissorCount = viewportInfo.scissors.size();
		viewportStateCreateInfo.pScissors = getVectorDataPointer(viewportInfo.scissors);
	}

	inline void GraphicsPipeline::specifyPipelineRasterizationState(GraphicsPipelineRasterizerCreationParams& rasterizerCreationParams, VkPipelineRasterizationStateCreateInfo& rasterizationStateCreateInfo)
	{
		rasterizationStateCreateInfo = {};
		rasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizationStateCreateInfo.pNext = nullptr;
		rasterizationStateCreateInfo.flags = 0;
		rasterizationStateCreateInfo.depthClampEnable = rasterizerCreationParams.depthClampEnable;
		rasterizationStateCreateInfo.rasterizerDiscardEnable = rasterizerCreationParams.rasterizerDiscardEnable;
		rasterizationStateCreateInfo.polygonMode = rasterizerCreationParams.polygonMode;
		rasterizationStateCreateInfo.cullMode = rasterizerCreationParams.cullingMode;
		rasterizationStateCreateInfo.frontFace = rasterizerCreationParams.frontFace;
		rasterizationStateCreateInfo.depthBiasEnable = rasterizerCreationParams.depthBiasEnable;
		rasterizationStateCreateInfo.depthBiasConstantFactor = rasterizerCreationParams.depthBiasConstantFactor;
		rasterizationStateCreateInfo.depthBiasClamp = rasterizerCreationParams.depthBiasClamp;
		rasterizationStateCreateInfo.depthBiasSlopeFactor = rasterizerCreationParams.depthBiasSlopeFactor;
		rasterizationStateCreateInfo.lineWidth = rasterizerCreationParams.lineWidth;
	}

	inline void GraphicsPipeline::specifyPipelineMultisampleState(GraphicsPipelineMultiSampleCreationParams& multisampleCreationParams, VkPipelineMultisampleStateCreateInfo& multisampleStateCreateInfo)
	{
		multisampleStateCreateInfo = {};
		multisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampleStateCreateInfo.pNext = nullptr;
		multisampleStateCreateInfo.flags = 0;
		multisampleStateCreateInfo.rasterizationSamples = multisampleCreationParams.sampleCount;
		multisampleStateCreateInfo.sampleShadingEnable = multisampleCreationParams.perSampleShadingEnable;
		multisampleStateCreateInfo.minSampleShading = multisampleCreationParams.minSampleShading;
		multisampleStateCreateInfo.pSampleMask = multisampleCreationParams.sampleMasks;
		multisampleStateCreateInfo.alphaToCoverageEnable = multisampleCreationParams.alphaToCoverageEnable;
		multisampleStateCreateInfo.alphaToOneEnable = multisampleCreationParams.alphaToOneEnable;
	}

	inline void GraphicsPipeline::specifyPipelineDepthAndStencilState(GraphicsPipelineDepthStencilCreationParams& depthStencilCreationParams, VkPipelineDepthStencilStateCreateInfo& depthAndStencilStateCreateInfo)
	{
		depthAndStencilStateCreateInfo = {};
		depthAndStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthAndStencilStateCreateInfo.pNext = nullptr;
		depthAndStencilStateCreateInfo.flags = 0;
		depthAndStencilStateCreateInfo.depthTestEnable = depthStencilCreationParams.depthTestEnable;
		depthAndStencilStateCreateInfo.depthWriteEnable = depthStencilCreationParams.depthWriteEnable;
		depthAndStencilStateCreateInfo.depthCompareOp = depthStencilCreationParams.depthCompareOp;
		depthAndStencilStateCreateInfo.depthBoundsTestEnable = depthStencilCreationParams.depthBoundTestEnable;
		depthAndStencilStateCreateInfo.stencilTestEnable = depthStencilCreationParams.stencilTestEnable;
		depthAndStencilStateCreateInfo.front = depthStencilCreationParams.frontStencilTestParameters;
		depthAndStencilStateCreateInfo.back = depthStencilCreationParams.backStencilTestParameters;
		depthAndStencilStateCreateInfo.minDepthBounds = depthStencilCreationParams.minDepthBounds;
		depthAndStencilStateCreateInfo.maxDepthBounds = depthStencilCreationParams.maxDepthBounds;
	}

	inline void GraphicsPipeline::specifyPipelineBlendState(GraphicsPipelineBlendlCreationParams& depthStencilCreationParams, VkPipelineColorBlendStateCreateInfo& blendStateCreateInfo)
	{
		blendStateCreateInfo = {};
		blendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		blendStateCreateInfo.pNext = nullptr;
		blendStateCreateInfo.flags = 0;
		blendStateCreateInfo.logicOpEnable = depthStencilCreationParams.blendLogicOpEnable;
		blendStateCreateInfo.logicOp = depthStencilCreationParams.blendLogicOp;
		blendStateCreateInfo.attachmentCount = depthStencilCreationParams.attachmentBlendStates.size();
		blendStateCreateInfo.pAttachments = getVectorDataPointer(depthStencilCreationParams.attachmentBlendStates);
		blendStateCreateInfo.blendConstants[0] = depthStencilCreationParams.blendConstants[0];
		blendStateCreateInfo.blendConstants[1] = depthStencilCreationParams.blendConstants[1];
		blendStateCreateInfo.blendConstants[2] = depthStencilCreationParams.blendConstants[2];
		blendStateCreateInfo.blendConstants[3] = depthStencilCreationParams.blendConstants[3];
	}

	void GraphicsPipeline::specifyPipelineDynamicState(std::vector<VkDynamicState>& dynamicState, VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo)
	{
		dynamicStateCreateInfo = {};
		dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		dynamicStateCreateInfo.pNext = nullptr;
		dynamicStateCreateInfo.flags = 0;
		dynamicStateCreateInfo.dynamicStateCount = dynamicState.size();
		dynamicStateCreateInfo.pDynamicStates = getVectorDataPointer(dynamicState);
	}

	bool GraphicsPipeline::createPipelineLayout(std::vector<VkDescriptorSetLayout>& descriptorSetLayout, std::vector<VkPushConstantRange>& pushConstantRange)
	{
		VkPipelineLayoutCreateInfo pipelineCreateInfo = {};
		pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineCreateInfo.pNext = nullptr;
		pipelineCreateInfo.flags = 0;
		pipelineCreateInfo.setLayoutCount = descriptorSetLayout.size();
		pipelineCreateInfo.pSetLayouts = getVectorDataPointer(descriptorSetLayout);
		pipelineCreateInfo.pushConstantRangeCount = pushConstantRange.size();
		pipelineCreateInfo.pPushConstantRanges = getVectorDataPointer(pushConstantRange);

		VkResult result = vkCreatePipelineLayout(logicalDevice, &pipelineCreateInfo, nullptr, &layoutHandle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createPipelineLayout);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool GraphicsPipeline::createPipelineCacheObject(std::vector<unsigned char>& cacheData, VkPipelineCache& pipelineCacheObject)
	{
		VkPipelineCacheCreateInfo cacheCreateInfo = {};
		cacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
		cacheCreateInfo.pNext = nullptr;
		cacheCreateInfo.flags = 0;
		cacheCreateInfo.initialDataSize = cacheData.size();
		cacheCreateInfo.pInitialData = cacheData.data();

		VkResult result = vkCreatePipelineCache(logicalDevice, &cacheCreateInfo, nullptr, &pipelineCacheObject);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createPipelineLayout);
			#endif
			return false;
		}
		#endif

		return true;
	}

	bool GraphicsPipeline::getPipelineCacheObjectData(VkPipelineCache pipelineCache, std::vector<unsigned char>& pipelineCacheData)
	{
		size_t datatSize = 0;
		{
			VkResult result = vkGetPipelineCacheData(logicalDevice, pipelineCache, &datatSize, nullptr);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_getPipelineCacheDataSize);
				#endif
				return false;
			}
			#endif
		}


		pipelineCacheData.resize(datatSize);

		VkResult result = vkGetPipelineCacheData(logicalDevice, pipelineCache, &datatSize, &pipelineCacheData[0]);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_getPipelineCacheData);
			#endif
			return false;
		}
		#endif

		return true;
	}

	bool GraphicsPipeline::mergeMultiplePipelineCacheObjects(VkPipelineCache targetPipelineCache, std::vector<VkPipelineCache>& sourcePipelineCaches)
	{
		if (sourcePipelineCaches.size() > 0)
		{
			VkResult result = vkMergePipelineCaches(logicalDevice, targetPipelineCache, sourcePipelineCaches.size(), &sourcePipelineCaches[0]);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_mergePipelineCacheObjects);
				#endif
				return false;
			}
			#endif
		}

		return true;
	}

	inline void GraphicsPipeline::createInputDescriptions(
		std::vector<VertexBufferSignature> vertexBufferInput,
		std::vector<VkVertexInputAttributeDescription>& vertexInputAttributeDescriptions,
		std::vector<VkVertexInputBindingDescription>& vertexInputBindingDescriptions)
	{
		for (auto vbs = vertexBufferInput.begin(); vbs != vertexBufferInput.end(); vbs++)
		{
			uint32 totalSize = 0;
			uint32 offset = 0;
			for (auto vbb = vbs->blocks.begin(); vbb != vbs->blocks.end(); vbb++)
			{
				for (int i = 0; i < vbb->count; i++)
				{
					VkVertexInputAttributeDescription attribDescription = {};
					attribDescription.binding = vbs->bindingIndex;
					attribDescription.format = vbb->format;
					attribDescription.location = vbb->shaderLocation + i;
					attribDescription.offset = offset;

					offset += vbb->typeSize;
					totalSize += vbb->typeSize;

					vertexInputAttributeDescriptions.push_back(attribDescription);
				}
			}
			VkVertexInputBindingDescription bindingDescription = {};
			bindingDescription.binding = vbs->bindingIndex;
			bindingDescription.inputRate = vbs->inputRate;
			bindingDescription.stride = totalSize;

			vertexInputBindingDescriptions.push_back(bindingDescription);
		}
	}

	bool GraphicsPipeline::createGraphicsPipelines(std::vector<VkGraphicsPipelineCreateInfo>& graphicsPipelineCreateInfos, VkPipelineCache pipelineCache, std::vector<VkPipeline>& graphicsPipelines)
	{
		graphicsPipelines.resize(graphicsPipelineCreateInfos.size());
		return createGraphicsPipelines(getVectorDataPointer(graphicsPipelineCreateInfos), pipelineCache, getVectorDataPointer(graphicsPipelines), graphicsPipelines.size());
	}

	bool GraphicsPipeline::createGraphicsPipelines(VkGraphicsPipelineCreateInfo* graphicsPipelineCreateInfos, VkPipelineCache pipelineCache, VkPipeline* graphicsPipelines, uint32 amountOfGraphicsPipeline)
	{
		if (amountOfGraphicsPipeline > 0)
		{
			VkResult result = vkCreateGraphicsPipelines(logicalDevice, pipelineCache, amountOfGraphicsPipeline, graphicsPipelineCreateInfos, nullptr, graphicsPipelines);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_createGraphicsPipelines);
				#endif
				return false;
			}
			#endif
		}
		return true;
	}



}