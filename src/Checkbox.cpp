#pragma once
#include "stdafx.h"

#include "Checkbox.h"
#include "ThroneFramework.h"
#include "Mouse.h"
#include "MeshRenderer.h"
#include "Material.h"
#include "RenderPass.h"
#include "RenderedObject.h"

Checkbox::Checkbox()
{
	onClickHandle.function = std::bind(&Checkbox::click, this);

	Mouse::onButtonPressed[Mouse::Left] += onClickHandle;
}

Checkbox::~Checkbox()
{
}

void Checkbox::initializeObject()
{
	transition = ColorTint;
	colorState.normal = Color(255, 255, 255);
	colorState.highlightedColor = Color(245, 245, 245);
	colorState.pressedColor = Color(200, 200, 200);
	colorState.disabledColor = Color(162, 162, 162, 128);

	targetGraphic.mainTexture.borderImage.setBorders(10, 22, 10, 22);
	targetGraphic.rectTransform.setRect(0, 0, 20, 20);

	targetGraphic.setMaterialActive(1, isOn);
}

void Checkbox::initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex)
{
	checkmarkTexture.initialize(path_uiCheckmarkTexture);

	Material* checkMarkMaterial = new Material();
	checkMarkMaterial->initialize(checkmarkTexture.sampler, 0, 0, 1);

	std::deque<Material*> materials;
	materials.emplace_back(checkMarkMaterial);

	Selectable::initialize(renderPass, graphicsPipeline, descriptorSet, materials, meshRendererBindingIndex, modelMatrixBindingIndex);
}

void Checkbox::click()
{
	if (isHighlighted)
	{
		onClick();

		isOn = !isOn;
		targetGraphic.setMaterialActive(1, isOn);
	}
}
