#include "stdafx.h"

#include "Material.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	uint32 Material::nextHandle = 0;
	DescriptorSetLayout Material::materialDescriptorSetLayout;

	Material::Material()
	{
	}


	Material::~Material()
	{
	}

	/*bool Material::initialize(ImageSampler* imageSampler, float shineDamper, float reflectivity, DescriptorSet* descriptorSet)
	{
		this->imageSampler = imageSampler; 
		this->shineDamper = shineDamper;
		this->reflectivity = reflectivity;
		this->descriptorSet = descriptorSet;
		initHandle(this);
		return true;
	}*/

	void Material::initializeStaticMaterialDescriptorSetLayout()
	{
		materialDescriptorSetLayout.initialize(std::vector<VkDescriptorSetLayoutBinding>{ImageSampler::getLayoutBinding()});
	}

	bool Material::initialize(ImageSampler * imageSampler, float shineDamper, float reflectivity, uint32 descriptorSetLocation)
	{
		this->imageSampler = imageSampler;
		this->shineDamper = shineDamper;
		this->reflectivity = reflectivity;
		
		descriptorSet = new DescriptorSet();
		descriptorSet->initialize(std::vector<Descriptor*>{imageSampler}, materialDescriptorSetLayout, descriptorSetLocation);

		initHandle(this);
		return true;
		return false;
	}

	void Material::initHandle(Material* material)
	{
		material->handle = nextHandle;
		nextHandle++;
	}
}


