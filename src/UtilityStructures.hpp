#pragma once
#include "UtilityStructures.h"
#include "RenderedObject.h"

namespace ThroneEngine
{
	inline void RenderedObjectCommandRecorder::recordCommands(CommandBuffer& commandBuffer)
	{
		if (renderedObjectPart->isActive == true)
		{
			VkDeviceSize offsets[] = { 0 };
			commandBuffer.cmdBindVertexBuffers(1, 1, &renderedObject->modelMatrixBuffer.handle, offsets);
			commandBuffer.cmdDraw(renderedObjectPart->meshPart.vertexCount, 1, renderedObjectPart->meshPart.vertexOffset, 0);
		}
	}

	inline void GraphicsPipelineViewportDynamicState::recordCommands(CommandBuffer* commandBuffer)
	{
		commandBuffer->cmdSetViewport(0, 1, &viewport);
	}

	inline void GraphicsPipelineScissorDynamicState::recordCommands(CommandBuffer* commandBuffer)
	{
		commandBuffer->cmdSetScissor(0, 1, &scissor);
	}
}

