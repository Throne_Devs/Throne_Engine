#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"
#include "EventWindow.h"

namespace ThroneEngine
{
	class Throne;

	class Window
	{
	public:
		Window(uint32 sizeX, uint32 sizeY, std::string name, Throne* throneInstance);
		~Window();

		void close();
		bool isOpen() const;

		bool pollEvent(EventWindow& event);
		void pushEvent(const EventWindow& event);
		void popEvent();

		bool initialize();
		bool initOsWindow();
		void deInitOsWindow();
		void updateOsWindow() const;
		void initOsSurface();

		bool initSurface();
		void deInitSurface() const;
		bool choosePresentMode(VkPresentModeKHR& presentMode) const;

		bool initSwapChain();
		void deInitSwapChain() const;

		bool initSwapChainImages();
		void deInitSwapChainImages();

		void recreateSwapChain();

		void setKeyRepeatEnabled(bool enabled);

		vec2 getSize() const;
		void setSize(const vec2& size) const;
		void setTitle(const std::string title) const;
		vec2 getPosition() const;
		void setPosition(const vec2& position) const;

		void requestFocus() const;
		bool hasFocus() const;
		void setVisible(const bool visible) const;
		void switchToFullscreen(const vec2& mode);
		void setMouseCursorVisible(bool visible);
		void setMouseCursorGrabbed(bool grabbed);
		void grabCursor(bool grabbed) const;
		void setTracking(bool track) const;

		VkSurfaceKHR surface = VK_NULL_HANDLE;
		VkSwapchainKHR swapchain = VK_NULL_HANDLE;

		std::string windowName = sdo_windowName;
		uint32 swapchainImageCount = 2;

		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		VkSurfaceFormatKHR surfaceFormat;
		VkExtent2D swapchainExtent;

		std::vector<VkImage> swapchainImages;
		std::vector<VkImageView> swapchainImageViews;

		bool windowShouldRun = true;
		bool resizing = false;
		bool cursorGrabbed = false;
		bool keyRepeatEnable = true;
		bool mouseInside = true;
		bool mouseCursorVisible = true;
		bool fullscreenWindow = false;
		uint16 surrogate = 0;
		vec2 lastSize;

		Throne* throneInstance;

		std::queue<EventWindow> events;

		#if defined PLATFORM_WINDOWS
		HINSTANCE win32Instance = nullptr;
		HWND win32Window = nullptr;
		std::string win32ClassName;
		static uint64 win32ClassIdCounter;
		LONG_PTR callback = 0;

		static LRESULT CALLBACK windowsEventHandler(const HWND hWnd, const UINT uMsg, const WPARAM wParam, const LPARAM lParam);
		void processEvent(const UINT uMsg, const WPARAM wParam, const LPARAM lParam);
		static Keyboard::Key virtualKeyCode(WPARAM key, LPARAM flags);
		HWND getSystemHandle() const;
		#endif
	};
}
