#pragma once
#include "stdafx.h"

#include "ThroneFramework.h"
#include "Font.h"
#include "RenderedObject.h"

namespace ThroneEngine
{
	class Text
	{
	public:
		Text();
		~Text();

		RenderedObject textObject;
		Material* material;
		Font* font;
		uint32 characterSize = 1;
		tvec3<byte> textColor;
		bool staticTextString = false;
		
		void initializeMaterial(Font* font, uint32 characterSize, const std::wstring& textString, bool staticTextString);
		void initializeMesh(RenderPass& renderPass, GraphicsPipeline* graphicsPipeline, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex, tvec3<byte>& textColor = glm::tvec3<byte>(255, 255, 255),
			vec3& position = vec3{ 0, 0, 0 }, vec3& rotation = vec3{ 0, 0, 0 }, vec3& scale = vec3{ 1, 1, 1 });

		bool setTextString(std::wstring& textString, CommandQueue& commandQueue, RenderPass& renderPass, std::vector<Fence>& waitFences);
		std::wstring& getTextString();

	private:
		std::wstring textString = L"";
		void buildString(Mesh* mesh);
		void addLetter(Glyph& glyph, std::vector<float>& data, float* xVertexIndex, float yVertexIndex, float padding);
	};
}



