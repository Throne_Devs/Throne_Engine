#include "stdafx.h"

#include "BindingTree.h"
#include "RenderPass.h"
#include "BindingTreePredicats.h"
#include "UtilityStructures.hpp"
#include "Clock.h"

namespace ThroneEngine
{
	BindingTree::BindingTree()
	{
	}


	BindingTree::~BindingTree()
	{

	}

	void BindingTree::initializeTree(std::vector<RenderedObject*>& renderedObjects, RenderPass& renderPass)
	{
		pipelineNodes.clear();
		occurenceMap.clear();

		std::unordered_set<GraphicsPipeline*> pipelines;

		fillOccurenceMap(renderedObjects, pipelines, renderPass);

		initializePipelineNodes(pipelines);

		initializeBindingNodes(renderedObjects, renderPass);
	}

	void BindingTree::recordCommands(CommandBuffer& commandBuffer)
	{
		BindableBindingInfo bindingInfo = {};
		bindingInfo.commandBuffer = &commandBuffer;

		for (auto it = pipelineNodes.begin(); it != pipelineNodes.end(); it++)
		{
			bindingInfo.pipelineLayout = it->first->layoutHandle;
			it->second.bind(bindingInfo);
		}
	}

	void BindingTree::fillOccurenceMap(std::vector<RenderedObject*>& renderedObjects, std::unordered_set<GraphicsPipeline*>& pipelines, RenderPass& renderPass)
	{
		for (int i = 0; i < renderedObjects.size(); i++)
		{
			for (RenderedObjectPart& renderedObjectPart : renderedObjects[i]->getRenderPassInfoByRenderPassHandle(renderPass.handle).renderedObjectParts)
			{
				pipelines.insert(renderedObjectPart.graphicsPipeline);

				occurenceMap[renderedObjects[i]->meshRenderer]++;
				if (renderedObjectPart.material != nullptr)
				{
					occurenceMap[renderedObjectPart.material->descriptorSet]++;
				}
				for (DescriptorSet* descriptorSet : renderedObjects[i]->getRenderPassInfoByRenderPassHandle(renderPass.handle).batchableDescriptorSets)
				{
					occurenceMap[descriptorSet]++;
				}
			}
		}
	}

	void BindingTree::initializePipelineNodes(std::unordered_set<GraphicsPipeline*>& pipelines)
	{
		for (auto it = pipelines.begin(); it != pipelines.end(); it++)
		{
			pipelineNodes[*it] = BindingNode(*it);
		}
	}

	void BindingTree::initializeBindingNodes(std::vector<RenderedObject*>& renderedObjects, RenderPass& renderPass)
	{	
		std::vector<std::pair<Bindable*, int>> objectBindables;
		GreaterPredicat greaterPredicat;
		RenderedObjectCommandRecorder commandRecorder;
		std::pair<Bindable*, int> materialDescriptorSet;

		for (int i = 0; i < renderedObjects.size(); i++)
		{			
			objectBindables.clear();
			
			objectBindables.push_back(*occurenceMap.find(renderedObjects[i]->meshRenderer));

			for (DescriptorSet* descriptorSet : renderedObjects[i]->getRenderPassInfoByRenderPassHandle(renderPass.handle).batchableDescriptorSets)
			{
				objectBindables.push_back(*occurenceMap.find(descriptorSet));
			}

			std::sort(objectBindables.begin(), objectBindables.end(), greaterPredicat);

			commandRecorder.renderedObject = renderedObjects[i];

			for (RenderedObjectPart& renderedObjectPart : renderedObjects[i]->getRenderPassInfoByRenderPassHandle(renderPass.handle).renderedObjectParts)
			{
				commandRecorder.renderedObjectPart = &renderedObjectPart;
				
				std::vector<std::pair<Bindable*, int>>::iterator materialPosition;
				if (renderedObjectPart.material != nullptr)
				{
					materialDescriptorSet = *occurenceMap.find(renderedObjectPart.material->descriptorSet);
					materialPosition = std::upper_bound(objectBindables.begin(), objectBindables.end(), materialDescriptorSet, greaterPredicat);
					materialPosition = objectBindables.insert(materialPosition, materialDescriptorSet);
				}

				pipelineNodes.at(renderedObjectPart.graphicsPipeline).addRenderedObjectPart(objectBindables, 0, commandRecorder);

				if (renderedObjectPart.material != nullptr)
				{
					objectBindables.erase(materialPosition);
				}
			}
		}
	}
}

