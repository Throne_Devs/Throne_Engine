#pragma once
#include "stdafx.h"

#include "GpuBuffer.h"
#include "CommandBuffer.h"
#include "UtilityFunctions.hpp"
#include "Fence.h"
#include "ThroneFramework.h"

namespace ThroneEngine
{
	GpuBuffer::GpuBuffer()
	{
	}


	GpuBuffer::~GpuBuffer()
	{
	}

	bool GpuBuffer::initialize(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage)
	{
		VkBufferCreateInfo createInfos = {};
		createInfos.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		createInfos.flags = 0;
		createInfos.pNext = nullptr;
		createInfos.size = sizeInBytes;
		createInfos.usage = usage;
		createInfos.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfos.queueFamilyIndexCount = 0;
		createInfos.pQueueFamilyIndices = nullptr;

		VkResult result = vkCreateBuffer(logicalDevice, &createInfos, nullptr, &handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createBuffer);
			#endif
			return false;
		}
		#endif

		currentBufferSizeInBytes = sizeInBytes;

		return true;
	}

	void GpuBuffer::destroy()
	{
		//if (mappedMemory != nullptr)
		//{
		//	vkUnmapMemory(logicalDevice, memoryObject);
		//}

		destroyBufferView();

		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyBuffer(logicalDevice, handle, nullptr);
			handle = VK_NULL_HANDLE;
		}
	}

	void GpuBuffer::destroyBufferView()
	{
		if (handle != VK_NULL_HANDLE)
		{
			vkDestroyBufferView(logicalDevice, viewHandle, nullptr);
			viewHandle = VK_NULL_HANDLE;
		}
	}

	bool GpuBuffer::allocateMemory(VkMemoryPropertyFlags properties)
	{
		return allocateMemory(properties, memoryObject);
	}

	bool GpuBuffer::allocateMemory(VkMemoryPropertyFlags properties, VkDeviceMemory& memoryObject)
	{
		VkPhysicalDeviceMemoryProperties memoryProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);
		VkMemoryRequirements memoryRequirements;
		vkGetBufferMemoryRequirements(logicalDevice, handle, &memoryRequirements);

		for (int i = 0; i < memoryProperties.memoryTypeCount; i++)
		{
			if ((memoryRequirements.memoryTypeBits & (1 << i)) && ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties))
			{
				VkMemoryAllocateInfo bufferMemoryAllocateInfo = {};
				bufferMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				bufferMemoryAllocateInfo.pNext = nullptr;
				bufferMemoryAllocateInfo.allocationSize = memoryRequirements.size;
				bufferMemoryAllocateInfo.memoryTypeIndex = i;

				VkResult result = vkAllocateMemory(logicalDevice, &bufferMemoryAllocateInfo, nullptr, &memoryObject);
				#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
				if (result != VK_SUCCESS)
				{
					#if ERROR_MESSAGE_NEEDED
					ERROR_MESSAGE(err_allocateBufferMemory);
					#endif
					return false;
				}
				#endif
				break;
			}
		}

		if (memoryObject != VK_NULL_HANDLE)
		{
			VkResult result = vkBindBufferMemory(logicalDevice, handle, memoryObject, 0);
			#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
			if (result != VK_SUCCESS)
			{
				#if ERROR_MESSAGE_NEEDED
				ERROR_MESSAGE(err_bindBufferMemory);
				#endif
				return false;
			}
			#endif
		}
		else
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_nullBufferMemoryObject);
			#endif
			return false;
		}

		this->memoryProperties = properties;

		return true;
	}

	bool GpuBuffer::createView(VkFormat format, VkDeviceSize memoryOffset, VkDeviceSize memorySize)
	{
		VkBufferViewCreateInfo bufferViewCreateInfos = {};
		bufferViewCreateInfos.sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
		bufferViewCreateInfos.pNext = nullptr;
		bufferViewCreateInfos.flags = 0;
		bufferViewCreateInfos.buffer = handle;
		bufferViewCreateInfos.format = format;
		bufferViewCreateInfos.offset = memoryOffset;
		bufferViewCreateInfos.range = memorySize;

		VkResult result = vkCreateBufferView(logicalDevice, &bufferViewCreateInfos, nullptr, &viewHandle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_createBufferView);
			#endif
			return false;
		}
		#endif

		return true;
	}

	bool GpuBuffer::mapHostVisibleMemory(VkDeviceSize offset, VkDeviceSize dataSize, void* data, bool unMapWhenFinished)
	{
		return mapHostVisibleMemory(memoryObject, offset, dataSize, data, unMapWhenFinished);
	}

	bool GpuBuffer::mapHostVisibleMemory(VkDeviceMemory& memoryObject, VkDeviceSize offset, VkDeviceSize dataSize, void* data, bool unMapWhenFinished)
	{
		VkResult result = vkMapMemory(logicalDevice, memoryObject, offset, dataSize, 0, &mappedMemory);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_mapMemory);
			#endif
			return false;
		}
		#endif
		memcpy(reinterpret_cast<byte*>(mappedMemory) + offset, data, dataSize);

		memoryRange = {};
		memoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
		memoryRange.pNext = nullptr;
		memoryRange.memory = memoryObject;
		memoryRange.offset = offset;
		//memoryRange.size = dataSize;
		memoryRange.size = VK_WHOLE_SIZE;
		//Can also be a vector of memory range

		result = vkFlushMappedMemoryRanges(logicalDevice, 1, &memoryRange);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_flushMappedMemory);
			#endif
			return false;
		}
		#endif

		if (unMapWhenFinished == true)
		{
			vkUnmapMemory(logicalDevice, memoryObject);
		}

		this->memoryObject = memoryObject;

		return true;
	}

	bool GpuBuffer::updateHostVisibleMemory(VkDeviceSize offset, VkDeviceSize dataSize, void * data)
	{
		memcpy(reinterpret_cast<byte*>(mappedMemory) + offset, data, dataSize);

		memoryRange.offset = offset;
		//memoryRange.size = dataSize;
		memoryRange.size = VK_WHOLE_SIZE;

		VkResult result = vkFlushMappedMemoryRanges(logicalDevice, 1, &memoryRange);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_flushMappedMemory);
			#endif
			return false;
		}
		#endif
		return true;
	}

	void GpuBuffer::copyDataFromBufferToBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, std::vector<VkBufferCopy>& regions)
	{
		copyDataFromBufferToBuffer(commandBuffer, srcBuffer, &regions[0], regions.size());
	}

	void GpuBuffer::copyDataFromBufferToBuffer(CommandBuffer& commandBuffer, VkBuffer srcBuffer, VkBufferCopy* regions, uint32 numberOfRegion)
	{
		commandBuffer.cmdCopyBuffer(srcBuffer, handle, numberOfRegion, regions);
	}

	void GpuBuffer::setMemoryBarrier(
		CommandBuffer& commandBuffer,
		std::vector<VkBufferMemoryBarrier>& bufferBarriers,
		VkPipelineStageFlags currentPipelineStage,
		VkPipelineStageFlags newPipelineStage)
	{
		setMemoryBarrier(commandBuffer, getVectorDataPointer(bufferBarriers), bufferBarriers.size(), currentPipelineStage, newPipelineStage);
	}

	void GpuBuffer::setMemoryBarrier(
		CommandBuffer& commandBuffer,
		VkBufferMemoryBarrier* bufferBarrier,
		uint32 numberOfBarrier,
		VkPipelineStageFlags currentPipelineStage,
		VkPipelineStageFlags newPipelineStage)
	{
		for (int i = 0; i < numberOfBarrier; i++)
		{
			bufferBarrier[i].sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
			bufferBarrier[i].pNext = nullptr;
			bufferBarrier[i].offset = 0;
			bufferBarrier[i].size = VK_WHOLE_SIZE;
		}
		commandBuffer.cmdPipelineBarrier(currentPipelineStage, newPipelineStage, 0, 0, nullptr, numberOfBarrier, bufferBarrier, 0, nullptr);
	}

	bool GpuBuffer::updateWithDeviceLocalMemory(
		CommandBuffer& commandBuffer,
		void* data,
		VkDeviceSize dataSize,
		VkDeviceSize dstOffset,
		VkAccessFlags dstCurrentAccess,
		VkAccessFlags dstNewAccess,
		VkPipelineStageFlags dstCurrentStage,
		VkPipelineStageFlags dstNewStage,
		CommandQueue& queue,
		SemaphorePool& semaphoresToSignal)
	{
		GpuBuffer stagingBuffer;
		if (stagingBuffer.initialize(dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT) == false)
		{
			return false;
		}

		VkDeviceMemory memoryObject;
		if (stagingBuffer.allocateMemory(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memoryObject) == false)
		{
			return false;
		}
		if (stagingBuffer.mapHostVisibleMemory(memoryObject, 0, dataSize, data, true) == false)
		{
			return false;
		}

		if (commandBuffer.beginRecording(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr) == false)
		{
			return false;
		}

		VkBufferMemoryBarrier bufferBarrier = {};
		bufferBarrier.buffer = handle;
		bufferBarrier.srcAccessMask = dstCurrentAccess;
		bufferBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		bufferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		bufferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		setMemoryBarrier(commandBuffer, &bufferBarrier, 1, dstCurrentStage, VK_PIPELINE_STAGE_TRANSFER_BIT);

		VkBufferCopy region = {};
		region.dstOffset = dstOffset;
		region.size = dataSize;
		region.srcOffset = 0;
		copyDataFromBufferToBuffer(commandBuffer, stagingBuffer.handle, &region, 1);


		bufferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		bufferBarrier.dstAccessMask = dstNewAccess;
		setMemoryBarrier(commandBuffer, &bufferBarrier, 1, VK_PIPELINE_STAGE_TRANSFER_BIT, dstNewStage);

		if (commandBuffer.endRecording() == false)
		{
			return false;
		}

		Fence fence;
		if (fence.initialize() == false)
		{
			return false;
		}
		fence.fenceInUse = true;

		WaitSemaphoreInfo waitSemaphoresInfos = {};
		if (queue.submitCommandBuffer(waitSemaphoresInfos, commandBuffer, semaphoresToSignal, fence) == false)
		{
			return false;
		}

		if (fence.waitForFence(TIMEOUT_FENCE) == false)
		{
			return false;
		}

		//destroyBuffer(); // TODO : Add the detroys when its done
		//freeMemoryObject();
		return true;
	}
}