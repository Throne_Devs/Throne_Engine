#include "stdafx.h"

#include "RenderedObject.h"

namespace ThroneEngine
{
	RenderedObject::RenderedObject()
	{
	}


	RenderedObject::~RenderedObject()
	{
	}

	bool RenderedObject::initialize(MeshRenderer* meshRenderer, Material* material, GraphicsPipeline* graphicsPipeline, 
		RenderPass* renderPasses, uint32 modelMatrixBindingIndex, glm::vec3& position, glm::vec3& rotation, glm::vec3& scale)
	{
		this->meshRenderer = meshRenderer;
		
		std::deque<RenderedObjectPart> rendererObjectParts;
		rendererObjectParts.emplace_back(meshRenderer->mesh->parts[0], material, graphicsPipeline);

		renderPassesInfo.push_back(RenderedObjectRenderPassInfo(rendererObjectParts, renderPasses));

		if (initializeModelMatrixBuffer(position, rotation, scale, modelMatrixBindingIndex) == false)
		{
			return false;
		}

		return true;
	}
	bool RenderedObject::initialize(MeshRenderer* meshRenderer, std::deque<RenderedObjectRenderPassInfo>& renderPassesInfo, uint32 modelMatrixBindingIndex,
		glm::vec3& position, glm::vec3& rotation, glm::vec3& scale)
	{
		this->meshRenderer = meshRenderer;

		this->renderPassesInfo = renderPassesInfo;

		if (initializeModelMatrixBuffer(position, rotation, scale, modelMatrixBindingIndex) == false)
		{
			return false;
		}

		return true;
	}

	bool RenderedObject::initializeModelMatrixBuffer(vec3& position, vec3& rotation, vec3& scale, uint32 modelMatrixBindingIndex)
	{
		modelMatrix = Transform(position, rotation, scale);

		if (modelMatrixBuffer.initialize(sizeof(mat4), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, modelMatrixBindingIndex) == false)
		{
			return false;
		}
		if (modelMatrixBuffer.allocateMemory(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == false)
		{
			return false;
		}
		if (modelMatrixBuffer.mapHostVisibleMemory(0, sizeof(mat4), reinterpret_cast<float*>(&modelMatrix), false) == false)
		{
			return false;
		}
		return true;
	}
}


