#pragma once
#include "stdafx.h"

#include "VulkanFunctions.h"
#include "Descriptor.h"
#include "Image.h"
#include "CommandQueue.h"
#include "PixelMap.h"
#include "UtilityFunctions.hpp"

namespace ThroneEngine
{
	class ImageSampler : public Descriptor
	{
	public:
		ImageSampler();
		~ImageSampler();

		Image* image;
		VkSampler samplerHandle = VK_NULL_HANDLE;
		bool dedicatedImage = false;

		bool initialize(std::string imagePath, CommandQueue& queue);
		bool initialize(Image* image, bool dedicatedImage);
		bool initialize(VkExtent3D& imageDimensions, VkImageUsageFlags usage, VkImageAspectFlags imageAspect, VkFormat imageFormat, uint32 numberOfLayer, VkImageViewType viewType);
		bool initialize(PixelMap& imageData, CommandQueue& queue);
		void destroy();

		bool initializeAsCubeMap(std::vector<std::string> imagePaths, CommandQueue& queue);

		void initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding) override;

		VkExtent3D getSize() const;

		static VkDescriptorSetLayoutBinding getLayoutBinding(VkShaderStageFlags shaderStage = VK_SHADER_STAGE_FRAGMENT_BIT, uint32 descriptorCount = 1);

	private:
		bool initializeSampler();
		bool initializeSampler(VkFilter minFilter, VkFilter magFilter, VkSamplerMipmapMode mipmapMode, VkSamplerAddressMode adressMode, float mipLoadBias, bool anisotropyEnabled, float maxAnisotropy, bool compareEnable, VkCompareOp compareFunction, float minLod, float maxLod, VkBorderColor borderColor, bool unnormalizedCoordinates);

		virtual void setDescriptorType() override;	
	};
}