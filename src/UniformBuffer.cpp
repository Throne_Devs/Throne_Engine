#include "stdafx.h"

#include "UniformBuffer.h"

namespace ThroneEngine
{

	UniformBuffer::UniformBuffer()
	{
	}


	UniformBuffer::~UniformBuffer()
	{

	}

	bool UniformBuffer::initializeAsUniformBuffer(VkDeviceSize sizeInBytes, VkShaderStageFlags shaderStage, VkMemoryPropertyFlags memoryProperties, VkBufferCreateFlags additionalUsage)
	{
		return createUniformBuffer(sizeInBytes, additionalUsage | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, memoryProperties, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, shaderStage, *this);
	}

	bool UniformBuffer::initializeAsStorageBuffer(VkDeviceSize sizeInBytes, VkShaderStageFlags shaderStage, VkMemoryPropertyFlags memoryProperties, VkBufferCreateFlags additionalUsage)
	{
		return createUniformBuffer(sizeInBytes, additionalUsage | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, memoryProperties, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, shaderStage, *this);
	}

	void UniformBuffer::initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding)
	{
		initializeGenericWriteDescriptorSet(descriptorSetHandle, binding);

		VkDescriptorBufferInfo* bufferInfos = new VkDescriptorBufferInfo();
		bufferInfos->buffer = handle;
		bufferInfos->offset = 0;
		bufferInfos->range = VK_WHOLE_SIZE;

		if (writeDescriptorSet.pBufferInfo != nullptr)
		{
			delete writeDescriptorSet.pBufferInfo;
		}

		writeDescriptorSet.pImageInfo = nullptr;
		writeDescriptorSet.pBufferInfo = bufferInfos;
		writeDescriptorSet.pTexelBufferView = nullptr;

		writeDescriptorSet.descriptorType = descriptorType;
		writeDescriptorSet.descriptorCount = descriptorCount;
	}

	VkDescriptorSetLayoutBinding UniformBuffer::getLayoutBinding(VkShaderStageFlags shaderStage)
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1; // for arrays of descriptor (example arrays of imageSampler)
		binding.stageFlags = shaderStage;
		binding.pImmutableSamplers = nullptr;
		return binding;
	}

	inline bool UniformBuffer::createUniformBuffer(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage, VkMemoryPropertyFlags memoryProperties, VkDescriptorType descriptorType, VkShaderStageFlags shaderStage, UniformBuffer& buffer)
	{
		buffer.descriptorType = descriptorType;
		buffer.descriptorShaderStage = shaderStage;
		if (buffer.initialize(sizeInBytes, usage) == false)
		{
			return false;
		}

		if (buffer.allocateMemory(memoryProperties) == false)
		{
			return false;
		}

		return true;
	}

	void UniformBuffer::setDescriptorType()
	{
		descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	}

}