#include "stdafx.h"

#include "Clock.h"

#if defined PLATFORM_WINDOWS

namespace
{
	LARGE_INTEGER getFrequency()
	{
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		return frequency;
	}

	Time getCurrentTime()
	{
		static LARGE_INTEGER frequency = getFrequency();
		LARGE_INTEGER time;
		QueryPerformanceCounter(&time);
		return microseconds(1000000 * time.QuadPart / frequency.QuadPart);
	}
}

namespace ThroneEngine
{
	Clock::Clock() : startTime(getCurrentTime())
	{
	}

	Time Clock::getElapsedTime() const
	{
		return getCurrentTime() - startTime;
	}

	Time Clock::restart()
	{
		const Time now = getCurrentTime();
		const Time elapsed = now - startTime;
		startTime = now;
		return elapsed;
	}

	void Clock::printToConsoleElapsedTimeMs(std::string& message)
	{
		std::cout << message << getElapsedTime().asMilliseconds() << "\n";
	}

	void Clock::printToConsoleElapsedTimeMs(const char * message)
	{
		std::cout << message << getElapsedTime().asMilliseconds() << "\n";
	}

	void Clock::printToConsoleElapsedTimeMicroS(std::string & message)
	{
		std::cout << message << getElapsedTime().asMicroseconds() << "\n";
	}

	void Clock::printToConsoleElapsedTimeMicroS(const char * message)
	{
		std::cout << message << getElapsedTime().asMicroseconds() << "\n";
	}
}
#endif