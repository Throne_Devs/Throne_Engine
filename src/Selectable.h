#pragma once
#include "stdafx.h"

#include "SpriteState.h"
#include "ColorState.h"
#include "Graphic.h"
#include "Event.h"

namespace ThroneEngine
{
	enum Transition
	{
		None = 0,
		ColorTint = 1,
		Sprites = 2,
		Animation = 3
	};

	enum SelectionState
	{
		Normal = 0,
		Highlighted = 1,
		Pressed = 2,
		Disabled = 3,
	};

	class Selectable
	{
	public:
		Selectable();
		virtual ~Selectable();

		virtual void initializeObject();
		virtual void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex);
		virtual void initialize(RenderPass* renderPass, GraphicsPipeline* graphicsPipeline, DescriptorSet& descriptorSet, std::deque<Material*> materials, 
			uint32 meshRendererBindingIndex, uint32 modelMatrixBindingIndex);

		bool isInteractable = true;
		Graphic targetGraphic;
		SpriteState spriteState;
		ColorState colorState;
		Transition transition = None;

	protected:
		FunctionHandle<void(const vec2&)> onMouseMoveHandle;

		void mouseMove(const vec2& mousePos);

		SelectionState selectionState = Normal;
		bool isPressed = false;
		bool isHighlighted = false;

	};
}
