#pragma once
#include "stdafx.h"

#include "File.h"
#include "Glyph.h"
#include "PixelMap.h"
#include "ImageSampler.h"

namespace ThroneEngine
{

	class Text;

	class Font
	{
	public:
		struct Info { std::string family; };

		Font();
		Font(const Font& copy);
		~Font();

		bool loadFromFile(const std::string& filePath);
		const Info& getInfo() const;
		const Glyph& getGlyph(uint32 codePoint, uint32 characterSize, bool bold, float outlineThickness = 0) const;
		float getKerning(uint32 first, uint32 second, uint32 characterSize) const;
		float getLineSpacing(uint32 characterSize) const;
		float getUnderLinePosition(uint32 characterSize) const;
		float getUnderLineThickness(uint32 characterSize) const;
		ImageSampler* getTexture(uint32 characterSize) const;
		PixelMap& getPixelMap(uint32 characterSize) const;
		bool updateTexture(std::vector<Fence>& imageFences) const;
		void registerTextureResizeEvent(Text* text, uint32 characterSize) const;
		void removeTextureResizeEvent(Text* text, uint32 characterSize) const;

		Font& operator =(const Font& right);

	private:
		struct Row
		{
			Row(const uint32 rowTop) : width(0), top(rowTop) {}

			uint32 width;
			uint32 top;
		};

		typedef std::map<uint64, Glyph> GlyphTable;

		struct Page
		{
			Page();

			GlyphTable glyphs;
			PixelMap pixelMap;
			ImageSampler* imageSampler = nullptr;
			bool textureNeedUpdate = false;
			uint32 nextRow;
			std::vector<Row> rows;
			int index = 0;
			std::vector<Text*> textureResizeEvent;
		};

		void cleanup();
		Glyph loadGlyph(uint32 codePoint, uint32 characterSize, bool bold, float outlineThickness) const;
		IntRect findGlyphRect(Page& page, uint32 width, uint32 height) const;
		bool setCurrentSize(uint32 characterSize) const;

		const int PIXEL_MAP_WIDTH_PADDING = 100;
		const int BUFFER_DATA_WIDTH = 4;
		const int NUMBER_ROWS = 16;
		const int PADDING = 1;
		const float UNDERLINE_PADDING_DIVIDE = 10.f;
		const float UNDERLINE_THICKNESS_DIVIDE = 14.f;
		const float LEFT_SHIFT = static_cast<float>(1 << 6);

		void* library;
		void* face;
		void* streamRec;
		void* stroker;
		int* refCount;
		Info info;
		mutable std::map<uint32, Page> pages;
		mutable std::vector<uint8> pixelBuffer;
	};
}
