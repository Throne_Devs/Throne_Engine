#pragma once
#include "stdafx.h"

#include "BindingNode.h"

namespace ThroneEngine
{
	class BindingTree
	{
	public:
		BindingTree();
		~BindingTree();

		std::unordered_map<GraphicsPipeline*, BindingNode> pipelineNodes;
		

		void initializeTree(std::vector<RenderedObject*>& renderedObjects, RenderPass& renderPass);

		void recordCommands(CommandBuffer& commandBuffer);
		

	private:
		std::unordered_map<Bindable*, int> occurenceMap;
		void fillOccurenceMap(std::vector<RenderedObject*>& renderedObjects, std::unordered_set<GraphicsPipeline*>& pipelines,
			RenderPass& renderPass);
		void initializePipelineNodes(std::unordered_set<GraphicsPipeline*>& pipelines);
		void initializeBindingNodes(std::vector<RenderedObject*>& renderedObjects, RenderPass& renderPass);
	};
}



