#pragma once
#include "stdafx.h"

#include "Rect.h"
#include <ft2build.h>
#include FT_GLYPH_H

namespace ThroneEngine
{
	class Glyph
	{
	public:

		Glyph() : advance(0) {}

		float advance;
		FloatRect bounds;
		IntRect textureRect;
		FT_Glyph_Metrics metrics;
	};
}
