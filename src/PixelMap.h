#pragma once
#include "stdafx.h"

#include "Color.h"

namespace ThroneEngine
{
	class PixelMap
	{
	public:
		PixelMap();
		~PixelMap();

		bool pixelMapResized = true;
		std::vector<byte> pixels;
		tvec3<uint32> dimensionsInPixel;
		uint32 numberOfComponent = 4;

		void initialize(unsigned int width, unsigned int height, const Color& color = Color(255, 255, 255));
		void setPixel(unsigned int x, unsigned int y, const Color& color);

		void update(const PixelMap& pixelMap);
		void update(const std::vector<uint8>& pixels, unsigned int w, unsigned int h, unsigned int x, unsigned int y);
		void swap(PixelMap& pixelMap) noexcept;
		void resize(unsigned int newWidth, unsigned int newHeight);

		bool saveToFile(const std::string& filePath);

		tvec3<uint32> getSize() const;
		static int getMaximumSize();



	};
}
