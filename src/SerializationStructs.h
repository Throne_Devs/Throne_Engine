#pragma once
#include "stdafx.h"

#include "Vertex.h"

namespace ThroneEngine
{
	#define serialize_ \
	friend class boost::serialization::access; \
	template <typename Archive> \
	void serialize(Archive& ar, const unsigned int version)

	//SerializationStruct

	//RenderedObject
	//Renderer -> probablement vector des objectID de renderedObjects 

	struct MaterialSerializationStruct
	{
		uint64 objectID;

		uint64 imageSamplerObjectID; 
		float shineDamper;
		float reflectivity;
		uint32 descriptorSetLocation;

		serialize_
		{
			ar & objectID;

			ar & imageSamplerObjectID;
			ar & shineDamper;
			ar & reflectivity;
			ar & descriptorSetLocation;
		}
	};

	struct DescriptorSetSerializationStruct
	{
		uint64 objectID;

		std::vector<uint64> descriptorsObjectsID;
		uint64 descriptorSetLayoutObjectID;
		uint32 descriptorSetLocation;

		serialize_
		{
			ar & objectID;

			ar & descriptorsObjectsID;
			ar & descriptorSetLayoutObjectID;
			ar & descriptorSetLocation;
		}
	};

	struct GraphicsPipelineSerializationStruct
	{
		uint64 objectID;

		VkPipelineCreateFlags additionalOptions;
		uint64 renderPassObjectID;
		uint32 subpass; 
		uint64 basePipelineObjectID;

		std::vector<uint64> shaders;

		std::vector<uint64> descriptorSetLayoutsObjectID;
		std::vector<VkPushConstantRange> pushConstantRanges;

		VkPrimitiveTopology primitiveType;
		bool primitiveRestartEnable;

		VkViewport viewport;
		VkRect2D scissor;

		GraphicsPipelineRasterizerSerializationStruct rasterizerSerializationStruct;
		GraphicsPipelineMultiSampleSerializationStruct multisampleSerializationStruct;
		GraphicsPipelineDepthStencilSerializationStruct depthStencilSerializationStruct;
		GraphicsPipelineBlendlSerializationStruct blendSerializationStruct;

		serialize_
		{
			ar & objectID;

			ar & additionalOptions;
			ar & renderPassObjectID;
			ar & subpass;
			ar & basePipelineObjectID;

			ar & shaders;

			ar & descriptorSetLayoutsObjectID;
			ar & pushConstantRanges;

			ar & primitiveType;
			ar & primitiveRestartEnable;

			ar & viewport;
			ar & scissor;

			ar & rasterizerSerializationStruct;
			ar & multisampleSerializationStruct;
			ar & depthStencilSerializationStruct;
			ar & blendSerializationStruct;
		}
	};

	struct GraphicsPipelineRasterizerSerializationStruct
	{
		bool depthClampEnable;
		bool rasterizerDiscardEnable;
		bool depthBiasEnable;
		float depthBiasConstantFactor;
		float depthBiasClamp;
		float depthBiasSlopeFactor;
		float lineWidth;
		VkPolygonMode polygonMode;
		VkCullModeFlags cullingMode;
		VkFrontFace frontFace;

		serialize_
		{
			ar & depthClampEnable;
			ar & rasterizerDiscardEnable;
			ar & depthBiasEnable;
			ar & depthBiasConstantFactor;
			ar & depthBiasClamp;
			ar & depthBiasSlopeFactor;
			ar & lineWidth;
			ar & polygonMode;
			ar & cullingMode;
			ar & frontFace;
		}
	};

	struct GraphicsPipelineMultiSampleSerializationStruct
	{
		VkSampleCountFlagBits sampleCount;
		bool perSampleShadingEnable;
		float minSampleShading;
		VkSampleMask sampleMasks;
		bool alphaToCoverageEnable;
		bool alphaToOneEnable;

		serialize_
		{
			ar & sampleCount;
			ar & perSampleShadingEnable;
			ar & minSampleShading;
			ar & sampleMasks;
			ar & alphaToCoverageEnable;
			ar & alphaToOneEnable;
		}
	};

	struct GraphicsPipelineDepthStencilSerializationStruct
	{
		bool depthTestEnable;
		bool depthWriteEnable;
		VkCompareOp depthCompareOp;
		bool depthBoundTestEnable;
		float minDepthBounds;
		float maxDepthBounds;
		bool stencilTestEnable;
		VkStencilOpState frontStencilTestParameters;
		VkStencilOpState backStencilTestParameters;

		serialize_
		{
			ar & depthTestEnable;
			ar & depthWriteEnable;
			ar & depthCompareOp;
			ar & depthBoundTestEnable;
			ar & minDepthBounds;
			ar & maxDepthBounds;
			ar & stencilTestEnable;
			ar & frontStencilTestParameters;
			ar & backStencilTestParameters;
		}
	};

	struct GraphicsPipelineBlendlSerializationStruct
	{
		bool blendLogicOpEnable;
		VkLogicOp blendLogicOp;
		std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates;
		std::array<float, 4> blendConstants;

		serialize_
		{
			ar & blendLogicOpEnable;
			ar & blendLogicOp;
			ar & attachmentBlendStates;
			ar & blendConstants;
		}
	};

	struct ShaderSerializationStruct
	{
		uint64 objectID;

		std::string shaderFilePath;
		VkShaderStageFlagBits shaderStage;
		std::string entryPointName;
		std::vector<VkVertexInputAttributeDescription> attributesDescriptions; // Only used for shaders with a vertex input
		std::vector<VkVertexInputBindingDescription> inputBindingDescriptions; // Only used for shaders with a vertex input

		serialize_
		{
			ar & objectID;

			ar & shaderFilePath;
			ar & shaderStage;
			ar & entryPointName;

			if (shaderStage & VK_SHADER_STAGE_VERTEX_BIT)
			{
				ar & attributesDescriptions;
				ar & inputBindingDescriptions;
			}
		}
	};

	struct FrameBufferSerializationStruct
	{
		uint64 objectID;

		uint64 renderPassObjectID;
		uint64 colorAttachmentObjectID = 0;
		VkExtent2D bufferDimensions;
		bool useSwapchainColorAttachment = true;
		bool useDepthAttachment = true;
		uint64 depthAttachmentObjectID = 0;
		uint32 layers;

		serialize_
		{
			ar & objectID;
			ar & renderPassObjectID;
			ar & colorAttachmentObjectID;
			ar & bufferDimensions;
			ar & useSwapchainColorAttachment;
			ar & useDepthAttachment;
			ar & depthAttachmentObjectID;
			ar & layers;
		}
	};

	struct RenderPassSerializationStruct
	{
		uint64 objectID;

		std::vector<VkAttachmentDescription> attachmentsDescription;
		std::vector<SubpassParameters> subpassParameters;
		std::vector<VkSubpassDependency> subpassDependencies;

		serialize_
		{
			ar & objectID;
			ar & attachmentsDescription;
			ar & subpassParameters;
			ar & subpassDependencies;
		}
	};

	struct MeshRendererSerializationStruct
	{
		uint64 objectID;

		VkMemoryPropertyFlags memoryProperties;
		uint64 meshObjectID;

		serialize_
		{
			ar & objectID;
			ar & memoryProperties;
			ar & meshObjectID;
		}
	};

	struct MeshSerializationStruct
	{
		uint64 objectID;

		std::string meshPath;
		uint32 vertexSizeInBytes;
		VertexFormat vertexFormat;

		serialize_
		{
			ar & objectID;
			ar & meshPath;
			ar & vertexSizeInBytes;
			ar & vertexFormat;
		}
	};

	struct DescriptorSetLayoutSerializationStruct
	{
		uint64 objectID;

		std::vector<VkDescriptorSetLayoutBinding> bindings;

		serialize_
		{
			ar & objectID;
			ar & bindings;
		}
	};

	struct UniformBufferSerializationStruct
	{
		uint64 objectID;

		VkDeviceSize sizeInBytes;
		VkShaderStageFlags shaderStage;
		VkMemoryPropertyFlags memoryProperties;
		VkBufferCreateFlags additionalUsage = 0;

		serialize_
		{
			ar & objectID;

			ar & sizeInBytes;
			ar & shaderStage;
			ar & memoryProperties;
			ar & additionalUsage;
		}
	};

	struct ImageSamplerSerializationStruct
	{
		uint64 objectID;

		uint64 imageObjectID;
		VkFilter minFilter;
		VkFilter magFilter;
		VkSamplerMipmapMode mipmapMode;
		VkSamplerAddressMode adressMode;
		float mipLoadBias;
		bool anisotropyEnabled;
		float maxAnisotropy;
		bool compareEnable;
		VkCompareOp compareFunction;
		float minLod;
		float maxLod;
		VkBorderColor borderColor;
		bool unnormalizedCoordinates;

		serialize_
		{
			ar & objectID;
			ar & imageObjectID;
			ar & minFilter;
			ar & magFilter;
			ar & mipmapMode;
			ar & adressMode;
			ar & mipLoadBias;
			ar & anisotropyEnabled;
			ar & maxAnisotropy;
			ar & compareEnable;
			ar & compareFunction;
			ar & minLod;
			ar & maxLod;
			ar & borderColor;
			ar & unnormalizedCoordinates;
		}
	};

	struct ImageSerializationStruct
	{
		uint64 objectID;

		VkFormat format;
		VkImageUsageFlags usage;
		VkImageType type;
		VkFormat format;
		bool loadFromFile;
		std::vector<std::string> imageFilePaths;
		VkExtent3D dimensions;
		uint32 numMipmaps;
		uint32 numLayers;
		VkSampleCountFlagBits samples;
		VkImageUsageFlags usageScenarios;
		bool isCubemap;
		VkImageLayout imageLayout;
		bool hasImageView;
		VkImageViewType imageViewType;

		serialize_
		{
			ar & objectID;
			ar & format;
			ar & usage;
			ar & type;
			ar & format;
			ar & loadFromFile;
			ar & imageFilePaths;
			ar & dimensions;
			ar & numMipmaps;
			ar & numLayers;
			ar & samples;
			ar & usageScenarios;
			ar & isCubemap;
			ar & imageLayout;
			ar & hasImageView;
			ar & imageViewType;
		}
	};
}