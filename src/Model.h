#pragma once
#include "stdafx.h"

#include "Mesh.h"

namespace ThroneEngine
{
	class Model
	{
	public:
		static bool loadModel(const std::string& filename, Mesh* mesh, const bool loadNormals = false, const bool loadTexCoords = false, const bool loadColor = false);
	};
}
