#pragma once
#include "stdafx.h"

#include "Scene.h"
#include "GraphicsPipeline.h"
#include "Image.h"
#include "Camera.h"
#include "UniformBuffer.h"
#include "RenderedObject.h"
#include "Renderer.h"
#include "RenderPass.h"
#include "FrameBuffer.h"
#include "Font.h"
#include "Text.h"
#include "DescriptorSetLayout.h"
#include "ImageSamplerArray.h"
#include "Button.h"
#include "Checkbox.h"
#include "Slider.h"

namespace ThroneEngine
{
	class Clock;

	class SceneTest1 : Scene
	{
	public:
		SceneTest1();
		~SceneTest1();

		virtual bool initializeScene(Window* window, CommandQueue& commandQueue) override;
		virtual bool windowResizeEvent() override;
		virtual void update(float deltaTime) override;
		virtual void draw() override;

		Semaphore renderFinishedSemaphore;

		RenderPass sceneRenderPass;
		GraphicsPipeline sceneGraphicsPipeline;
		GraphicsPipeline skyboxGraphicsPipeline;
		std::vector<FrameBuffer> sceneFrameBuffers;

		VkPresentModeKHR desiredPresentMode;
		VkPresentModeKHR presentMode;

		Image depthImage;

		WaitSemaphoreInfo beginRenderPassSemaphoreInfos;
		VkPresentInfoKHR presentInfo;


		CommandPool cmdBuffersRenderPass;
		std::vector<CommandPool> secondaryCmdBuffersRenderPass;

		Camera* mainCamera;
		Camera* camera1;
		Camera* camera2;
		int cameraIndexUsed = 0;
		glm::mat4 projectionMatrix;
		glm::mat4 lightProjectionMatrix;
		glm::mat4 viewMatrix;
		UniformBuffer viewProjectionUniformBuffer;
		UniformBuffer uiViewProjectionUniformBuffer;


		int amountOfBox = 1;
		int spaceBetweenBoxes = 5;
		float boxesRotation = 0;
		std::vector<RenderedObject> boxes;

		RenderedObject skybox;

		RenderedObject terrain;

		RenderedObject plane;

		RenderedObject tree;

		Renderer mainRenderer;

		


		GraphicsPipeline shadowTransparentGraphicsPipeline;
		GraphicsPipeline sceneTransparentGraphicsPipeline;

		Renderer shadowRenderer;
		Camera* sunCamera;
		glm::mat4 lightViewMatrix;
		glm::mat4 orthographicProjectionMatrix;
		std::vector<CommandPool> shadowSecondaryCommandBuffer;
		UniformBuffer lightDataUniformBuffer;
		DirectionalLight sun;
		float lightAngle = 0.0f;
		static const uint32 numberOfCascade = 4;
		static const int basePcfRange = 6;
		glm::mat4 cascadeOrthoProjections[numberOfCascade];
		UniformBuffer pcfDataUniformBuffer;
		glm::vec4* pcfData = nullptr;
		VkViewport cascadesViewports[numberOfCascade] = { VkViewport{ 0, 0, 16384, 16384, 0, 1},
			VkViewport{ 0, 0, 6144, 6144, 0, 1 },
			VkViewport{ 0, 0, 4096, 4096, 0, 1 },
			VkViewport{ 0, 0, 2048, 2048, 0, 1 }
		};
		VkRect2D cascadesScissors[numberOfCascade] = { VkRect2D{ VkOffset2D{0, 0}, VkExtent2D{ 16384, 16384 }},
			VkRect2D{ VkOffset2D{ 0, 0 }, VkExtent2D{ 6144, 6144 } },
			VkRect2D{ VkOffset2D{ 0, 0 }, VkExtent2D{ 4096, 4096 } },
			VkRect2D{ VkOffset2D{ 0, 0 }, VkExtent2D{ 2048, 2048 } }
		};
		//ImageSampler shadowMap;
		ImageSamplerArray shadowMap;
		ImageSampler shadowMapImageSamplers[numberOfCascade];
		RenderPass shadowRenderPass;
		std::vector<FrameBuffer> shadowFrameBuffer;
		GraphicsPipeline shadowGraphicsPipeline;

		VertexBufferSignature defaultSceneRenderingVertexBufferSignature;
		VertexBufferSignature defaultSceneRenderingModelMatrixBufferSignature;

		DescriptorSetLayout globalsDescriptorSetLayout;
		DescriptorSetLayout viewProjectionDescriptorSetLayout;
		DescriptorSetLayout uiProjectionDescriptorSetLayout;

		// Contains the viewProjectionUniformBuffer, the shadowMap and the lightDataUniformBuffer
		DescriptorSet globalsDescriptorSet;

		// Contains the viewProjectionUniformBuffer only 
		DescriptorSet viewProjectionDescriptorSet;

		DescriptorSet uiProjectionDescriptorSet;

		vec4* directions;
		uint64 frameId = 0;
		float frameTime = 0;

		Material* terrainMaterial;
		Material* boxMaterial;
		Material* skyboxMaterial;
		std::vector<Material*> treeMaterials;
		bool* transparentTreeObjectParts;
		bool renderTransparentPartsTree = false;

		Button button;
		Checkbox checkbox;
		Slider slider;
		GraphicsPipeline uiButtonGraphicsPipeline;

		Font arialFont;
		Text testText;
		Text debugText;
		GraphicsPipeline uiTextGraphicsPipeline;
		RenderPass uiRenderPass;
		std::vector<FrameBuffer> uiFrameBuffers;
		VkDescriptorSetLayout uiDescriptorSetLayout;
		Renderer uiRenderer;
		std::vector<CommandPool> secondaryCmdBuffersUiRenderPass;
		std::vector<Fence> commandBuffersFences;
		std::vector<Fence> acquireNextImageFences;

		Clock* testingClock;

		int debugValueUseLightProjMatrix = -1;

		bool setupCommandBuffers(bool initializeCmdBuffers, bool initializeTree);
		bool initializeCommmandBuffers();

		void createRenderPassesAndFramebuffers();
		void createSceneRenderPass();
		void createSceneFramebuffers();
		void createShadowRenderPass();
		void createShadowFramebuffer();
		void createUiRenderPass();
		void createUiFrameBuffers();

		void createMaterials();
		void createGrassTerrainMaterial();
		void createBoxMaterial();
		void createSkyboxMaterial();
		void createTreeMaterials();
		void createTextMaterial();

		void initializeVertexBuffersSignatures();

		bool createGraphicsPipelines();
		bool createSceneGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createSkyboxGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createShadowPassGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createSceneTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createShadowsTransparentGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createUiTextGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);
		bool createUiButtonGraphicsPipeline(std::vector<DescriptorSetLayout>& descriptorSets);


		void initializeDepthImage();
		void initializeShadowMap();

		void waitForDevice(VkDevice logicalDevice);

		void createRenderedObjects();
		void createTreeObject(CommandPool& commandBuffers, int bufferIndex);
		void createBoxObjects(CommandPool& commandBuffers, int bufferIndex);
		void createTerrainObject(CommandPool& commandBuffers, int bufferIndex);
		void createSkyboxObject(CommandPool& commandBuffers, int bufferIndex);
		void createTextObject();
		void createButtonObject();

		void addObjectsToRenderers();
		void addObjectsToMainRenderer();
		void addObjectsToShadowRenderer();
		void addObjectsToUiRenderer();

		void initializeLight(float deltaTime);
		void initializeLightTESTING(glm::vec3 pos, glm::vec3 viewPoint);

		void initializeViewProjectionMatrices();
		void initializeViewProjectionMatricesTESTING(double near_, double far_, float fov, double nearLightFrustrum, double farLightFrustrum, double fovLightFrustrum);
		void initializeViewProjectionMatrices(double near_, double far_, float fov);
		void initializeUiViewProjectionMatrices();

		void switchPlayerCamera();

		void initializeViewprojectionMatricesCascade();
		void initializeShaderPcfData();


		void setViewProjectionUniformBufferData(float* viewProjection, float* viewMatrix, float* shadowViewProjection, glm::vec3* cameraPosition);
		void setViewProjectionUniformBufferData(float* viewProjection, float* viewMatrix, glm::vec4* cameraPosition, float* shadowViewProjectionMatrices);
	};
}


