#include "stdafx.h"

#include "UtilityFunctions.hpp"
#include "ThroneFramework.h"
#include "Window.h"
#include "Edge.h"

namespace ThroneEngine
{
	namespace Utility
	{
		static unsigned int perm[512] =
		{ 151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
			142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
			203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
			74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
			105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
			187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
			64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
			47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
			153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
			112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
			235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
			127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
			156,180,151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,
			142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,
			203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,
			74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,
			105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,
			187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,
			64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
			47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,
			153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,
			112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,
			235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,
			127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,
			156,180 };


		static float unit = 1.0f / sqrt(2);
		static float gradient2[8][2] = { { unit,unit },{ -unit,unit },{ unit,-unit },{ -unit,-unit },{ 1,0 },{ -1,0 },{ 0,1 },{ 0,-1 } };


		inline float radianToDegree(const float radian)
		{
			return 180.0f / PI * radian;
		}

		inline float degreeToRadian(const float degree)
		{
			return PI / 180.0f * degree;
		}

		float get2DPerlinNoiseValue(float x, float y, float res)
		{
			float tempX, tempY;
			int x0, y0, ii, jj, gi0, gi1, gi2, gi3;
			float tmp, s, t, u, v, Cx, Cy, Li1, Li2;

			//Adapter pour la r�solution
			x /= res;
			y /= res;

			//On r�cup�re les positions de la grille associ�e � (x,y)
			//Convertit les positions float en int
			x0 = (int)(x);
			y0 = (int)(y);

			// Obtiens une valeur entre x0//y0 et 255
			ii = x0 & 255;
			jj = y0 & 255;

			//Pour r�cup�rer les vecteurs
			gi0 = perm[ii + perm[jj]] % 8;
			gi1 = perm[ii + 1 + perm[jj]] % 8;

			gi2 = perm[ii + perm[jj + 1]] % 8;
			gi3 = perm[ii + 1 + perm[jj + 1]] % 8;

			//on r�cup�re les vecteurs et on pond�re
			tempX = x - x0;
			tempY = y - y0;
			s = gradient2[gi0][0] * tempX + gradient2[gi0][1] * tempY;

			tempX = x - (x0 + 1);
			tempY = y - y0;
			t = gradient2[gi1][0] * tempX + gradient2[gi1][1] * tempY;

			tempX = x - x0;
			tempY = y - (y0 + 1);
			u = gradient2[gi2][0] * tempX + gradient2[gi2][1] * tempY;

			tempX = x - (x0 + 1);
			tempY = y - (y0 + 1);
			v = gradient2[gi3][0] * tempX + gradient2[gi3][1] * tempY;


			//Lissage
			tmp = x - x0;
			Cx = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

			Li1 = s + Cx * (t - s);
			Li2 = u + Cx * (v - u);

			tmp = y - y0;
			Cy = 3 * tmp * tmp - 2 * tmp * tmp * tmp;

			return Li1 + Cy * (Li2 - Li1);
		}

		vec4 normalize(vec4&  vec)
		{
			const float lenght = sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z) + (vec.w *  vec.w));
			if(lenght != 0)
			{
				return vec / lenght;
			}
			return vec;
		}

		void crossProductVec3(vec3& v1, vec3& v2, vec3& finalVector)
		{
			finalVector.x = v1.y * v2.z - v1.z * v2.y;
			finalVector.y = v1.x * v2.z - v1.z * v2.x;
			finalVector.z = v1.x * v2.y - v1.y * v2.x;
		}

		void printVec3(std::string& message, vec3& vec)
		{
			std::cout << message << "X: " << vec.x << ", Y: " << vec.y << ", Z: " << vec.z << "\n";
		}

		THRONE_API_ENTRY void printVec3(char message[], glm::vec3 & vec)
		{
			std::cout << message << "X: " << vec.x << ", Y: " << vec.y << ", Z: " << vec.z << "\n";
		}

		void printVec4(std::string& message, glm::vec4& vec, bool printFourthComponent)
		{
			std::cout << message << "X: " << vec.x << ", Y: " << vec.y << ", Z: " << vec.z;
			if (printFourthComponent == true)
			{
				std::cout << ", W:" << vec.w;
			}
			std::cout << "\n";
		}

		bool pointIn2DConvexShape(const vec2 point, const std::vector<vec2>& vertices)
		{
			if(vertices.size() > 2)
			{
				int pos = 0;
				int neg = 0;

				for(int first = 0; first < vertices.size(); first++)
				{
					if(vertices[first] == point)
					{
						return true;
					}

					const int second = first < vertices.size() - 1 ? first + 1 : 0;

					const float x1 = vertices[first].x;
					const float y1 = vertices[first].y;					

					const float x2 = vertices[second].x;
					const float y2 = vertices[second].y;

					const float cross = (point.x - x1)*(y2 - y1) - (point.y - y1)*(x2 - x1);

					if (cross > 0) pos++;
					if (cross < 0) neg++;

					if (pos > 0 && neg > 0)
						return false;
				}
				return true;
			}
			return false;
		}

		bool pointIn2DShape(const vec2& pointToCheck, const std::vector<vec2>& points)
		{
			std::vector<Edge> edges;
			for(int first = 0; first < points.size(); first++)
			{
				const int second = first < points.size() - 1 ? first + 1 : 0;
				edges.emplace_back(points[first], points[second]);
			}

			int count = 0;
			for(Edge& e : edges)
			{
				if(e(pointToCheck))
				{
					count++;
				}
			}
			return count % 2 != 0;
		}

		float cross(const vec2& original, const vec2& first, const vec2& second)
		{
			return (first.x - original.x) * (second.y - original.y) - (first.y - original.y) * (second.x - original.x);
		}

		vec2 coordsToPixels(const vec2& coord)
		{
			return {(coord.x + 1) * (WINDOW_WIDTH / 2.f),
					(coord.y + 1) * (WINDOW_HEIGTH / 2.f) };
		}

		vec2 pixelsToCoords(const vec2& pixels)
		{
			return { 2.f / WINDOW_WIDTH * pixels.x - 1,
					 1 - 2.f / WINDOW_HEIGTH * pixels.y };
		}

		vec2 pixelsToCoordsScale(const vec2& pixels)
		{
			return { pixels.x / WINDOW_WIDTH * 2, pixels.y / WINDOW_HEIGTH * 2 };
		}

		vec2 coordsToPixelsScale(const vec2& coords)
		{
			return { coords.x * WINDOW_WIDTH, coords.y * WINDOW_HEIGTH };
		}
	}
}
