#include "stdafx.h"

#include "Keyboard.h"
#include "Mouse.h"

namespace ThroneEngine
{
	std::map<Keyboard::Key, Event<void>> Keyboard::onKeyPressed;
	std::map<Keyboard::Key, Event<void>> Keyboard::onKeyMaintained;
	std::map<Keyboard::Key, Event<void>> Keyboard::onKeyReleased;

	void Keyboard::notifyKeyPressed()
	{
		for (std::pair<Key, Event<void>> pair : onKeyPressed)
		{
			pair.second();
		}
	}

	void Keyboard::notifyKeyReleased()
	{
		for (std::pair<Key, Event<void>> pair : onKeyReleased)
		{
			pair.second();
		}
	}

	void Keyboard::notifyKeyMaintained()
	{
		for (const std::pair<Key, Event<void>> pair : onKeyMaintained)
		{
			keyPressed(pair.first);
		}
	}

	void Keyboard::keyPressed(const Key key)
	{
		if (isKeyPressed(key))
		{
			onKeyMaintained[key]();
		}
	}
}