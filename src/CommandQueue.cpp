#include "stdafx.h"

#include "CommandQueue.h"
#include "UtilityFunctions.hpp"

namespace ThroneEngine
{

	CommandQueue::CommandQueue()
	{
	}


	CommandQueue::~CommandQueue()
	{
	}


	bool CommandQueue::waitForQueue()
	{
		VkResult result = vkQueueWaitIdle(handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_queueWaitIdle);
			#endif
			return false;
		}
		#endif
		return true;
	}

	bool CommandQueue::submitCommandBuffer(WaitSemaphoreInfo& waitSemaphoresInfos, CommandBuffer commandBuffer, Semaphore& semaphoreToSignal, Fence& signalFence)
	{
		return submitCommandBuffers(waitSemaphoresInfos, &commandBuffer, 1, &semaphoreToSignal.handle, 1, signalFence);
	}

	bool CommandQueue::submitCommandBuffer(WaitSemaphoreInfo& waitSemaphoresInfos, CommandBuffer commandBuffer, SemaphorePool& semaphoresToSignal, Fence& signalFence)
	{
		return submitCommandBuffers(waitSemaphoresInfos, &commandBuffer, 1, 
			getVectorDataPointer(semaphoresToSignal.semaphoreHandles), semaphoresToSignal.semaphoreHandles.size(), signalFence);
	}

	bool CommandQueue::submitCommandBuffers(WaitSemaphoreInfo& waitSemaphoresInfos, CommandPool& commandBuffers, Semaphore& semaphoreToSignal, Fence& signalFence)
	{
		return submitCommandBuffers(waitSemaphoresInfos, getVectorDataPointer(commandBuffers.commandBuffers), commandBuffers.commandBuffers.size(),
			&semaphoreToSignal.handle, 1, signalFence);
	}

	bool CommandQueue::submitCommandBuffers(WaitSemaphoreInfo& waitSemaphoresInfos, CommandPool& commandBuffers, SemaphorePool& semaphoresToSignal, Fence& signalFence)
	{
		return submitCommandBuffers(waitSemaphoresInfos, getVectorDataPointer(commandBuffers.commandBuffers), commandBuffers.commandBuffers.size(),
			getVectorDataPointer(semaphoresToSignal.semaphoreHandles), semaphoresToSignal.semaphoreHandles.size(), signalFence);
	}

	bool CommandQueue::submitCommandBuffers(
		WaitSemaphoreInfo& waitSemaphoresInfos, 
		CommandBuffer* commandBuffers, 
		uint32 numberOfCommandBuffer, 
		VkSemaphore* semaphoreToSignalHandles, 
		uint32 numberOfSemaphoreToSignal, 
		Fence& fenceToSignal)
	{
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = waitSemaphoresInfos.semaphores.semaphoreHandles.size();
		submitInfo.pWaitSemaphores = getVectorDataPointer(waitSemaphoresInfos.semaphores.semaphoreHandles);
		submitInfo.pWaitDstStageMask = getVectorDataPointer(waitSemaphoresInfos.waitingStage);
		submitInfo.commandBufferCount = numberOfCommandBuffer;
		submitInfo.pCommandBuffers = reinterpret_cast<VkCommandBuffer*>(commandBuffers);
		submitInfo.signalSemaphoreCount = numberOfSemaphoreToSignal;
		submitInfo.pSignalSemaphores = semaphoreToSignalHandles;

		VkResult result = vkQueueSubmit(handle, 1, &submitInfo, fenceToSignal.handle);
		#if defined ERROR_VALIDATION_VKSUCCESS_CHECK
		if (result != VK_SUCCESS)
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_queueSubmit);
			#endif
			return false;
		}
		#endif
		return true;
	}

}