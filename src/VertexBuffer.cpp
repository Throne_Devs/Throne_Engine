#include "stdafx.h"

#include "VertexBuffer.h"

namespace ThroneEngine
{

	VertexBuffer::VertexBuffer()
	{
	}


	VertexBuffer::~VertexBuffer()
	{
	}

	bool VertexBuffer::initialize(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage, uint32 bindingIndex)
	{
		bool result = GpuBuffer::initialize(sizeInBytes, usage);
		
		this->bindingIndex = bindingIndex;

		return result;
	}


}