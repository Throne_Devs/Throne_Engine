#include "stdafx.h"

#include "MeshRenderer.h"
#include "UtilityFunctions.hpp"

namespace ThroneEngine
{
	MeshRenderer::MeshRenderer()
	{
	}


	MeshRenderer::~MeshRenderer()
	{
	}

	bool MeshRenderer::initialize(Mesh* mesh, CommandQueue& commandQueue, uint32 bindingIndex, VkMemoryPropertyFlags memoryProperty)
	{
		initializeBase(mesh, memoryProperty, bindingIndex);

		if (memoryProperty & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		{
			CommandPool stagingCommandBuffers;
			stagingCommandBuffers.initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, 1, VK_COMMAND_BUFFER_LEVEL_PRIMARY);

			if (vertexBuffer.updateWithDeviceLocalMemory(stagingCommandBuffers.commandBuffers[0], getVectorDataPointer(mesh->data), mesh->totalSizeInBytes,
				0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, commandQueue, SemaphorePool()) == false)
			{
				stagingCommandBuffers.destroy();
				return false;
			}

			stagingCommandBuffers.destroy();

		}
		else if (memoryProperty & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		{
			if (vertexBuffer.mapHostVisibleMemory(0, mesh->totalSizeInBytes, getVectorDataPointer(mesh->data), true) == false)
			{
				return false;
			}
		}
		else
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_invalidMeshRendererMemoryProperties);
			#endif
			return false;
		}

		return true;
	}

	bool MeshRenderer::initialize(Mesh* mesh, CommandQueue& commandQueue, CommandBuffer& stagingCommandBuffer, uint32 bindingIndex, VkMemoryPropertyFlags memoryProperty)
	{
		initializeBase(mesh, memoryProperty, bindingIndex);

		if (memoryProperty & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		{
			if (vertexBuffer.updateWithDeviceLocalMemory(stagingCommandBuffer, getVectorDataPointer(mesh->data), mesh->totalSizeInBytes,
				0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, commandQueue, SemaphorePool()) == false)
			{
				return false;
			}
		}
		else if (memoryProperty & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		{
			if (vertexBuffer.mapHostVisibleMemory(0, mesh->totalSizeInBytes, getVectorDataPointer(mesh->data), true) == false)
			{
				return false;
			}
		}
		else
		{
			#if ERROR_MESSAGE_NEEDED
			ERROR_MESSAGE(err_invalidMeshRendererMemoryProperties);
			#endif
			return false;
		}

		return true;
	}

	inline bool MeshRenderer::initializeBase(Mesh* mesh, VkMemoryPropertyFlags memoryProperty, uint32 bindingIndex)
	{
		this->mesh = mesh;

		if (vertexBuffer.initialize(mesh->totalSizeInBytes,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | (memoryProperty & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT ? VK_BUFFER_USAGE_TRANSFER_DST_BIT : 0), bindingIndex) == false)
		{
			return false;
		}
		if (vertexBuffer.allocateMemory(memoryProperty) == false)
		{
			return false;
		}

		return true;
	}

	void MeshRenderer::updateMeshData(float* data, uint32 totalSizeInBytes)
	{
		mesh->data.clear();
		mesh->data.resize(totalSizeInBytes / sizeof(float));

		memcpy(mesh->data.data(), data, totalSizeInBytes);
	}

	bool MeshRenderer::updateBufferData(CommandQueue& commandQueue, std::vector<Fence>& waitFences)
	{
		if (mesh->totalSizeInBytes > vertexBuffer.currentBufferSizeInBytes)
		{
			for (int i = 0; i < waitFences.size(); i++)
			{
				waitFences[i].waitForFence(TIMEOUT_FENCE);
			}
			vertexBuffer.destroy();
			initialize(mesh, commandQueue, vertexBuffer.bindingIndex, vertexBuffer.memoryProperties);
		}
		else if (vertexBuffer.memoryProperties & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		{
			CommandPool stagingCommandBuffers;
			stagingCommandBuffers.initialize(commandQueue, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, 1, VK_COMMAND_BUFFER_LEVEL_PRIMARY);

			bool returnValue = updateBufferDataWithDeviceLocal(commandQueue, stagingCommandBuffers.commandBuffers[0]);

			stagingCommandBuffers.destroy();

			return returnValue;
		}
		else if (vertexBuffer.memoryProperties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		{
			updateBufferDataWithHostVisible(commandQueue);
		}

		return false;
	}

	bool MeshRenderer::updateBufferDataWithDeviceLocal(CommandQueue& commandQueue, CommandBuffer& stagingCommandBuffers)
	{
		if (vertexBuffer.updateWithDeviceLocalMemory(stagingCommandBuffers, getVectorDataPointer(mesh->data), mesh->totalSizeInBytes,
			0, 0, VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, commandQueue, SemaphorePool()) == false)
		{
			return false;
		}
		return true;
	}

	bool MeshRenderer::updateBufferDataWithHostVisible(CommandQueue& commandQueue)
	{
		if (vertexBuffer.mapHostVisibleMemory(0, mesh->totalSizeInBytes, getVectorDataPointer(mesh->data), true) == false)
		{
			return false;
		}
		return true;
	}
}


