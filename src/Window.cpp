#pragma once
#include "stdafx.h"

#include "ThroneFramework.h"
#include "Window.h"
#include "Throne.h"

using namespace R::S::ErrorMessage;
using namespace R::S::Path;
using namespace R::S::StandardOutput;

namespace ThroneEngine
{
	Window::Window(const uint32 sizeX, const uint32 sizeY, const std::string name, Throne* throneInstance)
	{
		swapchainExtent.width = sizeX;
		swapchainExtent.height = sizeY;
		windowName = name;
		this->throneInstance = throneInstance;
	}

	Window::~Window()
	{
		deInitSurface();
		deInitOsWindow();
	}

	void Window::close()
	{
		windowShouldRun = false;
	}

	bool Window::isOpen() const
	{
		updateOsWindow();
		return windowShouldRun;
	}

	bool Window::pollEvent(EventWindow& event)
	{
		if (events.size() > 0)
		{
			event = events.front();
			popEvent();
			return true;
		}
		return false;
	}

	void Window::pushEvent(const EventWindow& event)
	{
		events.push(event);
	}

	void Window::popEvent()
	{
		events.pop();
	}

	bool Window::initialize()
	{
		if (initOsWindow() == false)
			return false;
		initOsSurface();
		return false;
	}

	bool Window::initSurface()
	{
		VkBool32 WSISupported = false;
		if (vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, 0, surface, &WSISupported)  != VK_SUCCESS || WSISupported == false)
		{
			return false;
		}
		
		if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities) != VK_SUCCESS || surfaceCapabilities.currentExtent.width >= UINT32_MAX)
		{
			return false;
		}

		swapchainExtent.width = surfaceCapabilities.currentExtent.width;
		swapchainExtent.height = surfaceCapabilities.currentExtent.height;

		uint32 formatCount = 0;
		if (vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr) != VK_SUCCESS || formatCount == 0)
		{
			return false;
		}

		std::vector<VkSurfaceFormatKHR> formats(formatCount);
		if(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, formats.data()) != VK_SUCCESS)
		{
			return false;
		}

		if (formats[0].format == VK_FORMAT_UNDEFINED)
		{
			surfaceFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
			surfaceFormat.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
		}
		else
		{
			surfaceFormat = formats[0];
		}

		return true;
	}

	void Window::deInitSurface() const
	{
		vkDestroySurfaceKHR(instance, surface, nullptr);
	}

	bool Window::choosePresentMode(VkPresentModeKHR& presentMode) const
	{
		presentMode = VK_PRESENT_MODE_FIFO_KHR;

		uint32 presentModeCount = 0;
		VkResult error = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (error != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_physicalSurfacePresentMode);
			#endif
			return false;
		}
		#endif

		std::vector<VkPresentModeKHR> presentModeList(presentModeCount);
		error = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModeList.data());
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (error != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_physicalSurfacePresentMode);
			#endif
			return false;
		}
		#endif

		for (VkPresentModeKHR m : presentModeList)
		{
			if (m == VK_PRESENT_MODE_MAILBOX_KHR)
				presentMode = m;
		}

		return true;
	}

	bool Window::initSwapChain()
	{
		initSurface();

		swapchainImageCount = surfaceCapabilities.minImageCount;

		VkPresentModeKHR presentMode;
		if (choosePresentMode(presentMode) == false) 
			return false;

		VkSurfaceTransformFlagBitsKHR surfaceTransform;
		if(surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
		{
			surfaceTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		}
		else
		{
			surfaceTransform = surfaceCapabilities.currentTransform;
		}

		VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
		swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapchainCreateInfo.surface = surface;
		swapchainCreateInfo.minImageCount = swapchainImageCount;
		swapchainCreateInfo.imageFormat = surfaceFormat.format;
		swapchainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
		swapchainCreateInfo.imageExtent = swapchainExtent;
		swapchainCreateInfo.imageArrayLayers = 1;
		swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapchainCreateInfo.queueFamilyIndexCount = 0;
		swapchainCreateInfo.pQueueFamilyIndices = nullptr;
		swapchainCreateInfo.preTransform = surfaceTransform;
		swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swapchainCreateInfo.presentMode = presentMode;
		swapchainCreateInfo.clipped = VK_TRUE;
		swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

		VkResult error = vkCreateSwapchainKHR(logicalDevice, &swapchainCreateInfo, nullptr, &swapchain);
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (error != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_swapChain);
			#endif
			return false;
		}
		#endif
		error = vkGetSwapchainImagesKHR(logicalDevice, swapchain, &swapchainImageCount, nullptr);
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (error != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_swapChainImages);
			#endif
			return false;
		}
		#endif
		return true;
	}

	void Window::deInitSwapChain() const
	{
		vkDestroySwapchainKHR(logicalDevice, swapchain, nullptr);
	}

	bool Window::initSwapChainImages()
	{
		VkResult result = vkGetSwapchainImagesKHR(logicalDevice, swapchain, &swapchainImageCount, nullptr);
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (result != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_swapChainImages);
			#endif
			return false;
		}
		#endif

		swapchainImages.resize(swapchainImageCount);
		swapchainImageViews.resize(swapchainImageCount);

		result = vkGetSwapchainImagesKHR(logicalDevice, swapchain, &swapchainImageCount, swapchainImages.data());
		#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
		if (result != VK_SUCCESS)
		{
			#if defined(ERROR_MESSAGE_NEEDED)
			ERROR_MESSAGE(err_swapChainImages);
			#endif
			return false;
		}
		#endif
		for (uint32 i = 0; i < swapchainImageCount; i++)
		{
			VkImageViewCreateInfo imageViewCreateInfo{};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.image = swapchainImages[i];
			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = surfaceFormat.format;
			imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;
			result = vkCreateImageView(logicalDevice, &imageViewCreateInfo, nullptr, &swapchainImageViews[i]);
			#if defined(ERROR_VALIDATION_VKSUCCESS_CHECK)
			if (result != VK_SUCCESS)
			{
				#if defined(ERROR_MESSAGE_NEEDED)
				ERROR_MESSAGE(err_imageViewCreation);
				#endif
				return false;
			}
			#endif
		}
		return true;
	}

	void Window::deInitSwapChainImages()
	{
		for (VkImageView imageView : swapchainImageViews)
		{
			vkDestroyImageView(logicalDevice, imageView, nullptr);
		}
	}

	void Window::recreateSwapChain()
	{		
		deInitSwapChainImages();
		deInitSwapChain();		

		initSwapChain();
		initSwapChainImages();
		
		WINDOW_WIDTH = swapchainExtent.width;
		WINDOW_HEIGTH = swapchainExtent.height;

		throneInstance->onWindowResizeEvent();
	}

	void Window::setKeyRepeatEnabled(const bool enabled)
	{
		keyRepeatEnable = enabled;
	}
}
