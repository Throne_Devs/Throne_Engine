#include "stdafx.h"

#include "Color.h"

namespace ThroneEngine
{
	const Color Color::Black(0, 0, 0);
	const Color Color::White(255, 255, 255);
	const Color Color::Red(255, 0, 0);
	const Color Color::Green(0, 255, 0);
	const Color Color::Blue(0, 0, 255);
	const Color Color::Yellow(255, 255, 0);
	const Color Color::Magenta(255, 0, 255);
	const Color Color::Transparent(0, 0, 0, 0);

	Color::Color() :
		r(0),
		g(0),
		b(0),
		a(255)
	{
	}

	Color::Color(uint8 red, uint8 green, uint8 blue, uint8 alpha) :
		r(red),
		g(green),
		b(blue),
		a(alpha)
	{
	}

	Color::Color(uint32 color) :
		r((color & 0xff000000) >> 24),
		g((color & 0x00ff0000) >> 16),
		b((color & 0x0000ff00) >> 8),
		a((color & 0x000000ff) >> 0)
	{
	}

	uint32 Color::toInteger() const
	{
		return (r << 24) | (g << 16) | (b << 8) | a;
	}

	bool Color::operator==(const Color& right) const
	{
		return r == right.r &&
			g == right.g &&
			b == right.b &&
			a == right.a;
	}

	bool Color::operator!=(const Color & right) const
	{
		return !(*this == right);
	}

	Color Color::operator+(const Color& right) const
	{
		return {
			uint8(std::min(int(r) + right.r, 255)),
			uint8(std::min(int(g) + right.g, 255)),
			uint8(std::min(int(b) + right.b, 255)),
			uint8(std::min(int(a) + right.a, 255))
		};
	}

	Color Color::operator-(const Color& right) const
	{
		return {
			uint8(std::max(int(r) - right.r, 0)),
			uint8(std::max(int(g) - right.g, 0)),
			uint8(std::max(int(b) - right.b, 0)),
			uint8(std::max(int(a) - right.a, 0))
		};
	}

	Color Color::operator*(const Color& right) const
	{
		return {
			uint8(int(r) * right.r / 255),
			uint8(int(g) * right.g / 255),
			uint8(int(b) * right.b / 255),
			uint8(int(a) * right.a / 255)
		};
	}

	Color& Color::operator+=(const Color& right)
	{
		return *this = *this + right;
	}

	Color& Color::operator-=(const Color& right)
	{
		return *this = *this - right;
	}

	Color& Color::operator*=(const Color& right)
	{
		return *this = *this * right;
	}
}