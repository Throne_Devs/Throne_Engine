#pragma once
#include "stdafx.h"
#include "CommandBuffer.h"

namespace ThroneEngine
{
	class CommandQueue;

	class CommandPool
	{
	public:
		CommandPool();
		~CommandPool();

		std::vector<CommandBuffer> commandBuffers;
		VkCommandPool handle = VK_NULL_HANDLE;

		bool initialize(CommandQueue& queue, VkCommandPoolCreateFlags parameters);
		bool initialize(CommandQueue& queue, VkCommandPoolCreateFlags parameters, int numberOfCommandBuffersToCreate, VkCommandBufferLevel commandBufferLevel);
		void destroy();
		bool reset(VkCommandPoolResetFlags releaseResources);
		bool addCommandBuffers(int numberOfCommandBuffers, VkCommandBufferLevel commandBufferLevel);

	private:
		bool createCommandPool(CommandQueue& queue, VkCommandPoolCreateFlags parameters);
	};

}