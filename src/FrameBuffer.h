#pragma once
#include "stdafx.h"

#include "RenderPass.h"

namespace ThroneEngine
{
	class FrameBuffer
	{
	public:
		FrameBuffer();
		~FrameBuffer();

		VkFramebuffer handle = VK_NULL_HANDLE;

		bool initialize(RenderPass& renderPass, std::vector<VkImageView>& attachments, VkExtent2D& frameBufferSize, uint32 layers);
		void destroy();
	};
}



