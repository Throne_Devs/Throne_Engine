#version 450
#extension GL_ARB_separate_shader_objects : enable


layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 inTextureUV;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform viewProjectionBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;
	mat4 shadowViewProjection;
	vec3 cameraPosition;
};

layout (set = 0, binding = 2) uniform lightBuffer
{
	vec3 lightPosition;
	vec3 lightDirection;	
};

layout (location = 0) out vec2 textureUV;
layout (location = 1) out vec4 shadowTextureCoords;
layout (location = 2) out vec3 normalVector;
layout (location = 3) out vec3 vertexToLight;
layout (location = 4) out vec3 vertexToCamera;
layout (location = 5) out vec3 _lightDirection;

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 );

void main() 
{
	vec4 pos = modelMatrix * vec4(inVertexPosition, 1.0);
    gl_Position = viewProjection * pos;

	shadowTextureCoords = shadowViewProjection * pos;

	normalVector = normalize((transpose(inverse(modelMatrix)) * vec4(normal, 1.0)).xyz);

	vertexToLight = normalize(pos.xyz - lightPosition);

	vertexToCamera = normalize(pos.xyz - cameraPosition);

	_lightDirection = lightDirection;
	textureUV = inTextureUV;
}