#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_KHR_vulkan_glsl : enable


layout (set = 1, binding = 0) uniform sampler2D textureSampler;
layout (set = 0, binding = 1) uniform sampler2D shadowMap;

layout (location = 0) in vec2 textureUV;
layout (location = 1) in vec4 shadowTextureCoords;
layout (location = 2) in vec3 normalVector;
layout (location = 3) in vec3 vertexToLight;
layout (location = 4) in vec3 vertexToCamera;
layout (location = 5) in vec3 lightDirection;

layout (location = 0) out vec4 outColor;


float computeShadowFactor(vec4 shadowSpacePos);
float ambient = 0.4;

void main() {
	vec4 fragColor = texture(textureSampler, textureUV);
	if (fragColor.w < 1.0)
	{
		discard;
	}
	
	float shadow = computeShadowFactor(shadowTextureCoords);

	outColor = fragColor * shadow;
}



float computeShadowFactor(vec4 shadowSpacePos)
{
	vec3 shadowCoords = shadowTextureCoords.xyz / shadowTextureCoords.w;

	if	(abs(shadowCoords.x) > 1.0 || abs(shadowCoords.y) > 1.0 || abs(shadowCoords.z) > 1.0)
	{
		return ambient;
	}

	float shadowValue = 1.0;

	//if (shadowCoords.w > 0.0)
	{
		if	(shadowCoords.z - 0.00001 > texture(shadowMap, shadowCoords.xy * 0.5 + 0.5).r)
		{
			shadowValue = ambient;
		}
	}

	

	return shadowValue;

}