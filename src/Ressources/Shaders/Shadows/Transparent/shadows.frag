#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_KHR_vulkan_glsl : enable

layout (set = 1, binding = 0) uniform sampler2D textureSampler;

layout (location = 0) in vec2 textureUV;

void main() 
{
	vec4 fragColor = texture(textureSampler, textureUV);
	if (fragColor.w < 1.0)
	{
		discard;
	}
}