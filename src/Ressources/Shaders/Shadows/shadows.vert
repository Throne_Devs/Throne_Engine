#version 450
#extension GL_ARB_separate_shader_objects : enable


layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 inTextureUV;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;
	mat4 shadowViewProjection;
	vec3 cameraPosition;
};

void main() 
{
    gl_Position = shadowViewProjection * modelMatrix * vec4(inVertexPosition, 1.0);
}