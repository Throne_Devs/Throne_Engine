#version 450
#extension GL_ARB_separate_shader_objects : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec3 inColor;
layout (location = 2) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;
	//mat4 shadowViewProjection;	
	mat4 shadowViewProjectionMatrices[SHADOW_MAP_CASCADE_COUNT];
	vec4 cameraPosition;
};
layout (location = 0) out vec4 outVertexSceneZValue;
layout (location = 1) out vec3 outColor;

void main() 
{
	vec4 pos = modelMatrix * vec4(inVertexPosition, 1.0);

	outVertexSceneZValue = viewMatrix * pos;

    gl_Position = viewProjection * pos;

	outColor = inColor;
}