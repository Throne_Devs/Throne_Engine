#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_KHR_vulkan_glsl : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (set = 0, binding = 3) uniform UniformBuffer
{
	vec2 planeValues[SHADOW_MAP_CASCADE_COUNT];
};

layout (location = 0) in vec4 inVertexSceneZValue;
layout (location = 1) in vec3 inColor;

layout (location = 0) out vec4 outColor;


void main() 
{
	outColor = vec4(inColor, 1.0f);
//	if (inVertexSceneZValue.z * -1 < planeValues[0].y)
//	{
//		outColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
//	}
//	else if (inVertexSceneZValue.z * -1 <  planeValues[1].y)
//	{
//		outColor = vec4(0.0f, 1.0f, 0.0f, 1.0f);
//	}	
//	else if (inVertexSceneZValue.z * -1 <  planeValues[2].y)
//	{
//		outColor = vec4(0.0f, 0.0f, 1.0f, 1.0f);
//	}
//	else if (inVertexSceneZValue.z * -1 <  planeValues[3].y)
//	{
//		outColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	}
//	else
//	{
//		outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
//	}


}