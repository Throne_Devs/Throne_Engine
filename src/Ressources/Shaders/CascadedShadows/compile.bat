%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V CascadedShadows.vert -o CascadedShadowsVert.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V CascadedScene.vert -o CascadedSceneVert.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V CascadedScene.frag -o CascadedSceneFrag.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V Transparent/CascadedTransparentScene.frag -o Transparent/CascadedTransparentSceneFrag.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V Transparent/CascadedTransparentShadows.frag -o Transparent/CascadedTransparentShadowsFrag.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V Transparent/CascadedTransparentShadows.vert -o Transparent/CascadedTransparentShadowsVert.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V DebugCascadedShadows.vert -o DebugCascadedShadowsVert.spv
%~dp0..\..\..\..\Libraries\LunarGSDK\1.1.82.0\Bin32\glslangValidator.exe -V DebugCascadedShadows.frag -o DebugCascadedShadowsFrag.spv
pause