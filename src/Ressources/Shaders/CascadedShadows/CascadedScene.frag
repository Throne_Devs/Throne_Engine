#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_KHR_vulkan_glsl : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (set = 1, binding = 0) uniform sampler2D textureSampler;
layout (set = 0, binding = 1) uniform sampler2D shadowMap[SHADOW_MAP_CASCADE_COUNT];

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;	
	mat4 shadowViewProjectionMatrices[SHADOW_MAP_CASCADE_COUNT];
	vec4 cameraPosition;
};

layout (set = 0, binding = 3) uniform UniformBuffer
{
	vec4 pcfData[SHADOW_MAP_CASCADE_COUNT]; // dx, dy, range, inverseCount | inverseCount is in 1/count form
};


layout (location = 0) in vec2 textureUV;
layout (location = 1) in vec4 inPos;
layout (location = 2) in vec3 normalVector;
layout (location = 3) in vec3 cameraToVertex;
layout (location = 4) in vec3 lightDirection;
layout (location = 5) in vec4 inVertexSceneZValue;

layout (location = 0) out vec4 outColor;

const float ambient = 0.25;

float computeShadowFactor();
float filterPCF(const vec3 shadowCoords, const int cascadeIndex);
float resolvePcf(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex, const int range);
float pcf1x1(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf2x2(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf3x3(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf4x4(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf5x5(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf6x6(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf7x7(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf8x8(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf9x9(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);
float pcf10x10(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex);

float samplerProj(const vec3 shadowCoords, const sampler2D shadowMapTemp, const vec2 offset);
float samplerProj(const vec3 shadowCoords, const sampler2D shadowMapTemp);

void main() 
{
	float dotValue = dot(-lightDirection, normalVector);
	float brigthness = min(max(dotValue * 2.5f, ambient), 1.0f);
	vec3 diffuse = brigthness * vec3(1, 1, 1);

	float shadow = computeShadowFactor();
	if (dotValue < 0)
	{
		shadow = min(max((1 + dotValue - ambient), ambient), shadow);
	}

	vec4 fragColor = texture(textureSampler, textureUV);
	outColor = vec4(diffuse * fragColor.rgb * shadow, fragColor.a);
}



float computeShadowFactor()
{
	vec4 shadowCoords;
	for (int i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++)
	{
		shadowCoords = shadowViewProjectionMatrices[i] * inPos;
		shadowCoords = shadowCoords / shadowCoords.w;
		if	(shadowCoords.x < 1.0 && shadowCoords.x > -1.0 && 
			shadowCoords.y < 1.0 && shadowCoords.y > -1.0 && 
			shadowCoords.z < 1.0 && shadowCoords.z > 0.0)
		{
			shadowCoords.xy = (shadowCoords.xy * 0.5) + 0.5;
			shadowCoords.z -= 0.001;

			return filterPCF(shadowCoords.xyz, i);
		}
	}

	return ambient;	// If outside the frustum
}


float filterPCF(const vec3 shadowCoords, const int cascadeIndex)
{	
	float shadowFactor = 0.0;
	const int range = int(pcfData[cascadeIndex].z);
	
	if (range > 0)
	{
		const float dx = pcfData[cascadeIndex].x;
		const float dy = pcfData[cascadeIndex].y;	
		shadowFactor = resolvePcf(shadowCoords, shadowMap[cascadeIndex], dx, dy, cascadeIndex, range);
	}
	else
	{
		shadowFactor = samplerProj(shadowCoords, shadowMap[cascadeIndex]);
	}

	return shadowFactor;
}

float resolvePcf(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex, const int range)
{
	switch (range)
	{
		case 1:
		{
			return pcf1x1(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 2:
		{
			return pcf2x2(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 3:
		{
			return pcf3x3(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 4:
		{
			return pcf4x4(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 5:
		{
			return pcf5x5(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 6:
		{
			return pcf6x6(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 7:
		{
			return pcf7x7(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 8:
		{
			return pcf8x8(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 9:
		{
			return pcf9x9(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		case 10:
		{
			return pcf10x10(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
		default:
		{
			return pcf1x1(shadowCoords, shadowMapTemp, dx, dy, cascadeIndex);
			break;
		}
	}
}

float pcf1x1(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 1;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf2x2(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 2;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf3x3(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 3;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf4x4(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 4;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf5x5(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 5;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf6x6(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 6;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf7x7(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 7;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf8x8(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 8;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf9x9(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 9;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}
float pcf10x10(const vec3 shadowCoords, const sampler2D shadowMapTemp, const float dx, const float dy, const int cascadeIndex)
{
	float shadowFactor = 0.0f;
	const int range = 10;
	const vec2 dxdy = vec2(dx, dy);
	for (int x = -range; x <= range; x++) 
	{
		for (int y = -range; y <= range; y++) 
		{
			shadowFactor += samplerProj(shadowCoords, shadowMapTemp, dxdy * (x, y));
		}
	}
	return shadowFactor * pcfData[cascadeIndex].w;
}


float samplerProj(const vec3 shadowCoords, const sampler2D shadowMapTemp, const vec2 offset)
{
	if	(shadowCoords.z > texture(shadowMapTemp, shadowCoords.xy + offset).r)
	{
		return ambient;
	}
	return 1.0f;
}

float samplerProj(const vec3 shadowCoords, const sampler2D shadowMapTemp)
{
	if	(shadowCoords.z > texture(shadowMapTemp, shadowCoords.xy).r)
	{
		return ambient;
	}
	return 1.0f;
}


//	for (int i = SHADOW_MAP_CASCADE_COUNT - 2; i > -1; i--)
//	{
//		if(inVertexSceneZValue.z <= planeValues[i].y)
//		{
//			shadowCoords = shadowViewProjectionMatrices[i] * inPos;
//			shadowCoords = shadowCoords / shadowCoords.w;
//
//			shadowCoords.xy = (shadowCoords.xy * 0.5) + 0.5;
//			shadowCoords.z -= 0.005;
//
//			return filterPCF(shadowCoords.xyz, i);
//			break;
//		}
//	}