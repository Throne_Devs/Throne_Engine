#version 450
#extension GL_ARB_separate_shader_objects : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 inTextureUV;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;	
	mat4 shadowViewProjectionMatrices[SHADOW_MAP_CASCADE_COUNT];
	vec4 cameraPosition;
};

layout (push_constant) uniform PushConstants
{
	int cascadeIndex;
};

layout (location = 0) out vec2 textureUV;

void main() 
{
	gl_Position = shadowViewProjectionMatrices[cascadeIndex] * modelMatrix * vec4(inVertexPosition, 1.0);
	textureUV = inTextureUV;
}