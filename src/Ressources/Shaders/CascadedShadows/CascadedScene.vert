#version 450
#extension GL_ARB_separate_shader_objects : enable

#define SHADOW_MAP_CASCADE_COUNT 4

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 inTextureUV;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 viewMatrix;
	//mat4 shadowViewProjection;	
	mat4 shadowViewProjectionMatrices[SHADOW_MAP_CASCADE_COUNT];
	vec4 cameraPosition;
};

layout (set = 0, binding = 2) uniform lightBuffer
{
	vec4 lightPosition;
	vec4 lightDirection;	
};

layout (location = 0) out vec2 textureUV;
layout (location = 1) out vec4 outPos;
layout (location = 2) out vec3 normalVector;
layout (location = 3) out vec3 cameraToVertex;
layout (location = 4) out vec3 _lightDirection;
layout (location = 5) out vec4 outVertexSceneZValue;

void main() 
{
	vec4 pos = modelMatrix * vec4(inVertexPosition, 1.0);

	outVertexSceneZValue = viewMatrix * pos;

    gl_Position = viewProjection * pos;

	outPos = pos;

	//normalVector = normalize((transpose(inverse(modelMatrix)) * vec4(normal, 1.0)).xyz);
	normalVector = normalize((transpose(inverse(modelMatrix)) * vec4(normal, 1.0)).xyz);

	cameraToVertex = normalize(pos.xyz - cameraPosition.xyz);

	_lightDirection = lightDirection.xyz;
	textureUV = inTextureUV;
}