#version 450

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
	mat4 shadowViewProjection;
};

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in mat4 modelMatrix;

layout (location = 0) out vec3 textureUV;

void main() 
{
    gl_Position = viewProjection * modelMatrix * vec4(inVertexPosition, 1.0);
	textureUV = vec3(1 - inVertexPosition.x, inVertexPosition.y, inVertexPosition.z);
}