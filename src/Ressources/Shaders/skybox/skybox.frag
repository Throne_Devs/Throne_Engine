#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (set = 1, binding = 0) uniform samplerCube cubemap;

layout (location = 0) in vec3 textureUV;
layout (location = 0) out vec4 outColor;

void main() {
	outColor = texture(cubemap, textureUV);
}