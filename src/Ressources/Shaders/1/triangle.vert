#version 450
#extension GL_ARB_separate_shader_objects : enable


layout (push_constant) uniform ModelViewProjection {	
	mat4 projection;
	mat4 view;
} PushConstant;

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec3 inVertexColor;

layout (location = 0) out vec3 fragColor;

void main() 
{
    gl_Position = PushConstant.projection * PushConstant.view * vec4(inVertexPosition, 1.0);
	fragColor = inVertexColor;
}