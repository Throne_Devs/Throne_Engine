#version 450

layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 textureUV;
layout (location = 2) in vec3 color;
layout (location = 3) in mat4 modelMatrix;

layout (location = 0) out vec2 fragTextureUV;
layout (location = 1) out vec3 fragColor;

void main() 
{
	mat4 correctionMatrix = mat4(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.0f, 0.0f, 0.5f, 1.0f);
    gl_Position = correctionMatrix * modelMatrix * vec4(inVertexPosition, 1.0);
	fragTextureUV = textureUV;
	fragColor = color;
}