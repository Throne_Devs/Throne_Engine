#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (set = 0, binding = 0) uniform sampler2D fontTexture;

layout (location = 0) in vec2 fragTextureUV;
layout (location = 1) in vec3 fragColor;

layout (location = 0) out vec4 outColor;

void main() 
{
	vec2 normalizedTextureUV = fragTextureUV / textureSize(fontTexture, 0);
	outColor = texture(fontTexture, normalizedTextureUV) * vec4(fragColor, 1.0);
}