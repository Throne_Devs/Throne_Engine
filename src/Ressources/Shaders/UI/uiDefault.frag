#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (set = 1, binding = 0) uniform sampler2D textureSampler;

layout (location = 0) in vec2 fragTextureUV;
layout (location = 1) in vec3 fragColor;

layout (location = 0) out vec4 outColor;

void main() 
{
	vec4 color = texture(textureSampler, fragTextureUV) * vec4(fragColor, 1.0);

	if(color.w == 0.0)
		discard;

	outColor = color;
}