#version 450
#extension GL_ARB_separate_shader_objects : enable


layout (location = 0) in vec3 inVertexPosition;
layout (location = 1) in vec2 inTextureUV;
layout (location = 2) in mat4 modelMatrix;

layout (set = 0, binding = 0) uniform uniformBuffer
{
	mat4 viewProjection;
};

layout (location = 0) out vec2 textureUV;

void main() 
{
    gl_Position = viewProjection * modelMatrix * vec4(inVertexPosition, 1.0);
	textureUV = inTextureUV;
}