#pragma once
#include "stdafx.h"

#include "Time.h"

namespace ThroneEngine
{
	class Clock
	{
	public:
		Clock();
		Time getElapsedTime() const;
		Time restart();

		void printToConsoleElapsedTimeMs(std::string& message);
		void printToConsoleElapsedTimeMs(const char* message);
		void printToConsoleElapsedTimeMicroS(std::string& message);
		void printToConsoleElapsedTimeMicroS(const char* message);

	private:
		Time startTime;
	};
}
