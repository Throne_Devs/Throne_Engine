#pragma once
#include "stdafx.h"

#include "Mouse.h"
#include "Window.h"

#if defined PLATFORM_WINDOWS

std::vector<std::string> Mouse::buttonToString =
{
	"Unknow",
	"Left",
	"Right",
	"Middle",
	"XButton1",
	"XButton2"
};

std::vector<std::string> Mouse::wheelToString =
{
	"VerticalWheel",
	"HorizontalWheel"
};

bool Mouse::isButtonPressed(Button button)
{
	int vkey = 0;
	switch (button)
	{
		case Left: vkey = GetSystemMetrics(SM_SWAPBUTTON) ? VK_RBUTTON : VK_LBUTTON;
			break;
		case Right: vkey = GetSystemMetrics(SM_SWAPBUTTON) ? VK_LBUTTON : VK_RBUTTON;
			break;
		case Middle: vkey = VK_MBUTTON;
			break;
		case XButton1: vkey = VK_XBUTTON1;
			break;
		case XButton2: vkey = VK_XBUTTON2;
			break;
		default: vkey = 0;
			break;
	}

	return (GetAsyncKeyState(vkey) & 0x8000) != 0;
}

vec2 Mouse::getPosition()
{
	POINT point;
	GetCursorPos(&point);
	return vec2(point.x, point.y);
}

vec2 Mouse::getPosition(const Window& relativeTo)
{
	HWND handle = relativeTo.getSystemHandle();
	if (handle)
	{
		POINT point;
		GetCursorPos(&point);
		ScreenToClient(handle, &point);
		return vec2(point.x, point.y);
	}
	return vec2(0, 0);
}

void Mouse::setPosition(const vec2& position)
{
	SetCursorPos(position.x, position.y);
}

void Mouse::setPosition(const vec2& position, const Window& relativeTo)
{
	HWND handle = relativeTo.getSystemHandle();
	if (handle)
	{
		POINT point = {position.x, position.y};
		ClientToScreen(handle, &point);
		SetCursorPos(point.x, point.y);
	}
}
#endif
