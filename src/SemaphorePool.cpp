#include "stdafx.h"

#include "SemaphorePool.h"

namespace ThroneEngine
{

	SemaphorePool::SemaphorePool()
	{
	}


	SemaphorePool::~SemaphorePool()
	{
	}

	bool SemaphorePool::initialize(int numberOfSemaphore)
	{
		return addSemaphores(numberOfSemaphore);
	}

	bool SemaphorePool::addSemaphores(int numberOfSemaphore)
	{
		int beginIndex = semaphoreHandles.size() > 0 ? semaphoreHandles.size() - 1 : 0;
		semaphoreHandles.resize(semaphoreHandles.size() + numberOfSemaphore);
		bool result = true;
		for (int i = beginIndex; i < semaphoreHandles.size(); i++)
		{
			result &= Semaphore::createSemaphore(semaphoreHandles[i]);
		}
		return result == true;
	}

}