#pragma once
#include "stdafx.h"

namespace ThroneEngine
{
	template<typename Signature>
	struct FunctionHandle
	{
		typedef std::function<Signature> EventFunction;
		EventFunction function;
		int handle = 0;

		FunctionHandle() {}
		FunctionHandle(const EventFunction& function) : function(function) {}
	};

	template<typename Result, typename ...Args>
	struct Event
	{
		static int count;

		Event& operator+=(FunctionHandle<Result(Args...)>& function);
		Event& operator-=(FunctionHandle<Result(Args...)>& function);
		void operator()(Args... args);

	private:
		std::map<int, FunctionHandle<Result(Args...)>> functions;
	};

	template<typename Result, typename ...Args>
	int Event<Result, Args...>::count = 0;

	template <typename Result, typename ... Args>
	Event<Result, Args...>& Event<Result, Args...>::operator+=(FunctionHandle<Result(Args...)>& function)
	{
		function.handle = ++count;
		functions[function.handle] = function;
		return *this;
	}

	template <typename Result, typename ... Args>
	Event<Result, Args...>& Event<Result, Args...>::operator-=(FunctionHandle<Result(Args...)>& function)
	{
		if (function.handle != 0)
		{
			for (std::map<int, FunctionHandle<Result(Args...)>>::iterator it = functions.begin(); it != functions.end(); it++)
			{
				if (it->first == function.handle)
				{
					functions.erase(it);
					break;
				}
			}
		}
		return *this;
	}

	template <typename Result, typename ... Args>
	void Event<Result, Args...>::operator()(Args... args)
	{
		for (const std::pair<int, FunctionHandle<Result(Args...)>>& eventFunction : functions)
		{
			eventFunction.second.function(args...);
		}
	}
}