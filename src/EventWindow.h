#pragma once
#include "stdafx.h"

#include "Keyboard.h"
#include "Mouse.h"

namespace ThroneEngine
{
	class EventWindow
	{
	public:

		struct SizeEvent
		{
			unsigned int width;
			unsigned int height;
		};

		struct KeyEvent
		{
			Keyboard::Key code;
			bool alt;
			bool control;
			bool shift;
			bool system;
		};

		struct TextEvent
		{
			unsigned int unicode;
		};

		struct MouseMoveEvent
		{
			int x;
			int y;
			int xRel;
			int yRel;
		};

		struct MouseButtonEvent
		{
			Mouse::Button button;
			int x;
			int y;
		};

		struct MouseWheelEvent
		{
			int delta;
			int x;
			int y;
		};

		struct MouseWheelScrollEvent
		{
			Mouse::Wheel wheel;
			float delta;
			int x;
			int y;
		};

		enum EventType
		{
			Closed,
			Resized,
			LostFocus,
			GainedFocus,
			TextEntered,
			KeyPressed,
			KeyReleased,
			MouseWheelMoved,
			MouseWheelScrolled,
			MouseButtonPressed,
			MouseButtonReleased,
			MouseMoved,
			MouseEntered,
			MouseLeft,
			SensorChanged,

			Count
		};

		EventType type;

		union
		{
			SizeEvent size;
			KeyEvent key;
			TextEvent text;
			MouseMoveEvent mouseMove;
			MouseButtonEvent mouseButton;
			MouseWheelEvent mouseWheel;
			MouseWheelScrollEvent mouseWheelScroll;
		};
	};
}
