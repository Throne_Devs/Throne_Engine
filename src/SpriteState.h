#pragma once
#include "stdafx.h"

#include "Sprite.h"

namespace ThroneEngine
{
	struct SpriteState
	{
		Sprite normal;
		Sprite highlightedSprite;
		Sprite pressedSprite;
		Sprite disabledSprite;
	};
}
