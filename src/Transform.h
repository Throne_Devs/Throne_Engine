#pragma once
#include "stdafx.h"

#include "Rect.h"

namespace ThroneEngine
{
	class Transform
	{
	public:
		Transform();
		Transform(const glm::vec3& position, const glm::vec3& angle = glm::vec3(0, 0, 0), glm::vec3& scaling = glm::vec3(1, 1, 1));
		~Transform();

		glm::mat4 matrix;

		void reset();

		void translate(const glm::vec3& translation);

		void rotate(const glm::vec3& angle);
		void rotate(const float xAngle, const float yAngle, const float zAngle);
		void rotate(const glm::vec3& axis, const float angle);

		void rotateAround(glm::vec3& angle, glm::vec3& position);

		void scale(glm::vec3& scaling);

		void lookAt(Transform& target);
		void lookAt(glm::vec3& position);

		void printPosition();

		static void getModelMatrixAttributeDesctription(std::vector<VkVertexInputAttributeDescription>& attributes, uint32 beginLocation);
		static void getModelMatrixBindingDescription(std::vector<VkVertexInputBindingDescription>& bindings);

		glm::vec2 transformPoint(float x, float y);
		glm::vec2 transformPoint(const glm::vec2& point);

		void setPosition(const glm::vec3& position);
		void setRotation(const glm::vec3& rotation);
		void setScale(const glm::vec3& scaling);

		glm::mat4 getViewMatrix();
		glm::vec3 getScale();
		glm::vec3 getEulerAngles() const;
		glm::vec3 getPosition() const;
		FloatRect transformRect(const FloatRect& rectangle);

		glm::vec4* right();
		glm::vec4* up();
		glm::vec4* forward();
		glm::vec4* position();
	};

	__forceinline glm::vec4* Transform::right()
	{
		return &matrix[0];
	}

	__forceinline glm::vec4* Transform::up()
	{
		return &matrix[1];
	}

	__forceinline glm::vec4* Transform::forward()
	{
		return &matrix[2];
	}

	__forceinline glm::vec4* Transform::position()
	{
		return &matrix[3];
	}
}
