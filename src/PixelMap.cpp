#include "stdafx.h"

#include "PixelMap.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <Stbi/stb_image_write.h>

namespace ThroneEngine
{
	PixelMap::PixelMap()
	{
	}

	PixelMap::~PixelMap()
	{
	}

	void PixelMap::initialize(unsigned int width, unsigned int height, const Color& color)
	{
		dimensionsInPixel = vec3(width, height, 1);

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				pixels.push_back(color.r);
				pixels.push_back(color.g);
				pixels.push_back(color.b);
				pixels.push_back(color.a);
			}
		}
	}

	void PixelMap::setPixel(unsigned int x, unsigned int y, const Color& color)
	{
		const int index = x + y * dimensionsInPixel.x * numberOfComponent;
		pixels[index + 0] = color.r;
		pixels[index + 1] = color.g;
		pixels[index + 2] = color.b;
		pixels[index + 3] = color.a;
	}

	void PixelMap::update(const PixelMap& pixelMap)
	{
		if (pixels.size() < pixelMap.pixels.size())
			pixels.resize(pixelMap.pixels.size());

		for (int i = 0; i < pixels.size() && i < pixelMap.pixels.size(); i++)
		{
			pixels[i] = pixelMap.pixels[i];
		}
	}

	void PixelMap::update(const std::vector<uint8>& pixels, unsigned int w, unsigned int h, unsigned int x, unsigned int y)
	{
		int sourceIncrement = 0;
		for (int i = y; i < y + h; i++)
		{
			const int index = (x * numberOfComponent) + i * dimensionsInPixel.x * numberOfComponent;
			std::memcpy(this->pixels.data() + index, pixels.data() + sourceIncrement, w * numberOfComponent);
			sourceIncrement += w * numberOfComponent;
		}
	}

	void PixelMap::swap(PixelMap& pixelMap) noexcept
	{
		std::swap(pixels, pixelMap.pixels);
		std::swap(dimensionsInPixel, pixelMap.dimensionsInPixel);
	}

	void PixelMap::resize(unsigned int newWidth, unsigned int newHeight)
	{
		std::vector<byte> oldPixels = pixels;
		const tvec3<uint32> oldSize = dimensionsInPixel;
		const tvec3<uint32> newSize = { newWidth, newHeight, 1 };

		pixels.clear();
		pixels.resize(newWidth * newHeight * numberOfComponent, 0);

		for (int dstRowIndex = 0, srcRowIndex = 0; srcRowIndex < oldSize.y; dstRowIndex++, srcRowIndex++)
		{
			std::memcpy(pixels.data() + (dstRowIndex * newSize.x * numberOfComponent),
						oldPixels.data() + (srcRowIndex * oldSize.x * numberOfComponent),
						oldSize.x * numberOfComponent);
		}

		dimensionsInPixel = newSize;
		pixelMapResized = true;
	}

	bool PixelMap::saveToFile(const std::string& filePath)
	{
		if (!pixels.empty() && dimensionsInPixel.x > 0 && dimensionsInPixel.y > 0)
		{
			const std::size_t dot = filePath.find_last_of('.');
			const std::string extension = filePath.substr(dot + 1, filePath.size() - dot);
			//resize(dimensionsInPixel.x + 1000, dimensionsInPixel.y + 100);
			if (extension == "bmp")
			{
				if (stbi_write_bmp(filePath.c_str(), dimensionsInPixel.x, dimensionsInPixel.y, numberOfComponent, &pixels[0]))
					return true;
			}
			if (extension == "tga")
			{
				if (stbi_write_tga(filePath.c_str(), dimensionsInPixel.x, dimensionsInPixel.y, numberOfComponent, &pixels[0]))
					return true;
			}
			if (extension == "png")
			{
				if (stbi_write_png(filePath.c_str(), dimensionsInPixel.x, dimensionsInPixel.y, numberOfComponent, &pixels[0], 0))
					return true;
			}
			if (extension == "jpg" || extension == "jpeg")
			{
				if (stbi_write_jpg(filePath.c_str(), dimensionsInPixel.x, dimensionsInPixel.y, numberOfComponent, &pixels[0], 0))
					return true;
			}
		}

		#if defined ERROR_MESSAGE_NEEDED
		ERROR_MESSAGE("Faile to save image " + filePath);
		#endif

		return false;
	}

	tvec3<uint32> PixelMap::getSize() const
	{
		return dimensionsInPixel;
	}

	int PixelMap::getMaximumSize()
	{
		//Todo: Changer le nombre par celui donner par vulkan
		return 8192;
	}
}
