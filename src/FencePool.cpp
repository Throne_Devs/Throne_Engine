#include "stdafx.h"

#include "FencePool.h"
#include "Fence.h"
#include "UtilityFunctions.hpp"

namespace ThroneEngine
{

	FencePool::FencePool()
	{
	}


	FencePool::~FencePool()
	{
	}

	bool FencePool::initialize(int numberOfFence)
	{
		return addFences(numberOfFence);
	}

	bool FencePool::addFences(int numberOfFence)
	{
		int beginIndex = fenceHandles.size() > 0 ? fenceHandles.size() - 1 : 0;
		fenceHandles.resize(fenceHandles.size() + numberOfFence);
		bool result = true;
		for (int i = beginIndex; i < fenceHandles.size(); i++)
		{
			result &= Fence::createFence(fenceHandles[i]);
		}
		return result == true;
	}

	bool FencePool::waitForFences(VkBool32 waitForAllFences, double timeout)
	{
		return Fence::waitForFences(fenceHandles.size(), getVectorDataPointer(fenceHandles), waitForAllFences, timeout);
	}

	bool FencePool::resetFences()
	{
		return Fence::resetFences(fenceHandles.size(), getVectorDataPointer(fenceHandles));
	}

}