#pragma once
#include "stdafx.h"

#include "ImageSampler.h"
#include "DescriptorSet.h"


namespace ThroneEngine
{
	class Material
	{
	public:
		Material();
		~Material();
	
		static DescriptorSetLayout materialDescriptorSetLayout;

		ImageSampler* imageSampler;
		float shineDamper;
		float reflectivity;
		DescriptorSet* descriptorSet;		

		uint32 handle;

		static void initializeStaticMaterialDescriptorSetLayout();

		//bool initialize(ImageSampler* imageSampler, float shineDamper, float reflectivity, DescriptorSet* descriptorSet);
		bool initialize(ImageSampler* imageSampler, float shineDamper, float reflectivity, uint32 descriptorSetLocation);


	private:
		static void initHandle(Material* material);
		static uint32 nextHandle;
	};
}



