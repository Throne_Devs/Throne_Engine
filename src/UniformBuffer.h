#pragma once
#include "stdafx.h"

#include "GpuBuffer.h"
#include "Descriptor.h"

namespace ThroneEngine
{
	class UniformBuffer : public GpuBuffer, public Descriptor
	{
	public:
		UniformBuffer();
		~UniformBuffer();

		bool initializeAsUniformBuffer(VkDeviceSize sizeInBytes, VkShaderStageFlags shaderStage, VkMemoryPropertyFlags memoryProperties, VkBufferCreateFlags additionalUsage = 0);
		bool initializeAsStorageBuffer(VkDeviceSize sizeInBytes, VkShaderStageFlags shaderStage, VkMemoryPropertyFlags memoryProperties, VkBufferCreateFlags additionalUsage = 0);

		void initializeWriteDescriptorSet(VkDescriptorSet descriptorSetHandle, uint32 binding);

		static VkDescriptorSetLayoutBinding getLayoutBinding(VkShaderStageFlags shaderStage);

	private:
		static bool createUniformBuffer(VkDeviceSize sizeInBytes, VkBufferCreateFlags usage, VkMemoryPropertyFlags memoryProperties, VkDescriptorType descriptorType, VkShaderStageFlags shaderStage, UniformBuffer& buffer);

		virtual void setDescriptorType() override;	
	};
}



