#pragma once
#include "VulkanFunctions.h"


namespace ThroneEngine
{
	class RenderPass;
	class FrameBuffer;


	class CommandBuffer
	{
	public:		
		CommandBuffer();
		~CommandBuffer();

		VkCommandBuffer handle = VK_NULL_HANDLE;

		bool beginRecordingOnPrimaryCommandBuffer(VkCommandBufferUsageFlags usage);
		bool beginRecordingOnSecondaryCommandBuffer(VkCommandBufferUsageFlags usage, RenderPass& renderPass, uint32 subpass, FrameBuffer& frameBuffer, VkBool32 occlusionQuerryEnable, VkQueryControlFlags queryFlags, VkQueryPipelineStatisticFlags pipelineStatistics);
		bool beginRecordingOnSecondaryCommandBuffer(VkCommandBufferUsageFlags usage, VkCommandBufferInheritanceInfo* inheritanceInfos);
		bool beginRecording(VkCommandBufferUsageFlags usage, VkCommandBufferInheritanceInfo* secondaryBufferInfo);
		bool endRecording();
		bool reset(VkCommandBufferResetFlags releaseResources);

		static bool createCommandBuffers(VkCommandPool commandPoolHandle, VkCommandBufferLevel level, std::vector<CommandBuffer>& commandBuffers);
		static bool createCommandBuffers(VkCommandPool commandPoolHandle, VkCommandBufferLevel level, std::vector<CommandBuffer>& commandBuffers, int beginIndex, int numberOfBuffer);


		void cmdPipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, uint32 memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32 bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32 imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers);
		void cmdCopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, uint32 regionCount, const VkBufferCopy* pRegions);
		void cmdCopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkBufferImageCopy* pRegions);
		void cmdCopyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32 regionCount, const VkBufferImageCopy* pRegions);
		void cmdBindDescriptorSets(VkPipelineBindPoint pipelineBindPoint, VkPipelineLayout layout, uint32 firstSet, uint32 descriptorSetCount, const VkDescriptorSet* pDescriptorSets, uint32 dynamicOffsetCount, const uint32* pDynamicOffsets);
		void cmdBeginRenderPass(const VkRenderPassBeginInfo* pRenderPassBegin, VkSubpassContents contents);
		void cmdNextSubpass(VkSubpassContents contents);
		void cmdEndRenderPass();
		void cmdBindPipeline(VkPipelineBindPoint pipelineBindPoint, VkPipeline pipeline);
		void cmdSetViewport(uint32 firstViewport, uint32 viewportCount, const VkViewport* pViewports);
		void cmdSetScissor(uint32 firstScissor, uint32 scissorCount, const VkRect2D* pScissors);
		void cmdBindVertexBuffers(uint32 firstBinding, uint32 bindingCount, const VkBuffer* pBuffers, const VkDeviceSize* pOffsets);
		void cmdDraw(uint32 vertexCount, uint32 instanceCount, uint32 firstVertex, uint32 firstInstance);
		void cmdDrawIndexed(uint32 indexCount, uint32 instanceCount, uint32 firstIndex, int32 vertexOffset, uint32 firstInstance);
		void cmdDispatch(uint32 groupCountX, uint32 groupCountY, uint32 groupCountZ);
		void cmdCopyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageCopy* pRegions);
		void cmdPushConstants(VkPipelineLayout layout, VkShaderStageFlags stageFlags, uint32 offset, uint32 size, const void* pValues);
		void cmdClearColorImage(VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, uint32 rangeCount, const VkImageSubresourceRange* pRanges);
		void cmdClearDepthStencilImage(VkImage image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, uint32 rangeCount, const VkImageSubresourceRange* pRanges);
		void cmdBindIndexBuffer(VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType);
		void cmdSetLineWidth(float lineWidth);
		void cmdSetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);
		void cmdSetBlendConstants(const float blendConstants[4]);
		void cmdExecuteCommands(uint32 commandBufferCount, const VkCommandBuffer* pCommandBuffers);
		void cmdClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount, const VkClearRect* pRects);	
	};


	inline void CommandBuffer::cmdPipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, uint32 memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32 bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32 imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers)
	{
		funcptr_vkCmdPipelineBarrier(handle, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
	}

	inline void CommandBuffer::cmdBindDescriptorSets(VkPipelineBindPoint pipelineBindPoint, VkPipelineLayout layout, uint32 firstSet, uint32 descriptorSetCount, const VkDescriptorSet* pDescriptorSets, uint32 dynamicOffsetCount, const uint32* pDynamicOffsets)
	{
		funcptr_vkCmdBindDescriptorSets(handle, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets);
	}

	inline void CommandBuffer::cmdBeginRenderPass(const VkRenderPassBeginInfo* pRenderPassBegin, VkSubpassContents contents)
	{
		funcptr_vkCmdBeginRenderPass(handle, pRenderPassBegin, contents);
	}

	inline void CommandBuffer::cmdNextSubpass(VkSubpassContents contents)
	{
		funcptr_vkCmdNextSubpass(handle, contents);
	}

	inline void CommandBuffer::cmdBindPipeline(VkPipelineBindPoint pipelineBindPoint, VkPipeline pipeline)
	{
		funcptr_vkCmdBindPipeline(handle, pipelineBindPoint, pipeline);
	}

	inline void CommandBuffer::cmdSetViewport(uint32 firstViewport, uint32 viewportCount, const VkViewport* pViewports)
	{
		funcptr_vkCmdSetViewport(handle, firstViewport, viewportCount, pViewports);
	}

	inline void CommandBuffer::cmdSetScissor(uint32 firstScissor, uint32 scissorCount, const VkRect2D* pScissors)
	{
		funcptr_vkCmdSetScissor(handle, firstScissor, scissorCount, pScissors);
	}

	inline void CommandBuffer::cmdDraw(uint32 vertexCount, uint32 instanceCount, uint32 firstVertex, uint32 firstInstance)
	{
		funcptr_vkCmdDraw(handle, vertexCount, instanceCount, firstVertex, firstInstance);
	}

	inline void CommandBuffer::cmdDrawIndexed(uint32 indexCount, uint32 instanceCount, uint32 firstIndex, int32 vertexOffset, uint32 firstInstance)
	{
		funcptr_vkCmdDrawIndexed(handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
	}

	inline void CommandBuffer::cmdDispatch(uint32 groupCountX, uint32 groupCountY, uint32 groupCountZ)
	{
		funcptr_vkCmdDispatch(handle, groupCountX, groupCountY, groupCountZ);
	}

	inline void CommandBuffer::cmdCopyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageCopy* pRegions)
	{
		funcptr_vkCmdCopyImage(handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
	}

	inline void CommandBuffer::cmdPushConstants(VkPipelineLayout layout, VkShaderStageFlags stageFlags, uint32 offset, uint32 size, const void* pValues)
	{
		funcptr_vkCmdPushConstants(handle, layout, stageFlags, offset, size, pValues);
	}

	inline void CommandBuffer::cmdClearColorImage(VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, uint32 rangeCount, const VkImageSubresourceRange* pRanges)
	{
		funcptr_vkCmdClearColorImage(handle, image, imageLayout, pColor, rangeCount, pRanges);
	}

	inline void CommandBuffer::cmdClearDepthStencilImage(VkImage image, VkImageLayout imageLayout, const VkClearDepthStencilValue* pDepthStencil, uint32 rangeCount, const VkImageSubresourceRange* pRanges)
	{
		funcptr_vkCmdClearDepthStencilImage(handle, image, imageLayout, pDepthStencil, rangeCount, pRanges);
	}

	inline void CommandBuffer::cmdEndRenderPass()
	{
		funcptr_vkCmdEndRenderPass(handle);
	}

	inline void CommandBuffer::cmdCopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, uint32 regionCount, const VkBufferCopy* pRegions)
	{
		funcptr_vkCmdCopyBuffer(handle, srcBuffer, dstBuffer, regionCount, pRegions);
	}

	inline void CommandBuffer::cmdCopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkBufferImageCopy* pRegions)
	{
		funcptr_vkCmdCopyBufferToImage(handle, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
	}

	inline void CommandBuffer::cmdCopyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32 regionCount, const VkBufferImageCopy* pRegions)
	{
		funcptr_vkCmdCopyImageToBuffer(handle, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);
	}

	inline void CommandBuffer::cmdBindVertexBuffers(uint32 firstBinding, uint32 bindingCount, const VkBuffer* pBuffers, const VkDeviceSize* pOffsets)
	{
		funcptr_vkCmdBindVertexBuffers(handle, firstBinding, bindingCount, pBuffers, pOffsets);
	}

	inline void CommandBuffer::cmdBindIndexBuffer(VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType)
	{
		funcptr_vkCmdBindIndexBuffer(handle, buffer, offset, indexType);
	}

	inline void CommandBuffer::cmdSetLineWidth(float lineWidth)
	{
		funcptr_vkCmdSetLineWidth(handle, lineWidth);
	}

	inline void CommandBuffer::cmdSetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor)
	{
		funcptr_vkCmdSetDepthBias(handle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
	}

	inline void CommandBuffer::cmdSetBlendConstants(const float blendConstants[4])
	{
		funcptr_vkCmdSetBlendConstants(handle, blendConstants);
	}

	inline void CommandBuffer::cmdExecuteCommands(uint32 commandBufferCount, const VkCommandBuffer* pCommandBuffers)
	{
		funcptr_vkCmdExecuteCommands(handle, commandBufferCount, pCommandBuffers);
	}

	inline void CommandBuffer::cmdClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount, const VkClearRect* pRects)
	{
		funcptr_vkCmdClearAttachments(handle, attachmentCount, pAttachments, rectCount, pRects);
	}
}


