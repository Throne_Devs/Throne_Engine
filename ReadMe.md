# Throne Engine
This project is a game engine prototype made in C++ using the Vulkan API.  
The project is still in early development and is being developped as a hobby.

## Getting Started:
* First step is to make sure your hardware supports Vulkan.  
A list of Vulkan-supported GPUs can be found here : https://vulkan.gpuinfo.org/

* Next install the LunarG SDK. This SDK provides development and runtime components for building, running and debugging Vulkan applications.  
Download link found here : https://www.lunarg.com/vulkan-sdk/

* Last step is to install a suitable C++ IDE such as Visual Studio.  
Download link found here : https://www.visualstudio.com/downloads/

## Prerequisites:
* LunarG SDK
* C++ compiler
* Hardware that supports the latest version of the Vulkan API.

## Installing:
* Follow the installation instructions provided with the recommended softwares and open the project in Visual Studio.

## Required Vulkan Extensions:

### For outputing to a window:
* VK_KHR_surface
* VK_KHR_surface_win32_surface
* VK_KHR_swapchain

### For debug purposes:
* VK_EXT_debug_report

## Deployment:
* Launch the exe on a Windows Vulkan-supported device.

## Built With:
* [LunarG SDK] (https://www.lunarg.com/vulkan-sdk/) - Development SDK
* [GLM] (https://glm.g-truc.net/0.9.8/index.html) - C++ mathematics library

## Contributing:
Message us directly if you wish to contribute to the project.

## Authors:
* **Félix Lamontagne**
* **Louis-Philippe Hudon**
* **Lucas Desjardins**

## License:
This project is licensed under the MIT License - see the [License.md] (License.md) file for details.