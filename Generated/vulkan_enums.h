#pragma once

#include "Vulkan\vulkan.h"

namespace ThroneEngine
{
    inline std::string to_string(VkPipelineCacheHeaderVersion value)
    {
        switch(value)
        {
            case VK_PIPELINE_CACHE_HEADER_VERSION_ONE: return "one";
            case VK_PIPELINE_CACHE_HEADER_VERSION_BEGIN_RANGE: return "beginRange";
            case VK_PIPELINE_CACHE_HEADER_VERSION_END_RANGE: return "endRange";
            case VK_PIPELINE_CACHE_HEADER_VERSION_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkResult value)
    {
        switch(value)
        {
            case VK_SUCCESS: return "success";
            case VK_NOT_READY: return "notReady";
            case VK_TIMEOUT: return "timeout";
            case VK_EVENT_SET: return "eventSet";
            case VK_EVENT_RESET: return "eventReset";
            case VK_INCOMPLETE: return "incomplete";
            case VK_ERROR_OUT_OF_HOST_MEMORY: return "errorOutOfHostMemory";
            case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "errorOutOfDeviceMemory";
            case VK_ERROR_INITIALIZATION_FAILED: return "errorInitializationFailed";
            case VK_ERROR_DEVICE_LOST: return "errorDeviceLost";
            case VK_ERROR_MEMORY_MAP_FAILED: return "errorMemoryMapFailed";
            case VK_ERROR_LAYER_NOT_PRESENT: return "errorLayerNotPresent";
            case VK_ERROR_EXTENSION_NOT_PRESENT: return "errorExtensionNotPresent";
            case VK_ERROR_FEATURE_NOT_PRESENT: return "errorFeatureNotPresent";
            case VK_ERROR_INCOMPATIBLE_DRIVER: return "errorIncompatibleDriver";
            case VK_ERROR_TOO_MANY_OBJECTS: return "errorTooManyObjects";
            case VK_ERROR_FORMAT_NOT_SUPPORTED: return "errorFormatNotSupported";
            case VK_ERROR_FRAGMENTED_POOL: return "errorFragmentedPool";
            case VK_ERROR_OUT_OF_POOL_MEMORY: return "errorOutOfPoolMemory";
            case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "errorInvalidExternalHandle";
            case VK_ERROR_SURFACE_LOST_KHR: return "errorSurfaceLostKhr";
            case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "errorNativeWindowInUseKhr";
            case VK_SUBOPTIMAL_KHR: return "suboptimalKhr";
            case VK_ERROR_OUT_OF_DATE_KHR: return "errorOutOfDateKhr";
            case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "errorIncompatibleDisplayKhr";
            case VK_ERROR_VALIDATION_FAILED_EXT: return "errorValidationFailedExt";
            case VK_ERROR_INVALID_SHADER_NV: return "errorInvalidShaderNv";
            case VK_ERROR_NOT_PERMITTED_EXT: return "errorNotPermittedExt";
            case VK_ERROR_OUT_OF_POOL_MEMORY_KHR: return "errorOutOfPoolMemoryKhr";
            case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR: return "errorInvalidExternalHandleKhr";
            case VK_RESULT_BEGIN_RANGE: return "beginRange";
            case VK_RESULT_END_RANGE: return "endRange";
            case VK_RESULT_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkStructureType value)
    {
        switch(value)
        {
            case VK_STRUCTURE_TYPE_APPLICATION_INFO: return "applicationInfo";
            case VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO: return "instanceCreateInfo";
            case VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO: return "deviceQueueCreateInfo";
            case VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO: return "deviceCreateInfo";
            case VK_STRUCTURE_TYPE_SUBMIT_INFO: return "submitInfo";
            case VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO: return "memoryAllocateInfo";
            case VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE: return "mappedMemoryRange";
            case VK_STRUCTURE_TYPE_BIND_SPARSE_INFO: return "bindSparseInfo";
            case VK_STRUCTURE_TYPE_FENCE_CREATE_INFO: return "fenceCreateInfo";
            case VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO: return "semaphoreCreateInfo";
            case VK_STRUCTURE_TYPE_EVENT_CREATE_INFO: return "eventCreateInfo";
            case VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO: return "queryPoolCreateInfo";
            case VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO: return "bufferCreateInfo";
            case VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO: return "bufferViewCreateInfo";
            case VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO: return "imageCreateInfo";
            case VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO: return "imageViewCreateInfo";
            case VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO: return "shaderModuleCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO: return "pipelineCacheCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO: return "pipelineShaderStageCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO: return "pipelineVertexInputStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO: return "pipelineInputAssemblyStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO: return "pipelineTessellationStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO: return "pipelineViewportStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO: return "pipelineRasterizationStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO: return "pipelineMultisampleStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO: return "pipelineDepthStencilStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO: return "pipelineColorBlendStateCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO: return "pipelineDynamicStateCreateInfo";
            case VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO: return "graphicsPipelineCreateInfo";
            case VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO: return "computePipelineCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO: return "pipelineLayoutCreateInfo";
            case VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO: return "samplerCreateInfo";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO: return "descriptorSetLayoutCreateInfo";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO: return "descriptorPoolCreateInfo";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO: return "descriptorSetAllocateInfo";
            case VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET: return "writeDescriptorSet";
            case VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET: return "copyDescriptorSet";
            case VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO: return "framebufferCreateInfo";
            case VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO: return "renderPassCreateInfo";
            case VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO: return "commandPoolCreateInfo";
            case VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO: return "commandBufferAllocateInfo";
            case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO: return "commandBufferInheritanceInfo";
            case VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO: return "commandBufferBeginInfo";
            case VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO: return "renderPassBeginInfo";
            case VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER: return "bufferMemoryBarrier";
            case VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER: return "imageMemoryBarrier";
            case VK_STRUCTURE_TYPE_MEMORY_BARRIER: return "memoryBarrier";
            case VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO: return "loaderInstanceCreateInfo";
            case VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO: return "loaderDeviceCreateInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES: return "physicalDeviceSubgroupProperties";
            case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO: return "bindBufferMemoryInfo";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO: return "bindImageMemoryInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES: return "physicalDevice16bitStorageFeatures";
            case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS: return "memoryDedicatedRequirements";
            case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO: return "memoryDedicatedAllocateInfo";
            case VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO: return "memoryAllocateFlagsInfo";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO: return "deviceGroupRenderPassBeginInfo";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO: return "deviceGroupCommandBufferBeginInfo";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO: return "deviceGroupSubmitInfo";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO: return "deviceGroupBindSparseInfo";
            case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO: return "bindBufferMemoryDeviceGroupInfo";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO: return "bindImageMemoryDeviceGroupInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES: return "physicalDeviceGroupProperties";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO: return "deviceGroupDeviceCreateInfo";
            case VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2: return "bufferMemoryRequirementsInfo2";
            case VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2: return "imageMemoryRequirementsInfo2";
            case VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2: return "imageSparseMemoryRequirementsInfo2";
            case VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2: return "memoryRequirements2";
            case VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2: return "sparseImageMemoryRequirements2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2: return "physicalDeviceFeatures2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2: return "physicalDeviceProperties2";
            case VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2: return "formatProperties2";
            case VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2: return "imageFormatProperties2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2: return "physicalDeviceImageFormatInfo2";
            case VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2: return "queueFamilyProperties2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2: return "physicalDeviceMemoryProperties2";
            case VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2: return "sparseImageFormatProperties2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2: return "physicalDeviceSparseImageFormatInfo2";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES: return "physicalDevicePointClippingProperties";
            case VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO: return "renderPassInputAttachmentAspectCreateInfo";
            case VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO: return "imageViewUsageCreateInfo";
            case VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO: return "pipelineTessellationDomainOriginStateCreateInfo";
            case VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO: return "renderPassMultiviewCreateInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES: return "physicalDeviceMultiviewFeatures";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES: return "physicalDeviceMultiviewProperties";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES: return "physicalDeviceVariablePointerFeatures";
            case VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO: return "protectedSubmitInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES: return "physicalDeviceProtectedMemoryFeatures";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES: return "physicalDeviceProtectedMemoryProperties";
            case VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2: return "deviceQueueInfo2";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO: return "samplerYcbcrConversionCreateInfo";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO: return "samplerYcbcrConversionInfo";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO: return "bindImagePlaneMemoryInfo";
            case VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO: return "imagePlaneMemoryRequirementsInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES: return "physicalDeviceSamplerYcbcrConversionFeatures";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES: return "samplerYcbcrConversionImageFormatProperties";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO: return "descriptorUpdateTemplateCreateInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO: return "physicalDeviceExternalImageFormatInfo";
            case VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES: return "externalImageFormatProperties";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO: return "physicalDeviceExternalBufferInfo";
            case VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES: return "externalBufferProperties";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES: return "physicalDeviceIdProperties";
            case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO: return "externalMemoryBufferCreateInfo";
            case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO: return "externalMemoryImageCreateInfo";
            case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO: return "exportMemoryAllocateInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO: return "physicalDeviceExternalFenceInfo";
            case VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES: return "externalFenceProperties";
            case VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO: return "exportFenceCreateInfo";
            case VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO: return "exportSemaphoreCreateInfo";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO: return "physicalDeviceExternalSemaphoreInfo";
            case VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES: return "externalSemaphoreProperties";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES: return "physicalDeviceMaintenance3Properties";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT: return "descriptorSetLayoutSupport";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETER_FEATURES: return "physicalDeviceShaderDrawParameterFeatures";
            case VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR: return "swapchainCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PRESENT_INFO_KHR: return "presentInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR: return "deviceGroupPresentCapabilitiesKhr";
            case VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR: return "imageSwapchainCreateInfoKhr";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR: return "bindImageMemorySwapchainInfoKhr";
            case VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR: return "acquireNextImageInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR: return "deviceGroupPresentInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR: return "deviceGroupSwapchainCreateInfoKhr";
            case VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR: return "displayModeCreateInfoKhr";
            case VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR: return "displaySurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR: return "displayPresentInfoKhr";
            case VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR: return "xlibSurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR: return "xcbSurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR: return "waylandSurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_MIR_SURFACE_CREATE_INFO_KHR: return "mirSurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR: return "androidSurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR: return "win32SurfaceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT: return "debugReportCallbackCreateInfoExt";
            case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD: return "pipelineRasterizationStateRasterizationOrderAmd";
            case VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT: return "debugMarkerObjectNameInfoExt";
            case VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT: return "debugMarkerObjectTagInfoExt";
            case VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT: return "debugMarkerMarkerInfoExt";
            case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV: return "dedicatedAllocationImageCreateInfoNv";
            case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV: return "dedicatedAllocationBufferCreateInfoNv";
            case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV: return "dedicatedAllocationMemoryAllocateInfoNv";
            case VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD: return "textureLodGatherFormatPropertiesAmd";
            case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV: return "externalMemoryImageCreateInfoNv";
            case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV: return "exportMemoryAllocateInfoNv";
            case VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV: return "importMemoryWin32HandleInfoNv";
            case VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV: return "exportMemoryWin32HandleInfoNv";
            case VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV: return "win32KeyedMutexAcquireReleaseInfoNv";
            case VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT: return "validationFlagsExt";
            case VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN: return "viSurfaceCreateInfoNn";
            case VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR: return "importMemoryWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR: return "exportMemoryWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR: return "memoryWin32HandlePropertiesKhr";
            case VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR: return "memoryGetWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR: return "importMemoryFdInfoKhr";
            case VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR: return "memoryFdPropertiesKhr";
            case VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR: return "memoryGetFdInfoKhr";
            case VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR: return "win32KeyedMutexAcquireReleaseInfoKhr";
            case VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: return "importSemaphoreWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: return "exportSemaphoreWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR: return "d3d12FenceSubmitInfoKhr";
            case VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR: return "semaphoreGetWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR: return "importSemaphoreFdInfoKhr";
            case VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR: return "semaphoreGetFdInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR: return "physicalDevicePushDescriptorPropertiesKhr";
            case VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR: return "presentRegionsKhr";
            case VK_STRUCTURE_TYPE_OBJECT_TABLE_CREATE_INFO_NVX: return "objectTableCreateInfoNvx";
            case VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NVX: return "indirectCommandsLayoutCreateInfoNvx";
            case VK_STRUCTURE_TYPE_CMD_PROCESS_COMMANDS_INFO_NVX: return "cmdProcessCommandsInfoNvx";
            case VK_STRUCTURE_TYPE_CMD_RESERVE_SPACE_FOR_COMMANDS_INFO_NVX: return "cmdReserveSpaceForCommandsInfoNvx";
            case VK_STRUCTURE_TYPE_DEVICE_GENERATED_COMMANDS_LIMITS_NVX: return "deviceGeneratedCommandsLimitsNvx";
            case VK_STRUCTURE_TYPE_DEVICE_GENERATED_COMMANDS_FEATURES_NVX: return "deviceGeneratedCommandsFeaturesNvx";
            case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV: return "pipelineViewportWScalingStateCreateInfoNv";
            case VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT: return "surfaceCapabilities2Ext";
            case VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT: return "displayPowerInfoExt";
            case VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT: return "deviceEventInfoExt";
            case VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT: return "displayEventInfoExt";
            case VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT: return "swapchainCounterCreateInfoExt";
            case VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE: return "presentTimesInfoGoogle";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX: return "physicalDeviceMultiviewPerViewAttributesPropertiesNvx";
            case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV: return "pipelineViewportSwizzleStateCreateInfoNv";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT: return "physicalDeviceDiscardRectanglePropertiesExt";
            case VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT: return "pipelineDiscardRectangleStateCreateInfoExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT: return "physicalDeviceConservativeRasterizationPropertiesExt";
            case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT: return "pipelineRasterizationConservativeStateCreateInfoExt";
            case VK_STRUCTURE_TYPE_HDR_METADATA_EXT: return "hdrMetadataExt";
            case VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR: return "sharedPresentSurfaceCapabilitiesKhr";
            case VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR: return "importFenceWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR: return "exportFenceWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR: return "fenceGetWin32HandleInfoKhr";
            case VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR: return "importFenceFdInfoKhr";
            case VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR: return "fenceGetFdInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR: return "physicalDeviceSurfaceInfo2Khr";
            case VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR: return "surfaceCapabilities2Khr";
            case VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR: return "surfaceFormat2Khr";
            case VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK: return "iosSurfaceCreateInfoMvk";
            case VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK: return "macosSurfaceCreateInfoMvk";
            case VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT: return "debugUtilsObjectNameInfoExt";
            case VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT: return "debugUtilsObjectTagInfoExt";
            case VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT: return "debugUtilsLabelExt";
            case VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT: return "debugUtilsMessengerCallbackDataExt";
            case VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT: return "debugUtilsMessengerCreateInfoExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES_EXT: return "physicalDeviceSamplerFilterMinmaxPropertiesExt";
            case VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO_EXT: return "samplerReductionModeCreateInfoExt";
            case VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT: return "sampleLocationsInfoExt";
            case VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT: return "renderPassSampleLocationsBeginInfoExt";
            case VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT: return "pipelineSampleLocationsStateCreateInfoExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT: return "physicalDeviceSampleLocationsPropertiesExt";
            case VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT: return "multisamplePropertiesExt";
            case VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO_KHR: return "imageFormatListCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT: return "physicalDeviceBlendOperationAdvancedFeaturesExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT: return "physicalDeviceBlendOperationAdvancedPropertiesExt";
            case VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT: return "pipelineColorBlendAdvancedStateCreateInfoExt";
            case VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV: return "pipelineCoverageToColorStateCreateInfoNv";
            case VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV: return "pipelineCoverageModulationStateCreateInfoNv";
            case VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT: return "validationCacheCreateInfoExt";
            case VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT: return "shaderModuleValidationCacheCreateInfoExt";
            case VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT: return "deviceQueueGlobalPriorityCreateInfoExt";
            case VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT: return "importMemoryHostPointerInfoExt";
            case VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT: return "memoryHostPointerPropertiesExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT: return "physicalDeviceExternalMemoryHostPropertiesExt";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT: return "physicalDeviceVertexAttributeDivisorPropertiesExt";
            case VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT: return "pipelineVertexInputDivisorStateCreateInfoExt";
            case VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO_KHR: return "renderPassMultiviewCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR: return "physicalDeviceMultiviewFeaturesKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES_KHR: return "physicalDeviceMultiviewPropertiesKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2_KHR: return "physicalDeviceFeatures2Khr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2_KHR: return "physicalDeviceProperties2Khr";
            case VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2_KHR: return "formatProperties2Khr";
            case VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2_KHR: return "imageFormatProperties2Khr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2_KHR: return "physicalDeviceImageFormatInfo2Khr";
            case VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2_KHR: return "queueFamilyProperties2Khr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2_KHR: return "physicalDeviceMemoryProperties2Khr";
            case VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2_KHR: return "sparseImageFormatProperties2Khr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2_KHR: return "physicalDeviceSparseImageFormatInfo2Khr";
            case VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO_KHR: return "memoryAllocateFlagsInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO_KHR: return "deviceGroupRenderPassBeginInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO_KHR: return "deviceGroupCommandBufferBeginInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO_KHR: return "deviceGroupSubmitInfoKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO_KHR: return "deviceGroupBindSparseInfoKhr";
            case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO_KHR: return "bindBufferMemoryDeviceGroupInfoKhr";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO_KHR: return "bindImageMemoryDeviceGroupInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES_KHR: return "physicalDeviceGroupPropertiesKhr";
            case VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO_KHR: return "deviceGroupDeviceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO_KHR: return "physicalDeviceExternalImageFormatInfoKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES_KHR: return "externalImageFormatPropertiesKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO_KHR: return "physicalDeviceExternalBufferInfoKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES_KHR: return "externalBufferPropertiesKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES_KHR: return "physicalDeviceIdPropertiesKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO_KHR: return "externalMemoryBufferCreateInfoKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_KHR: return "externalMemoryImageCreateInfoKhr";
            case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_KHR: return "exportMemoryAllocateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO_KHR: return "physicalDeviceExternalSemaphoreInfoKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES_KHR: return "externalSemaphorePropertiesKhr";
            case VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO_KHR: return "exportSemaphoreCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES_KHR: return "physicalDevice16bitStorageFeaturesKhr";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO_KHR: return "descriptorUpdateTemplateCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO_KHR: return "physicalDeviceExternalFenceInfoKhr";
            case VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES_KHR: return "externalFencePropertiesKhr";
            case VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO_KHR: return "exportFenceCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES_KHR: return "physicalDevicePointClippingPropertiesKhr";
            case VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO_KHR: return "renderPassInputAttachmentAspectCreateInfoKhr";
            case VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO_KHR: return "imageViewUsageCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO_KHR: return "pipelineTessellationDomainOriginStateCreateInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES_KHR: return "physicalDeviceVariablePointerFeaturesKhr";
            case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS_KHR: return "memoryDedicatedRequirementsKhr";
            case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO_KHR: return "memoryDedicatedAllocateInfoKhr";
            case VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2_KHR: return "bufferMemoryRequirementsInfo2Khr";
            case VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2_KHR: return "imageMemoryRequirementsInfo2Khr";
            case VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2_KHR: return "imageSparseMemoryRequirementsInfo2Khr";
            case VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2_KHR: return "memoryRequirements2Khr";
            case VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2_KHR: return "sparseImageMemoryRequirements2Khr";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO_KHR: return "samplerYcbcrConversionCreateInfoKhr";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO_KHR: return "samplerYcbcrConversionInfoKhr";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO_KHR: return "bindImagePlaneMemoryInfoKhr";
            case VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO_KHR: return "imagePlaneMemoryRequirementsInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES_KHR: return "physicalDeviceSamplerYcbcrConversionFeaturesKhr";
            case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES_KHR: return "samplerYcbcrConversionImageFormatPropertiesKhr";
            case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO_KHR: return "bindBufferMemoryInfoKhr";
            case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO_KHR: return "bindImageMemoryInfoKhr";
            case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES_KHR: return "physicalDeviceMaintenance3PropertiesKhr";
            case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT_KHR: return "descriptorSetLayoutSupportKhr";
            case VK_STRUCTURE_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_STRUCTURE_TYPE_END_RANGE: return "endRange";
            case VK_STRUCTURE_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSystemAllocationScope value)
    {
        switch(value)
        {
            case VK_SYSTEM_ALLOCATION_SCOPE_COMMAND: return "command";
            case VK_SYSTEM_ALLOCATION_SCOPE_OBJECT: return "object";
            case VK_SYSTEM_ALLOCATION_SCOPE_CACHE: return "cache";
            case VK_SYSTEM_ALLOCATION_SCOPE_DEVICE: return "device";
            case VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE: return "instance";
            case VK_SYSTEM_ALLOCATION_SCOPE_BEGIN_RANGE: return "beginRange";
            case VK_SYSTEM_ALLOCATION_SCOPE_END_RANGE: return "endRange";
            case VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkInternalAllocationType value)
    {
        switch(value)
        {
            case VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE: return "executable";
            case VK_INTERNAL_ALLOCATION_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_INTERNAL_ALLOCATION_TYPE_END_RANGE: return "endRange";
            case VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFormat value)
    {
        switch(value)
        {
            case VK_FORMAT_UNDEFINED: return "undefined";
            case VK_FORMAT_R4G4_UNORM_PACK8: return "r4g4UnormPack8";
            case VK_FORMAT_R4G4B4A4_UNORM_PACK16: return "r4g4b4a4UnormPack16";
            case VK_FORMAT_B4G4R4A4_UNORM_PACK16: return "b4g4r4a4UnormPack16";
            case VK_FORMAT_R5G6B5_UNORM_PACK16: return "r5g6b5UnormPack16";
            case VK_FORMAT_B5G6R5_UNORM_PACK16: return "b5g6r5UnormPack16";
            case VK_FORMAT_R5G5B5A1_UNORM_PACK16: return "r5g5b5a1UnormPack16";
            case VK_FORMAT_B5G5R5A1_UNORM_PACK16: return "b5g5r5a1UnormPack16";
            case VK_FORMAT_A1R5G5B5_UNORM_PACK16: return "a1r5g5b5UnormPack16";
            case VK_FORMAT_R8_UNORM: return "r8Unorm";
            case VK_FORMAT_R8_SNORM: return "r8Snorm";
            case VK_FORMAT_R8_USCALED: return "r8Uscaled";
            case VK_FORMAT_R8_SSCALED: return "r8Sscaled";
            case VK_FORMAT_R8_UINT: return "r8Uint";
            case VK_FORMAT_R8_SINT: return "r8Sint";
            case VK_FORMAT_R8_SRGB: return "r8Srgb";
            case VK_FORMAT_R8G8_UNORM: return "r8g8Unorm";
            case VK_FORMAT_R8G8_SNORM: return "r8g8Snorm";
            case VK_FORMAT_R8G8_USCALED: return "r8g8Uscaled";
            case VK_FORMAT_R8G8_SSCALED: return "r8g8Sscaled";
            case VK_FORMAT_R8G8_UINT: return "r8g8Uint";
            case VK_FORMAT_R8G8_SINT: return "r8g8Sint";
            case VK_FORMAT_R8G8_SRGB: return "r8g8Srgb";
            case VK_FORMAT_R8G8B8_UNORM: return "r8g8b8Unorm";
            case VK_FORMAT_R8G8B8_SNORM: return "r8g8b8Snorm";
            case VK_FORMAT_R8G8B8_USCALED: return "r8g8b8Uscaled";
            case VK_FORMAT_R8G8B8_SSCALED: return "r8g8b8Sscaled";
            case VK_FORMAT_R8G8B8_UINT: return "r8g8b8Uint";
            case VK_FORMAT_R8G8B8_SINT: return "r8g8b8Sint";
            case VK_FORMAT_R8G8B8_SRGB: return "r8g8b8Srgb";
            case VK_FORMAT_B8G8R8_UNORM: return "b8g8r8Unorm";
            case VK_FORMAT_B8G8R8_SNORM: return "b8g8r8Snorm";
            case VK_FORMAT_B8G8R8_USCALED: return "b8g8r8Uscaled";
            case VK_FORMAT_B8G8R8_SSCALED: return "b8g8r8Sscaled";
            case VK_FORMAT_B8G8R8_UINT: return "b8g8r8Uint";
            case VK_FORMAT_B8G8R8_SINT: return "b8g8r8Sint";
            case VK_FORMAT_B8G8R8_SRGB: return "b8g8r8Srgb";
            case VK_FORMAT_R8G8B8A8_UNORM: return "r8g8b8a8Unorm";
            case VK_FORMAT_R8G8B8A8_SNORM: return "r8g8b8a8Snorm";
            case VK_FORMAT_R8G8B8A8_USCALED: return "r8g8b8a8Uscaled";
            case VK_FORMAT_R8G8B8A8_SSCALED: return "r8g8b8a8Sscaled";
            case VK_FORMAT_R8G8B8A8_UINT: return "r8g8b8a8Uint";
            case VK_FORMAT_R8G8B8A8_SINT: return "r8g8b8a8Sint";
            case VK_FORMAT_R8G8B8A8_SRGB: return "r8g8b8a8Srgb";
            case VK_FORMAT_B8G8R8A8_UNORM: return "b8g8r8a8Unorm";
            case VK_FORMAT_B8G8R8A8_SNORM: return "b8g8r8a8Snorm";
            case VK_FORMAT_B8G8R8A8_USCALED: return "b8g8r8a8Uscaled";
            case VK_FORMAT_B8G8R8A8_SSCALED: return "b8g8r8a8Sscaled";
            case VK_FORMAT_B8G8R8A8_UINT: return "b8g8r8a8Uint";
            case VK_FORMAT_B8G8R8A8_SINT: return "b8g8r8a8Sint";
            case VK_FORMAT_B8G8R8A8_SRGB: return "b8g8r8a8Srgb";
            case VK_FORMAT_A8B8G8R8_UNORM_PACK32: return "a8b8g8r8UnormPack32";
            case VK_FORMAT_A8B8G8R8_SNORM_PACK32: return "a8b8g8r8SnormPack32";
            case VK_FORMAT_A8B8G8R8_USCALED_PACK32: return "a8b8g8r8UscaledPack32";
            case VK_FORMAT_A8B8G8R8_SSCALED_PACK32: return "a8b8g8r8SscaledPack32";
            case VK_FORMAT_A8B8G8R8_UINT_PACK32: return "a8b8g8r8UintPack32";
            case VK_FORMAT_A8B8G8R8_SINT_PACK32: return "a8b8g8r8SintPack32";
            case VK_FORMAT_A8B8G8R8_SRGB_PACK32: return "a8b8g8r8SrgbPack32";
            case VK_FORMAT_A2R10G10B10_UNORM_PACK32: return "a2r10g10b10UnormPack32";
            case VK_FORMAT_A2R10G10B10_SNORM_PACK32: return "a2r10g10b10SnormPack32";
            case VK_FORMAT_A2R10G10B10_USCALED_PACK32: return "a2r10g10b10UscaledPack32";
            case VK_FORMAT_A2R10G10B10_SSCALED_PACK32: return "a2r10g10b10SscaledPack32";
            case VK_FORMAT_A2R10G10B10_UINT_PACK32: return "a2r10g10b10UintPack32";
            case VK_FORMAT_A2R10G10B10_SINT_PACK32: return "a2r10g10b10SintPack32";
            case VK_FORMAT_A2B10G10R10_UNORM_PACK32: return "a2b10g10r10UnormPack32";
            case VK_FORMAT_A2B10G10R10_SNORM_PACK32: return "a2b10g10r10SnormPack32";
            case VK_FORMAT_A2B10G10R10_USCALED_PACK32: return "a2b10g10r10UscaledPack32";
            case VK_FORMAT_A2B10G10R10_SSCALED_PACK32: return "a2b10g10r10SscaledPack32";
            case VK_FORMAT_A2B10G10R10_UINT_PACK32: return "a2b10g10r10UintPack32";
            case VK_FORMAT_A2B10G10R10_SINT_PACK32: return "a2b10g10r10SintPack32";
            case VK_FORMAT_R16_UNORM: return "r16Unorm";
            case VK_FORMAT_R16_SNORM: return "r16Snorm";
            case VK_FORMAT_R16_USCALED: return "r16Uscaled";
            case VK_FORMAT_R16_SSCALED: return "r16Sscaled";
            case VK_FORMAT_R16_UINT: return "r16Uint";
            case VK_FORMAT_R16_SINT: return "r16Sint";
            case VK_FORMAT_R16_SFLOAT: return "r16Sfloat";
            case VK_FORMAT_R16G16_UNORM: return "r16g16Unorm";
            case VK_FORMAT_R16G16_SNORM: return "r16g16Snorm";
            case VK_FORMAT_R16G16_USCALED: return "r16g16Uscaled";
            case VK_FORMAT_R16G16_SSCALED: return "r16g16Sscaled";
            case VK_FORMAT_R16G16_UINT: return "r16g16Uint";
            case VK_FORMAT_R16G16_SINT: return "r16g16Sint";
            case VK_FORMAT_R16G16_SFLOAT: return "r16g16Sfloat";
            case VK_FORMAT_R16G16B16_UNORM: return "r16g16b16Unorm";
            case VK_FORMAT_R16G16B16_SNORM: return "r16g16b16Snorm";
            case VK_FORMAT_R16G16B16_USCALED: return "r16g16b16Uscaled";
            case VK_FORMAT_R16G16B16_SSCALED: return "r16g16b16Sscaled";
            case VK_FORMAT_R16G16B16_UINT: return "r16g16b16Uint";
            case VK_FORMAT_R16G16B16_SINT: return "r16g16b16Sint";
            case VK_FORMAT_R16G16B16_SFLOAT: return "r16g16b16Sfloat";
            case VK_FORMAT_R16G16B16A16_UNORM: return "r16g16b16a16Unorm";
            case VK_FORMAT_R16G16B16A16_SNORM: return "r16g16b16a16Snorm";
            case VK_FORMAT_R16G16B16A16_USCALED: return "r16g16b16a16Uscaled";
            case VK_FORMAT_R16G16B16A16_SSCALED: return "r16g16b16a16Sscaled";
            case VK_FORMAT_R16G16B16A16_UINT: return "r16g16b16a16Uint";
            case VK_FORMAT_R16G16B16A16_SINT: return "r16g16b16a16Sint";
            case VK_FORMAT_R16G16B16A16_SFLOAT: return "r16g16b16a16Sfloat";
            case VK_FORMAT_R32_UINT: return "r32Uint";
            case VK_FORMAT_R32_SINT: return "r32Sint";
            case VK_FORMAT_R32_SFLOAT: return "r32Sfloat";
            case VK_FORMAT_R32G32_UINT: return "r32g32Uint";
            case VK_FORMAT_R32G32_SINT: return "r32g32Sint";
            case VK_FORMAT_R32G32_SFLOAT: return "r32g32Sfloat";
            case VK_FORMAT_R32G32B32_UINT: return "r32g32b32Uint";
            case VK_FORMAT_R32G32B32_SINT: return "r32g32b32Sint";
            case VK_FORMAT_R32G32B32_SFLOAT: return "r32g32b32Sfloat";
            case VK_FORMAT_R32G32B32A32_UINT: return "r32g32b32a32Uint";
            case VK_FORMAT_R32G32B32A32_SINT: return "r32g32b32a32Sint";
            case VK_FORMAT_R32G32B32A32_SFLOAT: return "r32g32b32a32Sfloat";
            case VK_FORMAT_R64_UINT: return "r64Uint";
            case VK_FORMAT_R64_SINT: return "r64Sint";
            case VK_FORMAT_R64_SFLOAT: return "r64Sfloat";
            case VK_FORMAT_R64G64_UINT: return "r64g64Uint";
            case VK_FORMAT_R64G64_SINT: return "r64g64Sint";
            case VK_FORMAT_R64G64_SFLOAT: return "r64g64Sfloat";
            case VK_FORMAT_R64G64B64_UINT: return "r64g64b64Uint";
            case VK_FORMAT_R64G64B64_SINT: return "r64g64b64Sint";
            case VK_FORMAT_R64G64B64_SFLOAT: return "r64g64b64Sfloat";
            case VK_FORMAT_R64G64B64A64_UINT: return "r64g64b64a64Uint";
            case VK_FORMAT_R64G64B64A64_SINT: return "r64g64b64a64Sint";
            case VK_FORMAT_R64G64B64A64_SFLOAT: return "r64g64b64a64Sfloat";
            case VK_FORMAT_B10G11R11_UFLOAT_PACK32: return "b10g11r11UfloatPack32";
            case VK_FORMAT_E5B9G9R9_UFLOAT_PACK32: return "e5b9g9r9UfloatPack32";
            case VK_FORMAT_D16_UNORM: return "d16Unorm";
            case VK_FORMAT_X8_D24_UNORM_PACK32: return "x8D24UnormPack32";
            case VK_FORMAT_D32_SFLOAT: return "d32Sfloat";
            case VK_FORMAT_S8_UINT: return "s8Uint";
            case VK_FORMAT_D16_UNORM_S8_UINT: return "d16UnormS8Uint";
            case VK_FORMAT_D24_UNORM_S8_UINT: return "d24UnormS8Uint";
            case VK_FORMAT_D32_SFLOAT_S8_UINT: return "d32SfloatS8Uint";
            case VK_FORMAT_BC1_RGB_UNORM_BLOCK: return "bc1RgbUnormBlock";
            case VK_FORMAT_BC1_RGB_SRGB_BLOCK: return "bc1RgbSrgbBlock";
            case VK_FORMAT_BC1_RGBA_UNORM_BLOCK: return "bc1RgbaUnormBlock";
            case VK_FORMAT_BC1_RGBA_SRGB_BLOCK: return "bc1RgbaSrgbBlock";
            case VK_FORMAT_BC2_UNORM_BLOCK: return "bc2UnormBlock";
            case VK_FORMAT_BC2_SRGB_BLOCK: return "bc2SrgbBlock";
            case VK_FORMAT_BC3_UNORM_BLOCK: return "bc3UnormBlock";
            case VK_FORMAT_BC3_SRGB_BLOCK: return "bc3SrgbBlock";
            case VK_FORMAT_BC4_UNORM_BLOCK: return "bc4UnormBlock";
            case VK_FORMAT_BC4_SNORM_BLOCK: return "bc4SnormBlock";
            case VK_FORMAT_BC5_UNORM_BLOCK: return "bc5UnormBlock";
            case VK_FORMAT_BC5_SNORM_BLOCK: return "bc5SnormBlock";
            case VK_FORMAT_BC6H_UFLOAT_BLOCK: return "bc6hUfloatBlock";
            case VK_FORMAT_BC6H_SFLOAT_BLOCK: return "bc6hSfloatBlock";
            case VK_FORMAT_BC7_UNORM_BLOCK: return "bc7UnormBlock";
            case VK_FORMAT_BC7_SRGB_BLOCK: return "bc7SrgbBlock";
            case VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK: return "etc2R8g8b8UnormBlock";
            case VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK: return "etc2R8g8b8SrgbBlock";
            case VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK: return "etc2R8g8b8a1UnormBlock";
            case VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK: return "etc2R8g8b8a1SrgbBlock";
            case VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK: return "etc2R8g8b8a8UnormBlock";
            case VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK: return "etc2R8g8b8a8SrgbBlock";
            case VK_FORMAT_EAC_R11_UNORM_BLOCK: return "eacR11UnormBlock";
            case VK_FORMAT_EAC_R11_SNORM_BLOCK: return "eacR11SnormBlock";
            case VK_FORMAT_EAC_R11G11_UNORM_BLOCK: return "eacR11g11UnormBlock";
            case VK_FORMAT_EAC_R11G11_SNORM_BLOCK: return "eacR11g11SnormBlock";
            case VK_FORMAT_ASTC_4x4_UNORM_BLOCK: return "astc4x4UnormBlock";
            case VK_FORMAT_ASTC_4x4_SRGB_BLOCK: return "astc4x4SrgbBlock";
            case VK_FORMAT_ASTC_5x4_UNORM_BLOCK: return "astc5x4UnormBlock";
            case VK_FORMAT_ASTC_5x4_SRGB_BLOCK: return "astc5x4SrgbBlock";
            case VK_FORMAT_ASTC_5x5_UNORM_BLOCK: return "astc5x5UnormBlock";
            case VK_FORMAT_ASTC_5x5_SRGB_BLOCK: return "astc5x5SrgbBlock";
            case VK_FORMAT_ASTC_6x5_UNORM_BLOCK: return "astc6x5UnormBlock";
            case VK_FORMAT_ASTC_6x5_SRGB_BLOCK: return "astc6x5SrgbBlock";
            case VK_FORMAT_ASTC_6x6_UNORM_BLOCK: return "astc6x6UnormBlock";
            case VK_FORMAT_ASTC_6x6_SRGB_BLOCK: return "astc6x6SrgbBlock";
            case VK_FORMAT_ASTC_8x5_UNORM_BLOCK: return "astc8x5UnormBlock";
            case VK_FORMAT_ASTC_8x5_SRGB_BLOCK: return "astc8x5SrgbBlock";
            case VK_FORMAT_ASTC_8x6_UNORM_BLOCK: return "astc8x6UnormBlock";
            case VK_FORMAT_ASTC_8x6_SRGB_BLOCK: return "astc8x6SrgbBlock";
            case VK_FORMAT_ASTC_8x8_UNORM_BLOCK: return "astc8x8UnormBlock";
            case VK_FORMAT_ASTC_8x8_SRGB_BLOCK: return "astc8x8SrgbBlock";
            case VK_FORMAT_ASTC_10x5_UNORM_BLOCK: return "astc10x5UnormBlock";
            case VK_FORMAT_ASTC_10x5_SRGB_BLOCK: return "astc10x5SrgbBlock";
            case VK_FORMAT_ASTC_10x6_UNORM_BLOCK: return "astc10x6UnormBlock";
            case VK_FORMAT_ASTC_10x6_SRGB_BLOCK: return "astc10x6SrgbBlock";
            case VK_FORMAT_ASTC_10x8_UNORM_BLOCK: return "astc10x8UnormBlock";
            case VK_FORMAT_ASTC_10x8_SRGB_BLOCK: return "astc10x8SrgbBlock";
            case VK_FORMAT_ASTC_10x10_UNORM_BLOCK: return "astc10x10UnormBlock";
            case VK_FORMAT_ASTC_10x10_SRGB_BLOCK: return "astc10x10SrgbBlock";
            case VK_FORMAT_ASTC_12x10_UNORM_BLOCK: return "astc12x10UnormBlock";
            case VK_FORMAT_ASTC_12x10_SRGB_BLOCK: return "astc12x10SrgbBlock";
            case VK_FORMAT_ASTC_12x12_UNORM_BLOCK: return "astc12x12UnormBlock";
            case VK_FORMAT_ASTC_12x12_SRGB_BLOCK: return "astc12x12SrgbBlock";
            case VK_FORMAT_G8B8G8R8_422_UNORM: return "g8b8g8r8422Unorm";
            case VK_FORMAT_B8G8R8G8_422_UNORM: return "b8g8r8g8422Unorm";
            case VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM: return "g8B8R83plane420Unorm";
            case VK_FORMAT_G8_B8R8_2PLANE_420_UNORM: return "g8B8r82plane420Unorm";
            case VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM: return "g8B8R83plane422Unorm";
            case VK_FORMAT_G8_B8R8_2PLANE_422_UNORM: return "g8B8r82plane422Unorm";
            case VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM: return "g8B8R83plane444Unorm";
            case VK_FORMAT_R10X6_UNORM_PACK16: return "r10x6UnormPack16";
            case VK_FORMAT_R10X6G10X6_UNORM_2PACK16: return "r10x6g10x6Unorm2pack16";
            case VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16: return "r10x6g10x6b10x6a10x6Unorm4pack16";
            case VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16: return "g10x6b10x6g10x6r10x6422Unorm4pack16";
            case VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16: return "b10x6g10x6r10x6g10x6422Unorm4pack16";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16: return "g10x6B10x6R10x63plane420Unorm3pack16";
            case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16: return "g10x6B10x6r10x62plane420Unorm3pack16";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16: return "g10x6B10x6R10x63plane422Unorm3pack16";
            case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16: return "g10x6B10x6r10x62plane422Unorm3pack16";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16: return "g10x6B10x6R10x63plane444Unorm3pack16";
            case VK_FORMAT_R12X4_UNORM_PACK16: return "r12x4UnormPack16";
            case VK_FORMAT_R12X4G12X4_UNORM_2PACK16: return "r12x4g12x4Unorm2pack16";
            case VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16: return "r12x4g12x4b12x4a12x4Unorm4pack16";
            case VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16: return "g12x4b12x4g12x4r12x4422Unorm4pack16";
            case VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16: return "b12x4g12x4r12x4g12x4422Unorm4pack16";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16: return "g12x4B12x4R12x43plane420Unorm3pack16";
            case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16: return "g12x4B12x4r12x42plane420Unorm3pack16";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16: return "g12x4B12x4R12x43plane422Unorm3pack16";
            case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16: return "g12x4B12x4r12x42plane422Unorm3pack16";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16: return "g12x4B12x4R12x43plane444Unorm3pack16";
            case VK_FORMAT_G16B16G16R16_422_UNORM: return "g16b16g16r16422Unorm";
            case VK_FORMAT_B16G16R16G16_422_UNORM: return "b16g16r16g16422Unorm";
            case VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM: return "g16B16R163plane420Unorm";
            case VK_FORMAT_G16_B16R16_2PLANE_420_UNORM: return "g16B16r162plane420Unorm";
            case VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM: return "g16B16R163plane422Unorm";
            case VK_FORMAT_G16_B16R16_2PLANE_422_UNORM: return "g16B16r162plane422Unorm";
            case VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM: return "g16B16R163plane444Unorm";
            case VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG: return "pvrtc12bppUnormBlockImg";
            case VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG: return "pvrtc14bppUnormBlockImg";
            case VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG: return "pvrtc22bppUnormBlockImg";
            case VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG: return "pvrtc24bppUnormBlockImg";
            case VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG: return "pvrtc12bppSrgbBlockImg";
            case VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG: return "pvrtc14bppSrgbBlockImg";
            case VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG: return "pvrtc22bppSrgbBlockImg";
            case VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG: return "pvrtc24bppSrgbBlockImg";
            case VK_FORMAT_G8B8G8R8_422_UNORM_KHR: return "g8b8g8r8422UnormKhr";
            case VK_FORMAT_B8G8R8G8_422_UNORM_KHR: return "b8g8r8g8422UnormKhr";
            case VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM_KHR: return "g8B8R83plane420UnormKhr";
            case VK_FORMAT_G8_B8R8_2PLANE_420_UNORM_KHR: return "g8B8r82plane420UnormKhr";
            case VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM_KHR: return "g8B8R83plane422UnormKhr";
            case VK_FORMAT_G8_B8R8_2PLANE_422_UNORM_KHR: return "g8B8r82plane422UnormKhr";
            case VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM_KHR: return "g8B8R83plane444UnormKhr";
            case VK_FORMAT_R10X6_UNORM_PACK16_KHR: return "r10x6UnormPack16Khr";
            case VK_FORMAT_R10X6G10X6_UNORM_2PACK16_KHR: return "r10x6g10x6Unorm2pack16Khr";
            case VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16_KHR: return "r10x6g10x6b10x6a10x6Unorm4pack16Khr";
            case VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16_KHR: return "g10x6b10x6g10x6r10x6422Unorm4pack16Khr";
            case VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16_KHR: return "b10x6g10x6r10x6g10x6422Unorm4pack16Khr";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16_KHR: return "g10x6B10x6R10x63plane420Unorm3pack16Khr";
            case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16_KHR: return "g10x6B10x6r10x62plane420Unorm3pack16Khr";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16_KHR: return "g10x6B10x6R10x63plane422Unorm3pack16Khr";
            case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16_KHR: return "g10x6B10x6r10x62plane422Unorm3pack16Khr";
            case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16_KHR: return "g10x6B10x6R10x63plane444Unorm3pack16Khr";
            case VK_FORMAT_R12X4_UNORM_PACK16_KHR: return "r12x4UnormPack16Khr";
            case VK_FORMAT_R12X4G12X4_UNORM_2PACK16_KHR: return "r12x4g12x4Unorm2pack16Khr";
            case VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16_KHR: return "r12x4g12x4b12x4a12x4Unorm4pack16Khr";
            case VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16_KHR: return "g12x4b12x4g12x4r12x4422Unorm4pack16Khr";
            case VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16_KHR: return "b12x4g12x4r12x4g12x4422Unorm4pack16Khr";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16_KHR: return "g12x4B12x4R12x43plane420Unorm3pack16Khr";
            case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16_KHR: return "g12x4B12x4r12x42plane420Unorm3pack16Khr";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16_KHR: return "g12x4B12x4R12x43plane422Unorm3pack16Khr";
            case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16_KHR: return "g12x4B12x4r12x42plane422Unorm3pack16Khr";
            case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16_KHR: return "g12x4B12x4R12x43plane444Unorm3pack16Khr";
            case VK_FORMAT_G16B16G16R16_422_UNORM_KHR: return "g16b16g16r16422UnormKhr";
            case VK_FORMAT_B16G16R16G16_422_UNORM_KHR: return "b16g16r16g16422UnormKhr";
            case VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM_KHR: return "g16B16R163plane420UnormKhr";
            case VK_FORMAT_G16_B16R16_2PLANE_420_UNORM_KHR: return "g16B16r162plane420UnormKhr";
            case VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM_KHR: return "g16B16R163plane422UnormKhr";
            case VK_FORMAT_G16_B16R16_2PLANE_422_UNORM_KHR: return "g16B16r162plane422UnormKhr";
            case VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM_KHR: return "g16B16R163plane444UnormKhr";
            case VK_FORMAT_BEGIN_RANGE: return "beginRange";
            case VK_FORMAT_END_RANGE: return "endRange";
            case VK_FORMAT_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageType value)
    {
        switch(value)
        {
            case VK_IMAGE_TYPE_1D: return "1d";
            case VK_IMAGE_TYPE_2D: return "2d";
            case VK_IMAGE_TYPE_3D: return "3d";
            case VK_IMAGE_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_IMAGE_TYPE_END_RANGE: return "endRange";
            case VK_IMAGE_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageTiling value)
    {
        switch(value)
        {
            case VK_IMAGE_TILING_OPTIMAL: return "optimal";
            case VK_IMAGE_TILING_LINEAR: return "linear";
            case VK_IMAGE_TILING_BEGIN_RANGE: return "beginRange";
            case VK_IMAGE_TILING_END_RANGE: return "endRange";
            case VK_IMAGE_TILING_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPhysicalDeviceType value)
    {
        switch(value)
        {
            case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "other";
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "integratedGpu";
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "discreteGpu";
            case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "virtualGpu";
            case VK_PHYSICAL_DEVICE_TYPE_CPU: return "cpu";
            case VK_PHYSICAL_DEVICE_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_PHYSICAL_DEVICE_TYPE_END_RANGE: return "endRange";
            case VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueryType value)
    {
        switch(value)
        {
            case VK_QUERY_TYPE_OCCLUSION: return "occlusion";
            case VK_QUERY_TYPE_PIPELINE_STATISTICS: return "pipelineStatistics";
            case VK_QUERY_TYPE_TIMESTAMP: return "timestamp";
            case VK_QUERY_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_QUERY_TYPE_END_RANGE: return "endRange";
            case VK_QUERY_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSharingMode value)
    {
        switch(value)
        {
            case VK_SHARING_MODE_EXCLUSIVE: return "exclusive";
            case VK_SHARING_MODE_CONCURRENT: return "concurrent";
            case VK_SHARING_MODE_BEGIN_RANGE: return "beginRange";
            case VK_SHARING_MODE_END_RANGE: return "endRange";
            case VK_SHARING_MODE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageLayout value)
    {
        switch(value)
        {
            case VK_IMAGE_LAYOUT_UNDEFINED: return "undefined";
            case VK_IMAGE_LAYOUT_GENERAL: return "general";
            case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL: return "colorAttachmentOptimal";
            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL: return "depthStencilAttachmentOptimal";
            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL: return "depthStencilReadOnlyOptimal";
            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: return "shaderReadOnlyOptimal";
            case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL: return "transferSrcOptimal";
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL: return "transferDstOptimal";
            case VK_IMAGE_LAYOUT_PREINITIALIZED: return "preinitialized";
            case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL: return "depthReadOnlyStencilAttachmentOptimal";
            case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL: return "depthAttachmentStencilReadOnlyOptimal";
            case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR: return "presentSrcKhr";
            case VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR: return "sharedPresentKhr";
            case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR: return "depthReadOnlyStencilAttachmentOptimalKhr";
            case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR: return "depthAttachmentStencilReadOnlyOptimalKhr";
            case VK_IMAGE_LAYOUT_BEGIN_RANGE: return "beginRange";
            case VK_IMAGE_LAYOUT_END_RANGE: return "endRange";
            case VK_IMAGE_LAYOUT_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageViewType value)
    {
        switch(value)
        {
            case VK_IMAGE_VIEW_TYPE_1D: return "1d";
            case VK_IMAGE_VIEW_TYPE_2D: return "2d";
            case VK_IMAGE_VIEW_TYPE_3D: return "3d";
            case VK_IMAGE_VIEW_TYPE_CUBE: return "cube";
            case VK_IMAGE_VIEW_TYPE_1D_ARRAY: return "1dArray";
            case VK_IMAGE_VIEW_TYPE_2D_ARRAY: return "2dArray";
            case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY: return "cubeArray";
            case VK_IMAGE_VIEW_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_IMAGE_VIEW_TYPE_END_RANGE: return "endRange";
            case VK_IMAGE_VIEW_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkComponentSwizzle value)
    {
        switch(value)
        {
            case VK_COMPONENT_SWIZZLE_IDENTITY: return "identity";
            case VK_COMPONENT_SWIZZLE_ZERO: return "zero";
            case VK_COMPONENT_SWIZZLE_ONE: return "one";
            case VK_COMPONENT_SWIZZLE_R: return "r";
            case VK_COMPONENT_SWIZZLE_G: return "g";
            case VK_COMPONENT_SWIZZLE_B: return "b";
            case VK_COMPONENT_SWIZZLE_A: return "a";
            case VK_COMPONENT_SWIZZLE_BEGIN_RANGE: return "beginRange";
            case VK_COMPONENT_SWIZZLE_END_RANGE: return "endRange";
            case VK_COMPONENT_SWIZZLE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkVertexInputRate value)
    {
        switch(value)
        {
            case VK_VERTEX_INPUT_RATE_VERTEX: return "";
            case VK_VERTEX_INPUT_RATE_INSTANCE: return "instance";
            case VK_VERTEX_INPUT_RATE_BEGIN_RANGE: return "beginRange";
            case VK_VERTEX_INPUT_RATE_END_RANGE: return "endRange";
            case VK_VERTEX_INPUT_RATE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPrimitiveTopology value)
    {
        switch(value)
        {
            case VK_PRIMITIVE_TOPOLOGY_POINT_LIST: return "pointList";
            case VK_PRIMITIVE_TOPOLOGY_LINE_LIST: return "lineList";
            case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP: return "lineStrip";
            case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST: return "triangleList";
            case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP: return "triangleStrip";
            case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN: return "triangleFan";
            case VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY: return "lineListWithAdjacency";
            case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY: return "lineStripWithAdjacency";
            case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY: return "triangleListWithAdjacency";
            case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY: return "triangleStripWithAdjacency";
            case VK_PRIMITIVE_TOPOLOGY_PATCH_LIST: return "patchList";
            case VK_PRIMITIVE_TOPOLOGY_BEGIN_RANGE: return "beginRange";
            case VK_PRIMITIVE_TOPOLOGY_END_RANGE: return "endRange";
            case VK_PRIMITIVE_TOPOLOGY_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPolygonMode value)
    {
        switch(value)
        {
            case VK_POLYGON_MODE_FILL: return "fill";
            case VK_POLYGON_MODE_LINE: return "line";
            case VK_POLYGON_MODE_POINT: return "point";
            case VK_POLYGON_MODE_FILL_RECTANGLE_NV: return "fillRectangleNv";
            case VK_POLYGON_MODE_BEGIN_RANGE: return "beginRange";
            case VK_POLYGON_MODE_END_RANGE: return "endRange";
            case VK_POLYGON_MODE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFrontFace value)
    {
        switch(value)
        {
            case VK_FRONT_FACE_COUNTER_CLOCKWISE: return "counterClockwise";
            case VK_FRONT_FACE_CLOCKWISE: return "clockwise";
            case VK_FRONT_FACE_BEGIN_RANGE: return "beginRange";
            case VK_FRONT_FACE_END_RANGE: return "endRange";
            case VK_FRONT_FACE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCompareOp value)
    {
        switch(value)
        {
            case VK_COMPARE_OP_NEVER: return "never";
            case VK_COMPARE_OP_LESS: return "less";
            case VK_COMPARE_OP_EQUAL: return "equal";
            case VK_COMPARE_OP_LESS_OR_EQUAL: return "lessOrEqual";
            case VK_COMPARE_OP_GREATER: return "greater";
            case VK_COMPARE_OP_NOT_EQUAL: return "notEqual";
            case VK_COMPARE_OP_GREATER_OR_EQUAL: return "greaterOrEqual";
            case VK_COMPARE_OP_ALWAYS: return "always";
            case VK_COMPARE_OP_BEGIN_RANGE: return "beginRange";
            case VK_COMPARE_OP_END_RANGE: return "endRange";
            case VK_COMPARE_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkStencilOp value)
    {
        switch(value)
        {
            case VK_STENCIL_OP_KEEP: return "keep";
            case VK_STENCIL_OP_ZERO: return "zero";
            case VK_STENCIL_OP_REPLACE: return "replace";
            case VK_STENCIL_OP_INCREMENT_AND_CLAMP: return "incrementAndClamp";
            case VK_STENCIL_OP_DECREMENT_AND_CLAMP: return "decrementAndClamp";
            case VK_STENCIL_OP_INVERT: return "invert";
            case VK_STENCIL_OP_INCREMENT_AND_WRAP: return "incrementAndWrap";
            case VK_STENCIL_OP_DECREMENT_AND_WRAP: return "decrementAndWrap";
            case VK_STENCIL_OP_BEGIN_RANGE: return "beginRange";
            case VK_STENCIL_OP_END_RANGE: return "endRange";
            case VK_STENCIL_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkLogicOp value)
    {
        switch(value)
        {
            case VK_LOGIC_OP_CLEAR: return "clear";
            case VK_LOGIC_OP_AND: return "and";
            case VK_LOGIC_OP_AND_REVERSE: return "andReverse";
            case VK_LOGIC_OP_COPY: return "copy";
            case VK_LOGIC_OP_AND_INVERTED: return "andInverted";
            case VK_LOGIC_OP_NO_OP: return "no";
            case VK_LOGIC_OP_XOR: return "xor";
            case VK_LOGIC_OP_OR: return "or";
            case VK_LOGIC_OP_NOR: return "nor";
            case VK_LOGIC_OP_EQUIVALENT: return "equivalent";
            case VK_LOGIC_OP_INVERT: return "invert";
            case VK_LOGIC_OP_OR_REVERSE: return "orReverse";
            case VK_LOGIC_OP_COPY_INVERTED: return "copyInverted";
            case VK_LOGIC_OP_OR_INVERTED: return "orInverted";
            case VK_LOGIC_OP_NAND: return "nand";
            case VK_LOGIC_OP_SET: return "set";
            case VK_LOGIC_OP_BEGIN_RANGE: return "beginRange";
            case VK_LOGIC_OP_END_RANGE: return "endRange";
            case VK_LOGIC_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBlendFactor value)
    {
        switch(value)
        {
            case VK_BLEND_FACTOR_ZERO: return "zero";
            case VK_BLEND_FACTOR_ONE: return "one";
            case VK_BLEND_FACTOR_SRC_COLOR: return "srcColor";
            case VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR: return "oneMinusSrcColor";
            case VK_BLEND_FACTOR_DST_COLOR: return "dstColor";
            case VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR: return "oneMinusDstColor";
            case VK_BLEND_FACTOR_SRC_ALPHA: return "srcAlpha";
            case VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA: return "oneMinusSrcAlpha";
            case VK_BLEND_FACTOR_DST_ALPHA: return "dstAlpha";
            case VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA: return "oneMinusDstAlpha";
            case VK_BLEND_FACTOR_CONSTANT_COLOR: return "constantColor";
            case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR: return "oneMinusConstantColor";
            case VK_BLEND_FACTOR_CONSTANT_ALPHA: return "constantAlpha";
            case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA: return "oneMinusConstantAlpha";
            case VK_BLEND_FACTOR_SRC_ALPHA_SATURATE: return "srcAlphaSaturate";
            case VK_BLEND_FACTOR_SRC1_COLOR: return "src1Color";
            case VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR: return "oneMinusSrc1Color";
            case VK_BLEND_FACTOR_SRC1_ALPHA: return "src1Alpha";
            case VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA: return "oneMinusSrc1Alpha";
            case VK_BLEND_FACTOR_BEGIN_RANGE: return "beginRange";
            case VK_BLEND_FACTOR_END_RANGE: return "endRange";
            case VK_BLEND_FACTOR_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBlendOp value)
    {
        switch(value)
        {
            case VK_BLEND_OP_ADD: return "add";
            case VK_BLEND_OP_SUBTRACT: return "subtract";
            case VK_BLEND_OP_REVERSE_SUBTRACT: return "reverseSubtract";
            case VK_BLEND_OP_MIN: return "min";
            case VK_BLEND_OP_MAX: return "max";
            case VK_BLEND_OP_ZERO_EXT: return "zeroExt";
            case VK_BLEND_OP_SRC_EXT: return "srcExt";
            case VK_BLEND_OP_DST_EXT: return "dstExt";
            case VK_BLEND_OP_SRC_OVER_EXT: return "srcOverExt";
            case VK_BLEND_OP_DST_OVER_EXT: return "dstOverExt";
            case VK_BLEND_OP_SRC_IN_EXT: return "srcInExt";
            case VK_BLEND_OP_DST_IN_EXT: return "dstInExt";
            case VK_BLEND_OP_SRC_OUT_EXT: return "srcOutExt";
            case VK_BLEND_OP_DST_OUT_EXT: return "dstOutExt";
            case VK_BLEND_OP_SRC_ATOP_EXT: return "srcAtopExt";
            case VK_BLEND_OP_DST_ATOP_EXT: return "dstAtopExt";
            case VK_BLEND_OP_XOR_EXT: return "xorExt";
            case VK_BLEND_OP_MULTIPLY_EXT: return "multiplyExt";
            case VK_BLEND_OP_SCREEN_EXT: return "screenExt";
            case VK_BLEND_OP_OVERLAY_EXT: return "overlayExt";
            case VK_BLEND_OP_DARKEN_EXT: return "darkenExt";
            case VK_BLEND_OP_LIGHTEN_EXT: return "lightenExt";
            case VK_BLEND_OP_COLORDODGE_EXT: return "colordodgeExt";
            case VK_BLEND_OP_COLORBURN_EXT: return "colorburnExt";
            case VK_BLEND_OP_HARDLIGHT_EXT: return "hardlightExt";
            case VK_BLEND_OP_SOFTLIGHT_EXT: return "softlightExt";
            case VK_BLEND_OP_DIFFERENCE_EXT: return "differenceExt";
            case VK_BLEND_OP_EXCLUSION_EXT: return "exclusionExt";
            case VK_BLEND_OP_INVERT_EXT: return "invertExt";
            case VK_BLEND_OP_INVERT_RGB_EXT: return "invertRgbExt";
            case VK_BLEND_OP_LINEARDODGE_EXT: return "lineardodgeExt";
            case VK_BLEND_OP_LINEARBURN_EXT: return "linearburnExt";
            case VK_BLEND_OP_VIVIDLIGHT_EXT: return "vividlightExt";
            case VK_BLEND_OP_LINEARLIGHT_EXT: return "linearlightExt";
            case VK_BLEND_OP_PINLIGHT_EXT: return "pinlightExt";
            case VK_BLEND_OP_HARDMIX_EXT: return "hardmixExt";
            case VK_BLEND_OP_HSL_HUE_EXT: return "hslHueExt";
            case VK_BLEND_OP_HSL_SATURATION_EXT: return "hslSaturationExt";
            case VK_BLEND_OP_HSL_COLOR_EXT: return "hslColorExt";
            case VK_BLEND_OP_HSL_LUMINOSITY_EXT: return "hslLuminosityExt";
            case VK_BLEND_OP_PLUS_EXT: return "plusExt";
            case VK_BLEND_OP_PLUS_CLAMPED_EXT: return "plusClampedExt";
            case VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT: return "plusClampedAlphaExt";
            case VK_BLEND_OP_PLUS_DARKER_EXT: return "plusDarkerExt";
            case VK_BLEND_OP_MINUS_EXT: return "minusExt";
            case VK_BLEND_OP_MINUS_CLAMPED_EXT: return "minusClampedExt";
            case VK_BLEND_OP_CONTRAST_EXT: return "contrastExt";
            case VK_BLEND_OP_INVERT_OVG_EXT: return "invertOvgExt";
            case VK_BLEND_OP_RED_EXT: return "redExt";
            case VK_BLEND_OP_GREEN_EXT: return "greenExt";
            case VK_BLEND_OP_BLUE_EXT: return "blueExt";
            case VK_BLEND_OP_BEGIN_RANGE: return "beginRange";
            case VK_BLEND_OP_END_RANGE: return "endRange";
            case VK_BLEND_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDynamicState value)
    {
        switch(value)
        {
            case VK_DYNAMIC_STATE_VIEWPORT: return "viewport";
            case VK_DYNAMIC_STATE_SCISSOR: return "scissor";
            case VK_DYNAMIC_STATE_LINE_WIDTH: return "lineWidth";
            case VK_DYNAMIC_STATE_DEPTH_BIAS: return "depthBias";
            case VK_DYNAMIC_STATE_BLEND_CONSTANTS: return "blendConstants";
            case VK_DYNAMIC_STATE_DEPTH_BOUNDS: return "depthBounds";
            case VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK: return "stencilCompareMask";
            case VK_DYNAMIC_STATE_STENCIL_WRITE_MASK: return "stencilWriteMask";
            case VK_DYNAMIC_STATE_STENCIL_REFERENCE: return "stencilReference";
            case VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV: return "viewportWScalingNv";
            case VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT: return "discardRectangleExt";
            case VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT: return "sampleLocationsExt";
            case VK_DYNAMIC_STATE_BEGIN_RANGE: return "beginRange";
            case VK_DYNAMIC_STATE_END_RANGE: return "endRange";
            case VK_DYNAMIC_STATE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFilter value)
    {
        switch(value)
        {
            case VK_FILTER_NEAREST: return "nearest";
            case VK_FILTER_LINEAR: return "linear";
            case VK_FILTER_CUBIC_IMG: return "cubicImg";
            case VK_FILTER_BEGIN_RANGE: return "beginRange";
            case VK_FILTER_END_RANGE: return "endRange";
            case VK_FILTER_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSamplerMipmapMode value)
    {
        switch(value)
        {
            case VK_SAMPLER_MIPMAP_MODE_NEAREST: return "nearest";
            case VK_SAMPLER_MIPMAP_MODE_LINEAR: return "linear";
            case VK_SAMPLER_MIPMAP_MODE_BEGIN_RANGE: return "beginRange";
            case VK_SAMPLER_MIPMAP_MODE_END_RANGE: return "endRange";
            case VK_SAMPLER_MIPMAP_MODE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSamplerAddressMode value)
    {
        switch(value)
        {
            case VK_SAMPLER_ADDRESS_MODE_REPEAT: return "repeat";
            case VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT: return "mirroredRepeat";
            case VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE: return "clampToEdge";
            case VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER: return "clampToBorder";
            case VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE: return "mirrorClampToEdge";
            case VK_SAMPLER_ADDRESS_MODE_BEGIN_RANGE: return "beginRange";
            case VK_SAMPLER_ADDRESS_MODE_END_RANGE: return "endRange";
            case VK_SAMPLER_ADDRESS_MODE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBorderColor value)
    {
        switch(value)
        {
            case VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK: return "floatTransparentBlack";
            case VK_BORDER_COLOR_INT_TRANSPARENT_BLACK: return "intTransparentBlack";
            case VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK: return "floatOpaqueBlack";
            case VK_BORDER_COLOR_INT_OPAQUE_BLACK: return "intOpaqueBlack";
            case VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE: return "floatOpaqueWhite";
            case VK_BORDER_COLOR_INT_OPAQUE_WHITE: return "intOpaqueWhite";
            case VK_BORDER_COLOR_BEGIN_RANGE: return "beginRange";
            case VK_BORDER_COLOR_END_RANGE: return "endRange";
            case VK_BORDER_COLOR_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDescriptorType value)
    {
        switch(value)
        {
            case VK_DESCRIPTOR_TYPE_SAMPLER: return "sampler";
            case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: return "combinedImageSampler";
            case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE: return "sampledImage";
            case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE: return "storageImage";
            case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER: return "uniformTexelBuffer";
            case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER: return "storageTexelBuffer";
            case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER: return "uniformBuffer";
            case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER: return "storageBuffer";
            case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC: return "uniformBufferDynamic";
            case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC: return "storageBufferDynamic";
            case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: return "inputAttachment";
            case VK_DESCRIPTOR_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_DESCRIPTOR_TYPE_END_RANGE: return "endRange";
            case VK_DESCRIPTOR_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkAttachmentLoadOp value)
    {
        switch(value)
        {
            case VK_ATTACHMENT_LOAD_OP_LOAD: return "";
            case VK_ATTACHMENT_LOAD_OP_CLEAR: return "clear";
            case VK_ATTACHMENT_LOAD_OP_DONT_CARE: return "dontCare";
            case VK_ATTACHMENT_LOAD_OP_BEGIN_RANGE: return "beginRange";
            case VK_ATTACHMENT_LOAD_OP_END_RANGE: return "endRange";
            case VK_ATTACHMENT_LOAD_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkAttachmentStoreOp value)
    {
        switch(value)
        {
            case VK_ATTACHMENT_STORE_OP_STORE: return "";
            case VK_ATTACHMENT_STORE_OP_DONT_CARE: return "dontCare";
            case VK_ATTACHMENT_STORE_OP_BEGIN_RANGE: return "beginRange";
            case VK_ATTACHMENT_STORE_OP_END_RANGE: return "endRange";
            case VK_ATTACHMENT_STORE_OP_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPipelineBindPoint value)
    {
        switch(value)
        {
            case VK_PIPELINE_BIND_POINT_GRAPHICS: return "graphics";
            case VK_PIPELINE_BIND_POINT_COMPUTE: return "compute";
            case VK_PIPELINE_BIND_POINT_BEGIN_RANGE: return "beginRange";
            case VK_PIPELINE_BIND_POINT_END_RANGE: return "endRange";
            case VK_PIPELINE_BIND_POINT_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCommandBufferLevel value)
    {
        switch(value)
        {
            case VK_COMMAND_BUFFER_LEVEL_PRIMARY: return "primary";
            case VK_COMMAND_BUFFER_LEVEL_SECONDARY: return "secondary";
            case VK_COMMAND_BUFFER_LEVEL_BEGIN_RANGE: return "beginRange";
            case VK_COMMAND_BUFFER_LEVEL_END_RANGE: return "endRange";
            case VK_COMMAND_BUFFER_LEVEL_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkIndexType value)
    {
        switch(value)
        {
            case VK_INDEX_TYPE_UINT16: return "uint16";
            case VK_INDEX_TYPE_UINT32: return "uint32";
            case VK_INDEX_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_INDEX_TYPE_END_RANGE: return "endRange";
            case VK_INDEX_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSubpassContents value)
    {
        switch(value)
        {
            case VK_SUBPASS_CONTENTS_INLINE: return "inline";
            case VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS: return "secondaryCommandBuffers";
            case VK_SUBPASS_CONTENTS_BEGIN_RANGE: return "beginRange";
            case VK_SUBPASS_CONTENTS_END_RANGE: return "endRange";
            case VK_SUBPASS_CONTENTS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkObjectType value)
    {
        switch(value)
        {
            case VK_OBJECT_TYPE_UNKNOWN: return "unknown";
            case VK_OBJECT_TYPE_INSTANCE: return "instance";
            case VK_OBJECT_TYPE_PHYSICAL_DEVICE: return "physicalDevice";
            case VK_OBJECT_TYPE_DEVICE: return "device";
            case VK_OBJECT_TYPE_QUEUE: return "queue";
            case VK_OBJECT_TYPE_SEMAPHORE: return "semaphore";
            case VK_OBJECT_TYPE_COMMAND_BUFFER: return "commandBuffer";
            case VK_OBJECT_TYPE_FENCE: return "fence";
            case VK_OBJECT_TYPE_DEVICE_MEMORY: return "deviceMemory";
            case VK_OBJECT_TYPE_BUFFER: return "buffer";
            case VK_OBJECT_TYPE_IMAGE: return "image";
            case VK_OBJECT_TYPE_EVENT: return "event";
            case VK_OBJECT_TYPE_QUERY_POOL: return "queryPool";
            case VK_OBJECT_TYPE_BUFFER_VIEW: return "bufferView";
            case VK_OBJECT_TYPE_IMAGE_VIEW: return "imageView";
            case VK_OBJECT_TYPE_SHADER_MODULE: return "shaderModule";
            case VK_OBJECT_TYPE_PIPELINE_CACHE: return "pipelineCache";
            case VK_OBJECT_TYPE_PIPELINE_LAYOUT: return "pipelineLayout";
            case VK_OBJECT_TYPE_RENDER_PASS: return "renderPass";
            case VK_OBJECT_TYPE_PIPELINE: return "pipeline";
            case VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT: return "descriptorSetLayout";
            case VK_OBJECT_TYPE_SAMPLER: return "sampler";
            case VK_OBJECT_TYPE_DESCRIPTOR_POOL: return "descriptorPool";
            case VK_OBJECT_TYPE_DESCRIPTOR_SET: return "descriptorSet";
            case VK_OBJECT_TYPE_FRAMEBUFFER: return "framebuffer";
            case VK_OBJECT_TYPE_COMMAND_POOL: return "commandPool";
            case VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION: return "samplerYcbcrConversion";
            case VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE: return "descriptorUpdateTemplate";
            case VK_OBJECT_TYPE_SURFACE_KHR: return "surfaceKhr";
            case VK_OBJECT_TYPE_SWAPCHAIN_KHR: return "swapchainKhr";
            case VK_OBJECT_TYPE_DISPLAY_KHR: return "displayKhr";
            case VK_OBJECT_TYPE_DISPLAY_MODE_KHR: return "displayModeKhr";
            case VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT: return "debugReportCallbackExt";
            case VK_OBJECT_TYPE_OBJECT_TABLE_NVX: return "tableNvx";
            case VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NVX: return "indirectCommandsLayoutNvx";
            case VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT: return "debugUtilsMessengerExt";
            case VK_OBJECT_TYPE_VALIDATION_CACHE_EXT: return "validationCacheExt";
            case VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR: return "descriptorUpdateTemplateKhr";
            case VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR: return "samplerYcbcrConversionKhr";
            case VK_OBJECT_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_OBJECT_TYPE_END_RANGE: return "endRange";
            case VK_OBJECT_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFormatFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT: return "sampledImageBit";
            case VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT: return "storageImageBit";
            case VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT: return "storageImageAtomicBit";
            case VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT: return "uniformTexelBufferBit";
            case VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT: return "storageTexelBufferBit";
            case VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT: return "storageTexelBufferAtomicBit";
            case VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT: return "vertexBufferBit";
            case VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT: return "colorAttachmentBit";
            case VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT: return "colorAttachmentBlendBit";
            case VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT: return "depthStencilAttachmentBit";
            case VK_FORMAT_FEATURE_BLIT_SRC_BIT: return "blitSrcBit";
            case VK_FORMAT_FEATURE_BLIT_DST_BIT: return "blitDstBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT: return "sampledImageFilterLinearBit";
            case VK_FORMAT_FEATURE_TRANSFER_SRC_BIT: return "transferSrcBit";
            case VK_FORMAT_FEATURE_TRANSFER_DST_BIT: return "transferDstBit";
            case VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT: return "midpointChromaSamplesBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT: return "sampledImageYcbcrConversionLinearFilterBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT: return "sampledImageYcbcrConversionSeparateReconstructionFilterBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT: return "sampledImageYcbcrConversionChromaReconstructionExplicitBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT: return "sampledImageYcbcrConversionChromaReconstructionExplicitForceableBit";
            case VK_FORMAT_FEATURE_DISJOINT_BIT: return "disjointBit";
            case VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT: return "cositedChromaSamplesBit";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG: return "sampledImageFilterCubicBitImg";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT: return "sampledImageFilterMinmaxBitExt";
            case VK_FORMAT_FEATURE_TRANSFER_SRC_BIT_KHR: return "transferSrcBitKhr";
            case VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR: return "transferDstBitKhr";
            case VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT_KHR: return "midpointChromaSamplesBitKhr";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT_KHR: return "sampledImageYcbcrConversionLinearFilterBitKhr";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT_KHR: return "sampledImageYcbcrConversionSeparateReconstructionFilterBitKhr";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT_KHR: return "sampledImageYcbcrConversionChromaReconstructionExplicitBitKhr";
            case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT_KHR: return "sampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr";
            case VK_FORMAT_FEATURE_DISJOINT_BIT_KHR: return "disjointBitKhr";
            case VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT_KHR: return "cositedChromaSamplesBitKhr";
            case VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageUsageFlagBits value)
    {
        switch(value)
        {
            case VK_IMAGE_USAGE_TRANSFER_SRC_BIT: return "transferSrcBit";
            case VK_IMAGE_USAGE_TRANSFER_DST_BIT: return "transferDstBit";
            case VK_IMAGE_USAGE_SAMPLED_BIT: return "sampledBit";
            case VK_IMAGE_USAGE_STORAGE_BIT: return "storageBit";
            case VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT: return "colorAttachmentBit";
            case VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT: return "depthStencilAttachmentBit";
            case VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT: return "transientAttachmentBit";
            case VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT: return "inputAttachmentBit";
            case VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageCreateFlagBits value)
    {
        switch(value)
        {
            case VK_IMAGE_CREATE_SPARSE_BINDING_BIT: return "sparseBindingBit";
            case VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT: return "sparseResidencyBit";
            case VK_IMAGE_CREATE_SPARSE_ALIASED_BIT: return "sparseAliasedBit";
            case VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT: return "mutableFormatBit";
            case VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT: return "cubeCompatibleBit";
            case VK_IMAGE_CREATE_ALIAS_BIT: return "aliasBit";
            case VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT: return "splitInstanceBindRegionsBit";
            case VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT: return "2dArrayCompatibleBit";
            case VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT: return "blockTexelViewCompatibleBit";
            case VK_IMAGE_CREATE_EXTENDED_USAGE_BIT: return "extendedUsageBit";
            case VK_IMAGE_CREATE_PROTECTED_BIT: return "protectedBit";
            case VK_IMAGE_CREATE_DISJOINT_BIT: return "disjointBit";
            case VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT: return "sampleLocationsCompatibleDepthBitExt";
            case VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR: return "splitInstanceBindRegionsBitKhr";
            case VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT_KHR: return "2dArrayCompatibleBitKhr";
            case VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT_KHR: return "blockTexelViewCompatibleBitKhr";
            case VK_IMAGE_CREATE_EXTENDED_USAGE_BIT_KHR: return "extendedUsageBitKhr";
            case VK_IMAGE_CREATE_DISJOINT_BIT_KHR: return "disjointBitKhr";
            case VK_IMAGE_CREATE_ALIAS_BIT_KHR: return "aliasBitKhr";
            case VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSampleCountFlagBits value)
    {
        switch(value)
        {
            case VK_SAMPLE_COUNT_1_BIT: return "1Bit";
            case VK_SAMPLE_COUNT_2_BIT: return "2Bit";
            case VK_SAMPLE_COUNT_4_BIT: return "4Bit";
            case VK_SAMPLE_COUNT_8_BIT: return "8Bit";
            case VK_SAMPLE_COUNT_16_BIT: return "16Bit";
            case VK_SAMPLE_COUNT_32_BIT: return "32Bit";
            case VK_SAMPLE_COUNT_64_BIT: return "64Bit";
            case VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueueFlagBits value)
    {
        switch(value)
        {
            case VK_QUEUE_GRAPHICS_BIT: return "graphicsBit";
            case VK_QUEUE_COMPUTE_BIT: return "computeBit";
            case VK_QUEUE_TRANSFER_BIT: return "transferBit";
            case VK_QUEUE_SPARSE_BINDING_BIT: return "sparseBindingBit";
            case VK_QUEUE_PROTECTED_BIT: return "protectedBit";
            case VK_QUEUE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkMemoryPropertyFlagBits value)
    {
        switch(value)
        {
            case VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT: return "deviceLocalBit";
            case VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT: return "hostVisibleBit";
            case VK_MEMORY_PROPERTY_HOST_COHERENT_BIT: return "hostCoherentBit";
            case VK_MEMORY_PROPERTY_HOST_CACHED_BIT: return "hostCachedBit";
            case VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT: return "lazilyAllocatedBit";
            case VK_MEMORY_PROPERTY_PROTECTED_BIT: return "protectedBit";
            case VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkMemoryHeapFlagBits value)
    {
        switch(value)
        {
            case VK_MEMORY_HEAP_DEVICE_LOCAL_BIT: return "deviceLocalBit";
            case VK_MEMORY_HEAP_MULTI_INSTANCE_BIT: return "multiInstanceBit";
            case VK_MEMORY_HEAP_MULTI_INSTANCE_BIT_KHR: return "multiInstanceBitKhr";
            case VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDeviceQueueCreateFlagBits value)
    {
        switch(value)
        {
            case VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT: return "protectedBit";
            case VK_DEVICE_QUEUE_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPipelineStageFlagBits value)
    {
        switch(value)
        {
            case VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT: return "topOfPipeBit";
            case VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT: return "drawIndirectBit";
            case VK_PIPELINE_STAGE_VERTEX_INPUT_BIT: return "vertexInputBit";
            case VK_PIPELINE_STAGE_VERTEX_SHADER_BIT: return "vertexShaderBit";
            case VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT: return "tessellationControlShaderBit";
            case VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT: return "tessellationEvaluationShaderBit";
            case VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT: return "geometryShaderBit";
            case VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT: return "fragmentShaderBit";
            case VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT: return "earlyFragmentTestsBit";
            case VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT: return "lateFragmentTestsBit";
            case VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT: return "colorAttachmentOutputBit";
            case VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT: return "computeShaderBit";
            case VK_PIPELINE_STAGE_TRANSFER_BIT: return "transferBit";
            case VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT: return "bottomOfPipeBit";
            case VK_PIPELINE_STAGE_HOST_BIT: return "hostBit";
            case VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT: return "allGraphicsBit";
            case VK_PIPELINE_STAGE_ALL_COMMANDS_BIT: return "allCommandsBit";
            case VK_PIPELINE_STAGE_COMMAND_PROCESS_BIT_NVX: return "commandProcessBitNvx";
            case VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkImageAspectFlagBits value)
    {
        switch(value)
        {
            case VK_IMAGE_ASPECT_COLOR_BIT: return "colorBit";
            case VK_IMAGE_ASPECT_DEPTH_BIT: return "depthBit";
            case VK_IMAGE_ASPECT_STENCIL_BIT: return "stencilBit";
            case VK_IMAGE_ASPECT_METADATA_BIT: return "metadataBit";
            case VK_IMAGE_ASPECT_PLANE_0_BIT: return "plane0Bit";
            case VK_IMAGE_ASPECT_PLANE_1_BIT: return "plane1Bit";
            case VK_IMAGE_ASPECT_PLANE_2_BIT: return "plane2Bit";
            case VK_IMAGE_ASPECT_PLANE_0_BIT_KHR: return "plane0BitKhr";
            case VK_IMAGE_ASPECT_PLANE_1_BIT_KHR: return "plane1BitKhr";
            case VK_IMAGE_ASPECT_PLANE_2_BIT_KHR: return "plane2BitKhr";
            case VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSparseImageFormatFlagBits value)
    {
        switch(value)
        {
            case VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT: return "singleMiptailBit";
            case VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT: return "alignedMipSizeBit";
            case VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT: return "nonstandardBlockSizeBit";
            case VK_SPARSE_IMAGE_FORMAT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSparseMemoryBindFlagBits value)
    {
        switch(value)
        {
            case VK_SPARSE_MEMORY_BIND_METADATA_BIT: return "metadataBit";
            case VK_SPARSE_MEMORY_BIND_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFenceCreateFlagBits value)
    {
        switch(value)
        {
            case VK_FENCE_CREATE_SIGNALED_BIT: return "signaledBit";
            case VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueryPipelineStatisticFlagBits value)
    {
        switch(value)
        {
            case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT: return "inputAssemblyVerticesBit";
            case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT: return "inputAssemblyPrimitivesBit";
            case VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT: return "vertexShaderInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT: return "geometryShaderInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT: return "geometryShaderPrimitivesBit";
            case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT: return "clippingInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT: return "clippingPrimitivesBit";
            case VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT: return "fragmentShaderInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT: return "tessellationControlShaderPatchesBit";
            case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT: return "tessellationEvaluationShaderInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT: return "computeShaderInvocationsBit";
            case VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueryResultFlagBits value)
    {
        switch(value)
        {
            case VK_QUERY_RESULT_64_BIT: return "64Bit";
            case VK_QUERY_RESULT_WAIT_BIT: return "waitBit";
            case VK_QUERY_RESULT_WITH_AVAILABILITY_BIT: return "withAvailabilityBit";
            case VK_QUERY_RESULT_PARTIAL_BIT: return "partialBit";
            case VK_QUERY_RESULT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBufferCreateFlagBits value)
    {
        switch(value)
        {
            case VK_BUFFER_CREATE_SPARSE_BINDING_BIT: return "sparseBindingBit";
            case VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT: return "sparseResidencyBit";
            case VK_BUFFER_CREATE_SPARSE_ALIASED_BIT: return "sparseAliasedBit";
            case VK_BUFFER_CREATE_PROTECTED_BIT: return "protectedBit";
            case VK_BUFFER_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBufferUsageFlagBits value)
    {
        switch(value)
        {
            case VK_BUFFER_USAGE_TRANSFER_SRC_BIT: return "transferSrcBit";
            case VK_BUFFER_USAGE_TRANSFER_DST_BIT: return "transferDstBit";
            case VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT: return "uniformTexelBit";
            case VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT: return "storageTexelBit";
            case VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT: return "uniformBit";
            case VK_BUFFER_USAGE_STORAGE_BUFFER_BIT: return "storageBit";
            case VK_BUFFER_USAGE_INDEX_BUFFER_BIT: return "indexBit";
            case VK_BUFFER_USAGE_VERTEX_BUFFER_BIT: return "vertexBit";
            case VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT: return "indirectBit";
            case VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPipelineCreateFlagBits value)
    {
        switch(value)
        {
            case VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT: return "disableOptimizationBit";
            case VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT: return "allowDerivativesBit";
            case VK_PIPELINE_CREATE_DERIVATIVE_BIT: return "derivativeBit";
            case VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT: return "viewIndexFromDeviceIndexBit";
            case VK_PIPELINE_CREATE_DISPATCH_BASE: return "dispatchBase";
            case VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT_KHR: return "viewIndexFromDeviceIndexBitKhr";
            case VK_PIPELINE_CREATE_DISPATCH_BASE_KHR: return "dispatchBaseKhr";
            case VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkShaderStageFlagBits value)
    {
        switch(value)
        {
            case VK_SHADER_STAGE_VERTEX_BIT: return "vertexBit";
            case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT: return "tessellationControlBit";
            case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT: return "tessellationEvaluationBit";
            case VK_SHADER_STAGE_GEOMETRY_BIT: return "geometryBit";
            case VK_SHADER_STAGE_FRAGMENT_BIT: return "fragmentBit";
            case VK_SHADER_STAGE_COMPUTE_BIT: return "computeBit";
            case VK_SHADER_STAGE_ALL_GRAPHICS: return "allGraphics";
            case VK_SHADER_STAGE_ALL: return "all";
            case VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCullModeFlagBits value)
    {
        switch(value)
        {
            case VK_CULL_MODE_NONE: return "none";
            case VK_CULL_MODE_FRONT_BIT: return "frontBit";
            case VK_CULL_MODE_BACK_BIT: return "backBit";
            case VK_CULL_MODE_FRONT_AND_BACK: return "frontAndBack";
            case VK_CULL_MODE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkColorComponentFlagBits value)
    {
        switch(value)
        {
            case VK_COLOR_COMPONENT_R_BIT: return "rBit";
            case VK_COLOR_COMPONENT_G_BIT: return "gBit";
            case VK_COLOR_COMPONENT_B_BIT: return "bBit";
            case VK_COLOR_COMPONENT_A_BIT: return "aBit";
            case VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDescriptorSetLayoutCreateFlagBits value)
    {
        switch(value)
        {
            case VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR: return "pushBitKhr";
            case VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDescriptorPoolCreateFlagBits value)
    {
        switch(value)
        {
            case VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT: return "freeSetBit";
            case VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkAttachmentDescriptionFlagBits value)
    {
        switch(value)
        {
            case VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT: return "mayAliasBit";
            case VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSubpassDescriptionFlagBits value)
    {
        switch(value)
        {
            case VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX: return "perViewAttributesBitNvx";
            case VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX: return "perViewPositionXOnlyBitNvx";
            case VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkAccessFlagBits value)
    {
        switch(value)
        {
            case VK_ACCESS_INDIRECT_COMMAND_READ_BIT: return "indirectCommandReadBit";
            case VK_ACCESS_INDEX_READ_BIT: return "indexReadBit";
            case VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT: return "vertexAttributeReadBit";
            case VK_ACCESS_UNIFORM_READ_BIT: return "uniformReadBit";
            case VK_ACCESS_INPUT_ATTACHMENT_READ_BIT: return "inputAttachmentReadBit";
            case VK_ACCESS_SHADER_READ_BIT: return "shaderReadBit";
            case VK_ACCESS_SHADER_WRITE_BIT: return "shaderWriteBit";
            case VK_ACCESS_COLOR_ATTACHMENT_READ_BIT: return "colorAttachmentReadBit";
            case VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT: return "colorAttachmentWriteBit";
            case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT: return "depthStencilAttachmentReadBit";
            case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT: return "depthStencilAttachmentWriteBit";
            case VK_ACCESS_TRANSFER_READ_BIT: return "transferReadBit";
            case VK_ACCESS_TRANSFER_WRITE_BIT: return "transferWriteBit";
            case VK_ACCESS_HOST_READ_BIT: return "hostReadBit";
            case VK_ACCESS_HOST_WRITE_BIT: return "hostWriteBit";
            case VK_ACCESS_MEMORY_READ_BIT: return "memoryReadBit";
            case VK_ACCESS_MEMORY_WRITE_BIT: return "memoryWriteBit";
            case VK_ACCESS_COMMAND_PROCESS_READ_BIT_NVX: return "commandProcessReadBitNvx";
            case VK_ACCESS_COMMAND_PROCESS_WRITE_BIT_NVX: return "commandProcessWriteBitNvx";
            case VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT: return "colorAttachmentReadNoncoherentBitExt";
            case VK_ACCESS_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDependencyFlagBits value)
    {
        switch(value)
        {
            case VK_DEPENDENCY_BY_REGION_BIT: return "byRegionBit";
            case VK_DEPENDENCY_DEVICE_GROUP_BIT: return "deviceGroupBit";
            case VK_DEPENDENCY_VIEW_LOCAL_BIT: return "viewLocalBit";
            case VK_DEPENDENCY_VIEW_LOCAL_BIT_KHR: return "viewLocalBitKhr";
            case VK_DEPENDENCY_DEVICE_GROUP_BIT_KHR: return "deviceGroupBitKhr";
            case VK_DEPENDENCY_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCommandPoolCreateFlagBits value)
    {
        switch(value)
        {
            case VK_COMMAND_POOL_CREATE_TRANSIENT_BIT: return "transientBit";
            case VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT: return "resetBufferBit";
            case VK_COMMAND_POOL_CREATE_PROTECTED_BIT: return "protectedBit";
            case VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCommandPoolResetFlagBits value)
    {
        switch(value)
        {
            case VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT: return "releaseResourcesBit";
            case VK_COMMAND_POOL_RESET_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCommandBufferUsageFlagBits value)
    {
        switch(value)
        {
            case VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT: return "oneTimeSubmitBit";
            case VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT: return "renderPassContinueBit";
            case VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT: return "simultaneousUseBit";
            case VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueryControlFlagBits value)
    {
        switch(value)
        {
            case VK_QUERY_CONTROL_PRECISE_BIT: return "preciseBit";
            case VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCommandBufferResetFlagBits value)
    {
        switch(value)
        {
            case VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT: return "releaseResourcesBit";
            case VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkStencilFaceFlagBits value)
    {
        switch(value)
        {
            case VK_STENCIL_FACE_FRONT_BIT: return "frontBit";
            case VK_STENCIL_FACE_BACK_BIT: return "backBit";
            case VK_STENCIL_FRONT_AND_BACK: return "frontAndBack";
            case VK_STENCIL_FACE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPointClippingBehavior value)
    {
        switch(value)
        {
            case VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES: return "allClipPlanes";
            case VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY: return "userClipPlanesOnly";
            case VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES_KHR: return "allClipPlanesKhr";
            case VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY_KHR: return "userClipPlanesOnlyKhr";
            case VK_POINT_CLIPPING_BEHAVIOR_BEGIN_RANGE: return "beginRange";
            case VK_POINT_CLIPPING_BEHAVIOR_END_RANGE: return "endRange";
            case VK_POINT_CLIPPING_BEHAVIOR_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkTessellationDomainOrigin value)
    {
        switch(value)
        {
            case VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT: return "upperLeft";
            case VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT: return "lowerLeft";
            case VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT_KHR: return "upperLeftKhr";
            case VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT_KHR: return "lowerLeftKhr";
            case VK_TESSELLATION_DOMAIN_ORIGIN_BEGIN_RANGE: return "beginRange";
            case VK_TESSELLATION_DOMAIN_ORIGIN_END_RANGE: return "endRange";
            case VK_TESSELLATION_DOMAIN_ORIGIN_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSamplerYcbcrModelConversion value)
    {
        switch(value)
        {
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY: return "rgbIdentity";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY: return "identity";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709: return "709";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601: return "601";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020: return "2020";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY_KHR: return "rgbIdentityKhr";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY_KHR: return "identityKhr";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709_KHR: return "709Khr";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601_KHR: return "601Khr";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020_KHR: return "2020Khr";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_BEGIN_RANGE: return "beginRange";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_END_RANGE: return "endRange";
            case VK_SAMPLER_YCBCR_MODEL_CONVERSION_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSamplerYcbcrRange value)
    {
        switch(value)
        {
            case VK_SAMPLER_YCBCR_RANGE_ITU_FULL: return "ituFull";
            case VK_SAMPLER_YCBCR_RANGE_ITU_NARROW: return "ituNarrow";
            case VK_SAMPLER_YCBCR_RANGE_ITU_FULL_KHR: return "ituFullKhr";
            case VK_SAMPLER_YCBCR_RANGE_ITU_NARROW_KHR: return "ituNarrowKhr";
            case VK_SAMPLER_YCBCR_RANGE_BEGIN_RANGE: return "begin";
            case VK_SAMPLER_YCBCR_RANGE_END_RANGE: return "end";
            case VK_SAMPLER_YCBCR_RANGE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkChromaLocation value)
    {
        switch(value)
        {
            case VK_CHROMA_LOCATION_COSITED_EVEN: return "cositedEven";
            case VK_CHROMA_LOCATION_MIDPOINT: return "midpoint";
            case VK_CHROMA_LOCATION_COSITED_EVEN_KHR: return "cositedEvenKhr";
            case VK_CHROMA_LOCATION_MIDPOINT_KHR: return "midpointKhr";
            case VK_CHROMA_LOCATION_BEGIN_RANGE: return "beginRange";
            case VK_CHROMA_LOCATION_END_RANGE: return "endRange";
            case VK_CHROMA_LOCATION_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDescriptorUpdateTemplateType value)
    {
        switch(value)
        {
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET: return "set";
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_PUSH_DESCRIPTORS_KHR: return "pushDescriptorsKhr";
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET_KHR: return "setKhr";
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_BEGIN_RANGE: return "beginRange";
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_END_RANGE: return "endRange";
            case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSubgroupFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_SUBGROUP_FEATURE_BASIC_BIT: return "basicBit";
            case VK_SUBGROUP_FEATURE_VOTE_BIT: return "voteBit";
            case VK_SUBGROUP_FEATURE_ARITHMETIC_BIT: return "arithmeticBit";
            case VK_SUBGROUP_FEATURE_BALLOT_BIT: return "ballotBit";
            case VK_SUBGROUP_FEATURE_SHUFFLE_BIT: return "shuffleBit";
            case VK_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT: return "shuffleRelativeBit";
            case VK_SUBGROUP_FEATURE_CLUSTERED_BIT: return "clusteredBit";
            case VK_SUBGROUP_FEATURE_QUAD_BIT: return "quadBit";
            case VK_SUBGROUP_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPeerMemoryFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT: return "copySrcBit";
            case VK_PEER_MEMORY_FEATURE_COPY_DST_BIT: return "copyDstBit";
            case VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT: return "genericSrcBit";
            case VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT: return "genericDstBit";
            case VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT_KHR: return "copySrcBitKhr";
            case VK_PEER_MEMORY_FEATURE_COPY_DST_BIT_KHR: return "copyDstBitKhr";
            case VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT_KHR: return "genericSrcBitKhr";
            case VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT_KHR: return "genericDstBitKhr";
            case VK_PEER_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkMemoryAllocateFlagBits value)
    {
        switch(value)
        {
            case VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT: return "deviceMaskBit";
            case VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT_KHR: return "deviceMaskBitKhr";
            case VK_MEMORY_ALLOCATE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalMemoryHandleTypeFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT: return "opaqueFdBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "opaqueWin32Bit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "opaqueWin32KmtBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT: return "d3d11TextureBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT: return "d3d11TextureKmtBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT: return "d3d12HeapBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT: return "d3d12ResourceBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT: return "dmaBufBitExt";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT: return "hostAllocationBitExt";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_MAPPED_FOREIGN_MEMORY_BIT_EXT: return "hostMappedForeignBitExt";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT_KHR: return "opaqueFdBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR: return "opaqueWin32BitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR: return "opaqueWin32KmtBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT_KHR: return "d3d11TextureBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT_KHR: return "d3d11TextureKmtBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT_KHR: return "d3d12HeapBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT_KHR: return "d3d12ResourceBitKhr";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalMemoryFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT: return "dedicatedOnlyBit";
            case VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT: return "exportableBit";
            case VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT: return "importableBit";
            case VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_KHR: return "dedicatedOnlyBitKhr";
            case VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_KHR: return "exportableBitKhr";
            case VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_KHR: return "importableBitKhr";
            case VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalFenceHandleTypeFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT: return "opaqueFdBit";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "opaqueWin32Bit";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "opaqueWin32KmtBit";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT: return "syncFdBit";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR: return "opaqueFdBitKhr";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR: return "opaqueWin32BitKhr";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR: return "opaqueWin32KmtBitKhr";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT_KHR: return "syncFdBitKhr";
            case VK_EXTERNAL_FENCE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalFenceFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT: return "exportableBit";
            case VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT: return "importableBit";
            case VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT_KHR: return "exportableBitKhr";
            case VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT_KHR: return "importableBitKhr";
            case VK_EXTERNAL_FENCE_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkFenceImportFlagBits value)
    {
        switch(value)
        {
            case VK_FENCE_IMPORT_TEMPORARY_BIT: return "temporaryBit";
            case VK_FENCE_IMPORT_TEMPORARY_BIT_KHR: return "temporaryBitKhr";
            case VK_FENCE_IMPORT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSemaphoreImportFlagBits value)
    {
        switch(value)
        {
            case VK_SEMAPHORE_IMPORT_TEMPORARY_BIT: return "temporaryBit";
            case VK_SEMAPHORE_IMPORT_TEMPORARY_BIT_KHR: return "temporaryBitKhr";
            case VK_SEMAPHORE_IMPORT_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalSemaphoreHandleTypeFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT: return "opaqueFdBit";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "opaqueWin32Bit";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "opaqueWin32KmtBit";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT: return "d3d12FenceBit";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT: return "syncFdBit";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR: return "opaqueFdBitKhr";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR: return "opaqueWin32BitKhr";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR: return "opaqueWin32KmtBitKhr";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT_KHR: return "d3d12FenceBitKhr";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT_KHR: return "syncFdBitKhr";
            case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalSemaphoreFeatureFlagBits value)
    {
        switch(value)
        {
            case VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT: return "exportableBit";
            case VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT: return "importableBit";
            case VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT_KHR: return "exportableBitKhr";
            case VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT_KHR: return "importableBitKhr";
            case VK_EXTERNAL_SEMAPHORE_FEATURE_FLAG_BITS_MAX_ENUM: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkColorSpaceKHR value)
    {
        switch(value)
        {
            case VK_COLOR_SPACE_SRGB_NONLINEAR_KHR: return "srgbNonlinear";
            case VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT: return "displayP3NonlinearExt";
            case VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT: return "extendedSrgbLinearExt";
            case VK_COLOR_SPACE_DCI_P3_LINEAR_EXT: return "dciP3LinearExt";
            case VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT: return "dciP3NonlinearExt";
            case VK_COLOR_SPACE_BT709_LINEAR_EXT: return "bt709LinearExt";
            case VK_COLOR_SPACE_BT709_NONLINEAR_EXT: return "bt709NonlinearExt";
            case VK_COLOR_SPACE_BT2020_LINEAR_EXT: return "bt2020LinearExt";
            case VK_COLOR_SPACE_HDR10_ST2084_EXT: return "hdr10St2084Ext";
            case VK_COLOR_SPACE_DOLBYVISION_EXT: return "dolbyvisionExt";
            case VK_COLOR_SPACE_HDR10_HLG_EXT: return "hdr10HlgExt";
            case VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT: return "adobergbLinearExt";
            case VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT: return "adobergbNonlinearExt";
            case VK_COLOR_SPACE_PASS_THROUGH_EXT: return "passThroughExt";
            case VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT: return "extendedSrgbNonlinearExt";
            case VK_COLOR_SPACE_BEGIN_RANGE_KHR: return "beginRange";
            case VK_COLOR_SPACE_END_RANGE_KHR: return "endRange";
            case VK_COLOR_SPACE_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkPresentModeKHR value)
    {
        switch(value)
        {
            case VK_PRESENT_MODE_IMMEDIATE_KHR: return "immediate";
            case VK_PRESENT_MODE_MAILBOX_KHR: return "mailbox";
            case VK_PRESENT_MODE_FIFO_KHR: return "fifo";
            case VK_PRESENT_MODE_FIFO_RELAXED_KHR: return "fifoRelaxed";
            case VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR: return "sharedDemandRefresh";
            case VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR: return "sharedContinuousRefresh";
            case VK_PRESENT_MODE_BEGIN_RANGE_KHR: return "beginRange";
            case VK_PRESENT_MODE_END_RANGE_KHR: return "endRange";
            case VK_PRESENT_MODE_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSurfaceTransformFlagBitsKHR value)
    {
        switch(value)
        {
            case VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR: return "identityBit";
            case VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR: return "rotate90Bit";
            case VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR: return "rotate180Bit";
            case VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR: return "rotate270Bit";
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR: return "horizontalMirrorBit";
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR: return "horizontalMirrorRotate90Bit";
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR: return "horizontalMirrorRotate180Bit";
            case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR: return "horizontalMirrorRotate270Bit";
            case VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR: return "inheritBit";
            case VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCompositeAlphaFlagBitsKHR value)
    {
        switch(value)
        {
            case VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR: return "opaqueBit";
            case VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR: return "preMultipliedBit";
            case VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR: return "postMultipliedBit";
            case VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR: return "inheritBit";
            case VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSwapchainCreateFlagBitsKHR value)
    {
        switch(value)
        {
            case VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR: return "splitInstanceBindRegionsBit";
            case VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR: return "protectedBit";
            case VK_SWAPCHAIN_CREATE_FLAG_BITS_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDeviceGroupPresentModeFlagBitsKHR value)
    {
        switch(value)
        {
            case VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR: return "localBit";
            case VK_DEVICE_GROUP_PRESENT_MODE_REMOTE_BIT_KHR: return "remoteBit";
            case VK_DEVICE_GROUP_PRESENT_MODE_SUM_BIT_KHR: return "sumBit";
            case VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_MULTI_DEVICE_BIT_KHR: return "localMultiBit";
            case VK_DEVICE_GROUP_PRESENT_MODE_FLAG_BITS_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDisplayPlaneAlphaFlagBitsKHR value)
    {
        switch(value)
        {
            case VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR: return "opaqueBit";
            case VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR: return "globalBit";
            case VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR: return "perPixelBit";
            case VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR: return "perPixelPremultipliedBit";
            case VK_DISPLAY_PLANE_ALPHA_FLAG_BITS_MAX_ENUM_KHR: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDebugReportObjectTypeEXT value)
    {
        switch(value)
        {
            case VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT: return "unknown";
            case VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT: return "instance";
            case VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT: return "physicalDevice";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT: return "device";
            case VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT: return "queue";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT: return "semaphore";
            case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT: return "commandBuffer";
            case VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT: return "fence";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT: return "deviceMemory";
            case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT: return "buffer";
            case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT: return "image";
            case VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT: return "event";
            case VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT: return "queryPool";
            case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT: return "bufferView";
            case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT: return "imageView";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT: return "shaderModule";
            case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT: return "pipelineCache";
            case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT: return "pipelineLayout";
            case VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT: return "renderPass";
            case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT: return "pipeline";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT: return "descriptorSetLayout";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT: return "sampler";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT: return "descriptorPool";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT: return "descriptorSet";
            case VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT: return "framebuffer";
            case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT: return "commandPool";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT: return "surfaceKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT: return "swapchainKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT: return "callback";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT: return "displayKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT: return "displayModeKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_OBJECT_TABLE_NVX_EXT: return "tableNvx";
            case VK_DEBUG_REPORT_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NVX_EXT: return "indirectCommandsLayoutNvx";
            case VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT: return "validationCache";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT: return "samplerYcbcrConversion";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT: return "descriptorUpdateTemplate";
            case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR_EXT: return "descriptorUpdateTemplateKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR_EXT: return "samplerYcbcrConversionKhr";
            case VK_DEBUG_REPORT_OBJECT_TYPE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_DEBUG_REPORT_OBJECT_TYPE_END_RANGE_EXT: return "endRange";
            case VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDebugReportFlagBitsEXT value)
    {
        switch(value)
        {
            case VK_DEBUG_REPORT_INFORMATION_BIT_EXT: return "informationBit";
            case VK_DEBUG_REPORT_WARNING_BIT_EXT: return "warningBit";
            case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT: return "performanceWarningBit";
            case VK_DEBUG_REPORT_ERROR_BIT_EXT: return "errorBit";
            case VK_DEBUG_REPORT_DEBUG_BIT_EXT: return "bit";
            case VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkRasterizationOrderAMD value)
    {
        switch(value)
        {
            case VK_RASTERIZATION_ORDER_STRICT_AMD: return "strict";
            case VK_RASTERIZATION_ORDER_RELAXED_AMD: return "relaxed";
            case VK_RASTERIZATION_ORDER_BEGIN_RANGE_AMD: return "beginRange";
            case VK_RASTERIZATION_ORDER_END_RANGE_AMD: return "endRange";
            case VK_RASTERIZATION_ORDER_MAX_ENUM_AMD: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkShaderInfoTypeAMD value)
    {
        switch(value)
        {
            case VK_SHADER_INFO_TYPE_STATISTICS_AMD: return "statistics";
            case VK_SHADER_INFO_TYPE_BINARY_AMD: return "binary";
            case VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD: return "disassembly";
            case VK_SHADER_INFO_TYPE_BEGIN_RANGE_AMD: return "beginRange";
            case VK_SHADER_INFO_TYPE_END_RANGE_AMD: return "endRange";
            case VK_SHADER_INFO_TYPE_MAX_ENUM_AMD: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalMemoryHandleTypeFlagBitsNV value)
    {
        switch(value)
        {
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV: return "opaqueWin32Bit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV: return "opaqueWin32KmtBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV: return "d3d11ImageBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV: return "d3d11ImageKmtBit";
            case VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM_NV: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkExternalMemoryFeatureFlagBitsNV value)
    {
        switch(value)
        {
            case VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV: return "dedicatedOnlyBit";
            case VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV: return "exportableBit";
            case VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV: return "importableBit";
            case VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM_NV: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkValidationCheckEXT value)
    {
        switch(value)
        {
            case VK_VALIDATION_CHECK_ALL_EXT: return "all";
            case VK_VALIDATION_CHECK_SHADERS_EXT: return "shaders";
            case VK_VALIDATION_CHECK_BEGIN_RANGE_EXT: return "beginRange";
            case VK_VALIDATION_CHECK_END_RANGE_EXT: return "endRange";
            case VK_VALIDATION_CHECK_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkIndirectCommandsTokenTypeNVX value)
    {
        switch(value)
        {
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_PIPELINE_NVX: return "pipeline";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DESCRIPTOR_SET_NVX: return "descriptorSet";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NVX: return "indexBuffer";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NVX: return "vertexBuffer";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NVX: return "pushConstant";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NVX: return "drawIndexed";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NVX: return "draw";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DISPATCH_NVX: return "dispatch";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_BEGIN_RANGE_NVX: return "beginRange";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_END_RANGE_NVX: return "endRange";
            case VK_INDIRECT_COMMANDS_TOKEN_TYPE_MAX_ENUM_NVX: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkObjectEntryTypeNVX value)
    {
        switch(value)
        {
            case VK_OBJECT_ENTRY_TYPE_DESCRIPTOR_SET_NVX: return "descriptorSet";
            case VK_OBJECT_ENTRY_TYPE_PIPELINE_NVX: return "pipeline";
            case VK_OBJECT_ENTRY_TYPE_INDEX_BUFFER_NVX: return "indexBuffer";
            case VK_OBJECT_ENTRY_TYPE_VERTEX_BUFFER_NVX: return "vertexBuffer";
            case VK_OBJECT_ENTRY_TYPE_PUSH_CONSTANT_NVX: return "pushConstant";
            case VK_OBJECT_ENTRY_TYPE_BEGIN_RANGE_NVX: return "beginRange";
            case VK_OBJECT_ENTRY_TYPE_END_RANGE_NVX: return "endRange";
            case VK_OBJECT_ENTRY_TYPE_MAX_ENUM_NVX: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkIndirectCommandsLayoutUsageFlagBitsNVX value)
    {
        switch(value)
        {
            case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NVX: return "unorderedSequencesBit";
            case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_SPARSE_SEQUENCES_BIT_NVX: return "sparseSequencesBit";
            case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EMPTY_EXECUTIONS_BIT_NVX: return "emptyExecutionsBit";
            case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NVX: return "indexedSequencesBit";
            case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_FLAG_BITS_MAX_ENUM_NVX: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkObjectEntryUsageFlagBitsNVX value)
    {
        switch(value)
        {
            case VK_OBJECT_ENTRY_USAGE_GRAPHICS_BIT_NVX: return "graphicsBit";
            case VK_OBJECT_ENTRY_USAGE_COMPUTE_BIT_NVX: return "computeBit";
            case VK_OBJECT_ENTRY_USAGE_FLAG_BITS_MAX_ENUM_NVX: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSurfaceCounterFlagBitsEXT value)
    {
        switch(value)
        {
            case VK_SURFACE_COUNTER_VBLANK_EXT: return "vblank";
            case VK_SURFACE_COUNTER_FLAG_BITS_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDisplayPowerStateEXT value)
    {
        switch(value)
        {
            case VK_DISPLAY_POWER_STATE_OFF_EXT: return "off";
            case VK_DISPLAY_POWER_STATE_SUSPEND_EXT: return "suspend";
            case VK_DISPLAY_POWER_STATE_ON_EXT: return "on";
            case VK_DISPLAY_POWER_STATE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_DISPLAY_POWER_STATE_END_RANGE_EXT: return "endRange";
            case VK_DISPLAY_POWER_STATE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDeviceEventTypeEXT value)
    {
        switch(value)
        {
            case VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT: return "displayHotplug";
            case VK_DEVICE_EVENT_TYPE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_DEVICE_EVENT_TYPE_END_RANGE_EXT: return "endRange";
            case VK_DEVICE_EVENT_TYPE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDisplayEventTypeEXT value)
    {
        switch(value)
        {
            case VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT: return "firstPixelOut";
            case VK_DISPLAY_EVENT_TYPE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_DISPLAY_EVENT_TYPE_END_RANGE_EXT: return "endRange";
            case VK_DISPLAY_EVENT_TYPE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkViewportCoordinateSwizzleNV value)
    {
        switch(value)
        {
            case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV: return "positiveX";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV: return "negativeX";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV: return "positiveY";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV: return "negativeY";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV: return "positiveZ";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV: return "negativeZ";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV: return "positiveW";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV: return "negativeW";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_BEGIN_RANGE_NV: return "beginRange";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_END_RANGE_NV: return "endRange";
            case VK_VIEWPORT_COORDINATE_SWIZZLE_MAX_ENUM_NV: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDiscardRectangleModeEXT value)
    {
        switch(value)
        {
            case VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT: return "inclusive";
            case VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT: return "exclusive";
            case VK_DISCARD_RECTANGLE_MODE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_DISCARD_RECTANGLE_MODE_END_RANGE_EXT: return "endRange";
            case VK_DISCARD_RECTANGLE_MODE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkConservativeRasterizationModeEXT value)
    {
        switch(value)
        {
            case VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT: return "disabled";
            case VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT: return "overestimate";
            case VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT: return "underestimate";
            case VK_CONSERVATIVE_RASTERIZATION_MODE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_CONSERVATIVE_RASTERIZATION_MODE_END_RANGE_EXT: return "endRange";
            case VK_CONSERVATIVE_RASTERIZATION_MODE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDebugUtilsMessageSeverityFlagBitsEXT value)
    {
        switch(value)
        {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: return "verboseBit";
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: return "infoBit";
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: return "warningBit";
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: return "errorBit";
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkDebugUtilsMessageTypeFlagBitsEXT value)
    {
        switch(value)
        {
            case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT: return "generalBit";
            case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: return "validationBit";
            case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: return "performanceBit";
            case VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkSamplerReductionModeEXT value)
    {
        switch(value)
        {
            case VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE_EXT: return "weightedAverage";
            case VK_SAMPLER_REDUCTION_MODE_MIN_EXT: return "min";
            case VK_SAMPLER_REDUCTION_MODE_MAX_EXT: return "max";
            case VK_SAMPLER_REDUCTION_MODE_BEGIN_RANGE_EXT: return "beginRange";
            case VK_SAMPLER_REDUCTION_MODE_END_RANGE_EXT: return "endRange";
            case VK_SAMPLER_REDUCTION_MODE_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkBlendOverlapEXT value)
    {
        switch(value)
        {
            case VK_BLEND_OVERLAP_UNCORRELATED_EXT: return "uncorrelated";
            case VK_BLEND_OVERLAP_DISJOINT_EXT: return "disjoint";
            case VK_BLEND_OVERLAP_CONJOINT_EXT: return "conjoint";
            case VK_BLEND_OVERLAP_BEGIN_RANGE_EXT: return "beginRange";
            case VK_BLEND_OVERLAP_END_RANGE_EXT: return "endRange";
            case VK_BLEND_OVERLAP_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkCoverageModulationModeNV value)
    {
        switch(value)
        {
            case VK_COVERAGE_MODULATION_MODE_NONE_NV: return "none";
            case VK_COVERAGE_MODULATION_MODE_RGB_NV: return "rgb";
            case VK_COVERAGE_MODULATION_MODE_ALPHA_NV: return "alpha";
            case VK_COVERAGE_MODULATION_MODE_RGBA_NV: return "rgba";
            case VK_COVERAGE_MODULATION_MODE_BEGIN_RANGE_NV: return "beginRange";
            case VK_COVERAGE_MODULATION_MODE_END_RANGE_NV: return "endRange";
            case VK_COVERAGE_MODULATION_MODE_MAX_ENUM_NV: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkValidationCacheHeaderVersionEXT value)
    {
        switch(value)
        {
            case VK_VALIDATION_CACHE_HEADER_VERSION_ONE_EXT: return "one";
            case VK_VALIDATION_CACHE_HEADER_VERSION_BEGIN_RANGE_EXT: return "beginRange";
            case VK_VALIDATION_CACHE_HEADER_VERSION_END_RANGE_EXT: return "endRange";
            case VK_VALIDATION_CACHE_HEADER_VERSION_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
    inline std::string to_string(VkQueueGlobalPriorityEXT value)
    {
        switch(value)
        {
            case VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT: return "low";
            case VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT: return "medium";
            case VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT: return "high";
            case VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT: return "realtime";
            case VK_QUEUE_GLOBAL_PRIORITY_BEGIN_RANGE_EXT: return "beginRange";
            case VK_QUEUE_GLOBAL_PRIORITY_END_RANGE_EXT: return "endRange";
            case VK_QUEUE_GLOBAL_PRIORITY_MAX_ENUM_EXT: return "maxEnum";
            default: return "invalid";
        }
    }
    
